// Rev: 02/04/10  M. Dilworth  Video Design Software Inc.
// Modified to use Stats Inc. as data source
// Modified for Altitude Sports
unit EngineIntf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OoMisc, ADTrmEmu, AdPacket, AdPort, AdWnPort, ExtCtrls, Globals, FileCtrl,
  ScktComp;

type
  TEngineInterface = class(TForm)
    AdTerminal1: TAdTerminal;
    PacketEnableTimer: TTimer;
    TickerCommandDelayTimer: TTimer;
    PacketIndicatorTimer: TTimer;
    TickerPacketTimeoutTimer: TTimer;
    JumpToNextTickerRecordTimer: TTimer;
    BugCommandDelayTimer: TTimer;
    ExtraLineCommandDelayTimer: TTimer;
    JumpToNextBugRecordTimer: TTimer;
    JumpToNextExtraLineRecordTimer: TTimer;
    DisplayClockTimer: TTimer;
    BugPacketTimeoutTimer: TTimer;
    ExtraLinePacketTimeoutTimer: TTimer;
    EnginePort: TClientSocket;
    EngineCOMPort: TApdComPort;
    ApdDataPacket1: TApdDataPacket;
    procedure FormActivate(Sender: TObject);
    //Timer related
    procedure PacketEnableTimerTimer(Sender: TObject);
    procedure TickerCommandDelayTimerTimer(Sender: TObject);
    procedure PacketIndicatorTimerTimer(Sender: TObject);
    procedure TickerPacketTimeoutTimerTimer(Sender: TObject);
    procedure JumpToNextTickerRecordTimerTimer(Sender: TObject);

    //Engine connection port
    procedure EnginePortConnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortDisconnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure EnginePortRead(Sender: TObject; Socket: TCustomWinSocket);

    //Logging
    procedure WriteToErrorLog (ErrorString: String);
    procedure WriteToAsRunLog (AsRunString: String);
    procedure EngineCOMPortPortOpen(Sender: TObject);
    procedure EngineCOMPortPortClose(Sender: TObject);
  private
    //Not used for Altitude Sports
    function ProcessStyleChips(CmdStr: String): String;
  public
    procedure SendTickerRecord(NextRecord: Boolean; RecordIndex: SmallInt);
    function GetLineupText(CurrentTickerEntryIndex: SmallInt; TickerMode: SmallInt): LineupText;
    function GetMixedCase(InStr: String): String;
    function GetSponsorLogoFileName(LogoName: String): String;

    function LoadTempTemplateFields(TemplateID: SmallInt): TemplateDefsRec;

    function CheckForActiveSponsorLogo: Boolean;
    function GetAlternateTemplateID(CurrentTemplateID: SmallInt): SmallInt;

    //Added for Altitude Sports
    procedure InitTicker(BackplateIn: Boolean);
    function LoadTempTemplateCommands(TemplateID: SmallInt): Boolean;
    //Function to send updated data to Icon Station text/image fields
    function GetLayerUpdateString(LayoutName: String; FieldCount: SmallInt; RegionName, FieldName, FieldText: Array of String): String;

    //General procedure to send command to Icon Station
    procedure SendCommandToIconStation(CmdStr: String);
    //Procedure to load layer
    procedure LoadIconStationLayer(LayerName, TemplateLayer: String);
    //Procedure to unload layer
    procedure UnloadLayer(TemplateLayer: String);

  end;

const
  SOT: String[1] = #1;
  ETX: String[1] = #3;
  EOT: String[1] = #4;
  GS: String[1] = #29;
  RS: String[1] = #30;
  NAK: String[1] = #21; //Used as No-Op
  EOB: String[1] = #23; //Send at end of last record; will cause SS 11 to be sent by engine
  SingleLineWinningLarge: String[1] = #180;
  SingleLineLosingLarge: String[1]  = #181;
  SingleLineWinningSmall: String[1]  = #182;
  SingleLineLosingSmall: String[1]  = #183;

var
  EngineInterface: TEngineInterface;
  DisableCommandTimer: Boolean;

const
 UseDataPacket = TRUE;

implementation

uses Main, //Main form
     DataModule, //Data module
     GameDataFunctions;

{$R *.DFM}

//Init
procedure TEngineInterface.FormActivate(Sender: TObject);
begin
end;

////////////////////////////////////////////////////////////////////////////////
// ADDED FOR ALTITUDE SPORTS
////////////////////////////////////////////////////////////////////////////////
procedure TEngineInterface.SendCommandToIconStation(CmdStr: String);
begin
  if (EngineParameters.Enabled) then
  begin
    //Check if COM Port
    if (EngineParameters.UseSerial) then
    begin
      EngineCOMPort.PutString(CmdStr);
    end
    //Check for TCP/IP port
    else begin
      EnginePort.Socket.SendText(CmdStr);
    end;
  end;
end;

//Procedure to bring ticker base template IN
procedure TEngineInterface.InitTicker(BackplateIn: Boolean);
var
  j: SmallInt;
  TemplateCommandRecPtr: ^TemplateCommandRec;
  CmdStr: String;
begin
  //Always clear ticker
  if (UseKillAllCommandForClear) then
  begin
    CmdStr := 'T\14\' + IconStationTemplateLayer + '\\';
    SendCommandToIconStation(CmdStr + #13#10);
  end
  else begin
    LoadTempTemplateCommands(TICKER_OUT_TEMPLATE);

    if (Temporary_Template_Commands_Collection.Count > 0) then
    begin
      for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
      begin
        TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
        if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_IN) then
        begin
          CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
          SendCommandToIconStation(CmdStr + #13#10);
        end;
      end;
    end;
  end;

  //Clear out sponsor info
  CurrentSponsorInfo.CurrentSponsorLogoName := '';
  CurrentSponsorInfo.CurrentSponsorLogoDwell := 0;
  CurrentSponsorInfo.CurrentSponsorTemplate := -99;
  CurrentSponsorInfo.Tagline_Top := '';
  CurrentSponsorInfo.Tagline_Bottom := '';

  //Bring backplate back in if enabled
  if (BackplateIn) then
  begin
    //Always clear ticker
    LoadTempTemplateCommands(TICKER_IN_TEMPLATE);

    if (Temporary_Template_Commands_Collection.Count > 0) then
    begin
      for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
      begin
        TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
        if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_IN) then
        begin
          CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
          SendCommandToIconStation(CmdStr + #13#10);
        end;
      end;
    end;
  end;
end;

//Procedure to load layer
procedure TEngineInterface.LoadIconStationLayer(LayerName, TemplateLayer: String);
var
  j: SmallInt;
  TemplateCommandRecPtr: ^TemplateCommandRec;
  CmdStr: String;
begin
  CmdStr := 'T\7\' + Trim(LayerName) + '/\' + Trim(TemplateLayer) + '\\';
  SendCommandToIconStation(CmdStr + #13#10);
end;

//Procedure to unload layer
procedure TEngineInterface.UnloadLayer(TemplateLayer: String);
var
  CmdStr: String;
begin
  CmdStr := 'T\14\' + Trim(TemplateLayer) + '\\';
  SendCommandToIconStation(CmdStr + #13#10);
end;

//Procedure to form region update command to send to Icon Station
function TEngineInterface.GetLayerUpdateString(LayoutName: String; FieldCount: SmallInt; RegionName, FieldName, FieldText: Array of String): String;
var
  i: SmallInt;
  OutStr: Array[0..19] of String;
  CmdStr: String;
begin
  //Check for valid array
  if (FieldCount > 0)   then
  begin
    for i := 0 to FieldCount-1 do OutStr[i] := FieldText[i];
    //Do substitutions for XML escape sequences
    for i := 0 to FieldCount-1 do
    begin
      OutStr[i] := StringReplace(OutStr[i], '&', '&amp;', [rfReplaceAll]);
      OutStr[i] := StringReplace(OutStr[i], '"', '&quot;', [rfReplaceAll]);
      OutStr[i] := StringReplace(OutStr[i], '''', '&apos;', [rfReplaceAll]);
      OutStr[i] := StringReplace(OutStr[i], '<', '&lt;', [rfReplaceAll]);
      OutStr[i] := StringReplace(OutStr[i], '>', '&gt;', [rfReplaceAll]);
    end;

    //Build command string
    CmdStr :=
      'I\42\<LayoutTags><LayoutName>' + Trim(Layoutname) + '</LayoutName>';
      for i := 0 to FieldCount-1 do
      begin
        if (Trim(RegionName[i]) <> '') then
        begin
          CmdStr := CmdStr +
            '<Region><Name>' + Trim(RegionName[i]) + '</Name>' +
            '<Tag><Name>' + Trim(FieldName[i]) + '</Name>' +
            '<Text>' + OutStr[i] + '</Text>' +
            '</Tag></Region>';
        end;
      end;
      CmdStr := CmdStr + '</LayoutTags>\\';
  end;
  //Return
  GetLayerUpdateString := CmdStr;
end;

//These are the graphics engine control procedures
////////////////////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Function to get a parsed token from a delited string
function GetItem(TokenNo: Integer; Data: string; Delimiter: Char = '|'): string;
var
  Token: string;
  StrLen, TEnd, TNum: Integer;
begin
  StrLen := Length(Data);
  //Init
  TNum := 1;
  TEnd := StrLen;
  //Walk through string and parse for delimiter
  while ((TNum <= TokenNo) and (TEnd <> 0)) do
  begin
    TEnd := Pos(Delimiter, Data);
    //Not last value
    if TEnd <> 0 then
    begin
      //Copy & delete data from input string; increment token counter
      Token := Copy(Data, 1, TEnd - 1);
      Delete(Data, 1, TEnd);
      Inc(TNum);
    end
    //Last value
    else begin
      Token := Data;
    end;
  end;
  //Return data
  if TNum >= TokenNo then
  begin
    Result := Token;
  end
  //Return error code
  else begin
    Result := #27;
  end;
end;

//Function to do style chip substitutions
function TEngineInterface.ProcessStyleChips(CmdStr: String): String;
var
  i,j: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  OutStr: String;
  SwitchPos: SmallInt;
begin
  //Check for presence of style chips
  //NOTE: Conversion to mixed case/upper case done in function to get field contents
  OutStr := CmdStr;
  if (StyleChip_Collection.Count > 0) AND (Length(CmdStr) >= 4) then
  begin
    for i := 0 to StyleChip_Collection.Count-1 do
    begin
      StyleChipRecPtr := StyleChip_Collection.At(i);
      //Check style chip type and substiture accordingly
      //Type 1 = Use substitution string
      if (StyleChipRecPtr^.StyleChip_Type = 1) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  StyleChipRecPtr^.StyleChip_String, [rfReplaceAll])
      //Type 2 = Use substitution font & character
      else if (StyleChipRecPtr^.StyleChip_Type = 2) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  '[f ' + IntToStr(StyleChipRecPtr^.StyleChip_FontCode) + ']' +
                  Char(StyleChipRecPtr^.StyleChip_CharacterCode), [rfReplaceAll])
    end;
  end;
  ProcessStyleChips := OutStr;
end;

// Function to take upper case string & return mixed-case string
function TEngineInterface.GetMixedCase(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := ANSILowerCase(InStr);
  if (Length(OutStr) > 0) then
  begin
    //Set first character to upper case if it's not a digit
    if (Ord(OutStr[1]) < 48) OR (Ord(OutStr[1]) > 57) then  OutStr[1] := Char(Ord(OutStr[1])-32);
    //Check if two words in state name, and set 2nd word to upper case
    if (Length(OutStr) > 2) then
    begin
      for i := 2 to Length(OutStr) do
      begin
        if (OutStr[i-1] = ' ') then OutStr[i] := Char(Ord(OutStr[i])-32);
      end;
    end;
  end;
  GetMixedCase := OutStr;
end;

//Function to get sponsor logo file name based on logo description
//Modified for Altitude Sports to add base logo file path
function TEngineInterface.GetSponsorLogoFilename(LogoName: String): String;
var
  i: SmallInt;
  OutStr: String;
  SponsorLogoPtr: ^SponsorLogoRec;
begin
  for i := 0 to SponsorLogo_Collection.Count-1 do
  begin
    SponsorLogoPtr := SponsorLogo_Collection.At(i);
    if (SponsorLogoPtr^.SponsorLogoName = LogoName) then
    begin
      OutStr := Sponsor_Logo_Base_Path + '\' + SponsorLogoPtr^.SponsorLogoFileName;
    end;
  end;
  GetSponsorLogoFilename := OutStr;
end;

//Procedure to load temporary collection for fields (for current template); also returns
//associated template information
function TEngineInterface.LoadTempTemplateFields(TemplateID: SmallInt): TemplateDefsRec;
var
  i: SmallInt;
  TemplateFieldsRecPtr, NewTemplateFieldsRecPtr: ^TemplateFieldsRec;
  TemplateRecPtr: ^TemplateDefsRec;
  OutRec: TemplateDefsRec;
  FoundMatch: Boolean;
begin
  //Clear the existing collection
  Temporary_Fields_Collection.Clear;
  Temporary_Fields_Collection.Pack;
  //First, get the engine template ID
  if (Template_Defs_Collection.Count > 0) then
  begin
    i := 0;
    FoundMatch := FALSE;
    repeat
      TemplateRecPtr := Template_Defs_Collection.At(i);
      if (TemplateID = TemplateRecPtr^.Template_ID) then
      begin
        OutRec.Template_ID := TemplateRecPtr^.Template_ID;
        OutRec.Template_Type := TemplateRecPtr^.Template_Type;
        OutRec.Template_Description := TemplateRecPtr^.Template_Description;
        OutRec.Record_Type := TemplateRecPtr^.Record_Type;
        OutRec.TemplateSponsorType := TemplateRecPtr^.TemplateSponsorType;
        OutRec.Engine_Template_ID := TemplateRecPtr^.Engine_Template_ID;
        OutRec.Default_Dwell := TemplateRecPtr^.Default_Dwell;
        OutRec.UsesGameData := TemplateRecPtr^.UsesGameData;
        OutRec.RequiredGameState := TemplateRecPtr^.RequiredGameState;
        OutRec.HideWebsiteURL := TemplateRecPtr^.HideWebsiteURL;
        OutRec.ShowLeagueChip := TemplateRecPtr^.ShowLeagueChip;
        OutRec.Use_Alert_Background := TemplateRecPtr^.Use_Alert_Background;
        OutRec.Template_Layer_Name := TemplateRecPtr^.Template_Layer_Name;
        FoundMatch := TRUE;
      end;
      Inc(i);
    until (FoundMatch) OR (i = Template_Defs_Collection.Count);
  end;
  //Now, get the fields for the template
  for i := 0 to Template_Fields_Collection.Count-1 do
  begin
    TemplateFieldsRecPtr := Template_Fields_Collection.At(i);
    if (TemplateFieldsRecPtr^.Template_ID = TemplateID) then
    begin
      GetMem (NewTemplateFieldsRecPtr, SizeOf(TemplateFieldsRec));
      With NewTemplateFieldsRecPtr^ do
      begin
        Template_ID := TemplateFieldsRecPtr^.Template_ID;
        Field_ID := TemplateFieldsRecPtr^.Field_ID;
        Field_Type := TemplateFieldsRecPtr^.Field_Type;
        Field_Is_Manual := TemplateFieldsRecPtr^.Field_Is_Manual;
        Field_Label := TemplateFieldsRecPtr^.Field_Label;
        Field_Contents := TemplateFieldsRecPtr^.Field_Contents;
        Field_Format_Prefix := TemplateFieldsRecPtr^.Field_Format_Prefix;
        Field_Format_Suffix := TemplateFieldsRecPtr^.Field_Format_Suffix;
        Field_Max_Length := TemplateFieldsRecPtr^.Field_Max_Length;
        Engine_Field_ID := TemplateFieldsRecPtr^.Engine_Field_ID;
        Layout_Region_Name := TemplateFieldsRecPtr^.Layout_Region_Name;
        Layout_Field_Name := TemplateFieldsRecPtr^.Layout_Field_Name;
        Field_Is_Crawl := TemplateFieldsRecPtr^.Field_Is_Crawl;
        Crawl_Pad_Leading := TemplateFieldsRecPtr^.Crawl_Pad_Leading;
        Crawl_Pad_Trailing := TemplateFieldsRecPtr^.Crawl_Pad_Trailing;
      end;
      if (Temporary_Fields_Collection.Count <= 100) then
      begin
        //Add to collection
        Temporary_Fields_Collection.Insert(NewTemplateFieldsRecPtr);
        Temporary_Fields_Collection.Pack;
      end;
    end;
  end;
  LoadTempTemplateFields := OutRec;
end;

//Added for Altitude Sports
//Procedure to load temporary collection for template commands (for current template)
function TEngineInterface.LoadTempTemplateCommands(TemplateID: SmallInt): Boolean;
var
  i: SmallInt;
  TemplateCommandRecPtr, NewTemplateCommandRecPtr: ^TemplateCommandRec;
  FoundMatch: Boolean;
begin
  if (Template_Commands_Collection.Count > 0) then
  begin
    FoundMatch := FALSE;
    //Clear the existing collection
    Temporary_Template_Commands_Collection.Clear;
    Temporary_Template_Commands_Collection.Pack;
    //Load the matching data
    for i := 0 to Template_Commands_Collection.Count-1 do
    begin
      TemplateCommandRecPtr := Template_Commands_Collection.At(i);
      if (TemplateCommandRecPtr^.Template_ID = TemplateID) then
      begin
        FoundMatch := TRUE;
        GetMem (NewTemplateCommandRecPtr, SizeOf(TemplateCommandRec));
        With NewTemplateCommandRecPtr^ do
        begin
          Template_ID := TemplateCommandRecPtr^.Template_ID;
          Command_Type := TemplateCommandRecPtr^.Command_Type;
          Command_Index := TemplateCommandRecPtr^.Command_Index;
          Command_Text := TemplateCommandRecPtr^.Command_Text;
        end;
        if (Temporary_Template_Commands_Collection.Count <= 500) then
        begin
          //Add to collection
          Temporary_Template_Commands_Collection.Insert(NewTemplateCommandRecPtr);
          Temporary_Template_Commands_Collection.Pack;
        end;
      end;
    end;
  end;
  LoadTempTemplateCommands := FoundMatch;
end;

//Utility function to strip off any ASCII characters above ASCII 127
function StripPrefix(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := '';
  if (Length(InStr) > 0) then
  begin
    for i := 1 to Length(InStr) do
    begin
      if (Ord(InStr[i]) <=127) then OutStr := OutStr + InStr[i];
    end;
  end;
  StripPrefix := OutStr;
end;


//FOR ODDS PAGES
const
  DELIMITER:STRING = Chr(160);

////////////////////////////////////////////////////////////////////////////////
// ENGINE COMMAND STRING FORMATTING FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Function to get & return text strings to be used for line-ups
function TEngineInterface.GetLineupText(CurrentTickerEntryIndex: SmallInt; TickerMode: SmallInt): LineupText;
var
  i,j: SmallInt;
  OutRec: LineupText;
  TickerRecPtrCurrent, TickerRecPtrPrevious,TickerRecPtrNext: ^TickerRec;
  PreviousEntryIndex, NextEntryIndex: SmallInt;
  CurrentEntryLeague, PreviousEntryLeague, NextEntryLeague: String;
  HeaderIndex, CollectionItemCounter: SmallInt;
  OneLeagueOnly: Boolean;
  FirstLeague: String;
  CurrentLineupTickerEntryIndex: SmallInt;
begin
  //Always set flag to do line-up
  OutRec.UseLineup := TRUE;

  //Get text for line-ups
  if (OutRec.UseLineup = TRUE) AND (CurrentTickerEntryIndex <> NOENTRYINDEX) then
  begin
    //Init
    HeaderIndex := 1;
    CollectionItemCounter := 1;

    //Get current entry types
    CurrentLineupTickerEntryIndex := CurrentTickerEntryIndex;
    TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);

    //Make sure record is enabled and within time window
    j := 0;
    While ((TickerRecPtrCurrent^.Enabled = FALSE) OR (TickerRecPtrCurrent^.StartEnableDateTime > Now) OR
          (TickerRecPtrCurrent^.EndEnableDateTime <= Now) OR (TickerRecPtrCurrent^.League = 'NONE')) AND
          (j < Ticker_Collection.Count) do
    begin
      Inc(CurrentLineupTickerEntryIndex);
      if (CurrentLineupTickerEntryIndex = Ticker_Collection.Count) then
      begin
        CurrentLineupTickerEntryIndex := 0;
      end;
      TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);
      Inc(j);
    end;

    //Special case to just show MLB for all MLB leagues
    CurrentEntryLeague :=  TickerRecPtrCurrent^.League;
    if (CurrentEntryLeague = 'AL') OR (CurrentEntryLeague = 'NL') OR (CurrentEntryLeague = 'ML') OR (CurrentEntryLeague = 'IL') then
      CurrentEntryLeague := 'MLB';

    NextEntryIndex := CurrentLineupTickerEntryIndex+1;
    //Get type of next entry
    if (NextEntryIndex = Ticker_Collection.Count) then NextEntryIndex := 0;

    //Set first entry
    OutRec.TextFields[HeaderIndex] := CurrentEntryLeague;

    //Get remaining entries
    //Disabled for Altitude Sports
    {
    repeat
      TickerRecPtrNext := Ticker_Collection.At(NextEntryIndex);
      NextEntryLeague := TickerRecPtrNext^.League;
      if (NextEntryLeague = 'AL') OR (NextEntryLeague = 'NL') OR (NextEntryLeague = 'ML') OR (NextEntryLeague = 'IL') then
        NextEntryLeague := 'MLB';

      //If new league or child to prior parent, set text field; otherwise, continue iterating through
      if (NextEntryLeague <> CurrentEntryLeague) AND
         (TickerRecPtrNext^.Enabled = TRUE) AND (TickerRecPtrNext^.StartEnableDateTime <= Now) AND
         (TickerRecPtrNext^.EndEnableDateTime > Now) AND (j < Ticker_Collection.Count) AND
         (TickerRecPtrNext^.League <> 'NONE') then
      begin
        Inc(HeaderIndex);
        //Use parent league for field and clear flag
        OutRec.TextFields[HeaderIndex] := NextEntryLeague;
        CurrentEntryLeague := NextEntryLeague;
      end;
      if (NextEntryIndex = Ticker_Collection.Count-1) then NextEntryIndex := 0
      else Inc(NextEntryIndex);
      Inc(CollectionItemCounter);
    until (HeaderIndex = 4) OR (CollectionItemCounter = Ticker_Collection.Count*3);
    }

    //Repeat fields if not enough after iterating
    if (HeaderIndex = 1) then
    begin
      OutRec.TextFields[2] :=  OutRec.TextFields[1];
      OutRec.TextFields[3] :=  OutRec.TextFields[1];
      OutRec.TextFields[4] :=  OutRec.TextFields[1];
    end
    else if (HeaderIndex = 2) then
    begin
      OutRec.TextFields[3] :=  OutRec.TextFields[1];
      OutRec.TextFields[4] :=  OutRec.TextFields[2];
    end
    else if (HeaderIndex = 3) then
    begin
      OutRec.TextFields[4] :=  OutRec.TextFields[1];
    end;
  end
  else
    for i := 1 to 4 do OutRec.TextFields[i] := ' ';
  //Return
  for i := 1 to 4 do if (OutRec.TextFields[i] = '') then OutRec.TextFields[i] := ' ';
  GetLineupText := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// ENGINE CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Function to take a template ID and return the alternate template ID for 1-line vs.
//2-line mode
function TEngineInterface.GetAlternateTemplateID(CurrentTemplateID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutVal: SmallInt;
begin
  OutVal := -1;
  if (Template_Defs_Collection.Count > 0) then
  begin
    //Walk through templates to find alternate mode ID
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      //Check for match between ticker element template ID and entry in template defs
      if (TemplateDefsRecPtr^.Template_ID = CurrentTemplateID) then
      begin
        //Alternate template ID found, so re-assign template ID
        if (TemplateDefsRecPtr^.AlternateModeTemplateID > 0) then
        begin
          OutVal := TemplateDefsRecPtr^.AlternateModeTemplateID;
        end;
      end;
    end;
  end;
  GetAlternateTemplateID := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//FUNCTIONS FOR ADVANCING RECORDS
//Handler for packet requesting more data for ticker
procedure TEngineInterface.EnginePortRead(Sender: TObject; Socket: TCustomWinSocket);
var
  i: SmallInt;
  RequiredDelay: SmallInt;
  Data: String;
begin
  Data := Socket.ReceiveText;

  //Disable timeout timer
  //Enable delay timer for next command if not previously in single command mode
  //Set current template ID
  if (Length(Data) >= 1) AND (Pos('*', ANSIUpperCase(Data)) > 0) then
  begin
    if (RunningTicker = TRUE) AND (DisableCommandTimer = FALSE) then
    begin
      if (USEDATAPACKET) then TickerCommandDelayTimer.Enabled := TRUE;
      TickerPacketTimeoutTimer.Enabled := FALSE;
    end;
  end
  else begin
    WriteToErrorLog('Icon Station error code received: ' + Data);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// DISPLAY DWELL TIMERS
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Handler for timer to send next command
procedure TEngineInterface.TickerCommandDelayTimerTimer(Sender: TObject);
begin
  if (Ticker_Collection.Count > 0) then
  begin
    //Prevent retrigger
    //TickerCommandDelayTimer.Enabled := FALSE;
    //Send next record
    //SendTickerRecord(TRUE, 0);
  end;
end;
//Handler for 1 mS timer used to send next command when jumping over stats headers
procedure TEngineInterface.JumpToNextTickerRecordTimerTimer(Sender: TObject);
begin
  if (Ticker_Collection.Count > 0) then
  begin
    //Prevent retrigger
    //JumpToNextTickerRecordTimer.Enabled := FALSE;
    //Send next record
    //SendTickerRecord(TRUE, 0);
  end;
end;

//Function to check the ticker collection for any sponsor logo with a valid time window
function TEngineInterface.CheckForActiveSponsorLogo: Boolean;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  OutVal: Boolean;
begin
  //Init
  OutVal := FALSE;
  //Check collection for any sponsor logo with a valid time window
  if (Ticker_Collection.Count > 0) then
  begin
    for i := 0 to Ticker_Collection.Count-1 do
    begin
      TickerRecPtr := Ticker_Collection.At(i);
      if ((TickerRecPtr^.Template_ID = 1) OR (TickerRecPtr^.Template_ID = 51) OR (TickerRecPtr^.Template_ID = 101) OR (TickerRecPtr^.Template_ID = 151))
      AND (TickerRecPtr^.StartEnableDateTime <= Now) AND
         (TickerRecPtr^.EndEnableDateTime > Now) then OutVal := TRUE;
    end;
  end;
  CheckForActiveSponsorLogo := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Procedure to send next record after receipt of packet
procedure TEngineInterface.SendTickerRecord(NextRecord: Boolean; RecordIndex: SmallInt);
var
  i,j,k: SmallInt;
  TempStr, CmdStr: String;
  TickerRecPtr, TickerRecPtrNew: ^TickerRec;
  FoundGame: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  IsLast: Boolean;
  OverlayIndex, OverlayIndexNext: SmallInt;
  IsNewOverlay, PadText: Boolean;
  InGameStatsRequired: Boolean;
  FoundGameInDatamart: Boolean;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec: GamePhaseRec;
  CurrentGameState: SmallInt;
  RecordIsSponsor: Boolean;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CurrentGameData: GameRec;
  OkToGo, OKToDisplay: Boolean;
  TokenCounter: SmallInt;

  //Added for Altitude Sports
  Layout_Region_Name: Array[0..20] of String;
  Layout_Field_Name: Array[0..20] of String;
  Layout_Field_Text: Array[0..20] of String;
  Layout_Field_Count: SmallInt;
  TemplateCommandRecPtr: ^TemplateCommandRec;
  League_Chip_Region_Name: Array[0..1] of String;
  League_Chip_Field_Name: Array[0..1] of String;
  League_Chip_Field_Text: Array[0..1] of String;
  Disable_Template_Out_Commands: Boolean;

begin
  //Init flags
  OKToDisplay := TRUE;
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  //Init FoundGame flag - used to prevent inadverant commands from being sent if game not found
  FoundGame := TRUE;
  RecordIsSponsor := FALSE;

  //Added for Altitude Sports
  Disable_Template_Out_Commands := FALSE;

  //Turn on indicator
  PacketIndicatorTimer.Enabled := TRUE;
  MainForm.ApdStatusLight2.Lit := TRUE;

  //Disable packets and enable timer to re-enable packets
  //PacketEnable := FALSE;
  PacketEnableTimer.Enabled := TRUE;
  try
    //Init
    IsLast := FALSE;

    //Make sure we haven't dumped put
    if ((EngineParameters.Enabled) AND (TickerAbortFlag = FALSE) AND (Ticker_Collection.Count > 0)) then
    begin
      OKToGo := TRUE;

      //Triggering specific record, so set current entry index
      CurrentTickerEntryIndex := RecordIndex;
      //Disble command timer
      DisableCommandTimer := TRUE;

      //Proceed if not at end or in looping mode and at beggining
      if (CurrentTickerEntryIndex <= Ticker_Collection.Count-1) AND (OKToGo) then
      begin
        //Get pointer to current record
        TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);

        ////////////////////////////////////////////////////////////////////////
        //MAIN PROCESSING LOOP FOR TICKER FIELDS
        ////////////////////////////////////////////////////////////////////////
        //Get template info and load the termporary fields collection
        TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);

        //Get game data if template requires it
        if (TemplateInfo.UsesGameData) then
        begin
          try
            //Use full game data
            CurrentGameData := GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to get game data for ticker');
          end;
          FoundGame := CurrentGameData.DataFound;
        end;

         //Set the default dwell time - minimum = 2000mS; set packet timeout interval
        if (TickerRecPtr^.DwellTime > 2000) then
        begin
          //If sponsor logo dwell is specified, it's a sponsor logo, so use that for the dwell time
          if (TickerRecPtr^.SponsorLogo_Dwell > 0) AND ((TemplateInfo.TemplateSponsorType = 1) OR (TemplateInfo.TemplateSponsorType = 2)) then
          begin
            TickerCommandDelayTimer.Interval := (TickerRecPtr^.SponsorLogo_Dwell*1000);
            TickerPacketTimeoutTimer.Interval := (TickerRecPtr^.SponsorLogo_Dwell*1000) + 5000;
            //Set the current sponsor logo name
            CurrentSponsorLogoName[1] := TickerRecPtr^.SponsorLogo_Name;
          end
          else begin
            TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
            TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 5000;
          end;
        end
        else begin
          TickerCommandDelayTimer.Interval := 2000;
          TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 7000;
        end;

        //Add the fields from the temporary fields collection
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          //Set field count
          Layout_Field_Count := Temporary_Fields_Collection.Count;
          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin
            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
            //Version 2.1
            //Check for single fields (not compound fields required for 2-line, new look engine)
            if (TemplateFieldsRecPtr^.Field_Type > 0) then
            begin
              //Filter out fields ID values of -1 => League designator
              if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
              begin
                //Add region command
                //If not a symbolic fields, send field contents directly
                if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
                begin
                  TempStr := TemplateFieldsRecPtr^.Field_Contents;
                  //Add prefix and/or suffix if applicable
                  if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                    TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                  if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                    TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                end
                //It's a symbolic name, so get the field contents
                else begin
                  try
                    TempStr := GetValueOfSymbol(TICKER, TemplateFieldsRecPtr^.Field_Contents, CurrentTickerEntryIndex,
                      CurrentGameData, TickerDisplayMode).SymbolValue;
                    //Add previx and/or suffix if applicable
                    if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                    if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                    //Added for Altitude Sports
                    //Pad if crawl text
                    if (TemplateFieldsRecPtr^.Field_Is_Crawl) then
                    begin
                      if (TemplateFieldsRecPtr^.Crawl_Pad_Leading > 0) then
                        for k := 1 to TemplateFieldsRecPtr^.Crawl_Pad_Leading do
                          TempStr := #160 + TempStr;
                      if (TemplateFieldsRecPtr^.Crawl_Pad_Trailing > 0) then
                        for k := 1 to TemplateFieldsRecPtr^.Crawl_Pad_Trailing do
                          TempStr := TempStr + #160;
                    end;
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to get value for main ticker data field');
                  end;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                end;

                //Don't send update data for image fields
                if (TemplateFieldsRecPtr^.Field_Type <> FIELD_TYPE_SPONSOR_LOGO) and (TemplateFieldsRecPtr^.Field_Type <> FIELD_TYPE_TEAM_LOGO_1A) and
                   (TemplateFieldsRecPtr^.Field_Type <> FIELD_TYPE_TEAM_LOGO_2A) then
                begin
                  //Set values for layers
                  Layout_Region_Name[j] :=  TemplateFieldsRecPtr^.Layout_Region_Name;
                  Layout_Field_Name[j] :=  TemplateFieldsRecPtr^.Layout_Field_Name;
                  Layout_Field_Text[j] :=  TempStr;
                end;

                //Update any logos
                //Check for sponsor logo
                if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_SPONSOR_LOGO) then
                begin
                  try
                    with dmMain.LogoUpdateQuery do
                    begin
                      Close;
                      SQL.Clear;
                      SQL.Add('sp_UpdateSponsorLogo ' + QuotedStr(TempStr));
                      ExecSQL;
                    end;
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to update sponsor logo in SQL database');
                  end;
                end
                //Check for alert logo - left
                else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1A) then
                begin
                  try
                    with dmMain.LogoUpdateQuery do
                    begin
                      Close;
                      SQL.Clear;
                      SQL.Add('sp_UpdateAlertLogoLeft ' + QuotedStr(TempStr));
                      ExecSQL;
                    end;
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to update left-side alert logo in SQL database');
                  end;
                end
                //Check for alert logo - right
                else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2A) then
                begin
                  try
                    with dmMain.LogoUpdateQuery do
                    begin
                      Close;
                      SQL.Clear;
                      SQL.Add('sp_UpdateAlertLogoRight ' + QuotedStr(TempStr));
                      ExecSQL;
                    end;
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to update right-side alert logo in SQL database');
                  end;
                end;
              end;
            end;
          end;
          //Write out to the as-run log if it's a sponsor logo
          if (Trim(TickerRecPtr^.SponsorLogo_Name) <> '') then
            WriteToAsRunLog('Started display of sponsor logo: ' + CurrentSponsorLogoName[1]);
        end;

        //Get the lineup text
        try
          if (GetLineuptext(CurrentTickerEntryIndex, TickerDisplayMode).UseLineup = TRUE) then
          begin
            LineupData := GetLineuptext(CurrentTickerEntryIndex, TickerDisplayMode);
            //Version 2.0.0.5 Modified to reflect change to line-up/sponsor interaction
            if (Trim(LineupData.TextFields[1]) = 'NONE') then
              LineupData.TextFields[1] := ' ';
            //Set league text - done below
          end;
        except
          if (ErrorLoggingEnabled = True) then
            WriteToErrorLog('Error occurred while trying to get and set main ticker line-up text');
        end;

        //Substitute style chip codes
        //try
        //  CmdStr := ProcessStyleChips(CmdStr);
        //except
        //  if (ErrorLoggingEnabled = True) then
        //    WriteToErrorLog('Error occurred while trying to process style chips for main ticker record');
        //end;

        ///////////////////////////////
        // NEW CODE FOR ALTITUDE
        ///////////////////////////////
        //if first proofing page, bring in backplate
        if not (TickerbackplateIn) then
        begin
          TickerbackplateIn := TRUE;

          //Load the layer on the Icon Station
          EngineInterface.LoadIconStationLayer(IconStationLayerName, IconStationTemplateLayer);

          Sleep(1000);

          //Always show ticker backplate
          LoadTempTemplateCommands(TICKER_IN_TEMPLATE);

          if (Temporary_Template_Commands_Collection.Count > 0) then
          begin
            for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
            begin
              TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
              if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_IN) then
              begin
                CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
                SendCommandToIconStation(CmdStr + #13#10);
              end;
            end;
          end;
        end;

        //Send the new data
        if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
        begin
          //Enable packet timeout timer
          if (NEXTRECORD) then TickerPacketTimeoutTimer.Enabled := TRUE;
          //Send commands
          try
            //Update text fields
            CmdStr := GetLayerUpdateString(TemplateInfo.Template_Layer_Name, Layout_Field_Count, Layout_Region_Name, Layout_Field_Name, Layout_Field_Text);
            SendCommandToIconStation(CmdStr + #13#10);

            //If this is a new template, fire the commands to take the previous template out
            if (LastActiveTemplateID <> TemplateInfo.Template_ID) and (LastActiveTemplateID <> 0) and not (Disable_Template_Out_Commands) then
            begin
              //Added for Altitude Sports - get the template commands
              LoadTempTemplateCommands(LastActiveTemplateID);

              //Fire the commands to take the old the template out
              if (Temporary_Template_Commands_Collection.Count > 0) then
              begin
                for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
                begin
                  TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
                  if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_OUT) then
                  begin
                    CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
                    SendCommandToIconStation(CmdStr + #13#10);
                  end;
                end;
              end;
            end
            //Same template so fire commands to take just text fields out
            else if (LastActiveTemplateID <> 0) then
            begin
              //Added for Altitude Sports - get the template commands
              LoadTempTemplateCommands(TemplateInfo.Template_ID);

              //Fire the commands to bring the template in
              if (Temporary_Template_Commands_Collection.Count > 0) then
              begin
                for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
                begin
                  TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
                  if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_UPDATE_START) then
                  begin
                    CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
                    SendCommandToIconStation(CmdStr + #13#10);
                  end;
                end;
              end;
            end;

            //Update the league chip if it's different than the last league designation
            if (LineupData.TextFields[1] <> Last_League_Text) and (LastActiveTemplateID <> 0) and (TemplateInfo.ShowLeagueChip) then
            begin
              //Added for Altitude Sports - get the template commands
              LoadTempTemplateCommands(LEAGUE_CHIP_TEMPLATE_ID);

              //Update league chip text
              League_Chip_Region_Name[0] := 'league';
              League_Chip_Field_Name[0] := 'Field1';
              League_Chip_Field_Text[0] := LineupData.TextFields[1];
              CmdStr := GetLayerUpdateString(TemplateInfo.Template_Layer_Name, 1, League_Chip_Region_Name, League_Chip_Field_Name, League_Chip_Field_Text);
              SendCommandToIconStation(CmdStr + #13#10);

              //Fire the commands to start the update
              if (Temporary_Template_Commands_Collection.Count > 0) then
              begin
                for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
                begin
                  TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
                  if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_UPDATE_START) then
                  begin
                    CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
                    SendCommandToIconStation(CmdStr + #13#10);
                  end;
                end;
              end;

              //Fire the commands to end the update
              if (Temporary_Template_Commands_Collection.Count > 0) then
              begin
                for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
                begin
                  TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
                  if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_UPDATE_END) then
                  begin
                    CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
                    SendCommandToIconStation(CmdStr + #13#10);
                  end;
                end;
              end;

              //Save new league name
              Last_League_Text := LineupData.TextFields[1];
            end;

            //Fire the commands to bring the template in
            //If this is a new template, fire the commands to bring the new template in
            if (LastActiveTemplateID <> TemplateInfo.Template_ID) then
            begin
              //Added for Altitude Sports - get the template commands
              LoadTempTemplateCommands(TemplateInfo.Template_ID);

              if (Temporary_Template_Commands_Collection.Count > 0) then
              begin
                for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
                begin
                  TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
                  if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_IN) then
                  begin
                    CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
                    SendCommandToIconStation(CmdStr + #13#10);
                  end;
                end;
              end;
            end
            //Same template so fire commands to take bring text fields in after update
            else begin
              //Added for Altitude Sports - get the template commands
              LoadTempTemplateCommands(TemplateInfo.Template_ID);

              //Fire the commands to bring the template in
              if (Temporary_Template_Commands_Collection.Count > 0) then
              begin
                for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
                begin
                  TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
                  if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_UPDATE_END) then
                  begin
                    CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
                    SendCommandToIconStation(CmdStr + #13#10);
                  end;
                end;
              end;
            end;

            //Bring the website URL in or out based on state of sponsor logo
            //Check if sponsor specified
            if (CurrentSponsorInfo.CurrentSponsorLogoName <> '') or (TemplateInfo.HideWebsiteURL = TRUE) then
              LoadTempTemplateCommands(WEBSITE_OUT_TEMPLATE)
            //No sponsor specified, so bring URL in
            else
              LoadTempTemplateCommands(WEBSITE_IN_TEMPLATE);
            //Send the commands
            if (Temporary_Template_Commands_Collection.Count > 0) then
            begin
              for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
              begin
                TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
                if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_IN) then
                begin
                  CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
                  SendCommandToIconStation(CmdStr + #13#10);
                end;
              end;
            end;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying send ticker command to engine');
          end;
          //Start timer to send next record
          //if (USEDATAPACKET = FALSE) AND (NEXTRECORD = TRUE) then TickerCommandDelayTimer.Enabled := TRUE;
          //Modified for Altitude to always use ticker command timer
          TickerCommandDelayTimer.Enabled := TRUE;
        end
        //Game not found, so don't send commands & go to next command
        else if (FoundGame = FALSE) then
        begin
          //Enable delay timer
          if (RunningTicker = TRUE) then JumpToNextTickerRecordTimer.Enabled := TRUE;
        end;
      end;
      //Set last template ID
      LastActiveTemplateID := TemplateInfo.Template_ID;
    end;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send additional records to ticker');
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// SOCKET EVENT HANDLES
////////////////////////////////////////////////////////////////////////////////
//Handler for packet re-enable timer
procedure TEngineInterface.PacketEnableTimerTimer(Sender: TObject);
begin
  //Disable to prevent retrigger
  PacketEnableTimer.Enabled := FALSE;
  //Re-enable packets
  PacketEnable := TRUE;
end;

//Handler to turn off packet recevied indicator after 250 mS
procedure TEngineInterface.PacketIndicatorTimerTimer(Sender: TObject);
begin
  PacketIndicatorTimer.Enabled := FALSE;
  MainForm.ApdStatusLight2.Lit := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// PACKET TIMEOUT TIMERS
////////////////////////////////////////////////////////////////////////////////
//Handler for ticker packet timeout timer - sends next command if status command not
//received
procedure TEngineInterface.TickerPacketTimeoutTimerTimer(Sender: TObject);
begin
  if (EngineParameters.Enabled) then
  begin
    //Disable timer to prevent retriggering
    TickerPacketTimeoutTimer.Enabled := FALSE;
    //Enable delay timer
    //if (RunningTicker = TRUE) then
      //Send next record
      //SendTickerRecord(TRUE, 0);
    //Log the timeout
    //if (ErrorLoggingEnabled = True) then
    //begin
    //  //WriteToErrorLog
    //  if (ErrorLoggingEnabled = True) then
    //    WriteToErrorLog('A timeout occurred while waiting for a Main Ticker command return status from the graphics engine.');
    //end;
  end;
end;

//Handler to light indicator
procedure TEngineInterface.EnginePortConnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.ApdStatusLight1.Lit := TRUE;
  SocketConnected := TRUE;
end;

//Handler to turn off indicator
procedure TEngineInterface.EnginePortDisconnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with connection to ticker graphics engine');
  end;
end;

//Handler for socket error
procedure TEngineInterface.EnginePortError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The IP Port connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with IP Port connection to ticker graphics engine');
  end;
end;

//Functions for COM port
//COM port open
procedure TEngineInterface.EngineCOMPortPortOpen(Sender: TObject);
begin
  MainForm.ApdStatusLight1.Lit := TRUE;
  SocketConnected := TRUE;
end;

//COM port close
procedure TEngineInterface.EngineCOMPortPortClose(Sender: TObject);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The COM Port connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with COM Port connection to ticker graphics engine');
  end;
end;


////////////////////////////////////////////////////////////////////////////////
// LOGGING FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToErrorLog (ErrorString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   ErrorLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists('c:\TSTN_Error_LogFiles') = FALSE) then CreateDir('c:\TSTN_Error_LogFiles');
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   ErrorLogStr := TimeStr + '    ' + DateStr + '    ' + ErrorString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := 'c:\TSTN_Error_Logfiles\TSTNErrorLog' + DayStr + '.txt';
   AssignFile (ErrorLogFile, FileName);
   //Write error information to one line in file
   //If file doesn't exist create a new one. Otherwise, check to see if it's one
   //month old. If so, delete & create a new one. Otherwise, append error to file
   if (Not (FileExists(FileName))) then
   begin
      ReWrite (ErrorLogFile);
      try
         Writeln (ErrorLogFile, ErrorLogStr);      finally         CloseFile (ErrorLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (ErrorLogFile);      try         Readln (ErrorLogFile, TestStr);      finally         CloseFile (ErrorLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (ErrorLogFile);         {Create a new logfile}         ReWrite (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end;   end;end;

// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToAsRunLog (AsRunString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   AsRunLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists(AsRunLogFileDirectoryPath) = FALSE) then
   begin
     if (DirectoryExists('c:\TSTN_AsRun_LogFiles') = FALSE) then
       CreateDir('c:\TSTN_AsRun_LogFiles');
     DirectoryStr := 'c:\TSTN_AsRun_LogFiles\';
   end
   else DirectoryStr := AsRunLogFileDirectoryPath;
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   AsRunLogStr := TimeStr + '    ' + DateStr + '    ' + AsRunString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := DirectoryStr + 'TSTNAsRunLog' + DayStr + '.txt';
   AssignFile (AsRunLogFile, FileName);
   {If (FileExists (ErrorLogFile)) then Erase (ErrorLogFile);}
   {Write error information to one line in file}
   {If file doesn't exist create a new one. Otherwise, check to see if it's one }
   {month old. If so, delete & create a new one. Otherwise, append error to file}
   if (Not (FileExists(FileName))) then begin
      ReWrite (AsRunLogFile);
      try
         Writeln (AsRunLogFile, AsRunLogStr);      finally         CloseFile (AsRunLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (AsRunLogFile);      try         Readln (AsRunLogFile, TestStr);      finally         CloseFile (AsRunLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then      begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (AsRunLogFile);         {Create a new logfile}         ReWrite (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end;   end;end;

end.


