object ScheduleEntryEditDlg: TScheduleEntryEditDlg
  Left = 831
  Top = 348
  BorderStyle = bsDialog
  Caption = 'Schedule Entry Time Editor'
  ClientHeight = 239
  ClientWidth = 443
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 16
    Top = 16
    Width = 409
    Height = 161
    TabOrder = 0
    object Label15: TLabel
      Left = 167
      Top = 10
      Width = 34
      Height = 16
      Caption = 'Date'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label19: TLabel
      Left = 19
      Top = 32
      Width = 125
      Height = 16
      Caption = 'Start Enable Time'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label37: TLabel
      Left = 19
      Top = 88
      Width = 120
      Height = 16
      Caption = 'End Enable Time'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label68: TLabel
      Left = 30
      Top = 60
      Width = 105
      Height = 16
      Caption = '(24 HR Format)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 277
      Top = 13
      Width = 36
      Height = 16
      Caption = 'Time'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DefaultDwellTimeLabel: TLabel
      Left = 19
      Top = 127
      Width = 206
      Height = 16
      Caption = 'Default Dwell Time (Seconds)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object ScheduleEntryStartDate: TDateTimePicker
      Left = 166
      Top = 34
      Width = 99
      Height = 24
      Date = 37340.801306944400000000
      Time = 37340.801306944400000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object ScheduleEntryEndDate: TDateTimePicker
      Left = 166
      Top = 82
      Width = 99
      Height = 24
      Date = 37340.801306944400000000
      Time = 37340.801306944400000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object ScheduleEntryStartTime: TDateTimePicker
      Left = 276
      Top = 34
      Width = 112
      Height = 24
      Date = 38760.853983645840000000
      Time = 38760.853983645840000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Kind = dtkTime
      ParentFont = False
      TabOrder = 2
    end
    object ScheduleEntryEndTime: TDateTimePicker
      Left = 276
      Top = 82
      Width = 112
      Height = 24
      Date = 38760.853983645840000000
      Time = 38760.853983645840000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Kind = dtkTime
      ParentFont = False
      TabOrder = 3
    end
    object DefaultDwellTime: TSpinEdit
      Left = 244
      Top = 120
      Width = 53
      Height = 26
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      MaxValue = 600
      MinValue = 2
      ParentFont = False
      TabOrder = 4
      Value = 2
      Visible = False
    end
  end
  object BitBtn1: TBitBtn
    Left = 109
    Top = 187
    Width = 97
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 237
    Top = 187
    Width = 97
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Kind = bkCancel
  end
end
