unit DataModule;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, ScktComp, ExtCtrls;

type
  ERecvError = class(Exception);

  TdmMain = class(TDataModule)
    dbTicker: TADOConnection;
    tblTicker_Groups: TADOTable;
    tblTicker_Elements: TADOTable;
    dsTicker_Groups: TDataSource;
    Query1: TADOQuery;
    dsQuery1: TDataSource;
    tblScheduled_Ticker_Groups: TADOTable;
    dsScheduled_Ticker_Groups: TDataSource;
    tblSponsor_Logos: TADOTable;
    dsSponsor_Logos: TDataSource;
    tblGame_Notes: TADOTable;
    tblLeagues: TADOTable;
    tblPromo_Logos: TADOTable;
    dsPromo_Logos: TDataSource;
    dbSportbase: TADOConnection;
    SportbaseQuery: TADOQuery;
    dsSportbaseQuery: TDataSource;
    Query2: TADOQuery;
    dsQuery2: TDataSource;
    Query3: TADOQuery;
    SportbaseQuery2: TADOQuery;
    tblBreakingNews_Elements: TADOTable;
    tblBreakingNews_Groups: TADOTable;
    tblScheduled_BreakingNews_Groups: TADOTable;
    dsBreakingNews_Groups: TDataSource;
    dsScheduled_BreakingNews_Groups: TDataSource;
    tblExtraline_Elements: TADOTable;
    SportbaseQuery3: TADOQuery;
    dsSportbaseQuery3: TDataSource;
    tblTemplate_Defs: TADOTable;
    tblTeams: TADOTable;
    dsTeams: TDataSource;
    tblGame_Phase_Codes: TADOTable;
    dsGame_Phase_Codes: TDataSource;
    tblCustom_Headers: TADOTable;
    dsCustom_Headers: TDataSource;
    GameDataQuery: TADOQuery;
    WeatherQuery: TADOQuery;
    tblDefault_Templates: TADOTable;
    Query4: TADOQuery;
    dsQuery4: TDataSource;
    dbLogoImages: TADOConnection;
    LogoUpdateQuery: TADOQuery;
  private
  public
  end;

var
  dmMain: TdmMain;

implementation

uses Main;

{$R *.DFM}

end.
