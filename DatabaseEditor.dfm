object DBEditorDlg: TDBEditorDlg
  Left = 302
  Top = 235
  BorderStyle = bsDialog
  Caption = 'Database Editor'
  ClientHeight = 549
  ClientWidth = 1027
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 16
    Top = 11
    Width = 995
    Height = 470
    BevelWidth = 2
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 14
      Width = 190
      Height = 20
      Caption = 'Teams Database Editor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 49
      Top = 398
      Width = 37
      Height = 20
      Caption = 'First'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 143
      Top = 398
      Width = 38
      Height = 20
      Caption = 'Prior'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 237
      Top = 398
      Width = 37
      Height = 20
      Caption = 'Next'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 334
      Top = 398
      Width = 36
      Height = 20
      Caption = 'Last'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 430
      Top = 398
      Width = 33
      Height = 20
      Caption = 'Add'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 514
      Top = 398
      Width = 54
      Height = 20
      Caption = 'Delete'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 624
      Top = 398
      Width = 33
      Height = 20
      Caption = 'Edit'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 715
      Top = 398
      Width = 37
      Height = 20
      Caption = 'Post'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 804
      Top = 398
      Width = 56
      Height = 20
      Caption = 'Cancel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 896
      Top = 398
      Width = 65
      Height = 20
      Caption = 'Refresh'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object tsDBGrid1: TtsDBGrid
      Left = 16
      Top = 40
      Width = 961
      Height = 353
      CellSelectMode = cmNone
      CheckBoxStyle = stCheck
      Cols = 11
      DatasetType = dstStandard
      DataSource = dmMain.dsTeams
      DefaultRowHeight = 18
      ExactRowCount = True
      ExportDelimiter = ','
      FieldState = fsCustomized
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HeadingFont.Charset = DEFAULT_CHARSET
      HeadingFont.Color = clWindowText
      HeadingFont.Height = -13
      HeadingFont.Name = 'MS Sans Serif'
      HeadingFont.Style = [fsBold]
      HeadingHeight = 20
      HeadingParentFont = False
      ParentFont = False
      ParentShowHint = False
      RowChangedIndicator = riAutoReset
      RowMoving = False
      ShowHint = False
      TabOrder = 0
      Version = '2.20.26'
      XMLExport.Version = '1.0'
      XMLExport.DataPacketVersion = '2.0'
      DataBound = True
      ColProperties = <
        item
          DataCol = 1
          FieldName = 'League'
          Col.FieldName = 'League'
          Col.Heading = 'League'
          Col.AssignedValues = '?'
        end
        item
          DataCol = 2
          FieldName = 'StatsIncID'
          Col.FieldName = 'StatsIncID'
          Col.Heading = 'Stats Inc ID'
          Col.Width = 91
          Col.AssignedValues = '?'
        end
        item
          DataCol = 3
          FieldName = 'DisplayName1'
          Col.FieldName = 'DisplayName1'
          Col.Heading = 'Display Name (Mnemonic)'
          Col.Width = 195
          Col.AssignedValues = '?'
        end
        item
          DataCol = 4
          FieldName = 'DisplayName2'
          Col.FieldName = 'DisplayName2'
          Col.Heading = 'Display Name (Long Name)'
          Col.Width = 206
          Col.AssignedValues = '?'
        end
        item
          DataCol = 5
          FieldName = 'NewSTTeamCode'
          Col.FieldName = 'NewSTTeamCode'
          Col.Heading = 'New ST Team Code'
          Col.Width = 150
          Col.AssignedValues = '?'
        end
        item
          DataCol = 6
          FieldName = 'LongName'
          Col.FieldName = 'LongName'
          Col.Heading = 'Long Name'
          Col.Width = 242
          Col.AssignedValues = '?'
        end
        item
          DataCol = 7
          FieldName = 'ShortName'
          Col.FieldName = 'ShortName'
          Col.Heading = 'Short Name'
          Col.Width = 152
          Col.AssignedValues = '?'
        end
        item
          DataCol = 8
          FieldName = 'BaseName'
          Col.FieldName = 'BaseName'
          Col.Heading = 'Base Name'
          Col.Width = 200
          Col.AssignedValues = '?'
        end
        item
          DataCol = 9
          FieldName = 'Rank'
          Col.FieldName = 'Rank'
          Col.Heading = 'Rank'
          Col.AssignedValues = '?'
        end
        item
          DataCol = 10
          FieldName = 'EntryDate'
          Col.FieldName = 'EntryDate'
          Col.Heading = 'Entry Date'
          Col.Width = 107
          Col.AssignedValues = '?'
        end
        item
          DataCol = 11
          FieldName = 'EntryOperator'
          Col.FieldName = 'EntryOperator'
          Col.Heading = 'Entry Operator'
          Col.Width = 147
          Col.AssignedValues = '?'
        end>
    end
    object DBNavigator1: TDBNavigator
      Left = 16
      Top = 424
      Width = 960
      Height = 33
      DataSource = dmMain.dsTeams
      TabOrder = 1
    end
  end
  object BitBtn1: TBitBtn
    Left = 461
    Top = 496
    Width = 105
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkClose
  end
end
