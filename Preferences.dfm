object Prefs: TPrefs
  Left = 389
  Top = 56
  BorderStyle = bsDialog
  Caption = 'User Preferences'
  ClientHeight = 734
  ClientWidth = 394
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 106
    Top = 685
    Width = 81
    Height = 38
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 210
    Top = 685
    Width = 81
    Height = 38
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkCancel
  end
  object Panel2: TPanel
    Left = 16
    Top = 167
    Width = 361
    Height = 202
    TabOrder = 2
    object Label2: TLabel
      Left = 16
      Top = 8
      Width = 253
      Height = 16
      Caption = 'SQL Server ADO Connection Strings:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 16
      Top = 88
      Width = 149
      Height = 16
      Caption = 'Sportbase Database:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 16
      Top = 32
      Width = 158
      Height = 16
      Caption = 'Main Ticker Database:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 16
      Top = 144
      Width = 235
      Height = 16
      Caption = 'Logo Image Filenames Database:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Edit2: TEdit
      Left = 16
      Top = 54
      Width = 329
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object Edit3: TEdit
      Left = 16
      Top = 110
      Width = 329
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object Edit4: TEdit
      Left = 16
      Top = 166
      Width = 329
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
  end
  object Panel1: TPanel
    Left = 16
    Top = 8
    Width = 361
    Height = 49
    TabOrder = 3
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 263
      Height = 16
      Caption = 'Authoring Station ID (Must Be Unique):'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SpinEdit1: TSpinEdit
      Left = 287
      Top = 12
      Width = 57
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 2
      MaxValue = 99
      MinValue = 1
      ParentFont = False
      TabOrder = 0
      Value = 1
    end
  end
  object Panel3: TPanel
    Left = 16
    Top = 373
    Width = 361
    Height = 71
    TabOrder = 4
    object Label3: TLabel
      Left = 16
      Top = 8
      Width = 254
      Height = 16
      Caption = 'Spellchecker Dictionary Folder Path:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Edit1: TEdit
      Left = 16
      Top = 30
      Width = 329
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
  end
  object GraphicsEngineEnable: TPanel
    Left = 16
    Top = 449
    Width = 361
    Height = 224
    TabOrder = 5
    object Label4: TLabel
      Left = 36
      Top = 100
      Width = 81
      Height = 16
      Caption = 'IP Address:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 236
      Top = 100
      Width = 33
      Height = 16
      Caption = 'Port:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 36
      Top = 156
      Width = 78
      Height = 16
      Caption = 'Baud Rate:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 228
      Top = 156
      Width = 70
      Height = 16
      Caption = 'COM Port:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object EngineEnable: TCheckBox
      Left = 34
      Top = 8
      Width = 255
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Ticker Graphics Engine Enabled'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object EngineIPAddress: TEdit
      Left = 36
      Top = 122
      Width = 173
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object EnginePort: TEdit
      Left = 236
      Top = 122
      Width = 85
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object RadioGroup1: TRadioGroup
      Left = 32
      Top = 40
      Width = 289
      Height = 45
      Caption = 'Communications Method'
      Columns = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemIndex = 0
      Items.Strings = (
        'TCP/IP'
        'COM Port')
      ParentFont = False
      TabOrder = 3
    end
    object EngineBaudRate: TComboBox
      Left = 37
      Top = 176
      Width = 145
      Height = 24
      Style = csDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 16
      ItemIndex = 0
      ParentFont = False
      TabOrder = 4
      Text = '9600'
      Items.Strings = (
        '9600'
        '19200')
    end
    object EngineComPortNumber: TComboBox
      Left = 229
      Top = 176
      Width = 92
      Height = 24
      Style = csDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 16
      ItemIndex = 0
      ParentFont = False
      TabOrder = 5
      Text = 'COM 1'
      Items.Strings = (
        'COM 1'
        'COM 2'
        'COM 3'
        'COM 4'
        'COM 5'
        'COM 6'
        'COM 7'
        'COM 8')
    end
  end
  object Panel4: TPanel
    Left = 16
    Top = 64
    Width = 361
    Height = 97
    TabOrder = 6
    object ForceUpperCaseCheckbox: TCheckBox
      Left = 56
      Top = 16
      Width = 248
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Force Upper-Case (Overall)'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object ForceUpperCaseGameInfoCheckbox: TCheckBox
      Left = 56
      Top = 40
      Width = 248
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Force Upper-Case (Game Info)'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object DefaultToSingleLoopCheckbox: TCheckBox
      Left = 56
      Top = 64
      Width = 248
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Default to Single Loop Through'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
  end
end
