// Rev: 02/04/10  M. Dilworth  Video Design Software Inc.
// Modified to use Stats Inc. as data source
unit EngineIntf_Default;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OoMisc, ADTrmEmu, AdPacket, AdPort, AdWnPort, ExtCtrls, Globals, FileCtrl,
  ScktComp;

type
  TEngineInterface = class(TForm)
    AdTerminal1: TAdTerminal;
    PacketEnableTimer: TTimer;
    TickerCommandDelayTimer: TTimer;
    PacketIndicatorTimer: TTimer;
    TickerPacketTimeoutTimer: TTimer;
    JumpToNextTickerRecordTimer: TTimer;
    BugCommandDelayTimer: TTimer;
    ExtraLineCommandDelayTimer: TTimer;
    JumpToNextBugRecordTimer: TTimer;
    JumpToNextExtraLineRecordTimer: TTimer;
    DisplayClockTimer: TTimer;
    BugPacketTimeoutTimer: TTimer;
    ExtraLinePacketTimeoutTimer: TTimer;
    EnginePort: TClientSocket;
    procedure FormActivate(Sender: TObject);
    //Timer related
    procedure PacketEnableTimerTimer(Sender: TObject);
    procedure TickerCommandDelayTimerTimer(Sender: TObject);
    procedure PacketIndicatorTimerTimer(Sender: TObject);
    procedure TickerPacketTimeoutTimerTimer(Sender: TObject);
    procedure JumpToNextTickerRecordTimerTimer(Sender: TObject);
    //Engine connection port
    procedure EnginePortConnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortDisconnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure EnginePortRead(Sender: TObject; Socket: TCustomWinSocket);
    //Logging
    procedure WriteToErrorLog (ErrorString: String);
    procedure WriteToAsRunLog (AsRunString: String);
  private
    function ProcessStyleChips(CmdStr: String): String;
  public
    procedure TriggerGraphic (Mode: SmallInt);
    procedure SetAudioFile (AudioFileID: SmallInt);
    procedure SendTickerRecord(NextRecord: Boolean; RecordIndex: SmallInt);
    procedure SendAlertRecord(RecordIndex: SmallInt);
    function GetLineupText(CurrentTickerEntryIndex: SmallInt; TickerMode: SmallInt): LineupText;
    function GetMixedCase(InStr: String): String;
    function GetSponsorLogoFileName(LogoName: String): String;
    function LoadTempTemplateFields(TemplateID: SmallInt): TemplateDefsRec;

    procedure ClearBreakingNews;
    function CheckForActiveSponsorLogo: Boolean;
    function GetAlternateTemplateID(CurrentTemplateID: SmallInt): SmallInt;
  end;

const
  SOT: String[1] = #1;
  ETX: String[1] = #3;
  EOT: String[1] = #4;
  GS: String[1] = #29;
  RS: String[1] = #30;
  NAK: String[1] = #21; //Used as No-Op
  EOB: String[1] = #23; //Send at end of last record; will cause SS 11 to be sent by engine
  SingleLineWinningLarge: String[1] = #180;
  SingleLineLosingLarge: String[1]  = #181;
  SingleLineWinningSmall: String[1]  = #182;
  SingleLineLosingSmall: String[1]  = #183;

var
  EngineInterface: TEngineInterface;
  DisableCommandTimer: Boolean;

const
 UseDataPacket = TRUE;

implementation

uses Main, //Main form
     DataModule, GameDataFunctions; //Data module

{$R *.DFM}

//Init
procedure TEngineInterface.FormActivate(Sender: TObject);
begin
end;

//These are the graphics engine control procedures
////////////////////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Function to get a parsed token from a delited string
function GetItem(TokenNo: Integer; Data: string; Delimiter: Char = '|'): string;
var
  Token: string;
  StrLen, TEnd, TNum: Integer;
begin
  StrLen := Length(Data);
  //Init
  TNum := 1;
  TEnd := StrLen;
  //Walk through string and parse for delimiter
  while ((TNum <= TokenNo) and (TEnd <> 0)) do
  begin
    TEnd := Pos(Delimiter, Data);
    //Not last value
    if TEnd <> 0 then
    begin
      //Copy & delete data from input string; increment token counter
      Token := Copy(Data, 1, TEnd - 1);
      Delete(Data, 1, TEnd);
      Inc(TNum);
    end
    //Last value
    else begin
      Token := Data;
    end;
  end;
  //Return data
  if TNum >= TokenNo then
  begin
    Result := Token;
  end
  //Return error code
  else begin
    Result := #27;
  end;
end;

//Function to do style chip substitutions
function TEngineInterface.ProcessStyleChips(CmdStr: String): String;
var
  i,j: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  OutStr: String;
  SwitchPos: SmallInt;
begin
  //Check for presence of style chips
  //NOTE: Conversion to mixed case/upper case done in function to get field contents
  OutStr := CmdStr;
  if (StyleChip_Collection.Count > 0) AND (Length(CmdStr) >= 4) then
  begin
    for i := 0 to StyleChip_Collection.Count-1 do
    begin
      StyleChipRecPtr := StyleChip_Collection.At(i);
      //Check style chip type and substiture accordingly
      //Type 1 = Use substitution string
      if (StyleChipRecPtr^.StyleChip_Type = 1) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  StyleChipRecPtr^.StyleChip_String, [rfReplaceAll])
      //Type 2 = Use substitution font & character
      else if (StyleChipRecPtr^.StyleChip_Type = 2) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  '[f ' + IntToStr(StyleChipRecPtr^.StyleChip_FontCode) + ']' +
                  Char(StyleChipRecPtr^.StyleChip_CharacterCode), [rfReplaceAll])
    end;
  end;
  ProcessStyleChips := OutStr;
end;

// Function to take upper case string & return mixed-case string
function TEngineInterface.GetMixedCase(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := ANSILowerCase(InStr);
  if (Length(OutStr) > 0) then
  begin
    //Set first character to upper case if it's not a digit
    if (Ord(OutStr[1]) < 48) OR (Ord(OutStr[1]) > 57) then  OutStr[1] := Char(Ord(OutStr[1])-32);
    //Check if two words in state name, and set 2nd word to upper case
    if (Length(OutStr) > 2) then
    begin
      for i := 2 to Length(OutStr) do
      begin
        if (OutStr[i-1] = ' ') then OutStr[i] := Char(Ord(OutStr[i])-32);
      end;
    end;
  end;
  GetMixedCase := OutStr;
end;

//Procedure to set audio file
procedure TEngineInterface.SetAudioFile (AudioFileID: SmallInt);
var
  CmdStr: String;
begin
  if (EngineParameters.Enabled) then
  begin
    try
      //Build & send command string
      CmdStr := SOT + 'SA' + ETX + IntToStr(AudioFileID) + EOT;
      if (SocketConnected) AND (EngineParameters.Enabled = TRUE) then EnginePort.Socket.SendText(CmdStr);
    except
    end;
  end;
end;

//Function to get sponsor logo file name based on logo description
function TEngineInterface.GetSponsorLogoFilename(LogoName: String): String;
var
  i: SmallInt;
  OutStr: String;
  SponsorLogoPtr: ^SponsorLogoRec;
begin
  for i := 0 to SponsorLogo_Collection.Count-1 do
  begin
    SponsorLogoPtr := SponsorLogo_Collection.At(i);
    if (SponsorLogoPtr^.SponsorLogoName = LogoName) then
    begin
      OutStr := SponsorLogoPtr^.SponsorLogoFileName;
    end;
  end;
  GetSponsorLogoFilename := OutStr;
end;

//Procedure to load temporary collection for fields (for current template); also returns
//associated template information
function TEngineInterface.LoadTempTemplateFields(TemplateID: SmallInt): TemplateDefsRec;
var
  i: SmallInt;
  TemplateFieldsRecPtr, NewTemplateFieldsRecPtr: ^TemplateFieldsRec;
  TemplateRecPtr: ^TemplateDefsRec;
  OutRec: TemplateDefsRec;
  FoundMatch: Boolean;
begin
  //Clear the existing collection
  Temporary_Fields_Collection.Clear;
  Temporary_Fields_Collection.Pack;
  //First, get the engine template ID
  if (Template_Defs_Collection.Count > 0) then
  begin
    i := 0;
    FoundMatch := FALSE;
    repeat
      TemplateRecPtr := Template_Defs_Collection.At(i);
      if (TemplateID = TemplateRecPtr^.Template_ID) then
      begin
        OutRec.Template_ID := TemplateRecPtr^.Template_ID;
        OutRec.Template_Type := TemplateRecPtr^.Template_Type;
        OutRec.Template_Description := TemplateRecPtr^.Template_Description;
        OutRec.Record_Type := TemplateRecPtr^.Record_Type;
        OutRec.TemplateSponsorType := TemplateRecPtr^.TemplateSponsorType;
        OutRec.Engine_Template_ID := TemplateRecPtr^.Engine_Template_ID;
        OutRec.Default_Dwell := TemplateRecPtr^.Default_Dwell;
        OutRec.UsesGameData := TemplateRecPtr^.UsesGameData;
        OutRec.RequiredGameState := TemplateRecPtr^.RequiredGameState;
        OutRec.Use_Alert_Background := TemplateRecPtr^.Use_Alert_Background;
        FoundMatch := TRUE;
      end;
      Inc(i);
    until (FoundMatch) OR (i = Template_Defs_Collection.Count);
  end;
  //Now, get the fields for the template
  for i := 0 to Template_Fields_Collection.Count-1 do
  begin
    TemplateFieldsRecPtr := Template_Fields_Collection.At(i);
    if (TemplateFieldsRecPtr^.Template_ID = TemplateID) then
    begin
      GetMem (NewTemplateFieldsRecPtr, SizeOf(TemplateFieldsRec));
      With NewTemplateFieldsRecPtr^ do
      begin
        Template_ID := TemplateFieldsRecPtr^.Template_ID;
        Field_ID := TemplateFieldsRecPtr^.Field_ID;
        Field_Type := TemplateFieldsRecPtr^.Field_Type;
        Field_Is_Manual := TemplateFieldsRecPtr^.Field_Is_Manual;
        Field_Label := TemplateFieldsRecPtr^.Field_Label;
        Field_Contents := TemplateFieldsRecPtr^.Field_Contents;
        Field_Format_Prefix := TemplateFieldsRecPtr^.Field_Format_Prefix;
        Field_Format_Suffix := TemplateFieldsRecPtr^.Field_Format_Suffix;
        Field_Max_Length := TemplateFieldsRecPtr^.Field_Max_Length;
        Engine_Field_ID := TemplateFieldsRecPtr^.Engine_Field_ID;
      end;
      if (Temporary_Fields_Collection.Count <= 100) then
      begin
        //Add to collection
        Temporary_Fields_Collection.Insert(NewTemplateFieldsRecPtr);
        Temporary_Fields_Collection.Pack;
      end;
    end;
  end;
  LoadTempTemplateFields := OutRec;
end;

//Utility function to strip off any ASCII characters above ASCII 127
function StripPrefix(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := '';
  if (Length(InStr) > 0) then
  begin
    for i := 1 to Length(InStr) do
    begin
      if (Ord(InStr[i]) <=127) then OutStr := OutStr + InStr[i];
    end;
  end;
  StripPrefix := OutStr;
end;


//FOR ODDS PAGES
const
  DELIMITER:STRING = Chr(160);

////////////////////////////////////////////////////////////////////////////////
// ENGINE COMMAND STRING FORMATTING FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Function to get & return text strings to be used for line-ups
function TEngineInterface.GetLineupText(CurrentTickerEntryIndex: SmallInt; TickerMode: SmallInt): LineupText;
var
  i,j: SmallInt;
  OutRec: LineupText;
  TickerRecPtrCurrent, TickerRecPtrPrevious,TickerRecPtrNext: ^TickerRec;
  PreviousEntryIndex, NextEntryIndex: SmallInt;
  CurrentEntryLeague, PreviousEntryLeague, NextEntryLeague: String;
  HeaderIndex, CollectionItemCounter: SmallInt;
  OneLeagueOnly: Boolean;
  FirstLeague: String;
  CurrentLineupTickerEntryIndex: SmallInt;
begin
  //Always set flag to do line-up
  OutRec.UseLineup := TRUE;

  //Get text for line-ups
  if (OutRec.UseLineup = TRUE) AND (CurrentTickerEntryIndex <> NOENTRYINDEX) then
  begin
    //Init
    HeaderIndex := 1;
    CollectionItemCounter := 1;

    //Get current entry types
    CurrentLineupTickerEntryIndex := CurrentTickerEntryIndex;
    TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);

    //Make sure record is enabled and within time window
    j := 0;
    While ((TickerRecPtrCurrent^.Enabled = FALSE) OR (TickerRecPtrCurrent^.StartEnableDateTime > Now) OR
          (TickerRecPtrCurrent^.EndEnableDateTime <= Now) OR (TickerRecPtrCurrent^.League = 'NONE')) AND
          (j < Ticker_Collection.Count) do
    begin
      Inc(CurrentLineupTickerEntryIndex);
      if (CurrentLineupTickerEntryIndex = Ticker_Collection.Count) then
      begin
        CurrentLineupTickerEntryIndex := 0;
      end;
      TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);
      Inc(j);
    end;

    //Special case to just show MLB for all MLB leagues
    CurrentEntryLeague :=  TickerRecPtrCurrent^.League;
    if (CurrentEntryLeague = 'AL') OR (CurrentEntryLeague = 'NL') OR (CurrentEntryLeague = 'ML') OR (CurrentEntryLeague = 'IL') then
      CurrentEntryLeague := 'MLB';

    NextEntryIndex := CurrentLineupTickerEntryIndex+1;
    //Get type of next entry
    if (NextEntryIndex = Ticker_Collection.Count) then NextEntryIndex := 0;

    //Set first entry
    OutRec.TextFields[HeaderIndex] := CurrentEntryLeague;

    //Get remaining entries
    repeat
      TickerRecPtrNext := Ticker_Collection.At(NextEntryIndex);
      NextEntryLeague := TickerRecPtrNext^.League;
      if (NextEntryLeague = 'AL') OR (NextEntryLeague = 'NL') OR (NextEntryLeague = 'ML') OR (NextEntryLeague = 'IL') then
        NextEntryLeague := 'MLB';

      //If new league or child to prior parent, set text field; otherwise, continue iterating through
      if (NextEntryLeague <> CurrentEntryLeague) AND
         (TickerRecPtrNext^.Enabled = TRUE) AND (TickerRecPtrNext^.StartEnableDateTime <= Now) AND
         (TickerRecPtrNext^.EndEnableDateTime > Now) AND (j < Ticker_Collection.Count) AND
         (TickerRecPtrNext^.League <> 'NONE') then
      begin
        Inc(HeaderIndex);
        //Use parent league for field and clear flag
        OutRec.TextFields[HeaderIndex] := NextEntryLeague;
        CurrentEntryLeague := NextEntryLeague;
      end;
      if (NextEntryIndex = Ticker_Collection.Count-1) then NextEntryIndex := 0
      else Inc(NextEntryIndex);
      Inc(CollectionItemCounter);
    until (HeaderIndex = 4) OR (CollectionItemCounter = Ticker_Collection.Count*3);

    //Repeat fields if not enough after iterating
    if (HeaderIndex = 1) then
    begin
      OutRec.TextFields[2] :=  OutRec.TextFields[1];
      OutRec.TextFields[3] :=  OutRec.TextFields[1];
      OutRec.TextFields[4] :=  OutRec.TextFields[1];
    end
    else if (HeaderIndex = 2) then
    begin
      OutRec.TextFields[3] :=  OutRec.TextFields[1];
      OutRec.TextFields[4] :=  OutRec.TextFields[2];
    end
    else if (HeaderIndex = 3) then
    begin
      OutRec.TextFields[4] :=  OutRec.TextFields[1];
    end;
  end
  else
    for i := 1 to 4 do OutRec.TextFields[i] := ' ';
  //Return
  for i := 1 to 4 do if (OutRec.TextFields[i] = '') then OutRec.TextFields[i] := ' ';
  GetLineupText := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// ENGINE CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Procedure to trigger current graphic
procedure TEngineInterface.TriggerGraphic (Mode: SmallInt);
var
  CmdStr: String;
begin
  if (EngineParameters.Enabled) then
  begin
    try
      //Build & send command string
      CmdStr := SOT + 'TG' + ETX + IntToStr(Mode) + EOT;
      if (SocketConnected) AND (EngineParameters.Enabled = TRUE) then EnginePort.Socket.SendText(CmdStr);
      //If aborting, reset packet and disable packet timeout timer
      if (Mode = 2) AND (SocketConnected) then
      begin
        //Disable packet timeout timer
        TickerPacketTimeoutTimer.Enabled := FALSE;
      end;
    except
      if (ErrorLoggingEnabled = True) then
      begin
        Error_Condition := True;
        MainForm.Label5.Caption := 'ERROR';
        //WriteToErrorLog
        if (ErrorLoggingEnabled = True) then
          WriteToErrorLog('Error occurred while trying to trigger ticker on the graphics engine.');
      end;
    end;
  end;
end;

//Function to take a template ID and return the alternate template ID for 1-line vs.
//2-line mode
function TEngineInterface.GetAlternateTemplateID(CurrentTemplateID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutVal: SmallInt;
begin
  OutVal := -1;
  if (Template_Defs_Collection.Count > 0) then
  begin
    //Walk through templates to find alternate mode ID
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      //Check for match between ticker element template ID and entry in template defs
      if (TemplateDefsRecPtr^.Template_ID = CurrentTemplateID) then
      begin
        //Alternate template ID found, so re-assign template ID
        if (TemplateDefsRecPtr^.AlternateModeTemplateID > 0) then
        begin
          OutVal := TemplateDefsRecPtr^.AlternateModeTemplateID;
        end;
      end;
    end;
  end;
  GetAlternateTemplateID := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//FUNCTIONS FOR ADVANCING RECORDS
//Handler for packet requesting more data for ticker
procedure TEngineInterface.EnginePortRead(Sender: TObject; Socket: TCustomWinSocket);
var
  i: SmallInt;
  CurrentTemplateIDNum: SmallInt;
  CurrentTemplateIDNumStr: String;
  RequiredDelay: SmallInt;
  Data: String;
begin
  Data := Socket.ReceiveText;

  CurrentTemplateIDNumStr := '';
  CurrentTemplateIDNum := 0;
  //Disable timeout timer
  //Enable delay timer for next command if not previously in single command mode
  //Set current template ID
  if (Length(Data) >= 9) AND (Pos('FAIL', ANSIUpperCase(Data)) = 0) AND (Pos('IN PLAY', ANSIUpperCase(Data)) = 0) then
  begin
    i := 7;
    Repeat
      CurrentTemplateIDNumStr := CurrentTemplateIDNumStr + Data[i];
      Inc(i);
    Until (i > Length(Data)) OR (Data[i] = ':');
    try
      CurrentTemplateIDNum := StrToInt(Trim(CurrentTemplateIDNumStr));
    except
      //Need exception handling here
      WriteToErrorLog('Error occurred while trying to convert template ID number; command may have failed');
    end;
    if (RunningTicker = TRUE) AND (DisableCommandTimer = FALSE) then
    begin
      if (USEDATAPACKET) then TickerCommandDelayTimer.Enabled := TRUE;
      TickerPacketTimeoutTimer.Enabled := FALSE;
    end;
  end
  else if (Pos('FAIL', ANSIUpperCase(Data)) <> 0) then
  begin
    WriteToErrorLog('Command status = FAILED');
  end
  else if (Pos('In Play', ANSIUpperCase(Data)) <> 0) then
  begin
    WriteToErrorLog('Command status = In Play');
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// DISPLAY DWELL TIMERS
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Handler for timer to send next command
procedure TEngineInterface.TickerCommandDelayTimerTimer(Sender: TObject);
begin
  if (Ticker_Collection.Count > 0) then
  begin
    //Prevent retrigger
    TickerCommandDelayTimer.Enabled := FALSE;
    //Send next record
    SendTickerRecord(TRUE, 0);
  end;
end;
//Handler for 1 mS timer used to send next command when jumping over stats headers
procedure TEngineInterface.JumpToNextTickerRecordTimerTimer(Sender: TObject);
begin
  if (Ticker_Collection.Count > 0) then
  begin
    //Prevent retrigger
    JumpToNextTickerRecordTimer.Enabled := FALSE;
    //Send next record
    SendTickerRecord(TRUE, 0);
  end;
end;

//Function to check the ticker collection for any sponsor logo with a valid time window
function TEngineInterface.CheckForActiveSponsorLogo: Boolean;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  OutVal: Boolean;
begin
  //Init
  OutVal := FALSE;
  //Check collection for any sponsor logo with a valid time window
  if (Ticker_Collection.Count > 0) then
  begin
    for i := 0 to Ticker_Collection.Count-1 do
    begin
      TickerRecPtr := Ticker_Collection.At(i);
      if ((TickerRecPtr^.Template_ID = 1) OR (TickerRecPtr^.Template_ID = 51) OR (TickerRecPtr^.Template_ID = 101) OR (TickerRecPtr^.Template_ID = 151))
      AND (TickerRecPtr^.StartEnableDateTime <= Now) AND
         (TickerRecPtr^.EndEnableDateTime > Now) then OutVal := TRUE;
    end;
  end;
  CheckForActiveSponsorLogo := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Procedure to send next record after receipt of packet
procedure TEngineInterface.SendTickerRecord(NextRecord: Boolean; RecordIndex: SmallInt);
var
  i,j: SmallInt;
  TempStr, CmdStr: String;
  TickerRecPtr, TickerRecPtrNew: ^TickerRec;
  FoundGame: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  IsLast: Boolean;
  OverlayIndex, OverlayIndexNext: SmallInt;
  IsNewOverlay, PadText: Boolean;
  InGameStatsRequired: Boolean;
  FoundGameInDatamart: Boolean;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec: GamePhaseRec;
  CurrentGameState: SmallInt;
  RecordIsSponsor: Boolean;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CurrentGameData: GameRec;
  OkToGo, OKToDisplay: Boolean;
  TokenCounter: SmallInt;
begin
  //Init flags
  OKToDisplay := TRUE;
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  //Init FoundGame flag - used to prevent inadverant commands from being sent if game not found
  FoundGame := TRUE;
  RecordIsSponsor := FALSE;

  //Turn on indicator
  PacketIndicatorTimer.Enabled := TRUE;
  MainForm.ApdStatusLight2.Lit := TRUE;

  //Disable packets and enable timer to re-enable packets
  //PacketEnable := FALSE;
  PacketEnableTimer.Enabled := TRUE;
  try
    //Init
    IsLast := FALSE;

    //Make sure we haven't dumped put
    if ((EngineParameters.Enabled) AND (TickerAbortFlag = FALSE) AND (Ticker_Collection.Count > 0)) then
    begin
      OKToGo := TRUE;

      //Triggering specific record, so set current entry index
      CurrentTickerEntryIndex := RecordIndex;
      //Disble command timer
      DisableCommandTimer := TRUE;

      //Proceed if not at end or in looping mode and at beggining
      if (CurrentTickerEntryIndex <= Ticker_Collection.Count-1) AND (OKToGo) then
      begin
        //Get pointer to current record
        TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);

        //Set default mode
        CmdStr := SOT + 'SM' + ETX + '2' + GS;

        ////////////////////////////////////////////////////////////////////////
        //MAIN PROCESSING LOOP FOR TICKER FIELDS
        ////////////////////////////////////////////////////////////////////////
        //Get template info and load the termporary fields collection
        TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);

        //Get game data if template requires it
        if (TemplateInfo.UsesGameData) then
        begin
          try
            //Use full game data
            CurrentGameData := GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to get game data for ticker');
          end;
          FoundGame := CurrentGameData.DataFound;
        end;

         //Set the default dwell time - minimum = 2000mS; set packet timeout interval
        if (TickerRecPtr^.DwellTime > 2000) then
        begin
          //If sponsor logo dwell is specified, it's a sponsor logo, so use that for the dwell time
          if (TickerRecPtr^.SponsorLogo_Dwell > 0) AND ((TemplateInfo.TemplateSponsorType = 1) OR (TemplateInfo.TemplateSponsorType = 2)) then
          begin
            TickerCommandDelayTimer.Interval := (TickerRecPtr^.SponsorLogo_Dwell*1000);
            TickerPacketTimeoutTimer.Interval := (TickerRecPtr^.SponsorLogo_Dwell*1000) + 5000;
            //Set the current sponsor logo name
            CurrentSponsorLogoName[1] := TickerRecPtr^.SponsorLogo_Name;
          end
          else begin
            TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
            TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 5000;
          end;
        end
        else begin
          TickerCommandDelayTimer.Interval := 2000;
          TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 7000;
        end;

        //If lower-right sponsor template, set sponsor name for use with next template displayed
        if (TemplateInfo.TemplateSponsorType = 3) then
        begin
          //Set the current sponsor logo name for the lower-right
          CurrentSponsorLogoName[2] := TickerRecPtr^.SponsorLogo_Name;
          Exit;
        end
        //If clear lower-right sponsor template, clear the name
        else if (TemplateInfo.TemplateSponsorType = 4) then
        begin
          CurrentSponsorLogoName[2] := '';
          Exit;
        end;

        //Set the template type
        CmdStr := CmdStr + 'ST' + ETX + IntToStr(TemplateInfo.Engine_Template_ID) + GS;

        //V1.2.0.1 Added check for insertion of persistent sponsor logo; if disabled, sponsor logo only shows up in
        //full-page templates
        if (EnablePersistentSponsorLogo) then
        begin
          //Add the sponsor logo region if specified and if not a full sponsor page (TID = 1)
          if (TRIM(CurrentSponsorLogoName[2]) <> '') AND
          (TemplateInfo.Template_ID <> 1) AND (TemplateInfo.Template_ID <> 51) AND
          (TemplateInfo.Template_ID <> 52) AND (TemplateInfo.Template_ID <> 53) then
            CmdStr := CmdStr + 'SD' + ETX + '1000' + ETX + CurrentSponsorLogoName[2] + GS
          else if (TRIM(CurrentSponsorLogoName[2]) <> '') AND
          (TemplateInfo.Template_ID <> 101) AND (TemplateInfo.Template_ID <> 151) AND
          (TemplateInfo.Template_ID <> 152) AND (TemplateInfo.Template_ID <> 153) then
            CmdStr := CmdStr + 'SD' + ETX + '1000' + ETX + CurrentSponsorLogoName[2] + GS
          else
            CmdStr := CmdStr + 'SD' + ETX + '1000' + ETX + 'clear.tga' + GS;
        end;

        //Add the fields from the temporary fields collection
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin
            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
            //Version 2.1
            //Check for single fields (not compound fields required for 2-line, new look engine)
            if (TemplateFieldsRecPtr^.Field_Type > 0) then
            begin
              //Filter out fields ID values of -1 => League designator
              if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
              begin
                //Add region command
                //If not a symbolic fields, send field contents directly
                if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
                begin
                  TempStr := TemplateFieldsRecPtr^.Field_Contents;
                  //Add prefix and/or suffix if applicable
                  if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                    TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                  if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                    TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                  CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) +
                    ETX + TempStr + GS;
                end
                //It's a symbolic name, so get the field contents
                else begin
                  try
                    TempStr := GetValueOfSymbol(TICKER, TemplateFieldsRecPtr^.Field_Contents, CurrentTickerEntryIndex,
                      CurrentGameData, TickerDisplayMode).SymbolValue;
                    //Add previx and/or suffix if applicable
                    if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                    if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to get value for main ticker data field');
                  end;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                  CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr + GS;
                end;
              end;
            end
            /////////////////////////////////////////////////////////////////
            //Code here for compound fields
            /////////////////////////////////////////////////////////////////
            else if (TemplateFieldsRecPtr^.Field_Type = -1) then
            begin
              //Init
              TokenCounter := 1;
              //Iterate while error code not returned
              While (GetItem(TokenCounter, TemplateFieldsRecPtr^.Field_Contents, '|') <> #27) do
              begin
                //Add region command
                //If not a symbolic fields, send field contents directly
                if (Pos('$', GetItem(TokenCounter, TemplateFieldsRecPtr^.Field_Contents, '|')) = 0) then
                begin
                  TempStr := GetItem(TokenCounter, TemplateFieldsRecPtr^.Field_Contents, '|');
                  if (Trim(TempStr) = '') then TempStr := ' ';
                  //Append field ID if first field of compound field
                  if (TokenCounter = 1) then
                    CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr
                  else CmdStr := CmdStr + '|' + TempStr;
                end
                //It's a symbolic name, so get the field contents
                else begin
                  try
                    TempStr := GetValueOfSymbol(TICKER, GetItem(TokenCounter, TemplateFieldsRecPtr^.Field_Contents, '|'), CurrentTickerEntryIndex,
                      CurrentGameData, TickerDisplayMode).SymbolValue;
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to get value for main ticker data field');
                  end;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                  //Append field ID if first field of compound field
                  if (TokenCounter = 1) then
                    CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr
                  else CmdStr := CmdStr + '|' + TempStr;
                end;
                //Increment token count
                Inc(TokenCounter);
              end;
              //Append GS
              CmdStr := CmdStr + GS
            end;
          end;
          //Write out to the as-run log if it's a sponsor logo
          if (Trim(TickerRecPtr^.SponsorLogo_Name) <> '') then
            WriteToAsRunLog('Started display of sponsor logo: ' + CurrentSponsorLogoName[1]);
        end;

        //Set the lineup text
        try
          if (GetLineuptext(CurrentTickerEntryIndex, TickerDisplayMode).UseLineup = TRUE) then
          begin
            LineupData := GetLineuptext(CurrentTickerEntryIndex, TickerDisplayMode);
            //Version 2.0.0.5 Modified to reflect change to line-up/sponsor interaction
            if (TickerRecPtr^.SponsorLogo_Dwell > 0) AND (BlankOutLineupForSponsors) then
            begin
              CmdStr := CmdStr + 'SI' + ETX + '!' + ETX + ' ' + ETX +
                        ' ' + ETX + ' ' + GS;
            end
            else begin
              if (Trim(LineupData.TextFields[1]) = 'NONE') then
                LineupData.TextFields[1] := ' ';
              //Check for use of alert (colored) background; if so, send '!' before first league
              if(AllowAlertBackgroundsForNews) AND (TemplateInfo.Use_Alert_Background) then
                CmdStr := CmdStr + 'SI' + ETX + '!' + LineupData.TextFields[1] + ETX
              else
                CmdStr := CmdStr + 'SI' + ETX + LineupData.TextFields[1] + ETX;
              CmdStr := CmdStr +
                        LineupData.TextFields[2] + ETX +
                        LineupData.TextFields[3] + ETX +
                        LineupData.TextFields[4] + GS;
            end;
          end;
        except
          if (ErrorLoggingEnabled = True) then
            WriteToErrorLog('Error occurred while trying to get and set main ticker line-up text');
        end;

        //Resubstitute lower-case formatting strings and line wrap command
        CmdStr := StringReplace(CmdStr, '[C', '[c', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[R', '[r', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[F', '[f', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[T', '[t', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[X', '[x', [rfReplaceAll]);

        //Substitute style chip codes
        try
          CmdStr := ProcessStyleChips(CmdStr);
        except
          if (ErrorLoggingEnabled = True) then
            WriteToErrorLog('Error occurred while trying to process style chips for main ticker record');
        end;

        //Send the new data
        CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
        if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
        begin
          //Enable packet timeout timer
          if (NEXTRECORD) then TickerPacketTimeoutTimer.Enabled := TRUE;
          //Send command
          try
            EnginePort.Socket.SendText(CmdStr);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying send ticker command to engine');
          end;
          //Start timer to send next record
          if (USEDATAPACKET = FALSE) AND (NEXTRECORD = TRUE) then TickerCommandDelayTimer.Enabled := TRUE;
        end
        //Game not found, so don't send commands & go to next command
        else if (FoundGame = FALSE) then
        begin
          //Enable delay timer
          if (RunningTicker = TRUE) then JumpToNextTickerRecordTimer.Enabled := TRUE;
        end;
      end;
    end;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send additional records to ticker');
    end;
  end;
end;

//Procedure to send alert record
procedure TEngineInterface.SendAlertRecord(RecordIndex: SmallInt);
var
  i,j: SmallInt;
  TempStr, CmdStr: String;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CurrentGameData: GameRec;
  OkToGo: Boolean;
  TokenCounter: SmallInt;
begin
  //Turn on indicator
  PacketIndicatorTimer.Enabled := TRUE;
  MainForm.ApdStatusLight2.Lit := TRUE;

  //Disable packets and enable timer to re-enable packets
  //PacketEnable := FALSE;
  PacketEnableTimer.Enabled := TRUE;
  try
    //Make sure we haven't dumped put
    if ((EngineParameters.Enabled) AND (TickerAbortFlag = FALSE) AND (BreakingNews_Collection.Count > 0)) then
    begin
      OKToGo := TRUE;
      //Disble command timer
      DisableCommandTimer := TRUE;

      //Proceed if not at end or in looping mode and at beggining
      if (RecordIndex <= BreakingNews_Collection.Count-1) then
      begin
        //Get pointer to current record
        BreakingNewsRecPtr := BreakingNews_Collection.At(RecordIndex);

        //Set default mode
        CmdStr := SOT + 'SM' + ETX + '2' + GS;

        ////////////////////////////////////////////////////////////////////////
        //MAIN PROCESSING LOOP FOR TICKER FIELDS
        ////////////////////////////////////////////////////////////////////////
        //Get template info and load the termporary fields collection
        TemplateInfo := LoadTempTemplateFields(BreakingNewsRecPtr^.Template_ID);
        //Do check to swap template ID if required (to ensure correct 1-line or 2-line mode
        //template is used
        if ((TemplateInfo.Template_ID < 1000) AND ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4))) OR
           ((TemplateInfo.Template_ID >= 1000) AND ((TickerDisplayMode = 1) OR (TickerDisplayMode = 2)))then
          TemplateInfo := LoadTempTemplateFields(GetAlternateTemplateID(BreakingNewsRecPtr^.Template_ID));

        //Set the template type
        CmdStr := CmdStr + 'ST' + ETX + IntToStr(TemplateInfo.Engine_Template_ID) + GS;

        //V1.2.0.1 Added check for insertion of persistent sponsor logo; if disabled, sponsor logo only shows up in template
        //with tagline
        if (EnablePersistentSponsorLogo) then
        begin
          CmdStr := CmdStr + 'SD' + ETX + '1000' + ETX + 'clear.tga' + GS;
        end;  

        //Add the fields from the temporary fields collection
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin
            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
            //Version 2.1
            //Check for single fields (not compound fields required for 2-line, new look engine)
            if (TemplateFieldsRecPtr^.Field_Type > 0) then
            begin
              //Filter out fields ID values of -1 => League designator
              if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
              begin
                //Add region command
                //If not a symbolic fields, send field contents directly
                if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
                begin
                  TempStr := TemplateFieldsRecPtr^.Field_Contents;
                  //Add previx and/or suffix if applicable
                  if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                    TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                  if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                    TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                  CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) +
                    ETX + TempStr + GS;
                end
                //It's a symbolic name, so get the field contents
                else begin
                  try
                    TempStr := GetValueOfSymbol(BREAKINGNEWS, TemplateFieldsRecPtr^.Field_Contents, RecordIndex,
                      CurrentGameData, TickerDisplayMode).SymbolValue;
                    //Add previx and/or suffix if applicable
                    if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                    if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to get value for main ticker data field');
                  end;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                  CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr + GS;
                end;
              end;
            end
            /////////////////////////////////////////////////////////////////
            //Code here for compound fields
            /////////////////////////////////////////////////////////////////
            else if (TemplateFieldsRecPtr^.Field_Type = -1) then
            begin
              //Init
              TokenCounter := 1;
              //Iterate while error code not returned
              While (GetItem(TokenCounter, TemplateFieldsRecPtr^.Field_Contents, '|') <> #27) do
              begin
                //Add region command
                //If not a symbolic fields, send field contents directly
                if (Pos('$', GetItem(TokenCounter, TemplateFieldsRecPtr^.Field_Contents, '|')) = 0) then
                begin
                  TempStr := GetItem(TokenCounter, TemplateFieldsRecPtr^.Field_Contents, '|');
                  if (Trim(TempStr) = '') then TempStr := ' ';
                  //Append field ID if first field of compound field
                  if (TokenCounter = 1) then
                    CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr
                  else CmdStr := CmdStr + '|' + TempStr;
                end
                //It's a symbolic name, so get the field contents
                else begin
                  try
                    TempStr := GetValueOfSymbol(BREAKINGNEWS, GetItem(TokenCounter, TemplateFieldsRecPtr^.Field_Contents, '|'), RecordIndex,
                      CurrentGameData, TickerDisplayMode).SymbolValue;
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to get value for main ticker data field');
                  end;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                  //Append field ID if first field of compound field
                  if (TokenCounter = 1) then
                    CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr
                  else CmdStr := CmdStr + '|' + TempStr;
                end;
                //Increment token count
                Inc(TokenCounter);
              end;
              //Append GS
              CmdStr := CmdStr + GS
            end;
          end;
        end;

        //Set the lineup text
        LineupData.TextFields[1] := BreakingNewsRecPtr^.League;
        //LineupData.TextFields[2] := '>>>>';
        //LineupData.TextFields[3] := '>>>>';
        //LineupData.TextFields[4] := '>>>>';
        LineupData.TextFields[2] := 'ALERT';
        LineupData.TextFields[3] := 'ALERT';
        LineupData.TextFields[4] := 'ALERT';
        //Set to league value
        CmdStr := CmdStr + 'SI' + ETX + LineupData.TextFields[1] + ETX +
                  LineupData.TextFields[2] + ETX +
                  LineupData.TextFields[3] + ETX +
                  LineupData.TextFields[4] + GS;

        //Resubstitute lower-case formatting strings and line wrap command
        CmdStr := StringReplace(CmdStr, '[C', '[c', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[R', '[r', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[F', '[f', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[T', '[t', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[X', '[x', [rfReplaceAll]);
        //Disabled due to use of pipe character for "compound" fields
        //CmdStr := StringReplace(CmdStr, '|', '[r 0]', [rfReplaceAll]);

        //Substitute style chip codes
        try
          CmdStr := ProcessStyleChips(CmdStr);
        except
          if (ErrorLoggingEnabled = True) then
            WriteToErrorLog('Error occurred while trying to process style chips for main ticker record');
        end;

        //Send the new data
        CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
        if (SocketConnected) AND (EngineParameters.Enabled = TRUE) then
        begin
          //Send command
          try
            EnginePort.Socket.SendText(CmdStr);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying send ticker command to engine');
          end;
        end;
      end;
    end;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send additional records to ticker');
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// SOCKET EVENT HANDLES
////////////////////////////////////////////////////////////////////////////////
//Handler for packet re-enable timer
procedure TEngineInterface.PacketEnableTimerTimer(Sender: TObject);
begin
  //Disable to prevent retrigger
  PacketEnableTimer.Enabled := FALSE;
  //Re-enable packets
  PacketEnable := TRUE;
end;

//Handler to turn off packet recevied indicator after 250 mS
procedure TEngineInterface.PacketIndicatorTimerTimer(Sender: TObject);
begin
  PacketIndicatorTimer.Enabled := FALSE;
  MainForm.ApdStatusLight2.Lit := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// PACKET TIMEOUT TIMERS
////////////////////////////////////////////////////////////////////////////////
//Handler for ticker packet timeout timer - sends next command if status command not
//received
procedure TEngineInterface.TickerPacketTimeoutTimerTimer(Sender: TObject);
begin
  if (EngineParameters.Enabled) then
  begin
    //Disable timer to prevent retriggering
    TickerPacketTimeoutTimer.Enabled := FALSE;
    //Enable delay timer
    if (RunningTicker = TRUE) then
      //Send next record
      SendTickerRecord(TRUE, 0);
    //Log the timeout
    if (ErrorLoggingEnabled = True) then
    begin
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('A timeout occurred while waiting for a Main Ticker command return status from the graphics engine.');
    end;
  end;
end;

//Handler to light indicator
procedure TEngineInterface.EnginePortConnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.ApdStatusLight1.Lit := TRUE;
  SocketConnected := TRUE;
end;

//Handler to turn off indicator
procedure TEngineInterface.EnginePortDisconnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with connection to ticker graphics engine');
  end;
end;

//Handler for socket error
procedure TEngineInterface.EnginePortError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with connection to ticker graphics engine');
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// BREAKING NEWS FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Procedure to clear breaking news
procedure TEngineInterface.ClearBreakingNews;
var
  CmdStr: String;
begin
  if (EngineParameters.Enabled) then
  begin
    //Don't send commands if not running
    //if (RunningTicker = TRUE) then
    begin
      //Set default mode
      CmdStr := SOT + 'SM' + ETX + '2' + GS;
      //Set template ID = 70 for logo/clock
      CmdStr := CmdStr + 'ST' + ETX + '51' + GS;
      CmdStr := CmdStr + 'SD' + ETX + '0' + ETX + 'EXTRALINE' + GS;
      CmdStr := CmdStr + 'SD' + ETX + '1' + ETX + ' ' + GS;
      CmdStr := CmdStr + 'SD' + ETX + '2' + ETX + ' ' + GS;
      CmdStr := CmdStr + 'SD' + ETX + '1000' + ETX + 'CLEAR' + GS;
      CmdStr := CmdStr + 'SD' + ETX + '1002' + ETX + '0' + GS;
      CmdStr := CmdStr + 'SI' + ETX + ' ' + ETX + ' ' + ETX + ' ' + ETX + ' ' + ETX + ' ' + GS;
      //Send the command
      CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
      if (SocketConnected) AND (EngineParameters.Enabled = TRUE) then
      begin
        //Send command
        EnginePort.Socket.SendText(CmdStr);
      end;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// LOGGING FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToErrorLog (ErrorString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   ErrorLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists('c:\TSTN_Error_LogFiles') = FALSE) then CreateDir('c:\TSTN_Error_LogFiles');
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   ErrorLogStr := TimeStr + '    ' + DateStr + '    ' + ErrorString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := 'c:\TSTN_Error_Logfiles\TSTNErrorLog' + DayStr + '.txt';
   AssignFile (ErrorLogFile, FileName);
   //Write error information to one line in file
   //If file doesn't exist create a new one. Otherwise, check to see if it's one
   //month old. If so, delete & create a new one. Otherwise, append error to file
   if (Not (FileExists(FileName))) then
   begin
      ReWrite (ErrorLogFile);
      try
         Writeln (ErrorLogFile, ErrorLogStr);      finally         CloseFile (ErrorLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (ErrorLogFile);      try         Readln (ErrorLogFile, TestStr);      finally         CloseFile (ErrorLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (ErrorLogFile);         {Create a new logfile}         ReWrite (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end;   end;end;

// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToAsRunLog (AsRunString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   AsRunLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists(AsRunLogFileDirectoryPath) = FALSE) then
   begin
     if (DirectoryExists('c:\TSTN_AsRun_LogFiles') = FALSE) then
       CreateDir('c:\TSTN_AsRun_LogFiles');
     DirectoryStr := 'c:\TSTN_AsRun_LogFiles\';
   end
   else DirectoryStr := AsRunLogFileDirectoryPath;
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   AsRunLogStr := TimeStr + '    ' + DateStr + '    ' + AsRunString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := DirectoryStr + 'TSTNAsRunLog' + DayStr + '.txt';
   AssignFile (AsRunLogFile, FileName);
   {If (FileExists (ErrorLogFile)) then Erase (ErrorLogFile);}
   {Write error information to one line in file}
   {If file doesn't exist create a new one. Otherwise, check to see if it's one }
   {month old. If so, delete & create a new one. Otherwise, append error to file}
   if (Not (FileExists(FileName))) then begin
      ReWrite (AsRunLogFile);
      try
         Writeln (AsRunLogFile, AsRunLogStr);      finally         CloseFile (AsRunLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (AsRunLogFile);      try         Readln (AsRunLogFile, TestStr);      finally         CloseFile (AsRunLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then      begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (AsRunLogFile);         {Create a new logfile}         ReWrite (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end;   end;end;

end.


