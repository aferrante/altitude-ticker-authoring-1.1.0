object dmMain: TdmMain
  OldCreateOrder = False
  Left = 341
  Top = 23
  Height = 817
  Width = 885
  object dbTicker: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB'
    Left = 16
    Top = 24
  end
  object tblTicker_Groups: TADOTable
    Connection = dbTicker
    IndexFieldNames = 'Playlist_Description'
    TableName = 'Ticker_Groups'
    Left = 104
    Top = 80
  end
  object tblTicker_Elements: TADOTable
    Connection = dbTicker
    TableName = 'Ticker_Elements'
    Left = 104
    Top = 24
  end
  object dsTicker_Groups: TDataSource
    DataSet = tblTicker_Groups
    Left = 256
    Top = 144
  end
  object Query1: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 424
    Top = 29
  end
  object dsQuery1: TDataSource
    DataSet = Query1
    Left = 544
    Top = 27
  end
  object tblScheduled_Ticker_Groups: TADOTable
    Connection = dbTicker
    TableName = 'Scheduled_Ticker_Groups'
    Left = 104
    Top = 144
  end
  object dsScheduled_Ticker_Groups: TDataSource
    DataSet = tblScheduled_Ticker_Groups
    Left = 257
    Top = 208
  end
  object tblSponsor_Logos: TADOTable
    Connection = dbTicker
    TableName = 'Sponsor_Logos'
    Left = 426
    Top = 412
  end
  object dsSponsor_Logos: TDataSource
    DataSet = tblSponsor_Logos
    Left = 546
    Top = 413
  end
  object tblGame_Notes: TADOTable
    Connection = dbTicker
    TableName = 'Game_Notes'
    Left = 424
    Top = 345
  end
  object tblLeagues: TADOTable
    Connection = dbTicker
    TableName = 'Leagues'
    Left = 426
    Top = 548
  end
  object tblPromo_Logos: TADOTable
    Connection = dbTicker
    TableName = 'Promo_Logos'
    Left = 426
    Top = 484
  end
  object dsPromo_Logos: TDataSource
    DataSet = tblPromo_Logos
    Left = 546
    Top = 485
  end
  object dbSportbase: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=SportBase'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 664
    Top = 29
  end
  object SportbaseQuery: TADOQuery
    Connection = dbSportbase
    Parameters = <>
    Left = 664
    Top = 93
  end
  object dsSportbaseQuery: TDataSource
    DataSet = SportbaseQuery
    Left = 760
    Top = 93
  end
  object Query2: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 424
    Top = 85
  end
  object dsQuery2: TDataSource
    DataSet = Query2
    Left = 544
    Top = 83
  end
  object Query3: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 424
    Top = 149
  end
  object SportbaseQuery2: TADOQuery
    Connection = dbSportbase
    Parameters = <>
    Left = 664
    Top = 157
  end
  object tblBreakingNews_Elements: TADOTable
    Connection = dbTicker
    TableName = 'BreakingNews_Elements'
    Left = 104
    Top = 208
  end
  object tblBreakingNews_Groups: TADOTable
    Connection = dbTicker
    TableName = 'BreakingNews_Groups'
    Left = 104
    Top = 264
  end
  object tblScheduled_BreakingNews_Groups: TADOTable
    Connection = dbTicker
    TableName = 'Scheduled_BreakingNews_Groups'
    Left = 104
    Top = 328
  end
  object dsBreakingNews_Groups: TDataSource
    DataSet = tblBreakingNews_Groups
    Left = 256
    Top = 328
  end
  object dsScheduled_BreakingNews_Groups: TDataSource
    DataSet = tblScheduled_BreakingNews_Groups
    Left = 257
    Top = 392
  end
  object tblExtraline_Elements: TADOTable
    Connection = dbTicker
    TableName = 'Extraline_Elements'
    Left = 104
    Top = 392
  end
  object SportbaseQuery3: TADOQuery
    Connection = dbSportbase
    Parameters = <>
    Left = 664
    Top = 221
  end
  object dsSportbaseQuery3: TDataSource
    DataSet = SportbaseQuery3
    Left = 760
    Top = 221
  end
  object tblTemplate_Defs: TADOTable
    Connection = dbTicker
    TableName = 'Template_Defs'
    Left = 104
    Top = 456
  end
  object tblTeams: TADOTable
    Connection = dbTicker
    TableName = 'Teams'
    Left = 430
    Top = 620
  end
  object dsTeams: TDataSource
    DataSet = tblTeams
    Left = 534
    Top = 621
  end
  object tblGame_Phase_Codes: TADOTable
    Connection = dbTicker
    TableName = 'Game_Phase_Codes'
    Left = 662
    Top = 372
  end
  object dsGame_Phase_Codes: TDataSource
    DataSet = tblGame_Phase_Codes
    Left = 782
    Top = 373
  end
  object tblCustom_Headers: TADOTable
    Connection = dbTicker
    TableName = 'Custom_Headers'
    Left = 104
    Top = 520
  end
  object dsCustom_Headers: TDataSource
    DataSet = tblCustom_Headers
    Left = 256
    Top = 520
  end
  object GameDataQuery: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 424
    Top = 285
  end
  object WeatherQuery: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 544
    Top = 285
  end
  object tblDefault_Templates: TADOTable
    Connection = dbTicker
    TableName = 'Default_Templates'
    Left = 104
    Top = 592
  end
  object Query4: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 424
    Top = 213
  end
  object dsQuery4: TDataSource
    DataSet = Query2
    Left = 544
    Top = 211
  end
  object dbLogoImages: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB'
    Left = 431
    Top = 710
  end
  object LogoUpdateQuery: TADOQuery
    Connection = dbLogoImages
    Parameters = <>
    Left = 535
    Top = 709
  end
end
