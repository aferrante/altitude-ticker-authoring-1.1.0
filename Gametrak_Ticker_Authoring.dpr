program Gametrak_Ticker_Authoring;

uses
  Forms,
  Main in 'Main.pas' {MainForm},
  Preferences in 'Preferences.pas' {Prefs},
  AboutBox in 'AboutBox.pas' {About},
  DataModule in 'DataModule.pas' {dmMain: TDataModule},
  SearchDataEntry in 'SearchDataEntry.pas' {TextSearchDlg},
  PlaylistGraphicsViewer in 'PlaylistGraphicsViewer.pas' {PlaylistViewerDlg},
  NoteEntryEditor in 'NoteEntryEditor.pas' {NoteEntryEditorDlg},
  ScheduleEntryTimeEditor in 'ScheduleEntryTimeEditor.pas' {ScheduleEntryEditDlg},
  NCAAManualGameEntry in 'NCAAManualGameEntry.pas' {NCAAMatchupManualDlg},
  Globals in 'Globals.pas',
  ZipperEntry in 'ZipperEntry.pas' {ZipperEntryDlg},
  BreakingNewsPlaylistSelect in 'BreakingNewsPlaylistSelect.pas' {BreakingNewsPlaylistSelectDlg},
  ZipperEntryEditor in 'ZipperEntryEditor.pas' {ZipperEntryEditorDlg},
  EnableDateTimeEditor in 'EnableDateTimeEditor.pas' {EnableDateTimeEditorDlg},
  DuplicationCountSelect in 'DuplicationCountSelect.pas' {DuplicationCountSelectDlg},
  EngineConnectionPreferences in 'EngineConnectionPreferences.pas' {EnginePrefsDlg},
  ManualGameEditor in 'ManualGameEditor.pas' {ManualGameDlg},
  SponsorLogoEditor in 'SponsorLogoEditor.pas' {SponsorLogoEditorDlg},
  CustomCategoryEditor in 'CustomCategoryEditor.pas' {CustomHeaderEditorDlg},
  GameDataFunctions in 'GameDataFunctions.pas',
  TemplateSelect in 'TemplateSelect.pas' {TemplateSelectDlg},
  DatabaseEditor in 'DatabaseEditor.pas' {DBEditorDlg},
  EngineIntf in 'EngineIntf.pas' {EngineInterface};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Gametrak_Ticker_Authoring';
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TPrefs, Prefs);
  Application.CreateForm(TAbout, About);
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TTextSearchDlg, TextSearchDlg);
  Application.CreateForm(TPlaylistViewerDlg, PlaylistViewerDlg);
  Application.CreateForm(TNoteEntryEditorDlg, NoteEntryEditorDlg);
  Application.CreateForm(TScheduleEntryEditDlg, ScheduleEntryEditDlg);
  Application.CreateForm(TNCAAMatchupManualDlg, NCAAMatchupManualDlg);
  Application.CreateForm(TZipperEntryDlg, ZipperEntryDlg);
  Application.CreateForm(TBreakingNewsPlaylistSelectDlg, BreakingNewsPlaylistSelectDlg);
  Application.CreateForm(TZipperEntryEditorDlg, ZipperEntryEditorDlg);
  Application.CreateForm(TEnableDateTimeEditorDlg, EnableDateTimeEditorDlg);
  Application.CreateForm(TDuplicationCountSelectDlg, DuplicationCountSelectDlg);
  Application.CreateForm(TEnginePrefsDlg, EnginePrefsDlg);
  Application.CreateForm(TManualGameDlg, ManualGameDlg);
  Application.CreateForm(TSponsorLogoEditorDlg, SponsorLogoEditorDlg);
  Application.CreateForm(TCustomHeaderEditorDlg, CustomHeaderEditorDlg);
  Application.CreateForm(TTemplateSelectDlg, TemplateSelectDlg);
  Application.CreateForm(TDBEditorDlg, DBEditorDlg);
  Application.CreateForm(TEngineInterface, EngineInterface);
  Application.Run;
end.
