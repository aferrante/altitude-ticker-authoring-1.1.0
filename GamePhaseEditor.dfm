object GamePhaseEditorDlg: TGamePhaseEditorDlg
  Left = 302
  Top = 237
  BorderStyle = bsDialog
  Caption = 'Game Phase Code Editor'
  ClientHeight = 547
  ClientWidth = 1081
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 16
    Top = 11
    Width = 1049
    Height = 470
    BevelWidth = 2
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 14
      Width = 204
      Height = 20
      Caption = 'Game Phase Code Editor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 61
      Top = 398
      Width = 37
      Height = 20
      Caption = 'First'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 188
      Top = 398
      Width = 38
      Height = 20
      Caption = 'Prior'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 314
      Top = 398
      Width = 37
      Height = 20
      Caption = 'Next'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 439
      Top = 398
      Width = 36
      Height = 20
      Caption = 'Last'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 564
      Top = 398
      Width = 33
      Height = 20
      Caption = 'Edit'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 687
      Top = 398
      Width = 37
      Height = 20
      Caption = 'Post'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 808
      Top = 398
      Width = 56
      Height = 20
      Caption = 'Cancel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 928
      Top = 398
      Width = 65
      Height = 20
      Caption = 'Refresh'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object tsDBGrid1: TtsDBGrid
      Left = 16
      Top = 40
      Width = 1009
      Height = 353
      CellSelectMode = cmNone
      CheckBoxStyle = stCheck
      Cols = 10
      DatasetType = dstStandard
      DataSource = dmMain.dsGame_Phase_Codes
      DefaultRowHeight = 18
      ExactRowCount = True
      ExportDelimiter = ','
      FieldState = fsCustomized
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HeadingFont.Charset = DEFAULT_CHARSET
      HeadingFont.Color = clWindowText
      HeadingFont.Height = -13
      HeadingFont.Name = 'MS Sans Serif'
      HeadingFont.Style = [fsBold]
      HeadingHeight = 20
      HeadingParentFont = False
      ParentFont = False
      ParentShowHint = False
      RowChangedIndicator = riAutoReset
      RowMoving = False
      ShowHint = False
      TabOrder = 0
      Version = '2.20.26'
      XMLExport.Version = '1.0'
      XMLExport.DataPacketVersion = '2.0'
      DataBound = True
      ColProperties = <
        item
          DataCol = 1
          FieldName = 'League'
          Col.FieldName = 'League'
          Col.Heading = 'League'
          Col.Width = 75
          Col.AssignedValues = '?'
        end
        item
          DataCol = 2
          FieldName = 'SI_Phase_Code'
          Col.FieldName = 'SI_Phase_Code'
          Col.Heading = 'SI Phase Code'
          Col.Width = 110
          Col.AssignedValues = '?'
        end
        item
          DataCol = 3
          FieldName = 'Phase_Code'
          Col.FieldName = 'Phase_Code'
          Col.Heading = 'ST Phase Code'
          Col.Width = 116
          Col.AssignedValues = '?'
        end
        item
          DataCol = 4
          FieldName = 'Display_Period'
          Col.FieldName = 'Display_Period'
          Col.Heading = 'Display'
          Col.Width = 101
          Col.AssignedValues = '?'
        end
        item
          DataCol = 5
          FieldName = 'End_Is_Half'
          Col.FieldName = 'End_Is_Half'
          Col.Heading = 'End is Half'
          Col.Width = 89
          Col.AssignedValues = '?'
        end
        item
          DataCol = 6
          FieldName = 'Display_Half'
          Col.FieldName = 'Display_Half'
          Col.Heading = 'Display Half'
          Col.Width = 98
          Col.AssignedValues = '?'
        end
        item
          DataCol = 7
          FieldName = 'Display_Final'
          Col.FieldName = 'Display_Final'
          Col.Heading = 'Display Final'
          Col.Width = 103
          Col.AssignedValues = '?'
        end
        item
          DataCol = 8
          FieldName = 'Display_Extended1'
          Col.FieldName = 'Display_Extended1'
          Col.Heading = 'Display Extended 1'
          Col.Width = 139
          Col.AssignedValues = '?'
        end
        item
          DataCol = 9
          FieldName = 'Display_Extended2'
          Col.FieldName = 'Display_Extended2'
          Col.Heading = 'Display Extended 2'
          Col.Width = 128
          Col.AssignedValues = '?'
        end
        item
          DataCol = 10
          FieldName = 'Game_OT'
          Col.FieldName = 'Game_OT'
          Col.Heading = 'Game OT'
          Col.Width = 78
          Col.AssignedValues = '?'
        end>
    end
    object DBNavigator1: TDBNavigator
      Left = 16
      Top = 424
      Width = 1008
      Height = 33
      DataSource = dmMain.dsGame_Phase_Codes
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbEdit, nbPost, nbCancel, nbRefresh]
      TabOrder = 1
    end
  end
  object BitBtn1: TBitBtn
    Left = 488
    Top = 496
    Width = 105
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkClose
  end
end
