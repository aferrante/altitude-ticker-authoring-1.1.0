// This unit contains functions related to getting game data from Sportsticker and Stats Inc.
// Original Rev: 12/21/2009
unit GameDataFunctions;

interface

uses  SysUtils,
      Globals,
      Main, //Main form
      DataModule, //Data module
      EngineIntf;

  //utility functions
  function GetLastName(FullName: String): String;
  function FormatGameClock(TimeMin: SmallInt; TimeSec: SmallInt): String;
  function GetGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
  function GetGameData (League: String; GCode: String): GameRec;
  function GetFootballGameSituationData(CurrentGameData: GameRec): FootballSituationRec;
  function GetTeamMnemonicFromID (League: String; TeamID: LongInt): String;

  //Get value of symbolic name
  function GetValueOfSymbol(PlaylistType: SmallInt; Symbolname: String; CurrentEntryIndex: SmallInt;
               CurrentGameData: GameRec; TickerMode: SmallInt): SymbolValueRec;

  //Function to pull weather forecast data for the home stadium
  function GetWeatherForecastData(League: String; HomeStatsID: LongInt): WeatherForecastRec;

  //Function to parse team records
  function ParseTeamRecord(RecordStringIn: String): TeamRecordRec;

  //General stats functions
  function GetBoxscoreData(League: String; GameCode: String): BoxscoreRec;

  //Baseball stats functions
  function GetStartingPitcherStats(GameCode: String): StartingPitcherStatsRec;
  function GetGameFinalPitcherStats(GameCode: String): FinalPitcherStatsRec;
  function GetGameFinalBatterStats(GameCode: String; TeamID: LongInt; StatNum: SmallInt): FinalBatterStatsRec;

  //Basketball stats functions
  function GetGameFinalBBallStats(League, GameCode: String; TeamID: LongInt; StatNum: SmallInt): FinalBBallStatsRec;

  //Hockey Stats functions
  function GetGameFinalSkaterStats(GameCode: String; TeamID: LongInt; StatNum: SmallInt): FinalSkaterStatsRec;
  function GetGameFinalGoalieStats(GameCode: String; TeamID: LongInt; StatNum: SmallInt): FinalGoalieStatsRec;

  //Football stats functions - all stats returned in 1 record
  function GetGameFinalFBStats(League, GameCode: String): FinalFBStatsRec;

  //Fantasy Stats functions
  function GetFantasyFootballPassingStats: FantasyFootballPassingStatsRec;
  function GetFantasyFootballRushingStats: FantasyFootballRushingStatsRec;
  function GetFantasyFootballReceivingStats: FantasyFootballReceivingStatsRec;
  function GetFantasyBaseballPitchingStats: FantasyBaseballPitchingStatsRec;
  function GetFantasyBaseballBattingStats: FantasyBaseballBattingStatsRec;
  function GetFantasyBasketballStats(StatType: String): FantasyBasketballStatsRec;
  function GetFantasyHockeyPointsStats: FantasyHockeyPointsStatsRec;
  function GetFantasyHockeyGoalsStats: FantasyHockeyGoalsStatsRec;
  function GetFantasyHockeyAssistsStats: FantasyHockeyAssistsStatsRec;

implementation

//Utility function to parse the player last name out of a full name string
function GetLastName(FullName: String): String;
var
  i: SmallInt;
  OutStr: String;
  Cursor: SmallInt;
begin
  OutStr := FullName;
  Cursor := Pos(',', FullName);
  if (Cursor > 1) then
  begin
    OutStr := '';
    for i := 1 to Cursor-1 do
      OutStr := OutStr + FullName[i];
  end;
  GetLastName := OutStr;
end;

//Function to parse team records
function ParseTeamRecord(RecordStringIn: String): TeamRecordRec;
var
  Tokens: Array[1..5] of String;
  i, Cursor, TokenIndex: SmallInt;
  ParamVal: SmallInt;
  OutRec: TeamRecordRec;
begin
  //Parse the string for the team record
  if (Length(RecordStringIn) >= 3) then
  begin
    //Parse for tokens
    for i := 1 to 4 do Tokens[i] := '';
    Cursor := 1;
    TokenIndex :=1;
    repeat
      if (RecordStringIn[Cursor] = '-') then
      begin
        Inc(Cursor);
        Inc(TokenIndex);
      end;
      Tokens[TokenIndex] := Tokens[TokenIndex] + RecordStringIn[Cursor];
      Inc(Cursor);
    until (Cursor > Length(RecordStringIn)) OR (TokenIndex = 5);
  end;
  //Set output record
  OutRec.Param[1] := Tokens[1];
  OutRec.Param[2] := Tokens[2];
  OutRec.Param[3] := Tokens[3];
  OutRec.Param[4] := Tokens[4];
  ParseTeamRecord := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
//Function to format the game clock into minutes and seconds
////////////////////////////////////////////////////////////////////////////////
function FormatGameClock(TimeMin: SmallInt; TimeSec: SmallInt): String;
var
  SecsStr: String;
  OutStr: String;
begin
  //Check for manual game as indicated by min, sec = -1
  if (TimeSec = -1) AND (TimeMin = -1) then OutStr := ''
  else begin
    if (TimeSec < 10) then SecsStr := '0' + IntToStr(TimeSec)
    else SecsStr := IntToStr(TimeSec);
    if (TimeMin > 0) then
      OutStr := IntToStr(TimeMin) + ':' + SecsStr
    else
      OutStr := ':' + SecsStr;
  end;
  FormatGameClock := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
//Function to take a league and a game phase code, and return a game phase record
////////////////////////////////////////////////////////////////////////////////
function GetGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
var
  i: SmallInt;
  GamePhaseRecPtr: ^GamePhaseRec; //Pointer to game phase record type
  OutRec: GamePhaseRec;
  FoundRecord: Boolean;
begin
  FoundRecord := FALSE;
  OutRec.League := ' ';
  OutRec.ST_Phase_Code := 0;
  OutRec.SI_Phase_Code := 0;
  OutRec.Display_Period := ' ';
  OutRec.End_Is_Half := FALSE;
  OutRec.Display_Half := ' ';
  OutRec.Display_Final := ' ';
  OutRec.Display_Extended1 := ' ';
  OutRec.Display_Extended2 := ' ';
  OutRec.Game_OT := FALSE;
  if (Game_Phase_Collection.Count > 0) then
  begin
    i := 0;
    repeat
      GamePhaseRecPtr := Game_Phase_Collection.At(i);
      if (Trim(GamePhaseRecPtr^.League) = Trim(League)) AND (GamePhaseRecPtr^.SI_Phase_Code = GPhase) then
      begin
        OutRec.League := Trim(League);
        OutRec.ST_Phase_Code := GPhase;
        OutRec.SI_Phase_Code := GPhase;
        OutRec.Label_Period := Trim(GamePhaseRecPtr^.Label_Period);
        OutRec.Display_Period := Trim(GamePhaseRecPtr^.Display_Period);
        OutRec.End_Is_Half := GamePhaseRecPtr^.End_Is_Half;
        OutRec.Display_Half := GamePhaseRecPtr^.Display_Half;
        OutRec.Display_Half := Trim(GamePhaseRecPtr^.Display_Half);
        OutRec.Display_Final := Trim(GamePhaseRecPtr^.Display_Final);
        OutRec.Display_Extended1 := Trim(GamePhaseRecPtr^.Display_Extended1);
        OutRec.Display_Extended2 := Trim(GamePhaseRecPtr^.Display_Extended2);
        OutRec.Game_OT := GamePhaseRecPtr^.Game_OT;
        FoundRecord := TRUE;
      end;
      Inc(i);
    until (i = Game_Phase_Collection.Count) OR (FoundRecord = TRUE);
  end;
  GetGamePhaseRec := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
//Function to run stored procedures to get game information
////////////////////////////////////////////////////////////////////////////////
function GetGameData (League: String; GCode: String): GameRec;
var
  OutRec: GameRec;
begin
  //Check if valid league first
  if (League <> 'NONE') then
  begin
    try
      //Execute required stored procedure
      dmMain.GameDataQuery.Active := FALSE;
      dmMain.GameDataQuery.SQL.Clear;
      dmMain.GameDataQuery.SQL.Add('/* */sp_GetSI' + League + 'GameInfoCSN' + ' ' + QuotedStr(GCode));
      dmMain.GameDataQuery.Open;
      //Check for record count > 0 and process
      if (dmMain.GameDataQuery.RecordCount > 0) then
      begin
        OutRec.League := dmMain.GameDataQuery.FieldByName('League').AsString;
        //Get extra fields if it's an MLB game
        if (League = 'MLB') then
        begin
          OutRec.SubLeague := dmMain.GameDataQuery.FieldByName('SubLeague').AsString;
          OutRec.DoubleHeader := dmMain.GameDataQuery.FieldByName('DoubleHeader').AsBoolean;
          OutRec.DoubleHeaderGameNumber := dmMain.GameDataQuery.FieldByName('DoubleHeaderGameNumber').AsInteger;
          OutRec.TimeMin := 0;
          OutRec.TimeSec := 0;
          OutRec.Count := dmMain.GameDataQuery.FieldByName('Count').AsString;;
          OutRec.Situation := dmMain.GameDataQuery.FieldByName('Situation').AsString;
          OutRec.Batter := dmMain.GameDataQuery.FieldByName('Batter').AsString;
          OutRec.Baserunners[1] := dmMain.GameDataQuery.FieldByName('FirstBaserunner').AsString;
          OutRec.Baserunners[2] := dmMain.GameDataQuery.FieldByName('SecondBaserunner').AsString;
          OutRec.Baserunners[3] := dmMain.GameDataQuery.FieldByName('ThirdBaserunner').AsString;
        end
        else if (League = 'CFB') OR (League = 'CFL') OR (League = 'NFL')then
        begin
          OutRec.SubLeague := ' ';
          OutRec.DoubleHeader := FALSE;
          OutRec.DoubleHeaderGameNumber := 0;
          OutRec.TimeMin := dmMain.GameDataQuery.FieldByName('TimeMin').AsInteger;
          OutRec.TimeSec := dmMain.GameDataQuery.FieldByName('TimeSec').AsInteger;
          OutRec.Count := '000';
          OutRec.Situation := dmMain.GameDataQuery.FieldByName('Situation').AsString;;
          OutRec.Batter := '';
          OutRec.Baserunners[1] := '';
          OutRec.Baserunners[2] := '';
          OutRec.Baserunners[3] := '';
        end
        else begin
          OutRec.SubLeague := ' ';
          OutRec.DoubleHeader := FALSE;
          OutRec.DoubleHeaderGameNumber := 0;
          OutRec.TimeMin := dmMain.GameDataQuery.FieldByName('TimeMin').AsInteger;
          OutRec.TimeSec := dmMain.GameDataQuery.FieldByName('TimeSec').AsInteger;
          OutRec.Count := '000';
          OutRec.Situation := 'FFF';
          OutRec.Batter := '';
          OutRec.Baserunners[1] := '';
          OutRec.Baserunners[2] := '';
          OutRec.Baserunners[3] := '';
        end;
        //Set team strength variables for NHL
        if (League = 'NHL') then
        begin
          if (ANSIUpperCase(dmMain.GameDataQuery.FieldByName('VStrength').AsString) = 'Powerplay') then
            OutRec.Visitor_Param1 := 1
          else
            OutRec.Visitor_Param1 := 0;
          if (ANSIUpperCase(dmMain.GameDataQuery.FieldByName('HStrength').AsString) = 'Powerplay') then
            OutRec.Home_Param1 := 1
          else
            OutRec.Home_Param1 := 0;
          OutRec.Visitor_Param2 := 0;
          OutRec.Home_Param2 := 0;
        end
        //Set hits & errors for MLB linescore
        else if (League = 'MLB') then
        begin
          OutRec.Visitor_Param1 := StrToIntDef(dmMain.GameDataQuery.FieldByName('VHits').AsString, 0);
          OutRec.Visitor_Param2 := StrToIntDef(dmMain.GameDataQuery.FieldByName('VErrors').AsString, 0);
          OutRec.Home_Param1 := StrToIntDef(dmMain.GameDataQuery.FieldByName('HHits').AsString, 0);
          OutRec.Home_Param2 := StrToIntDef(dmMain.GameDataQuery.FieldByName('HErrors').AsString, 0);
        end
        //Set to defaults for all other sports
        else begin
          OutRec.Visitor_Param1 := 0;
          OutRec.Visitor_Param2 := 0;
          OutRec.Home_Param1 := 0;
          OutRec.Home_Param2 := 0;
        end;
        //If college, get ranks
        if (League = 'NCAAB') OR (League = 'NCAAF') OR (League = 'NCAAW') OR
           (League = 'CFB') OR (League = 'CBK') OR (League = 'WCBK') then
        begin
          OutRec.VRank := dmMain.GameDataQuery.FieldByName('VRank').AsString;
          if (Trim(OutRec.VRank) = '0') then OutRec.VRank := '';
          OutRec.HRank := dmMain.GameDataQuery.FieldByName('HRank').AsString;
          if (Trim(OutRec.HRank) = '0') then OutRec.HRank := '';
        end
        else begin
          OutRec.VRank := ' ';
          OutRec.HRank := ' ';
        end;
        //Set remaining parameters
        OutRec.League := League;
        OutRec.GameID := GCode;
        OutRec.HStatsIncID := dmMain.GameDataQuery.FieldByName('HId').AsInteger;
        OutRec.VStatsIncID := dmMain.GameDataQuery.FieldByName('VId').AsInteger;
        OutRec.HName := dmMain.GameDataQuery.FieldByName('HName').AsString;
        OutRec.VName := dmMain.GameDataQuery.FieldByName('VName').AsString;
        OutRec.HMnemonic := dmMain.GameDataQuery.FieldByName('HMnemonic').AsString;
        OutRec.VMnemonic := dmMain.GameDataQuery.FieldByName('VMnemonic').AsString;
        OutRec.HScore := dmMain.GameDataQuery.FieldByName('HScore').AsInteger;
        OutRec.VScore := dmMain.GameDataQuery.FieldByName('VScore').AsInteger;
        OutRec.HRecord := dmMain.GameDataQuery.FieldByName('HRecord').AsString;
        OutRec.VRecord := dmMain.GameDataQuery.FieldByName('VRecord').AsString;
        OutRec.HStreak := dmMain.GameDataQuery.FieldByName('HStreak').AsString;
        OutRec.VStreak := dmMain.GameDataQuery.FieldByName('VStreak').AsString;
        OutRec.Phase := dmMain.GameDataQuery.FieldByName('Phase').AsInteger;
        //Set the game state
        if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'Pre-Game') then
          OutRec.State := 1
        else if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'In-Progress') then
          OutRec.State := 2
        else if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'Final') then
          OutRec.State := 3
        else if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'Postponed') then
          OutRec.State := 4
        else if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'Delayed') then
          OutRec.State := 5
        else if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'Suspended') then
          OutRec.State := 6
        else
          OutRec.State := -1;
        //Set state for in-progress check
        OutRec.StateForInProgressCheck := OutRec.State;  
        OutRec.Description := OutRec.League + ': ' + OutRec.VName + ' @ ' + OutRec.HName + '  (' +
          DateToStr(dmMain.GameDataQuery.FieldByName('Date').AsDateTime) + ')';
        OutRec.Date := dmMain.GameDataQuery.FieldByName('Date').AsDateTime;
        OutRec.StartTime := dmMain.GameDataQuery.FieldByName('StartTime').AsDateTime;
        OutRec.DataFound := TRUE;
      end
      else OutRec.DataFound := FALSE;
    except
      if (ErrorLoggingEnabled = True) then
        EngineInterface.WriteToErrorLog('Error occurred in GetGameData function for full game data');
      //Clear data found flag
      OutRec.DataFound := FALSE;
    end;
  end;
  //Return
  GetGameData := OutRec;
end;

function GetFootballGameSituationData(CurrentGameData: GameRec): FootballSituationRec;
var
  i: SmallInt;
  OutRec: FootballSituationRec;
  VTeamID, HTeamID, PossessionTeamID: Double;
  PossessionYardsFromGoal: SmallInt;
  Down, Distance: SmallInt;
  TeamRecPtr: ^TeamRec;
  StrCursor, SubStrCounter: SmallInt;
  Substrings: Array[1..4] of String;
begin
  //Init
  HTeamID := 0;
  VTeamID := 0;
  StrCursor := 1;
  SubStrCounter := 1;
  for i := 1 to 4 do Substrings[i] := '';

  if (Length(CurrentGameData.Situation) > 0) AND (CurrentGameData.Situation <> ' - - - ') then
  begin
    //Parse out situation string to components
    While (StrCursor <= Length(CurrentGameData.Situation)) do
    begin
      //Get substrings for data fields
      While (CurrentGameData.Situation[StrCursor] <> '-') do
      begin
        SubStrings[SubStrCounter] := SubStrings[SubStrCounter] + CurrentGameData.Situation[StrCursor];
        Inc(StrCursor);
      end;
      //Go to next field
      Inc(StrCursor);
      Inc(SubStrCounter);
    end;

    //Set values
    //Do ID of team with possession
    if (Trim(SubStrings[1]) <> '') then
    begin
      try
        PossessionTeamID := Trunc(StrToFloat(Substrings[1]));
      except
        PossessionTeamID := 0;
        EngineInterface.WriteToErrorLog('Error occurred while trying to convert team ID for football possession');
      end;
    end
    else PossessionTeamID := 0;

    //Get yards from goal
    if (Trim(SubStrings[2]) <> '') then
    begin
      try
        PossessionYardsFromGoal := StrToInt(Substrings[2]);
      except
        PossessionYardsFromGoal := 0;
        EngineInterface.WriteToErrorLog('Error occurred while trying to convert field position for football possession');
      end;
    end
    else PossessionYardsFromGoal := 0;

    //Get Down
    if (Trim(SubStrings[3]) <> '') then
    begin
      try
        Down := StrToInt(Substrings[3]);
      except
        Down := 0;
        EngineInterface.WriteToErrorLog('Error occurred while trying to down for football possession');
      end;
    end
    else Down := 0;

    //Get Distance
    if (Trim(SubStrings[4]) <> '') then
    begin
      try
        Distance := StrToInt(Substrings[4]);
      except
        Distance := 0;
        EngineInterface.WriteToErrorLog('Error occurred while trying to down for football possession');
      end;
    end
    else Distance := 0;

    //Get team IDs of visiting & home teams
    if (Team_Collection.Count > 0) then
    begin
      for i := 0 to Team_Collection.Count-1 do
      begin
        TeamRecPtr := Team_Collection.At(i);
        if (CurrentGameData.VMnemonic = TeamRecPtr^.DisplayName1) AND
           (CurrentGameData.League = TeamRecPtr^.League) then
          VTeamID := TeamRecPtr^.StatsIncId;
      end;
      for i := 0 to Team_Collection.Count-1 do
      begin
        TeamRecPtr := Team_Collection.At(i);
        if (CurrentGameData.HMnemonic = TeamRecPtr^.DisplayName1) AND
           (CurrentGameData.League = TeamRecPtr^.League) then
          HTeamID := TeamRecPtr^.StatsIncId;
      end;
    end;
    //Check to find team with possession
    if (VTeamID = PossessionTeamID) then
    begin
      OutRec.VisitorHasPossession := TRUE;
      OutRec.HomeHasPossession := FALSE;
    end
    else if (HTeamID = PossessionTeamID) then
    begin
      OutRec.VisitorHasPossession := FALSE;
      OutRec.HomeHasPossession := TRUE;
    end
    else begin
      OutRec.VisitorHasPossession := FALSE;
      OutRec.HomeHasPossession := FALSE;
    end;
    //Set remaining values
    OutRec.YardsFromGoal := PossessionYardsFromGoal;
    OutRec.Down := Down;
    OutRec.Distance := Distance;
  end
  else begin
    OutRec.VisitorHasPossession := FALSE;
    OutRec.HomeHasPossession := FALSE;
    OutRec.YardsFromGoal := -1;
    OutRec.Down := 0;
    OutRec.Distance := 0;
  end;
  //Set return value
  GetFootballGameSituationData := OutRec;
end;

//Function to get team mnemonic from team ID and league - used for Fantasy Stats
function GetTeamMnemonicFromID (League: String; TeamID: LongInt): String;
var
  i: SmallInt;
  TeamRecPtr: ^TeamRec;
  OutStr: String;
  FoundMatch: Boolean;
begin
  //Init
  i := 0;
  FoundMatch := FALSE;
  OutStr := '';
  //Look for match
  if (Team_Collection.Count > 0) then
  repeat
    TeamRecPtr := Team_Collection.At(i);
    if (TeamRecPtr^.League = League) AND (TeamRecPtr^.StatsIncID = TeamID) then
    begin
      OutStr := TeamRecPtr^.DisplayName1;
      FoundMatch := TRUE;
    end;
    inc(i);
  until (i = Team_Collection.Count) OR (FoundMatch);
  GetTeamMnemonicFromID := OutStr;
end;

//Function to take the integer for down (down & distance) & return with suffix
function IntToDown(Down: SmallInt): String;
var
  OutStr: String;
begin
  Case Down of
   1: OutStr := '1st';
   2: OutStr := '2nd';
   3: OutStr := '3rd';
   4: OutStr := '4th';
  end;
  IntToDown := OutStr; 
end;

////////////////////////////////////////////////////////////////////////////////
// Function to take a symbolic name and return the value of the string to be
// sent to the engine
////////////////////////////////////////////////////////////////////////////////
function GetValueOfSymbol(PlaylistType: SmallInt; Symbolname: String;
                          CurrentEntryIndex: SmallInt; CurrentGameData: GameRec;
                          TickerMode: SmallInt): SymbolValueRec;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  OutStr, OutStr2, OutStr3: String;
  TextFieldBias: SmallInt;
  GamePhaseData: GamePhaseRec;
  BaseName: String;
  CharPos: SmallInt;
  Suffix: String;
  SwitchPosMixed, SwitchPosUpper: SmallInt;
  SwitchPosStartBlack, SwitchPosEndBlack: SmallInt;
  TempStr: String;
  Days: array[1..7] of string;
  TeamRecord: TeamRecordRec;
  StartingPitcherStats: StartingPitcherStatsRec;
  FinalPitcherStats: FinalPitcherStatsRec;
  FinalBatterStats: FinalBatterStatsRec;
  FinalBBallStats: FinalBBallStatsRec;
  FinalSkaterStats: FinalSkaterStatsRec;
  FinalGoalieStats: FinalGoalieStatsRec;
  FootballSituation: FootballSituationRec;
//  FinalFBStats: FinalFBStatsRec;
  OKToDisplay: Boolean;
  LineWrap: Boolean;
  Index: SmallInt;
begin
  //Init
  OutStr := SymbolName;
  BaseName := '';
  Suffix := '';
  LongTimeFormat := 'h:mm AM/PM';
  OKToDisplay := TRUE;
  LineWrap := FALSE;

  TextFieldBias := 0;

  //Get info on current record
  if (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
     TICKER: TickerRecPtr := Ticker_Collection.At(CurrentEntryIndex);
     BREAKINGNEWS: BreakingNewsRecPtr := BreakingNews_Collection.At(CurrentEntryIndex);
    end;
  end;

  //Extract out base symbol name and any suffix
  CharPos := Pos('|', SymbolName);
  if (CharPos > 0) then
  begin
    //Get base name
    if (CharPos > 1) then
      for i := 1 to CharPos-1 do BaseName := BaseName + SymbolName[i];
    //Get suffix
    if (Length(Symbolname) > CharPos) then
      for i := CharPos+1 to Length(Symbolname) do Suffix := Suffix + SymbolName[i];
    //Set new base name
    SymbolName := BaseName;
  end;

  //Blank
  if (SymbolName = '$Blank') then OutStr := ' '
  //Sponsor logo
  else if (SymbolName = '$Sponsor_Logo') then
  begin
    if (CurrentEntryIndex <> NOENTRYINDEX) then
    begin
      Case  PlaylistType of
        TICKER: OutStr := TickerRecPtr^.SponsorLogo_Name;
      end;
    end
    else OutStr := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
  end
  //League
  else if (SymbolName = '$League') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      //TICKER: OutStr := UpperCase(TickerRecPtr^.Subleague_Mnemonic_Standard);
      TICKER: OutStr := UpperCase(TickerRecPtr^.League);
    end;
  end

  //User-defined text fields
  else if (SymbolName = '$Text_1') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[1+TextFieldBias];
              //end;
      BREAKINGNEWS: OutStr := BreakingNewsRecPtr^.UserData[1];
    end;
  end
  else if (SymbolName = '$Text_2') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[2+TextFieldBias];
      BREAKINGNEWS: OutStr := BREAKINGNEWSRecPtr^.UserData[2];
    end;
  end
  else if (SymbolName = '$Text_3') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[3+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_4') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[4+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_5') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[5+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_6') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[6+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_7') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[7+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_8') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[8+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_9') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[9+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_10') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[10];
    end;
  end
  else if (SymbolName = '$Text_11') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[11];
    end;
  end
  else if (SymbolName = '$Text_12') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[12];
    end;
  end
  else if (SymbolName = '$Text_13') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[13];
    end;
  end
  else if (SymbolName = '$Text_14') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[14];
    end;
  end
  else if (SymbolName = '$Text_15') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[15+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_16') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[16+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_17') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[17+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_18') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[18+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_19') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[19+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_20') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[20+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_21') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[21+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_22') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[22+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_23') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[23+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_24') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[24+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_25') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[25+TextFieldBias];
    end;
  end

  //Includes AM/PM + timezone suffix
  else if (SymbolName = '$Start_Time') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
    begin
      Days[1] := 'Sun';
      Days[2] := 'Mon';
      Days[3] := 'Tues';
      Days[4] := 'Wed';
      Days[5] := 'Thur';
      Days[6] := 'Fri';
      Days[7] := 'Sat';
      //Option to show day of week AND start time
      if (ShowTimeAndDay) then
      begin
        OutStr := '[r -20]' + Days[DayOfWeek(CurrentGameData.Date)];
        //Check for doubleheader in baseball, game #2
        if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
          OutStr := OutStr + '[r 40]' + 'GM 2'
        //Start time offset added for Version 1.0.6  03/31/08
        else OutStr := OutStr + '[r 40]' + TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset) + ' ' + TimeZoneSuffix;
      end
      else begin
        OutStr := Days[DayOfWeek(CurrentGameData.Date)];
      end;
    end
    else begin
      //Check for doubleheader in baseball, game #2
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := 'GM 2'
      //Start time offset added for Version 1.0.6  03/31/08
      else OutStr := TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset) + ' ' + TimeZoneSuffix;
    end;
    OutStr := Trim(OutStr);
  end

  //Remove timezone only (includes AM/PM)
  else if (SymbolName = '$Start_Time_No_Timezone') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
    begin
      Days[1] := 'Sun';
      Days[2] := 'Mon';
      Days[3] := 'Tues';
      Days[4] := 'Wed';
      Days[5] := 'Thur';
      Days[6] := 'Fri';
      Days[7] := 'Sat';
      //Option to show day of week AND start time
      if (ShowTimeAndDay) then
      begin
        OutStr := '[r -20]' + Days[DayOfWeek(CurrentGameData.Date)];
        //Check for doubleheader in baseball, game #2
        if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
          OutStr := OutStr + '[r 40]' + 'GM 2'
        //Start time offset added for Version 1.0.6  03/31/08
        else OutStr := OutStr + '[r 40]' + TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset);
      end
      else begin
        OutStr := Days[DayOfWeek(CurrentGameData.Date)];
      end;
    end
    else begin
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := 'GM 2'
      //Start time offset added for Version 1.0.6  03/31/08
      else OutStr := TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset);
    end;
    OutStr := Trim(OutStr);
  end

  //Remove AM/PM suffix
  else if (SymbolName = '$Start_Time_No_Suffix') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
    begin
      Days[1] := 'Sun';
      Days[2] := 'Mon';
      Days[3] := 'Tues';
      Days[4] := 'Wed';
      Days[5] := 'Thur';
      Days[6] := 'Fri';
      Days[7] := 'Sat';
      //Option to show day of week AND start time
      if (ShowTimeAndDay) then
      begin
        OutStr := '[r -20]' + Days[DayOfWeek(CurrentGameData.Date)];
        //Check for doubleheader in baseball, game #2
        if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
          OutStr := OutStr + '[r 40]' + 'GM 2'
        //Start time offset added for Version 1.0.6  03/31/08
        else begin
          OutStr := OutStr + '[r 40]' + TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset) + ' ' + TimeZoneSuffix;;
          OutStr := StringReplace(OutStr, 'AM', '', [rfReplaceAll]);
          OutStr := StringReplace(OutStr, 'PM', '', [rfReplaceAll]);
        end;
      end
      else begin
        OutStr := Days[DayOfWeek(CurrentGameData.Date)];
      end;
    end
    else begin
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := 'GM 2'
      else begin
        OutStr := TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset) + ' ' + TimeZoneSuffix;;
        OutStr := StringReplace(OutStr, 'AM', '', [rfReplaceAll]);
        OutStr := StringReplace(OutStr, 'PM', '', [rfReplaceAll]);
      end;
    end;
    OutStr := Trim(OutStr);
  end

  //No AM/PM or timezone suffixes
  else if (SymbolName = '$Start_Time_No_Suffix_No_Timezone') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
    begin
      Days[1] := 'Sun';
      Days[2] := 'Mon';
      Days[3] := 'Tues';
      Days[4] := 'Wed';
      Days[5] := 'Thur';
      Days[6] := 'Fri';
      Days[7] := 'Sat';
      //Option to show day of week AND start time
      if (ShowTimeAndDay) then
      begin
        OutStr := '[r -20]' + Days[DayOfWeek(CurrentGameData.Date)];
        //Check for doubleheader in baseball, game #2
        if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
          OutStr := OutStr + '[r 40]' + 'GM 2'
        //Start time offset added for Version 1.0.6  03/31/08
        else begin
          OutStr := OutStr + '[r 40]' + TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset);
          OutStr := StringReplace(OutStr, 'AM', '', [rfReplaceAll]);
          OutStr := StringReplace(OutStr, 'PM', '', [rfReplaceAll]);
        end;
      end
      else begin
        OutStr := Days[DayOfWeek(CurrentGameData.Date)];
      end;
    end
    else begin
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := 'GM 2'
      else begin
        OutStr := TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset);
        OutStr := StringReplace(OutStr, 'AM', '', [rfReplaceAll]);
        OutStr := StringReplace(OutStr, 'PM', '', [rfReplaceAll]);
      end;
    end;
    OutStr := Trim(OutStr);
  end

  //Timezone suffix only
  else if (SymbolName = '$Timezone') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
    begin
      OutStr := '';
    end
    else begin
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := ''
      else OutStr := TimeZoneSuffix;
    end;
    OutStr := Trim(OutStr);
  end

  //AM/PM suffix only
  else if (SymbolName = '$AMPM_StartTime_Suffix') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
      OutStr := ''
    else begin
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := ''
      else begin
        if (Pos('AM', TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset)) > 0) then OutStr := 'AM'
        else if (Pos('PM', TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset)) > 0) then OutStr := 'PM'
        else OutStr := '';
      end;
    end;
    OutStr := Trim(OutStr);
  end

  //Team ranks - will only be applicable for NCAAB & NCAAF
  else if (SymbolName = '$Visitor_Rank') then OutStr := CurrentGameData.VRank
  else if (SymbolName = '$Home_Rank') then OutStr := CurrentGameData.HRank

  //Standard team name
  else if (SymbolName = '$Visitor_Name') then
  begin
    OutStr := UpperCase(CurrentGameData.VName);
    //Set to gold color if game is final
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + OutStr
    //If NFL or NCAAF append possession indicator
    else if ((CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'NCAAF')) AND
      (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) then
    begin
      if (GetFootballGameSituationData(CurrentGameData).VisitorHasPossession = TRUE) then
        OutStr := OutStr + FOOTBALLPOSSESSION;
    end;
  end

  //Visiting team name with leading indicator
  else if (SymbolName = '$Visitor_Name_Leader_Indicator') then
  begin
    OutStr := UpperCase(CurrentGameData.VName);
    //Set to gold color if game is final; if in progress and team leading, add indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := LEADERINDICATOR + OutStr
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + OutStr;
  end

  //Visiting team leading indicator
  else if (SymbolName = '$Visitor_Leader_Indicator') then
  begin
    //If in progress and team leading, set indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := LEADERINDICATOR
    else OutStr := '';
  end

  else if (SymbolName = '$Home_Name') then
  begin
    OutStr := UpperCase(CurrentGameData.HName);
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + OutStr
    //If NFL or NCAAF append possession indicator
    else if ((CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'NCAAF')) AND
      (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0)then
    begin
      if (GetFootballGameSituationData(CurrentGameData).HomeHasPossession = TRUE) then
        OutStr := OutStr + FOOTBALLPOSSESSION;
    end;
  end

  //Home team name with leading indicator
  else if (SymbolName = '$Home_Name_Leader_Indicator') then
  begin
    OutStr := UpperCase(CurrentGameData.HName);
    //Set to gold color if game is final; if in progress and team leading, add indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := LEADERINDICATOR + OutStr
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + OutStr;
  end

  //Home team leading indicator
  else if (SymbolName = '$Home_Leader_Indicator') then
  begin
    //If in progress and team leading, set indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := LEADERINDICATOR
    else OutStr := '';
  end

  //Visitor mnemonic
  else if (SymbolName = '$Visitor_Mnemonic') then
  begin
    OutStr := UpperCase(CurrentGameData.VMnemonic);
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + OutStr
    //If NFL or NCAAF append possession indicator
    else if ((CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'NCAAF')) AND
      (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) then
    begin
      if (GetFootballGameSituationData(CurrentGameData).VisitorHasPossession = TRUE) then
        OutStr := OutStr + FOOTBALLPOSSESSION;
    end;
  end

  //Visitor mnemonic with leader indicator
  else if (SymbolName = '$Visitor_Mnemonic_Leader_Indicator') then
  begin
    OutStr := UpperCase(CurrentGameData.VMnemonic);
    //Set to gold color if game is final; if in progress and team leading, add indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := LEADERINDICATOR + OutStr
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + OutStr;
  end

  //Home mnemonic
  else if (SymbolName = '$Home_Mnemonic') then
  begin
    OutStr := UpperCase(CurrentGameData.HMnemonic);
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + OutStr
    //If NFL or NCAAF append possession indicator
    else if ((CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'NCAAF')) AND
      (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) then
    begin
      if (GetFootballGameSituationData(CurrentGameData).HomeHasPossession = TRUE) then
        OutStr := OutStr + FOOTBALLPOSSESSION;
    end;
  end

  //Home mnemonic with leader indicator
  else if (SymbolName = '$Home_Mnemonic_Leader_Indicator') then
  begin
    OutStr := UpperCase(CurrentGameData.HMnemonic);
    //Set to gold color if game is final; if in progress and team leading, add indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := LEADERINDICATOR + OutStr
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + OutStr;
  end

  else if (SymbolName = '$Matchup_TeamNames') then
  begin
    OutStr := UpperCase(CurrentGameData.VName) + '  at  '+ UpperCase(CurrentGameData.HName);
  end
  else if (SymbolName = '$Matchup_TeamNames_Ranked') then
  begin
    OutStr := '[f 3][c 20]' + Trim(CurrentGameData.VRank) + '[f 2][c 1] ' + UpperCase(CurrentGameData.VName) + '  at  '+
      '[f 3][c 20]' + Trim(CurrentGameData.HRank) + '[f 2][c 1] ' + UpperCase(CurrentGameData.HName);
  end
  else if (SymbolName = '$Matchup_TeamMnemonics') then
  begin
    OutStr := UpperCase(CurrentGameData.VMnemonic) + '  at  '+ UpperCase(CurrentGameData.HMnemonic);
  end
  else if (SymbolName = '$Matchup_TeamMnemonics_Ranked') then
  begin
    OutStr := '[f 3][c 20]' + Trim(CurrentGameData.VRank) + '[f 2][c 1] ' + UpperCase(CurrentGameData.VMnemonic) + '  at  '+
      '[f 3][c 20]' + Trim(CurrentGameData.HRank) + '[f 2][c 1] ' + UpperCase(CurrentGameData.HMnemonic);
  end

  //Scores
  else if (SymbolName = '$Visitor_Score') then
  begin
    OutStr := IntToStr(CurrentGameData.VScore);
  end
  else if (SymbolName = '$Visitor_Score_Highlighted_Final') then
  begin
    OutStr := IntToStr(CurrentGameData.VScore);
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + OutStr;
  end
  else if (SymbolName = '$Home_Score') then
  begin
    OutStr := IntToStr(CurrentGameData.HScore);
  end
  else if (SymbolName = '$Home_Score_Highlighted_Final') then
  begin
    OutStr := IntToStr(CurrentGameData.HScore);
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + OutStr;
  end

  //Baseball linescore
  else if (SymbolName = '$MLB_Visitor_Hits') then
    OutStr := IntToStr(CurrentGameData.Visitor_Param1)
  else if (SymbolName = '$MLB_Visitor_Errors') then
    OutStr := IntToStr(CurrentGameData.Visitor_Param2)
  else if (SymbolName = '$MLB_Home_Hits') then
    OutStr := IntToStr(CurrentGameData.Home_Param1)
  else if (SymbolName = '$MLB_Home_Errors') then
    OutStr := IntToStr(CurrentGameData.Home_Param2)

  //Baseball baserunner situation
  else if (SymbolName = '$MLB_Baserunner_Situation') then
  begin
    if (Trim(CurrentGameData.Situation) = 'FFF') then OutStr := #240
    else if (Trim(CurrentGameData.Situation) = 'TFF') then OutStr := #241
    else if (Trim(CurrentGameData.Situation) = 'FTF') then OutStr := #242
    else if (Trim(CurrentGameData.Situation) = 'TTF') then OutStr := #243
    else if (Trim(CurrentGameData.Situation) = 'FFT') then OutStr := #244
    else if (Trim(CurrentGameData.Situation) = 'TFT') then OutStr := #245
    else if (Trim(CurrentGameData.Situation) = 'FTT') then OutStr := #246
    else if (Trim(CurrentGameData.Situation) = 'TTT') then OutStr := #247;
  end

  //MLB Team and # outs
  else if (SymbolName = '$MLB_Outs') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    if (Length(CurrentGameData.Count) >= 3) then
    begin
      if (CurrentGameData.Count[3] = '0') then TempStr := '0 outs'
      else if (CurrentGameData.Count[3] = '1') then TempStr := '1 out'
      else if (CurrentGameData.Count[3] = '2') then TempStr := '2 outs'
      else if (CurrentGameData.Count[3] = '3') then TempStr := '3 outs';
      //Visitor at bat
      if ((GamePhaseData.SI_Phase_Code MOD 2) = 0) then
        OutStr :=  '[f 2][c 19]' + UpperCase(CurrentGameData.VMnemonic) + ': ' +
        '[f 2][c 1]' + TempStr
      //Home team at bat
      else
        OutStr :=  '[f 2][c 19]' + UpperCase(CurrentGameData.HMnemonic) + ': ' +
        '[f 2][c 1]' + TempStr;
    end
  end

  //MLB batter & baserunners
  else if (SymbolName = '$MLB_Batter') then
  begin
    if (Trim(CurrentGameData.Batter) <> '') then
      OutStr := GetLastName(Trim(CurrentGameData.Batter)) + ' at bat'
    else OutStr := '';
  end
  else if (SymbolName = '$MLB_Batter_Abbreviated') then
  begin
    if (Trim(CurrentGameData.Batter) <> '') then
      OutStr := 'AB: ' + GetLastName(Trim(CurrentGameData.Batter))
    else OutStr := '';
  end
  else if (SymbolName = '$MLB_Baserunner1') then
  begin
    if (Trim(CurrentGameData.Baserunners[1]) <> '') then
      OutStr := Trim(CurrentGameData.Baserunners[1]) + ' on 1st'
    else OutStr := '';
  end
  else if (SymbolName = '$MLB_Baserunner2') then
  begin
    if (Trim(CurrentGameData.Baserunners[2]) <> '') then
      OutStr := Trim(CurrentGameData.Baserunners[2]) + ' on 2nd'
    else OutStr := '';
  end
  else if (SymbolName = '$MLB_Baserunner3') then
  begin
    if (Trim(CurrentGameData.Baserunners[3]) <> '') then
      OutStr := Trim(CurrentGameData.Baserunners[3]) + ' on 3rd'
    else OutStr := '';
  end

  //Game clock
  else if (SymbolName = '$Game_Clock') then
  begin
    if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) then
      OutStr := ''
    //Special case for baseball
    else begin
      TempStr := CurrentGameData.League;
      if (TempStr = 'MLB') OR (TempStr = 'ML') OR (TempStr = 'NL') OR (TempStr = 'AL') OR (TempStr = 'GPFT') OR
         (TempStr = 'CAC') then
        OutStr := ' '
      else
        OutStr := FormatGameClock(CurrentGameData.TimeMin, CurrentGameData.TimeSec)
    end;
  end

  //Game phase
  else if (SymbolName = '$Game_Phase') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = PREGAME) then OutStr := ' '
    //In-Progress
    else if (CurrentGameData.State = INPROGRESS) then
    begin
      if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = FALSE) then
      begin
        TempStr := UpperCase(CurrentGameData.Subleague);
        if (TempStr = 'MLB') OR (TempStr = 'ML') OR (TempStr = 'AL') OR (TempStr = 'NL') then
          OutStr := UpperCase(GamePhaseData.Display_Period)
        else
          OutStr := 'END ' + UpperCase(GamePhaseData.Display_Period)
      end
      else if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = TRUE) then
        OutStr := UpperCase(GamePhaseData.Display_Half)
      else
        OutStr := UpperCase(GamePhaseData.Display_Period);
    end
    //Final
    else if (CurrentGameData.State = FINAL) then
    begin
      OutStr := UpperCase(GamePhaseData.Display_Final);
    end
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = POSTPONED) then
    begin
      OutStr := 'PPD';
    end
    //Delayed
    else if (CurrentGameData.State = DELAYED) then
    begin
      OutStr := 'DLY';
    end
    //Suspended
    else if (CurrentGameData.State = SUSPENDED) then
    begin
      OutStr := 'SUSP';
    end;
  end

  //Game phase with final highlighted in gold
  else if (SymbolName = '$Game_Phase_Highlighted_Final') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = PREGAME) then OutStr := ' '
    //In-Progress
    else if (CurrentGameData.State = INPROGRESS) then
    begin
      if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = FALSE) then
      begin
        TempStr := UpperCase(CurrentGameData.Subleague);
        if (TempStr = 'MLB') OR (TempStr = 'ML') OR (TempStr = 'AL') OR (TempStr = 'NL') then
          OutStr := UpperCase(GamePhaseData.Display_Period)
        else
          OutStr := 'END ' + UpperCase(GamePhaseData.Display_Period)
      end
      else if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = TRUE) then
        OutStr := UpperCase(GamePhaseData.Display_Half)
      else
        OutStr := UpperCase(GamePhaseData.Display_Period);
    end
    //Final
    else if (CurrentGameData.State = FINAL) then
    begin
      OutStr := '[f 2][c 19]' + UpperCase(GamePhaseData.Display_Final);
    end
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = POSTPONED) then
    begin
      OutStr := 'PPD';
    end
    //Delayed
    else if (CurrentGameData.State = DELAYED) then
    begin
      OutStr := 'DLY';
    end
    //Suspended
    else if (CurrentGameData.State = SUSPENDED) then
    begin
      OutStr := 'SUSP';
    end;
  end

  //Game clock + phase
  else if (SymbolName = '$Game_Clock_Phase') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = PREGAME) then
    begin
      OutStr := TimeToStr(CurrentGameData.StartTime) + ' ' + TimeZoneSuffix;
    end
    //In-Progress
    else if (CurrentGameData.State = INPROGRESS) AND (CurrentEntryIndex <> NOENTRYINDEX) then
    begin
      if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = FALSE) then
      begin
        TempStr := UpperCase(CurrentGameData.Subleague);
        if (TempStr = 'MLB') OR (TempStr = 'ML') OR (TempStr = 'AL') OR (TempStr = 'NL') then
          OutStr := UpperCase(GamePhaseData.Display_Period)
        else
          OutStr := 'END ' + UpperCase(GamePhaseData.Display_Period);
      end
      else if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = TRUE) then
        OutStr := UpperCase(GamePhaseData.Display_Half)
      else
        OutStr := FormatGameClock(CurrentGameData.TimeMin, CurrentGameData.TimeSec) + '  ' +
          UpperCase(GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase).Display_Period);
    end
    //Final
    else if (CurrentGameData.State = FINAL) then
    begin
      OutStr := UpperCase(GamePhaseData.Display_Final);
      //Check for doubleheader in baseball and append suffix if applicable
      if (CurrentGameData.DoubleHeader = TRUE) then
      begin
        //End of game, so display game number in note field
        if (CurrentGameData.DoubleHeaderGameNumber = 1) then
          OutStr := OutStr + ' - GM 1'
        else if (CurrentGameData.DoubleHeaderGameNumber = 2) then
          OutStr := OutStr + ' - GM 2'
      end;
    end
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = POSTPONED) then
    begin
      OutStr := 'PPD';
    end
    //Delayed
    else if (CurrentGameData.State = DELAYED) then
    begin
      OutStr := 'DLY';
    end
    //Suspended
    else if (CurrentGameData.State = SUSPENDED) then
    begin
      OutStr := 'SUSP';
    end
    else OutStr := '';
  end

  //Game inning (baseball only)
  else if (SymbolName = '$Game_Inning') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Remove top/bottom inning indicator characters
    GamePhaseData.Display_Period := StringReplace(GamePhaseData.Display_Period, '�', '', [rfReplaceAll]);
    GamePhaseData.Display_Period := StringReplace(GamePhaseData.Display_Period, '�', '', [rfReplaceAll]);

    //In-Game
    if (CurrentGameData.State = INPROGRESS) then
      OutStr := GamePhaseData.Display_Period
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = POSTPONED) then
    begin
      OutStr := 'PPD';
    end
    //Delayed
    else if (CurrentGameData.State = DELAYED) then
    begin
      OutStr := 'DLY';
    end
    //Suspended
    else if (CurrentGameData.State = SUSPENDED) then
    begin
      OutStr := 'SPD';
    end
    else
      OutStr := ' ';
  end

  //Top/bottom of inning indicator
  else if (SymbolName = '$Inning_Indicator') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //In-game only; check for upper-ASCII character as first in string
    if (CurrentGameData.State = 2) AND
       (Length(GamePhaseData.Display_Period) > 0) AND
       (Ord(GamePhaseData.Display_Period[1]) > 128) then
      OutStr := GamePhaseData.Display_Period[1]
    else OutStr := '';
  end

  //Football field position
  else if (SymbolName = '$Football_Field_Position') then
  begin
    FootballSituation := GetFootballGameSituationData(CurrentGameData);
    if (FootballSituation.VisitorHasPossession = TRUE) then
    begin
      //Normalize - template goes from 0 to 100
      OutStr := IntToStr(100-FootballSituation.YardsFromGoal);
    end
    else if (GetFootballGameSituationData(CurrentGameData).HomeHasPossession = TRUE) then
    begin
      //Normalize - template goes from 0 to 100; add "-" for home team
      OutStr := '-' + IntToStr(100-FootballSituation.YardsFromGoal);
    end
  end

  //Football down & distance
  else if (SymbolName = '$Football_Down_Distance') then
  begin
    FootballSituation := GetFootballGameSituationData(CurrentGameData);
    if ((FootballSituation.Down > 0) AND (FootballSituation.Distance > 0)) then
    begin
      if (FootballSituation.VisitorHasPossession = TRUE) then
      begin
        OutStr := '[c 19]' + CurrentGameData.VMnemonic + ': ';
        OutStr := OutStr + '[c 1]' + IntToDown(FootballSituation.Down) + ' & ' + IntToStr(FootballSituation.Distance);
        if (FootballSituation.YardsFromGoal  > 50) then
          OutStr :=  OutStr + ' on ' + IntToStr(100-FootballSituation.YardsFromGoal) + ' '
        else if (FootballSituation.YardsFromGoal  > 0) AND (FootballSituation.YardsFromGoal  <= 50) then
          OutStr :=  OutStr + ' on ' + IntToStr(FootballSituation.YardsFromGoal) + ' '
        else OutStr := ' ';
      end
      else if (FootballSituation.HomeHasPossession = TRUE) then
      begin
        OutStr := '[c 19]' + CurrentGameData.HMnemonic + ': ';
        OutStr := OutStr + '[c 1]' + IntToDown(FootballSituation.Down) + ' & ' + IntToStr(FootballSituation.Distance);
        if (FootballSituation.YardsFromGoal  > 50) then
          OutStr :=  OutStr + ' on ' + IntToStr(100-FootballSituation.YardsFromGoal) + ' '
        else if (FootballSituation.YardsFromGoal  > 0) AND (FootballSituation.YardsFromGoal  <= 50) then
          OutStr :=  OutStr + ' on ' + IntToStr(FootballSituation.YardsFromGoal) + ' '
        else OutStr := ' ';
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Weather related functions; store current league and home Stats ID in global variables to prevent
  //separate queries for each variable in Weather template
  else if (SymbolName = '$Weather_Stadium_Name') then
  begin
    if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
       (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := '[c 19]' + CurrentWeatherForecastRec.Weather_Stadium_Name;
  end
  else if (SymbolName = '$Weather_Current_Temperature') then
  begin
    if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
       (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := '[c 1]' + CurrentWeatherForecastRec.Weather_Current_Temperature + ' F';
  end
  else if (SymbolName = '$Weather_High_Temperature') then
  begin
    if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
       (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := '[c 1]' + CurrentWeatherForecastRec.Weather_High_Temperature + ' F';
  end
  else if (SymbolName = '$Weather_Low_Temperature') then
  begin
    if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
       (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := '[c 1]' + CurrentWeatherForecastRec.Weather_Low_Temperature + ' F';
  end
  else if (SymbolName = '$Weather_Current_Conditions') then
  begin
    if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
       (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := '[c 1]' + CurrentWeatherForecastRec.Weather_Current_Conditions;
  end
  else if (SymbolName = '$Weather_Current_Temp_Conditions') then
  begin
    if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
       (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := '[c 1]' + CurrentWeatherForecastRec.Weather_Current_Temperature + ' F  ' +
              CurrentWeatherForecastRec.Weather_Current_Conditions;
  end
  else if (SymbolName = '$Weather_Forecast_Icon') then
  begin
    if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
       (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      //Icon character codes are stored as strings in SQL DB 
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    //Get icon based on time of day
    if (CurrentGameData.StartTime > EncodeTime(20, 0, 0, 0)) then
      OutStr := Char(StrToInt(CurrentWeatherForecastRec.Weather_Forecast_Icon_Night))
    else
      OutStr := Char(StrToInt(CurrentWeatherForecastRec.Weather_Forecast_Icon_Day));
  end

  //Team records for supported leagues
  //These leagues - show only wins & losses
  else if (SymbolName = '$Team_Record_WL_Visitor_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + ': ' +
              '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) + ')';
  end
  else if (SymbolName = '$Team_Record_WL_Visitor_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName) + ': ' +
              '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) + ')';
  end
  else if (SymbolName = '$Team_Record_WL_Home_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + ': ' +
              '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) + ')';
  end
  else if (SymbolName = '$Team_Record_WL_Home_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName) + ': ' +
              '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) + ')';
  end
  //These leagues show wins, losses, ties
  else if (SymbolName = '$Team_Record_WLT_Visitor_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + ': ' +
              '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
              '-' + Trim(TeamRecord.Param[3]) + ')';
  end
  else if (SymbolName = '$Team_Record_WLT_Visitor_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName) + ': ' +
              '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
              '-' + Trim(TeamRecord.Param[3]) + ')';
  end
  else if (SymbolName = '$Team_Record_WLT_Home_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + ': ' +
              '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
              '-' + Trim(TeamRecord.Param[3]) + ')';
  end
  else if (SymbolName = '$Team_Record_WLT_Home_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName) + ': ' +
              '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
              '-' + Trim(TeamRecord.Param[3]) + ')';
  end
  //NHL - Show wins, losses, OT losses & total points
  else if (SymbolName = '$Team_Record_NHL_Visitor_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + ': ' +
              '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
              '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' pts' + ')';
  end
  else if (SymbolName = '$Team_Record_NHL_Home_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + ': ' +
              '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
              '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' pts' + ')';
  end
  else if (SymbolName = '$Team_Record_WL_Visitor_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName) + ': ' +
              '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) + ')';
  end
  else if (SymbolName = '$Team_Record_WL_Home_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName) + ': ' +
              '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) + ')';
  end
  //These leagues show wins, losses, ties
  else if (SymbolName = '$Team_Record_WLT_Visitor_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName) + ': ' +
              '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
              '-' + Trim(TeamRecord.Param[3]) + ')';
  end
  else if (SymbolName = '$Team_Record_WLT_Home_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName) + ': ' +
              '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
              '-' + Trim(TeamRecord.Param[3]) + ')';
  end
  //NHL - Show wins, losses, OT losses & total points
  else if (SymbolName = '$Team_Record_NHL_Visitor_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName) + ': ' +
              '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
              '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' pts' + ')';
  end
  else if (SymbolName = '$Team_Record_NHL_Home_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName) + ': ' +
              '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
              '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' pts' + ')';
  end

  //Team Streaks
  else if (SymbolName = '$Team_Streak_Visitor_Mnemonic') then
  begin
    if (Trim(CurrentGameData.VStreak) <> '') then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + ': ' +
                '[f 2][c 1]' + Trim(CurrentGameData.VStreak)
    else OutStr := ' ';
  end
  else if (SymbolName = '$Team_Streak_Home_Mnemonic') then
  begin
    if (Trim(CurrentGameData.HStreak) <> '') then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + ': ' +
                '[f 2][c 1]' + Trim(CurrentGameData.HStreak)
    else OutStr := ' ';
  end
  else if (SymbolName = '$Team_Streak_Visitor_Name') then
  begin
    if (Trim(CurrentGameData.VStreak) <> '') then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName) + ': ' +
                '[f 2][c 1]' + Trim(CurrentGameData.VStreak)
    else OutStr := ' ';
  end
  else if (SymbolName = '$Team_Streak_Home_Name') then
  begin
    if (Trim(CurrentGameData.HStreak) <> '') then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName) + ': ' +
                '[f 2][c 1]' + Trim(CurrentGameData.HStreak)
    else OutStr := ' ';
  end

  //MLB Pitcher Stat functions
  //Starting pitchers
  else if (SymbolName = '$Starting_Pitcher_Visitor') then
  begin
    if (CurrentStartingPitcherStatsRec.CurrentGameID <> CurrentGameData.GameID) then
    CurrentStartingPitcherStatsRec := GetStartingPitcherStats(CurrentGameData.GameID);
    if (Trim(CurrentStartingPitcherStatsRec.Visitor_Pitcher_Name_Last) <> '') then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + ': ' +
                '[f 2][c 1]' + Trim(CurrentStartingPitcherStatsRec.Visitor_Pitcher_Name_Last)
    else OutStr := ' ';
  end
  else if (SymbolName = '$Starting_Pitcher_Home') then
  begin
    if (CurrentStartingPitcherStatsRec.CurrentGameID <> CurrentGameData.GameID) then
    CurrentStartingPitcherStatsRec := GetStartingPitcherStats(CurrentGameData.GameID);
    if (Trim(CurrentStartingPitcherStatsRec.Home_Pitcher_Name_Last) <> '') then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + ': ' +
                '[f 2][c 1]' + Trim(CurrentStartingPitcherStatsRec.Home_Pitcher_Name_Last)
    else OutStr := ' ';
  end

  //Final pitcher record - Visitor
  else if (SymbolName = '$Final_Pitcher_Record_Visitor') then
  begin
    if (CurrentFinalPitcherStatsRec.CurrentGameID <> CurrentGameData.GameID) then
    CurrentFinalPitcherStatsRec := GetGameFinalPitcherStats(CurrentGameData.GameID);
    //Check for visitor is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) <> '') then
        OutStr := '[f 2][c 1]W - ' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) +
                  ' (' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Wins) + '-' +
                  Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Losses) + ')'
      else OutStr := ' ';
    end
    //Check for visitor is loser
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore < CurrentGameData.HScore) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) <> '') then
        OutStr := '[f 2][c 1]L - ' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) +
                  ' (' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Wins) + '-' +
                  Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Losses) + ')'
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Final pitcher record - Home
  else if (SymbolName = '$Final_Pitcher_Record_Home') then
  begin
    if (CurrentFinalPitcherStatsRec.CurrentGameID <> CurrentGameData.GameID) then
    CurrentFinalPitcherStatsRec := GetGameFinalPitcherStats(CurrentGameData.GameID);
    //Check for home is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) <> '') then
        OutStr := '[f 2][c 1]W - ' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) +
                  ' (' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Wins) + '-' +
                  Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Losses) + ')'
      else OutStr := ' ';
    end
    //Check for home is loser
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore < CurrentGameData.VScore) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) <> '') then
        OutStr := '[f 2][c 1]L - ' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) +
                  ' (' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Wins) + '-' +
                  Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Losses) + ')'
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Final pitcher record (saves) - Saving Pitcher
  else if (SymbolName = '$Final_Pitcher_Record_Save') then
  begin
    if (CurrentFinalPitcherStatsRec.CurrentGameID <> CurrentGameData.GameID) then
    CurrentFinalPitcherStatsRec := GetGameFinalPitcherStats(CurrentGameData.GameID);
    //Check for home is winner
    if (CurrentGameData.State = FINAL) AND (CurrentFinalPitcherStatsRec.NeedToAddSavingPitcher) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Saving_Pitcher_Name_Last) <> '') then
      begin
        OutStr := '[f 2][c 1]S - ' + Trim(CurrentFinalPitcherStatsRec.Saving_Pitcher_Name_Last) +
                  ' (' + Trim(CurrentFinalPitcherStatsRec.Saving_Pitcher_Saves) + ')';
        //If saving pitcher is the home team pitcher, move to second line using pipe delimiter
        if (CurrentGameData.HScore > CurrentGameData.VScore) AND ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := '|' + OutStr;
      end
      //Set text value as flag to skip record
      else begin
        OutStr := ' ';
        OKToDisplay := FALSE;
      end;
    end
    //Set text value as flag to skip record
    else begin
      OutStr := ' ';
      OKToDisplay := FALSE;
    end;
  end

  //Final pitcher stats - Visitor
  else if (SymbolName = '$Final_Pitcher_Stats_Visitor') then
  begin
    if (CurrentFinalPitcherStatsRec.CurrentGameID <> CurrentGameData.GameID) then
    CurrentFinalPitcherStatsRec := GetGameFinalPitcherStats(CurrentGameData.GameID);
    //Check for visitor is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) <> '') then
      begin
        OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + ': ' +
                  '[f 2][c 1]' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) +
                  '[f 2][c 1]' + ' ' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_IP) + ' IP';
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Hits) <> '0') then
          OutStr := OutStr + ', ' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Hits) + ' H';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then OutStr := OutStr + '|';
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Runs) <> '0') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Runs) + ' R';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_EarnedRuns) <> '0') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_EarnedRuns) + ' ER';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_K) <> '0') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_K) + ' K';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_BB) <> '0') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_BB) + ' BB';
        end;
      end
      else OutStr := ' ';
    end
    //Check for visitor is loser
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore < CurrentGameData.HScore) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) <> '') then
      begin
        OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + ': ' +
                  '[f 2][c 1]' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) +
                  '[f 2][c 1]' + ' ' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_IP) + ' IP';
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Hits) <> '0') then
          OutStr := OutStr + ', ' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Hits) + ' H';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then OutStr := OutStr + '|';
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Runs) <> '0') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Runs) + ' R';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_EarnedRuns) <> '0') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_EarnedRuns) + ' ER';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_K) <> '0') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_K) + ' K';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_BB) <> '0') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_BB) + ' BB';
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Final pitcher stats - Home
  else if (SymbolName = '$Final_Pitcher_Stats_Home') then
  begin
    if (CurrentFinalPitcherStatsRec.CurrentGameID <> CurrentGameData.GameID) then
    CurrentFinalPitcherStatsRec := GetGameFinalPitcherStats(CurrentGameData.GameID);
    //Check for visitor is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) <> '') then
      begin
        OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + ': ' +
                  '[f 2][c 1]' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) +
                  '[f 2][c 1]' + ' ' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_IP) + ' IP';
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Hits) <> '0') then
          OutStr := OutStr + ', ' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Hits) + ' H';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then OutStr := OutStr + '|';
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Runs) <> '0') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Runs) + ' R';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_EarnedRuns) <> '0') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_EarnedRuns) + ' ER';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_K) <> '0') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_K) + ' K';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_BB) <> '0') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_BB) + ' BB';
        end;
      end
      else OutStr := ' ';
    end
    //Check for visitor is loser
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore < CurrentGameData.VScore) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) <> '') then
      begin
        OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + ': ' +
                  '[f 2][c 1]' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) +
                  '[f 2][c 1]' + ' ' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_IP) + ' IP';
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Hits) <> '0') then
          OutStr := OutStr + ', ' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Hits) + ' H';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then OutStr := OutStr + '|';
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Runs) <> '0') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Runs) + ' R';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_EarnedRuns) <> '0') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_EarnedRuns) + ' ER';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_K) <> '0') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_K) + ' K';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_BB) <> '0') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_BB) + ' BB';
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Final batter stats - Visitor (can be 1st or 2nd in list)
  else if (SymbolName = '$Final_Batter_Stats1_Visitor') OR (SymbolName = '$Final_Batter_Stats2_Visitor') then
  begin
    if (SymbolName = '$Final_Batter_Stats1_Visitor') then
      FinalBatterStats := GetGameFinalBatterStats(CurrentGameData.GameID, CurrentGameData.VStatsIncID, 1)
    else if (SymbolName = '$Final_Batter_Stats2_Visitor') then
      FinalBatterStats := GetGameFinalBatterStats(CurrentGameData.GameID, CurrentGameData.VStatsIncID, 2);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(FinalBatterStats.Name_Last) <> '') then
      begin
        if (CurrentGameData.VScore > CurrentGameData.HScore) then
          OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + ': ' +
                    '[f 2][c 1]' + Trim(FinalBatterStats.Name_Last) +
                     //Add hits, at-bats
                    '[f 2][c 1]' + ' ' + Trim(FinalBatterStats.Hits) + '-' + Trim(FinalBatterStats.AtBats)
        else
          OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + ': ' +
                    '[f 2][c 1]' + Trim(FinalBatterStats.Name_Last) +
                     //Add hits, at-bats
                    '[f 2][c 1]' + ' ' + Trim(FinalBatterStats.Hits) + '-' + Trim(FinalBatterStats.AtBats);
        //Add doubles
        if (Trim(FinalBatterStats.Doubles) = '1') then
          OutStr := OutStr + ', ' + '2B'
        else if (Trim(FinalBatterStats.Doubles) > '1') then
          OutStr := OutStr + ', ' + Trim(FinalBatterStats.Doubles) + ' 2B';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then OutStr := OutStr + '|';
        //Add triples
        if (Trim(FinalBatterStats.Triples) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + '3B';
        end
        else if (Trim(FinalBatterStats.Triples) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.Triples) + ' 3B';
        end;
        //Add home-runs
        if (Trim(FinalBatterStats.HomeRuns) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + 'HR';
        end
        else if (Trim(FinalBatterStats.HomeRuns) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.HomeRuns) + ' HR';
        end;
        //Add RBI
        if (Trim(FinalBatterStats.RBI) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + 'RBI';
        end
        else if (Trim(FinalBatterStats.RBI) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.RBI) + ' RBI';
        end;
        //Add stolen bases
        if (Trim(FinalBatterStats.StolenBases) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + 'SB';
        end
        else if (Trim(FinalBatterStats.StolenBases) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.StolenBases) + ' SB';
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Final batter stats - Home (can be 1st or 2nd in list)
  else if (SymbolName = '$Final_Batter_Stats1_Home') OR (SymbolName = '$Final_Batter_Stats2_Home') then
  begin
    if (SymbolName = '$Final_Batter_Stats1_Home') then
      FinalBatterStats := GetGameFinalBatterStats(CurrentGameData.GameID, CurrentGameData.HStatsIncID, 1)
    else if (SymbolName = '$Final_Batter_Stats2_Home') then
      FinalBatterStats := GetGameFinalBatterStats(CurrentGameData.GameID, CurrentGameData.HStatsIncID, 2);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(FinalBatterStats.Name_Last) <> '') then
      begin
        if (CurrentGameData.HScore > CurrentGameData.VScore) then
          OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + ': ' +
                    '[f 2][c 1]' + Trim(FinalBatterStats.Name_Last) +
                     //Add hits, at-bats
                    '[f 2][c 1]' + ' ' + Trim(FinalBatterStats.Hits) + '-' + Trim(FinalBatterStats.AtBats)
        else
          OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + ': ' +
                    '[f 2][c 1]' + Trim(FinalBatterStats.Name_Last) +
                     //Add hits, at-bats
                    '[f 2][c 1]' + ' ' + Trim(FinalBatterStats.Hits) + '-' + Trim(FinalBatterStats.AtBats);
        //Add doubles
        if (Trim(FinalBatterStats.Doubles) = '1') then
          OutStr := OutStr + ', ' + '2B'
        else if (Trim(FinalBatterStats.Doubles) > '1') then
          OutStr := OutStr + ', ' + Trim(FinalBatterStats.Doubles) + ' 2B';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then OutStr := OutStr + '|';
        //Add triples
        if (Trim(FinalBatterStats.Triples) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + '3B';
        end
        else if (Trim(FinalBatterStats.Triples) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.Triples) + ' 3B';
        end;
        //Add home-runs
        if (Trim(FinalBatterStats.HomeRuns) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + 'HR';
        end
        else if (Trim(FinalBatterStats.HomeRuns) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.HomeRuns) + ' HR';
        end;
        //Add RBI
        if (Trim(FinalBatterStats.RBI) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + 'RBI';
        end
        else if (Trim(FinalBatterStats.RBI) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.RBI) + ' RBI';
        end;
        //Add stolen bases
        if (Trim(FinalBatterStats.StolenBases) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + 'SB';
        end
        else if (Trim(FinalBatterStats.StolenBases) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.StolenBases) + ' SB';
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Basketball stats - Visitor (can be 1st or 2nd in list)
  else if (SymbolName = '$Final_BBall_Stats1_Visitor_Mnemonic') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Mnemonic') OR
          (SymbolName = '$Final_BBall_Stats1_Visitor_Name') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Name') then
  begin
    if (SymbolName = '$Final_BBall_Stats1_Visitor_Mnemonic') OR (SymbolName = '$Final_BBall_Stats1_Visitor_Name') then
      FinalBBallStats := GetGameFinalBBallStats(CurrentGameData.League, CurrentGameData.GameID, CurrentGameData.VStatsIncID, 1)
    else if (SymbolName = '$Final_BBall_Stats2_Visitor_Mnemonic') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Name') then
      FinalBBallStats := GetGameFinalBBallStats(CurrentGameData.League, CurrentGameData.GameID, CurrentGameData.VStatsIncID, 2);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(FinalBBallStats.Name_Last) <> '') then
      begin
        if (CurrentGameData.VScore > CurrentGameData.HScore) then
        begin
          if (SymbolName = '$Final_BBall_Stats1_Visitor_Mnemonic') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Mnemonic') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + ': '
          else if (SymbolName = '$Final_BBall_Stats1_Visitor_Name') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Name') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName) + ': ';
        end
        else begin
          if (SymbolName = '$Final_BBall_Stats1_Visitor_Mnemonic') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Mnemonic') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + ': '
          else if (SymbolName = '$Final_BBall_Stats1_Visitor_Name') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Name') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VName) + ': ';
        end;
        OutStr := OutStr + '[f 2][c 1]' + Trim(FinalBBallStats.Name_Last);
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := OutStr + '|'
        else
          OutStr := OutStr + ' ';
        //Add points & rebounds
        OutStr := OutStr + '[f 2][c 1]' + Trim(FinalBBallStats.Points) + ' PTS';
        if (Trim(FinalBBallStats.Rebounds) <> '') AND (FinalBBallStats.Rebounds <> '0') then
          OutStr := OutStr + ', ' + Trim(FinalBBallStats.Rebounds) + ' REB';
        //Add assists
        if (Trim(FinalBBallStats.Assists) <> '') AND (FinalBBallStats.Assists <> '0') then
          OutStr := OutStr + ', ' + Trim(FinalBBallStats.Assists) + ' AST';
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Basketball stats - Home (can be 1st or 2nd in list)
  else if (SymbolName = '$Final_BBall_Stats1_Home_Mnemonic') OR (SymbolName = '$Final_BBall_Stats2_Home_Mnemonic') OR
          (SymbolName = '$Final_BBall_Stats1_Home_Name') OR (SymbolName = '$Final_BBall_Stats2_Home_Name') then
  begin
    if (SymbolName = '$Final_BBall_Stats1_Home_Mnemonic') OR (SymbolName = '$Final_BBall_Stats1_Home_Name') then
      FinalBBallStats := GetGameFinalBBallStats(CurrentGameData.League, CurrentGameData.GameID, CurrentGameData.HStatsIncID, 1)
    else if (SymbolName = '$Final_BBall_Stats2_Home_Mnemonic') OR (SymbolName = '$Final_BBall_Stats2_Home_Name') then
      FinalBBallStats := GetGameFinalBBallStats(CurrentGameData.League, CurrentGameData.GameID, CurrentGameData.HStatsIncID, 2);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(FinalBBallStats.Name_Last) <> '') then
      begin
        if (CurrentGameData.HScore > CurrentGameData.VScore) then
        begin
          if (SymbolName = '$Final_BBall_Stats1_Home_Mnemonic') OR (SymbolName = '$Final_BBall_Stats2_Home_Mnemonic') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + ': '
          else if (SymbolName = '$Final_BBall_Stats1_Home_Name') OR (SymbolName = '$Final_BBall_Stats2_Home_Name') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName) + ': ';
        end
        else begin
          if (SymbolName = '$Final_BBall_Stats1_Home_Mnemonic') OR (SymbolName = '$Final_BBall_Stats2_Home_Mnemonic') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + ': '
          else if (SymbolName = '$Final_BBall_Stats1_Home_Name') OR (SymbolName = '$Final_BBall_Stats2_Home_Name') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HName) + ': ';
        end;
        OutStr := OutStr + '[f 2][c 1]' + Trim(FinalBBallStats.Name_Last);
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := OutStr + '|'
        else
          OutStr := OutStr + ' ';
        //Add points & rebounds
        OutStr := OutStr + '[f 2][c 1]' + Trim(FinalBBallStats.Points) + ' PTS';
        if (Trim(FinalBBallStats.Rebounds) <> '') AND (FinalBBallStats.Rebounds <> '0') then
          OutStr := OutStr + ', ' + Trim(FinalBBallStats.Rebounds) + ' REB';
        //Add assists
        if (Trim(FinalBBallStats.Assists) <> '') AND (FinalBBallStats.Assists <> '0') then
          OutStr := OutStr + ', ' + Trim(FinalBBallStats.Assists) + ' AST';
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Skater stats - Visitor (can be 1st or 2nd in list)
  else if (SymbolName = '$Final_Skater_Stats1_Visitor') OR (SymbolName = '$Final_Skater_Stats2_Visitor') then
  begin
    if (SymbolName = '$Final_Skater_Stats1_Visitor') then
      FinalSkaterStats := GetGameFinalSkaterStats(CurrentGameData.GameID, CurrentGameData.VStatsIncID, 1)
    else if (SymbolName = '$Final_Skater_Stats2_Visitor') then
      FinalSkaterStats := GetGameFinalSkaterStats(CurrentGameData.GameID, CurrentGameData.VStatsIncID, 2);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(FinalSkaterStats.Name_Last) <> '') then
      begin
        if (CurrentGameData.VScore > CurrentGameData.HScore) then
        begin
          OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + ': ' +
                    '[f 2][c 1]' + Trim(FinalSkaterStats.Name_Last) +
                     //Add points & assists
                    '[f 2][c 1]' + ' ' + Trim(FinalSkaterStats.Goals) + ' G';
                    if (Trim(FinalSkaterStats.Assists) <> '') AND (FinalSkaterStats.Assists <> '0') then
                      OutStr := OutStr + ', ' + Trim(FinalSkaterStats.Assists) + ' AST';
        end
        else begin
          OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + ': ' +
                    '[f 2][c 1]' + Trim(FinalSkaterStats.Name_Last) +
                     //Add points & assists
                    '[f 2][c 1]' + ' ' + Trim(FinalSkaterStats.Goals) + ' G';
                    if (Trim(FinalSkaterStats.Assists) <> '') AND (FinalSkaterStats.Assists <> '0') then
                      OutStr := OutStr + ', ' + Trim(FinalSkaterStats.Assists) + ' AST';
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Skater stats - Home (can be 1st or 2nd in list)
  else if (SymbolName = '$Final_Skater_Stats1_Home') OR (SymbolName = '$Final_Skater_Stats2_Home') then
  begin
    if (SymbolName = '$Final_Skater_Stats1_Home') then
      FinalSkaterStats := GetGameFinalSkaterStats(CurrentGameData.GameID, CurrentGameData.HStatsIncID, 1)
    else if (SymbolName = '$Final_Skater_Stats2_Home') then
      FinalSkaterStats := GetGameFinalSkaterStats(CurrentGameData.GameID, CurrentGameData.HStatsIncID, 2);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(FinalSkaterStats.Name_Last) <> '') then
      begin
        if (CurrentGameData.HScore > CurrentGameData.VScore) then
        begin
          OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + ': ' +
                    '[f 2][c 1]' + Trim(FinalSkaterStats.Name_Last) +
                     //Add points & assists
                    '[f 2][c 1]' + ' ' + Trim(FinalSkaterStats.Goals) + ' G';
                    if (Trim(FinalSkaterStats.Assists) <> '') AND (FinalSkaterStats.Assists <> '0') then
                      OutStr := OutStr + ', ' + Trim(FinalSkaterStats.Assists) + ' AST';
        end
        else begin
          OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + ': ' +
                    '[f 2][c 1]' + Trim(FinalSkaterStats.Name_Last) +
                     //Add points & assists
                    '[f 2][c 1]' + ' ' + Trim(FinalSkaterStats.Goals) + ' G';
                    if (Trim(FinalSkaterStats.Assists) <> '') AND (FinalSkaterStats.Assists <> '0') then
                      OutStr := OutStr + ', ' + Trim(FinalSkaterStats.Assists) + ' AST';
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Goalie stats - Visitor (can be 1st or 2nd in list)
  else if (SymbolName = '$Final_Goalie_Stats1_Visitor') OR (SymbolName = '$Final_Goalie_Stats2_Visitor') then
  begin
    if (SymbolName = '$Final_Goalie_Stats1_Visitor') then
      FinalGoalieStats := GetGameFinalGoalieStats(CurrentGameData.GameID, CurrentGameData.VStatsIncID, 1)
    else if (SymbolName = '$Final_Goalie_Stats2_Visitor') then
      FinalGoalieStats := GetGameFinalGoalieStats(CurrentGameData.GameID, CurrentGameData.VStatsIncID, 2);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(FinalGoalieStats.Name_Last) <> '') then
      begin
        if (CurrentGameData.VScore > CurrentGameData.HScore) then
        begin
          OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + ': ' +
                    '[f 2][c 1]' + Trim(FinalGoalieStats.Name_Last) +
                     //Add shots & saves
                    '[f 2][c 1]' + ' ' + Trim(FinalGoalieStats.Saves) + ' SAVES';
                     //OutStr := OutStr + Trim(FinalGoalieStats.Shots) + ' SHOTS';
        end
        else begin
          OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + ': ' +
                    '[f 2][c 1]' + Trim(FinalGoalieStats.Name_Last) +
                     //Add shots & saves
                    '[f 2][c 1]' + ' ' + Trim(FinalGoalieStats.Saves) + ' SAVES';
                     //OutStr := OutStr + Trim(FinalGoalieStats.Shots) + ' SHOTS';
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Goalie stats - Home (can be 1st or 2nd in list)
  else if (SymbolName = '$Final_Goalie_Stats1_Home') OR (SymbolName = '$Final_Goalie_Stats2_Home') then
  begin
    if (SymbolName = '$Final_Goalie_Stats1_Home') then
      FinalGoalieStats := GetGameFinalGoalieStats(CurrentGameData.GameID, CurrentGameData.HStatsIncID, 1)
    else if (SymbolName = '$Final_Skater_Stats2_Home') then
      FinalGoalieStats := GetGameFinalGoalieStats(CurrentGameData.GameID, CurrentGameData.HStatsIncID, 2);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(FinalGoalieStats.Name_Last) <> '') then
      begin
        if (CurrentGameData.HScore > CurrentGameData.VScore) then
        begin
          OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + ': ' +
                    '[f 2][c 1]' + Trim(FinalGoalieStats.Name_Last) +
                     //Add shots & saves
                    '[f 2][c 1]' + ' ' + Trim(FinalGoalieStats.Saves) + ' SAVES';
                     //OutStr := OutStr + Trim(FinalGoalieStats.Shots) + ' SHOTS';
        end
        else begin
          OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + ': ' +
                    '[f 2][c 1]' + Trim(FinalGoalieStats.Name_Last) +
                     //Add shots & saves
                    '[f 2][c 1]' + ' ' + Trim(FinalGoalieStats.Saves) + ' SAVES';
                     //OutStr := OutStr + Trim(FinalGoalieStats.Shots) + ' SHOTS';
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Football stats
  //Quarterback stats - Visitor
  else if (SymbolName = '$Final_QB_Stats_Visitor_Mnemonic') OR (SymbolName = '$Final_QB_Stats_Visitor_Name') then
  begin
    //Only request data if new game or league
    if (CurrentFBStatsRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentFBStatsRec.CurrentLeague <> CurrentGameData.League) then
      CurrentFBStatsRec := GetGameFinalFBStats(CurrentGameData.League, CurrentGameData.GameID);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(CurrentFBStatsRec.VPassingName_Last) <> '') then
      begin
        if (CurrentGameData.VScore > CurrentGameData.HScore) then
        begin
          if (SymbolName = '$Final_QB_Stats_Visitor_Mnemonic') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + ': '
          else if (SymbolName = '$Final_QB_Stats_Visitor_Name') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName) + ': ';
        end
        else if (CurrentGameData.VScore < CurrentGameData.HScore) then
        begin
          if (SymbolName = '$Final_QB_Stats_Visitor_Mnemonic') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + ': '
          else if (SymbolName = '$Final_QB_Stats_Visitor_Name') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VName) + ': ';
        end;
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.VPassingName_Last) + ' ';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := OutStr + '|';
        //Add attempts & completions
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.VPassingCompletions) +
                  '-' + Trim(CurrentFBStatsRec.VPassingAttempts);
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.VPassingYards) + ' YDS';
        //Add TDs if applicable
        if (Trim(CurrentFBStatsRec.VPassingTouchDowns) <> '') AND (Trim(CurrentFBStatsRec.VPassingTouchDowns) <> '0') then
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.VPassingTouchdowns) + ' TD';
        //Add INTs if applicable
        if (Trim(CurrentFBStatsRec.VPassingInterceptions) <> '') AND (Trim(CurrentFBStatsRec.VPassingInterceptions) <> '0') then
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.VPassingInterceptions) + ' INT';
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Quarterback stats - Home
  else if (SymbolName = '$Final_QB_Stats_Home_Mnemonic') OR (SymbolName = '$Final_QB_Stats_Home_Name') then
  begin
    //Only request data if new game or league
    if (CurrentFBStatsRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentFBStatsRec.CurrentLeague <> CurrentGameData.League) then
      CurrentFBStatsRec := GetGameFinalFBStats(CurrentGameData.League, CurrentGameData.GameID);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(CurrentFBStatsRec.HPassingName_Last) <> '') then
      begin
        if (CurrentGameData.HScore > CurrentGameData.VScore) then
        begin
          if (SymbolName = '$Final_QB_Stats_Home_Mnemonic') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + ': '
          else if (SymbolName = '$Final_QB_Stats_Home_Name') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName) + ': ';
        end
        else if (CurrentGameData.HScore < CurrentGameData.VScore) then
        begin
          if (SymbolName = '$Final_QB_Stats_Home_Mnemonic') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + ': '
          else if (SymbolName = '$Final_QB_Stats_Home_Name') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HName) + ': ';
        end;
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.HPassingName_Last) + ' ';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := OutStr + '|';
        //Add attempts & completions
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.HPassingCompletions) +
                  '-' + Trim(CurrentFBStatsRec.HPassingAttempts);
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.HPassingYards) + ' YDS';
        //Add TDs if applicable
        if (Trim(CurrentFBStatsRec.HPassingTouchdowns) <> '') AND (Trim(CurrentFBStatsRec.HPassingTouchdowns) <> '0') then
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.HPassingTouchdowns) + ' TD';
        //Add INTs if applicable
        if (Trim(CurrentFBStatsRec.HPassingInterceptions) <> '') AND (Trim(CurrentFBStatsRec.HPassingInterceptions) <> '0') then
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.HPassingInterceptions) + ' INT';
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Running back stats - Visitor
  else if (SymbolName = '$Final_RB_Stats_Visitor_Mnemonic') OR (SymbolName = '$Final_RB_Stats_Visitor_Name') then
  begin
    //Only request data if new game or league
    if (CurrentFBStatsRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentFBStatsRec.CurrentLeague <> CurrentGameData.League) then
      CurrentFBStatsRec := GetGameFinalFBStats(CurrentGameData.League, CurrentGameData.GameID);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(CurrentFBStatsRec.VRushingName_Last) <> '') then
      begin
        if (CurrentGameData.VScore > CurrentGameData.HScore) then
        begin
          if (SymbolName = '$Final_RB_Stats_Visitor_Mnemonic') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + ': '
          else if (SymbolName = '$Final_RB_Stats_Visitor_Name') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName) + ': ';
        end
        else if (CurrentGameData.VScore < CurrentGameData.HScore) then
        begin
          if (SymbolName = '$Final_RB_Stats_Visitor_Mnemonic') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + ': '
          else if (SymbolName = '$Final_RB_Stats_Visitor_Name') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VName) + ': ';
        end;
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.VRushingName_Last) + ' ';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := OutStr + '|';
        //Add attempts
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.VRushingAttempts) + ' RUSH';
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.VRushingYards) + ' YDS';
        //Add TDs if applicable
        if (Trim(CurrentFBStatsRec.VRushingTouchdowns) <> '') AND (Trim(CurrentFBStatsRec.VRushingTouchdowns) <> '0') then
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.VRushingTouchdowns) + ' TD';
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Running back stats - Home
  else if (SymbolName = '$Final_RB_Stats_Home_Mnemonic') OR (SymbolName = '$Final_RB_Stats_Home_Name') then
  begin
    //Only request data if new game or league
    if (CurrentFBStatsRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentFBStatsRec.CurrentLeague <> CurrentGameData.League) then
      CurrentFBStatsRec := GetGameFinalFBStats(CurrentGameData.League, CurrentGameData.GameID);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(CurrentFBStatsRec.HRushingName_Last) <> '') then
      begin
        if (CurrentGameData.HScore > CurrentGameData.VScore) then
        begin
          if (SymbolName = '$Final_RB_Stats_Home_Mnemonic') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + ': '
          else if (SymbolName = '$Final_RB_Stats_Home_Name') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName) + ': ';
        end
        else if (CurrentGameData.HScore < CurrentGameData.VScore) then
        begin
          if (SymbolName = '$Final_RB_Stats_Home_Mnemonic') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + ': '
          else if (SymbolName = '$Final_RB_Stats_Home_Name') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HName) + ': ';
        end;
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.HRushingName_Last) + ' ';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := OutStr + '|';
        //Add attempts
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.HRushingAttempts) + ' RUSH';
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.HRushingYards) + ' YDS';
        //Add TDs if applicable
        if (Trim(CurrentFBStatsRec.HRushingTouchdowns) <> '') AND (Trim(CurrentFBStatsRec.HRushingTouchdowns) <> '0') then
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.HRushingTouchdowns) + ' TD';
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

   //Wide Receiver stats - Visitor
  else if (SymbolName = '$Final_WR_Stats_Visitor_Mnemonic') OR (SymbolName = '$Final_WR_Stats_Visitor_Name') then
  begin
    //Only request data if new game or league
    if (CurrentFBStatsRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentFBStatsRec.CurrentLeague <> CurrentGameData.League) then
      CurrentFBStatsRec := GetGameFinalFBStats(CurrentGameData.League, CurrentGameData.GameID);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(CurrentFBStatsRec.VReceivingName_Last) <> '') then
      begin
        if (CurrentGameData.VScore > CurrentGameData.HScore) then
        begin
          if (SymbolName = '$Final_WR_Stats_Visitor_Mnemonic') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + ': '
          else if (SymbolName = '$Final_WR_Stats_Visitor_Name') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName) + ': ';
        end
        else if (CurrentGameData.VScore < CurrentGameData.HScore) then
        begin
          if (SymbolName = '$Final_WR_Stats_Visitor_Mnemonic') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + ': '
          else if (SymbolName = '$Final_WR_Stats_Visitor_Name') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VName) + ': ';
        end;
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.VReceivingName_Last) + ' ';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := OutStr + '|';
        //Add attempts
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.VReceivingReceptions) + ' REC';
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.VReceivingYards) + ' YDS';
        //Add TDs if applicable
        if (Trim(CurrentFBStatsRec.VReceivingTouchdowns) <> '') AND (Trim(CurrentFBStatsRec.VReceivingTouchdowns) <> '0') then
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.VReceivingTouchdowns) + ' TD';
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Wide Receiver stats - Home
  else if (SymbolName = '$Final_WR_Stats_Home_Mnemonic') OR (SymbolName = '$Final_WR_Stats_Home_Name') then
  begin
    //Only request data if new game or league
    if (CurrentFBStatsRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentFBStatsRec.CurrentLeague <> CurrentGameData.League) then
      CurrentFBStatsRec := GetGameFinalFBStats(CurrentGameData.League, CurrentGameData.GameID);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(CurrentFBStatsRec.HReceivingName_Last) <> '') then
      begin
        if (CurrentGameData.HScore > CurrentGameData.VScore) then
        begin
          if (SymbolName = '$Final_WR_Stats_Home_Mnemonic') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + ': '
          else if (SymbolName = '$Final_WR_Stats_Home_Name') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName) + ': ';
        end
        else if (CurrentGameData.HScore < CurrentGameData.VScore) then
        begin
          if (SymbolName = '$Final_WR_Stats_Home_Mnemonic') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + ': '
          else if (SymbolName = '$Final_WR_Stats_Home_Name') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HName) + ': ';
        end;
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.HReceivingName_Last) + ' ';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := OutStr + '|';
        //Add attempts
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.HReceivingReceptions) + ' REC';
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.HReceivingYards) + ' YDS';
        //Add TDs if applicable
        if (Trim(CurrentFBStatsRec.HReceivingTouchdowns) <> '') AND (Trim(CurrentFBStatsRec.HReceivingTouchdowns) <> '0') then
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.HReceivingTouchdowns) + ' TD';
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Fantasy Stats - NFL
  else if (SymbolName = '$NFL_Fantasy_Stats_QB') then
  begin
    //Only request data if not for current date
    if (CurrentFantasyFootballPassingStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyFootballPassingStatsRec := GetFantasyFootballPassingStats;
    with CurrentFantasyFootballPassingStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);

        //Build string
        OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NFL', TeamID[Index])) + ': ';
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := OutStr + '|';
        //Add attempts & completions
        OutStr := OutStr + '[f 2][c 1]' + Trim(Completions[Index]) +
                  '-' + Trim(Attempts[Index]);
        OutStr := OutStr + ', ' + Trim(PassingYards[Index]) + ' YDS';
        //Add TDs if applicable
        if (Trim(PassingTDs[Index]) <> '') AND (Trim(PassingTDs[Index]) <> '0') then
        OutStr := OutStr + ', ' + Trim(PassingTDs[Index]) + ' TD';
      end
      else OutStr := '';
    end;
  end

  else if (SymbolName = '$NFL_Fantasy_Stats_RB') then
  begin
    //Only request data if not for current date
    if (CurrentFantasyFootballRushingStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyFootballRushingStatsRec := GetFantasyFootballRushingStats;
    with CurrentFantasyFootballRushingStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);

        //Build string
        OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NFL', TeamID[Index])) + ': ';
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := OutStr + '|';
        //Rushes
        OutStr := OutStr + '[f 2][c 1]' + Trim(Rushes[Index]) + ' RUSH';
        //Yards
        OutStr := OutStr + ', ' + Trim(Yards[Index]) + ' YDS';
        //Add TDs if applicable
        if (Trim(TDs[Index]) <> '') AND (Trim(TDs[Index]) <> '0') then
        OutStr := OutStr + ', ' + Trim(TDs[Index]) + ' TD';
      end
      else OutStr := '';
    end;
  end

  else if (SymbolName = '$NFL_Fantasy_Stats_WR') then
  begin
    //Only request data if not for current date
    if (CurrentFantasyFootballReceivingStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyFootballReceivingStatsRec := GetFantasyFootballReceivingStats;
    with CurrentFantasyFootballReceivingStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);

        //Build string
        OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NFL', TeamID[Index])) + ': ';
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := OutStr + '|';
        //Rushes
        OutStr := OutStr + '[f 2][c 1]' + Trim(Receptions[Index]) + ' REC';
        //Yards
        OutStr := OutStr + ', ' + Trim(Yards[Index]) + ' YDS';
        //Add TDs if applicable
        if (Trim(TDs[Index]) <> '') AND (Trim(TDs[Index]) <> '0') then
        OutStr := OutStr + ', ' + Trim(TDs[Index]) + ' TD';
      end
      else OutStr := '';
    end;
  end

  //Fantasy Stats - MLB
  else if (SymbolName = '$MLB_Fantasy_Stats_Batters') then
  begin
    //Only request data if not for current date
    if (CurrentFantasyBaseballBattingStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBaseballBattingStatsRec := GetFantasyBaseballBattingStats;
    with CurrentFantasyBaseballBattingStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);

        //Build string
        OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('MLB', TeamID[Index])) + ': ';
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := OutStr + '|';
        //Hits-AtBats
        OutStr := OutStr + '[f 2][c 1]' + Trim(Hits[Index]) + '-' +
          Trim(AtBats[Index]);
        //Add HomeRuns if applicable
        if (Trim(HomeRuns[Index]) <> '') AND (Trim(HomeRuns[Index]) <> '0') then
        OutStr := OutStr + ', ' + Trim(HomeRuns[Index]) + ' HR';
        //Add RBI if applicable
        if (Trim(RBI[Index]) <> '') AND (Trim(RBI[Index]) <> '0') then
        OutStr := OutStr + ', ' + Trim(RBI[Index]) + ' RBI';
        //Add Stolen Bases if applicable
        if (Trim(StolenBases[Index]) <> '') AND (Trim(StolenBases[Index]) <> '0') then
        OutStr := OutStr + ', ' + Trim(StolenBases[Index]) + ' SB';
      end
      else OutStr := '';
    end;
  end

  else if (SymbolName = '$MLB_Fantasy_Stats_Pitchers') then
  begin
    //Only request data if not for current date
    if (CurrentFantasyBaseballPitchingStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBaseballPitchingStatsRec := GetFantasyBaseballPitchingStats;
    with CurrentFantasyBaseballPitchingStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);

        //Build string
        OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('MLB', TeamID[Index])) + ': ';
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := OutStr + '|';
        //Wins
        OutStr := OutStr + '[f 2][c 1]' + Trim(Wins[Index]) + ' W';
        //Add Strikouts
        OutStr := OutStr + ', ' + Trim(StrikeOuts[Index]) + ' K';
        //Add Saves if applicable
        if (Trim(Saves[Index]) <> '') AND (Trim(Saves[Index]) <> '0') then
        OutStr := OutStr + ', ' + Trim(Saves[Index]) + ' SV';
        //Add Earned Runs if applicable
        if (Trim(EarnedRuns[Index]) <> '') then
        OutStr := OutStr + ', ' + Trim(EarnedRuns[Index]) + ' ER';
      end
      else OutStr := '';
    end;
  end

  else if (SymbolName = '$NHL_Fantasy_Stats_Points') then
  begin
    //Only request data if not for current date
    if (CurrentFantasyHockeyPointsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyHockeyPointsStatsRec := GetFantasyHockeyPointsStats;
    with CurrentFantasyHockeyPointsStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);

        //Build string
        OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NHL', TeamID[Index])) + ': ';
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := OutStr + '|';
        //Add points
        OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Points[Index]) + ' PTS';
      end
      else OutStr := '';
    end;
  end

  //Fantasy Stats - NHL
  else if (SymbolName = '$NHL_Fantasy_Stats_Goals') then
  begin
    //Only request data if not for current date
    if (CurrentFantasyHockeyGoalsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyHockeyGoalsStatsRec := GetFantasyHockeyGoalsStats;
    with CurrentFantasyHockeyGoalsStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);

        //Build string
        OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NHL', TeamID[Index])) + ': ';
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := OutStr + '|';
        //Add points
        OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Goals[Index]) + ' GLS';
      end
      else OutStr := '';
    end;
  end

  else if (SymbolName = '$NHL_Fantasy_Stats_Assists') then
  begin
    //Only request data if not for current date
    if (CurrentFantasyHockeyAssistsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyHockeyAssistsStatsRec := GetFantasyHockeyAssistsStats;
    with CurrentFantasyHockeyAssistsStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);

        //Build string
        OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NHL', TeamID[Index])) + ': ';
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
          OutStr := OutStr + '|';
        //Add points
        OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Assists[Index]) + ' AST';
      end
      else OutStr := '';
    end;
  end

  //Fantasy Stats - NBA
  else if (SymbolName = '$NBA_Fantasy_Stats_Points') then
  begin
    if (CurrentFantasyBasketballPointsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBasketballPointsStatsRec := GetFantasyBasketballStats('Points');
    with CurrentFantasyBasketballPointsStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);

        //Build string
        OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NBA', TeamID[Index])) + ': ';
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
        //Line-wrap
        //if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
        //  OutStr := OutStr + '|';
        //Add points
        OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Points[Index]) + ' PTS';
      end
      else OutStr := '';
    end;
  end

  else if (SymbolName = '$NBA_Fantasy_Stats_Rebounds') then
  begin
    if (CurrentFantasyBasketballReboundsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBasketballReboundsStatsRec := GetFantasyBasketballStats('Rebounds');
    with CurrentFantasyBasketballReboundsStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);

        //Build string
        OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NBA', TeamID[Index])) + ': ';
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
        //Line-wrap
        //if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
        //  OutStr := OutStr + '|';
        //Add points
        OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Rebounds[Index]) + ' REB';
      end
      else OutStr := '';
    end;
  end

  else if (SymbolName = '$NBA_Fantasy_Stats_Assists') then
  begin
    if (CurrentFantasyBasketballAssistsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBasketballAssistsStatsRec := GetFantasyBasketballStats('Assists');
    with CurrentFantasyBasketballAssistsStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);

        //Build string
        OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NBA', TeamID[Index])) + ': ';
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
        //Line-wrap
        //if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
        //  OutStr := OutStr + '|';
        //Add points
        OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Assists[Index]) + ' AST';
      end
      else OutStr := '';
    end;
  end

  //Boxscore stats
  else if (SymbolName = '$Box_V1') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.VPeriod1);
  end
  else if (SymbolName = '$Box_V2') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.VPeriod2);
  end
  else if (SymbolName = '$Box_V3') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.VPeriod3);
  end
  else if (SymbolName = '$Box_V4') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.VPeriod4);
  end
  else if (SymbolName = '$Box_V5') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.VPeriod5);
  end
  else if (SymbolName = '$Box_V6') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.VPeriod6);
  end
  else if (SymbolName = '$Box_V7') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.VPeriod7);
  end
  else if (SymbolName = '$Box_H1') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.HPeriod1);
  end
  else if (SymbolName = '$Box_H2') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.HPeriod2);
  end
  else if (SymbolName = '$Box_H3') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.HPeriod3);
  end
  else if (SymbolName = '$Box_H4') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.HPeriod4);
  end
  else if (SymbolName = '$Box_H5') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.HPeriod5);
  end
  else if (SymbolName = '$Box_H6') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.HPeriod6);
  end
  else if (SymbolName = '$Box_H7') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.HPeriod7);
  end;

  //Check if need to force to upper case
  if (ForceUpperCase) then OutStr := ANSIUpperCase(OutStr);

  //Return
  GetValueOfSymbol.SymbolValue := OutStr;
  GetValueOfSymbol.OKToDisplay := OKToDisplay;
end;

//Function to pull weather forecast data for the home stadium
function GetWeatherForecastData(League: String; HomeStatsID: LongInt): WeatherForecastRec;
var
  OutRec: WeatherForecastRec;
begin
  try
    //Run stored procedure to get weather data
    with dmMain.WeatherQuery do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add ('sp_GetWeatherData ' + QuotedStr(League) + ',' + IntToStr(HomeStatsID));
      Active := TRUE;
      if (RecordCount > 0) then
      begin
        OutRec.Weather_Stadium_Name := FieldByName('StadiumName').AsString;
        OutRec.Weather_Current_Temperature := FieldByName('CurrentTemperature').AsString;
        OutRec.Weather_High_Temperature := FieldByName('HighTemperature').AsString;
        OutRec.Weather_Low_Temperature := FieldByName('LowTemperature').AsString;
        OutRec.Weather_Current_Conditions := FieldByName('Condition').AsString;
        OutRec.Weather_Forecast_Icon_Day := FieldByName('DayIcon').AsString;
        OutRec.Weather_Forecast_Icon_Night := FieldByName('NightIcon').AsString;
      end;
      Active := FALSE;
    end;
  except
  end;
  GetWeatherForecastData := OutRec;
end;

//Function to get starting pitchers
function GetStartingPitcherStats(GameCode: String): StartingPitcherStatsRec;
var
  OutRec: StartingPitcherStatsRec;
begin
  //Init
  OutRec.Home_Pitcher_Name_Last := '';
  OutRec.Home_Pitcher_Name_First := '';
  OutRec.Visitor_Pitcher_Name_Last := '';
  OutRec.Visitor_Pitcher_Name_First := '';
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */SELECT * FROM dbo.tf_SI_Get_MLB_Game_Starting_Pitchers(' + GameCode + ')');
      Open;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        OutRec.CurrentGameID := GameCode;
        OutRec.Home_Pitcher_Name_Last := FieldByName('HomeLastName').AsString;
        OutRec.Home_Pitcher_Name_First := FieldByName('HomeFirstName').AsString;
        OutRec.Home_Pitcher_Wins := '0';
        OutRec.Home_Pitcher_Losses := '0';
        OutRec.Visitor_Pitcher_Name_Last := FieldByName('AwayLastName').AsString;
        OutRec.Visitor_Pitcher_Name_First := FieldByName('AwayFirstName').AsString;
        OutRec.Visitor_Pitcher_Wins := '0';
        OutRec.Visitor_Pitcher_Losses := '0';
      end;
    end;
  except

  end;
  GetStartingPitcherStats := OutRec;
end;

//Function to get winning, losing and saving pitchers
function GetGameFinalPitcherStats(GameCode: String): FinalPitcherStatsRec;
var
  OutRec: FinalPitcherStatsRec;
begin
  //Init
  OutRec.Winning_Pitcher_Name_Last := '';
  OutRec.Winning_Pitcher_Name_First := '';
  OutRec.Losing_Pitcher_Name_Last := '';
  OutRec.Losing_Pitcher_Name_First := '';
  OutRec.Saving_Pitcher_Name_Last := '';
  OutRec.Saving_Pitcher_Name_First := '';
  //Init
  OutRec.NeedToAddSavingPitcher := FALSE;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */SELECT * FROM dbo.tf_SI_Get_MLB_Game_Pitcher_Stats(' + GameCode + ') ' +
        'WHERE (IsWinningPitcher = ' + QuotedStr('true') + ' or IsLosingPitcher = ' + QuotedStr ('true') +
          ' or IsSavingPitcher = ' + QuotedStr('true') + ') ORDER BY TeamId ASC, LastName ASC');
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        OutRec.CurrentGameID := GameCode;
        OutRec.NeedToAddSavingPitcher := FALSE;
        repeat
          if (FieldByName('IsWinningPitcher').AsBoolean = TRUE) then
          begin
            OutRec.Winning_Pitcher_Name_Last := FieldByName('LastName').AsString;
            OutRec.Winning_Pitcher_Name_First := FieldByName('FirstName').AsString;
            OutRec.Winning_Pitcher_Wins := FieldByName('Wins').AsString;
            OutRec.Winning_Pitcher_Losses := FieldByName('Losses').AsString;
            OutRec.Winning_Pitcher_IP := FieldByName('InningsPitched').AsString;
            OutRec.Winning_Pitcher_K := FieldByName('Strikeouts').AsString;
            OutRec.Winning_Pitcher_BB := FieldByName('Walks').AsString;
            OutRec.Winning_Pitcher_Hits := FieldByName('Hits').AsString;
            OutRec.Winning_Pitcher_Runs := FieldByName('Runs').AsString;
            OutRec.Winning_Pitcher_EarnedRuns := FieldByName('EarnedRuns').AsString;
            OutRec.Winning_Pitcher_HomeRuns := FieldByName('HomeRunsAllowed').AsString;
          end
          else if (FieldByName('IsLosingPitcher').AsBoolean = TRUE) then
          begin
            OutRec.Losing_Pitcher_Name_Last := FieldByName('LastName').AsString;
            OutRec.Losing_Pitcher_Name_First := FieldByName('FirstName').AsString;
            OutRec.Losing_Pitcher_Wins := FieldByName('Wins').AsString;
            OutRec.Losing_Pitcher_Losses := FieldByName('Losses').AsString;
            OutRec.Losing_Pitcher_IP := FieldByName('InningsPitched').AsString;
            OutRec.Losing_Pitcher_K := FieldByName('Strikeouts').AsString;
            OutRec.Losing_Pitcher_BB := FieldByName('Walks').AsString;
            OutRec.Losing_Pitcher_Hits := FieldByName('Hits').AsString;
            OutRec.Losing_Pitcher_Runs := FieldByName('Runs').AsString;
            OutRec.Losing_Pitcher_EarnedRuns := FieldByName('EarnedRuns').AsString;
            OutRec.Losing_Pitcher_HomeRuns := FieldByName('HomeRunsAllowed').AsString;
          end
          else if (FieldByName('IsSavingPitcher').AsBoolean = TRUE) then
          begin
            OutRec.Saving_Pitcher_Name_Last := FieldByName('LastName').AsString;
            OutRec.Saving_Pitcher_Name_First := FieldByName('FirstName').AsString;
            OutRec.Saving_Pitcher_Saves := FieldByName('Saves').AsString;
            //Set flag to indicate saving pitcher needs to be added
            OutRec.NeedToAddSavingPitcher := TRUE;
          end;
          //Go to next record
          Next;
        until (dmMain.SportbaseQuery2.EOF);
      end;
    end;
  except

  end;
  //Return
  GetGameFinalPitcherStats := OutRec;
end;

//Function to get batter stats
function GetGameFinalBatterStats(GameCode: String; TeamID: LongInt; StatNum: SmallInt): FinalBatterStatsRec;
var
  i: SmallInt;
  OutRec: FinalBatterStatsRec;
begin
  //Init
  OutRec.Name_Last := '';
  OutRec.name_First := '';
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/**/SELECT * FROM dbo.tf_SI_Get_MLB_Game_Player_Stats(' + GameCode + ') ' +
        'WHERE TeamID = ' + IntToStr(TeamID) + ' ' +
        'ORDER BY Runs DESC, Hits DESC');
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Go to specified record number
        if (StatNum > 1) then for i := 1 to StatNum-1 do Next;
        //Set data values
        OutRec.Name_Last := FieldByName('LastName').AsString;
        OutRec.Name_First := FieldByName('FirstName').AsString;
        OutRec.Atbats := FieldByName('AtBats').AsString;
        OutRec.Hits := FieldByName('Hits').AsString;
        OutRec.Runs := FieldByName('Runs').AsString;
        OutRec.Doubles := FieldByName('Doubles').AsString;
        OutRec.Triples := FieldByName('Triples').AsString;
        OutRec.HomeRuns := FieldByName('HomeRuns').AsString;
        OutRec.RBI := FieldByName('RBI').AsString;
        OutRec.StolenBases := FieldByName('StolenBases').AsString;
      end;
    end;
  except

  end;
  //Return
  GetGameFinalBatterStats := OutRec;
end;

//Basketball stats
function GetGameFinalBBallStats(League, GameCode: String; TeamID: LongInt; StatNum: SmallInt): FinalBBallStatsRec;
var
  i: SmallInt;
  OutRec: FinalBBallStatsRec;
  LeagueStr: String;
begin
  //Init
  OutRec.Name_Last := '';
  OutRec.name_First := '';
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      if (League = 'NBA') then LeagueStr := 'NBA'
      else if (League = 'NCAAB') then LeagueStr := 'CBK'
      else if (League = 'NCAAW') then LeagueStr := 'WCBK';
      SQL.Add('/**/SELECT TOP (3) WITH TIES * FROM dbo.tf_SI_Get_' + LeagueStr + '_Game_Player_Stats(' + GameCode + ') ' +
        'WHERE TeamID = ' + IntToStr(TeamID) + ' ' +
        'ORDER BY Points DESC, ReboundsTotal DESC, Assists DESC');
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Go to specified record number
        if (StatNum > 1) then for i := 1 to StatNum-1 do Next;
        //Set data values
        OutRec.Name_Last := FieldByName('LastName').AsString;
        OutRec.Name_First := FieldByName('FirstName').AsString;
        OutRec.Points := FieldByName('Points').AsString;
        OutRec.Rebounds := FieldByName('ReboundsTotal').AsString;
        OutRec.Assists := FieldByName('Assists').AsString;
      end;
    end;
  except

  end;
  //Return
  GetGameFinalBBallStats := OutRec;
end;

//Football stats function - all stats returned in 1 record
function GetGameFinalFBStats(League, GameCode: String): FinalFBStatsRec;
var
  i: SmallInt;
  OutRec: FinalFBStatsRec;
  LeagueStr: String;
begin
  //Init
  OutRec.VPassingName_Last := '';
  OutRec.VPassingName_First := '';
  OutRec.HPassingName_Last := '';
  OutRec.HPassingName_First := '';
  OutRec.VRushingName_Last := '';
  OutRec.VRushingName_First := '';
  OutRec.HRushingName_Last := '';
  OutRec.HRushingName_First := '';
  OutRec.VReceivingName_Last := '';
  OutRec.VReceivingName_First := '';
  OutRec.HReceivingName_Last := '';
  OutRec.HReceivingName_First := '';
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      if (League = 'NCAAF') then LeagueStr := 'CFB'
      else if (League = 'NFL') then LeagueStr := 'NFL';
      SQL.Add('/**/p_SI_Get_' + LeagueStr + '_InGameStats ' + GameCode);
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set data values
        //Psssing stats
        OutRec.CurrentLeague := League;
        OutRec.CurrentGameID := GameCode;
        OutRec.VPassingName_Last := FieldByName('VPassingLastName').AsString;
        OutRec.VPassingName_First := FieldByName('VPassingFirstName').AsString;
        OutRec.VPassingAttempts := FieldByName('VPassingAttempts').AsString;
        OutRec.VPassingCompletions := FieldByName('VPassingCompletions').AsString;
        OutRec.VPassingYards := FieldByName('VPassingYards').AsString;
        OutRec.VPassingTouchdowns := FieldByName('VPassingTDs').AsString;
        OutRec.VPassingInterceptions := FieldByName('VPassingInterceptions').AsString;
        OutRec.HPassingName_Last := FieldByName('HPassingLastName').AsString;
        OutRec.HPassingName_First := FieldByName('HPassingFirstName').AsString;
        OutRec.HPassingAttempts := FieldByName('HPassingAttempts').AsString;
        OutRec.HPassingCompletions := FieldByName('HPassingCompletions').AsString;
        OutRec.HPassingYards := FieldByName('HPassingYards').AsString;
        OutRec.HPassingTouchdowns := FieldByName('HPassingTDs').AsString;
        OutRec.HPassingInterceptions := FieldByName('HPassingInterceptions').AsString;
        //Rushing stats
        OutRec.VRushingName_Last := FieldByName('VRushingLastName').AsString;
        OutRec.VRushingName_First := FieldByName('VRushingFirstName').AsString;
        OutRec.VRushingAttempts := FieldByName('VRushingAttempts').AsString;
        OutRec.VRushingYards := FieldByName('VRushingYards').AsString;
        OutRec.VRushingTouchdowns := FieldByName('VRushingTDs').AsString;
        OutRec.HRushingName_Last := FieldByName('HRushingLastName').AsString;
        OutRec.HRushingName_First := FieldByName('HRushingFirstName').AsString;
        OutRec.HRushingAttempts := FieldByName('HRushingAttempts').AsString;
        OutRec.HRushingYards := FieldByName('HRushingYards').AsString;
        OutRec.HRushingTouchdowns := FieldByName('HRushingTDs').AsString;
        //Receiving stats
        OutRec.VReceivingName_Last := FieldByName('VReceivingLastName').AsString;
        OutRec.VReceivingName_First := FieldByName('VReceivingFirstName').AsString;
        OutRec.VReceivingReceptions := FieldByName('VReceivingReceptions').AsString;
        OutRec.VReceivingYards := FieldByName('VReceivingYards').AsString;
        OutRec.VReceivingTouchdowns := FieldByName('VReceivingTDs').AsString;
        OutRec.HReceivingName_Last := FieldByName('HReceivingLastName').AsString;
        OutRec.HReceivingName_First := FieldByName('HReceivingFirstName').AsString;
        OutRec.HReceivingReceptions := FieldByName('HReceivingReceptions').AsString;
        OutRec.HReceivingYards := FieldByName('HReceivingYards').AsString;
        OutRec.HReceivingTouchdowns := FieldByName('HReceivingTDs').AsString;
      end;
    end;
  except

  end;
  //Return
  GetGameFinalFBStats := OutRec;
end;

//Hockey Stats functions
//NHL Player Stats
function GetGameFinalSkaterStats(GameCode: String; TeamID: LongInt; StatNum: SmallInt): FinalSkaterStatsRec;
var
  i: SmallInt;
  OutRec: FinalSkaterStatsRec;
  LeagueStr: String;
begin
  //Init
  OutRec.Name_Last := '';
  OutRec.name_First := '';
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/**/SELECT TOP (3) WITH TIES * FROM dbo.tf_SI_Get_NHL_Game_Skater_Stats(' + GameCode + ') ' +
        'WHERE TeamID = ' + IntToStr(TeamID) + ' ' +
        'ORDER BY Goals DESC, ASSISTS DESC, SOG DESC');
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Go to specified record number
        if (StatNum > 1) then for i := 1 to StatNum-1 do Next;
        //Set data values
        OutRec.Name_Last := FieldByName('LastName').AsString;
        OutRec.Name_First := FieldByName('FirstName').AsString;
        OutRec.Goals := FieldByName('Goals').AsString;
        OutRec.Assists := FieldByName('Assists').AsString;
        OutRec.SOG := FieldByName('SOG').AsString;
      end;
    end;
  except

  end;
  //Return
  GetGameFinalSkaterStats := OutRec;
end;

//NHL Goalie Stats
function GetGameFinalGoalieStats(GameCode: String; TeamID: LongInt; StatNum: SmallInt): FinalGoalieStatsRec;
var
  i: SmallInt;
  OutRec: FinalGoalieStatsRec;
  LeagueStr: String;
begin
  //Init
  OutRec.Name_Last := '';
  OutRec.name_First := '';
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/**/SELECT * FROM dbo.tf_SI_Get_NHL_Game_Goalie_Stats(' + GameCode + ') ' +
        'WHERE TeamID = ' + IntToStr(TeamID)+ ' ' +
        'ORDER BY SAVES DESC');
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Go to specified record number
        if (StatNum > 1) then for i := 1 to StatNum-1 do Next;
        //Set data values
        OutRec.Name_Last := FieldByName('LastName').AsString;
        OutRec.Name_First := FieldByName('FirstName').AsString;
        OutRec.Shots := FieldByName('Shots').AsString;
        OutRec.Saves := FieldByName('Saves').AsString;
        OutRec.GoalsAgainst := FieldByName('GoalsAgainst').AsString;
      end;
    end;
  except

  end;
  //Return
  GetGameFinalGoalieStats := OutRec;
end;

//Function to return the boxscore data for a specified league and game; covers
//all leagues other than MLB
function GetBoxscoreData(League: String; GameCode: String): BoxscoreRec;
var
  i: SmallInt;
  LeagueStr: String;
  OutRec: BoxscoreRec;
begin
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      //Build query based on league
      if (League = 'NFL') then
        SQL.Add('/* */p_SI_Get_NFL_Game_Boxscore ' + GameCode)
      else if (League = 'NHL') then
        SQL.Add('/* */p_SI_Get_NHL_Game_Boxscore ' + GameCode)
      else if (League = 'NBA') then
        SQL.Add('/* */p_SI_Get_NBA_Game_Boxscore ' + GameCode)
      else if (League = 'MLS') then
        SQL.Add('/* */p_SI_Get_MLS_Game_Boxscore ' + GameCode)
      else if (League = 'NCAAF') then
        SQL.Add('/* */p_SI_Get_CFB_Game_Boxscore ' + GameCode)
      else if (League = 'NCAAB') then
        SQL.Add('/* */p_SI_Get_CBK_Game_Boxscore ' + GameCode)
      else if (League = 'NCAAW') then
        SQL.Add('/* */p_SI_Get_WCBK_Game_Boxscore ' + GameCode);
      Open;
      First;

      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        OutRec.CurrentLeague := League;
        OutRec.CurrentGameID := GameCode;
        //Set data values based on league; not - MLB not included here - use simple line score
        //handled in other functions
        if (League = 'MLS') OR (League = 'NCAAB') OR (League = 'NCAAW') then
        begin
          //Determine number of periods
          OutRec.VNumPeriods := 0;
          if (Trim(FieldByName('VHalf1').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VHalf2').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VHalf3').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VHalf4').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VHalf5').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          //Set scoring by period
          OutRec.VScore := Trim(FieldByName('VScore').AsString);
          OutRec.VPeriod1 := Trim(FieldByName('VHalf1').AsString);
          OutRec.VPeriod2 := Trim(FieldByName('VHalf2').AsString);
          OutRec.VPeriod3 := Trim(FieldByName('VHalf3').AsString);
          OutRec.VPeriod4 := Trim(FieldByName('VHalf4').AsString);
          OutRec.VPeriod5 := Trim(FieldByName('VHalf5').AsString);
          OutRec.VPeriod6 := '';
          OutRec.VPeriod7 := '';
          //Determine number of periods
          OutRec.HNumPeriods := 0;
          if (Trim(FieldByName('HHalf1').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HHalf2').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HHalf3').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HHalf4').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HHalf5').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          //Set scoring by period
          OutRec.HScore := Trim(FieldByName('HScore').AsString);
          OutRec.HPeriod1 := Trim(FieldByName('HHalf1').AsString);
          OutRec.HPeriod2 := Trim(FieldByName('HHalf2').AsString);
          OutRec.HPeriod3 := Trim(FieldByName('HHalf3').AsString);
          OutRec.HPeriod4 := Trim(FieldByName('HHalf4').AsString);
          OutRec.HPeriod5 := Trim(FieldByName('HHalf5').AsString);
          OutRec.HPeriod6 := '';
          OutRec.HPeriod7 := '';
        end
        else if (League = 'NBA') OR (League = 'NFL') OR (League = 'NCAAF') then
        begin
          //Determine number of periods
          OutRec.VNumPeriods := 0;
          if (Trim(FieldByName('VQuarter1').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VQuarter2').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VQuarter3').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VQuarter4').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VQuarter5').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VQuarter6').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VQuarter7').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          //Set scoring by period
          OutRec.VScore := Trim(FieldByName('VScore').AsString);
          OutRec.VPeriod1 := Trim(FieldByName('VQuarter1').AsString);
          OutRec.VPeriod2 := Trim(FieldByName('VQuarter2').AsString);
          OutRec.VPeriod3 := Trim(FieldByName('VQuarter3').AsString);
          OutRec.VPeriod4 := Trim(FieldByName('VQuarter4').AsString);
          OutRec.VPeriod5 := Trim(FieldByName('VQuarter5').AsString);
          OutRec.VPeriod6 := Trim(FieldByName('VQuarter6').AsString);
          OutRec.VPeriod7 := Trim(FieldByName('VQuarter7').AsString);
          //Determine number of periods
          OutRec.HNumPeriods := 0;
          if (Trim(FieldByName('HQuarter1').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HQuarter2').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HQuarter3').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HQuarter4').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HQuarter5').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HQuarter6').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HQuarter7').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          //Set scoring by period
          OutRec.HScore := Trim(FieldByName('HScore').AsString);
          OutRec.HPeriod1 := Trim(FieldByName('HQuarter1').AsString);
          OutRec.HPeriod2 := Trim(FieldByName('HQuarter2').AsString);
          OutRec.HPeriod3 := Trim(FieldByName('HQuarter3').AsString);
          OutRec.HPeriod4 := Trim(FieldByName('HQuarter4').AsString);
          OutRec.HPeriod5 := Trim(FieldByName('HQuarter5').AsString);
          OutRec.HPeriod6 := Trim(FieldByName('HQuarter6').AsString);
          OutRec.HPeriod7 := Trim(FieldByName('HQuarter7').AsString);
        end
        else if (League = 'NHL') then
        begin
          //Determine number of periods
          OutRec.VNumPeriods := 0;
          if (Trim(FieldByName('VPeriod1').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VPeriod2').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VPeriod3').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VPeriod4').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VSOScore').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          //Set scoring by period
          OutRec.VScore := Trim(FieldByName('VScore').AsString);
          OutRec.VPeriod1 := Trim(FieldByName('VPeriod1').AsString);
          OutRec.VPeriod2 := Trim(FieldByName('VPeriod2').AsString);
          OutRec.VPeriod3 := Trim(FieldByName('VPeriod3').AsString);
          OutRec.VPeriod4 := Trim(FieldByName('VPeriod4').AsString);
          OutRec.VPeriod5 := Trim(FieldByName('VSOScore').AsString);
          OutRec.VPeriod6 := '';
          OutRec.VPeriod7 := '';
          //Determine number of periods
          OutRec.HNumPeriods := 0;
          if (Trim(FieldByName('HPeriod1').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HPeriod2').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HPeriod3').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HPeriod4').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HSOScore').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          //Set scoring by period
          OutRec.HScore := Trim(FieldByName('HScore').AsString);
          OutRec.HPeriod1 := Trim(FieldByName('HPeriod1').AsString);
          OutRec.HPeriod2 := Trim(FieldByName('HPeriod2').AsString);
          OutRec.HPeriod3 := Trim(FieldByName('HPeriod3').AsString);
          OutRec.HPeriod4 := Trim(FieldByName('HPeriod4').AsString);
          OutRec.HPeriod5 := Trim(FieldByName('HSOScore').AsString);
          OutRec.HPeriod6 := '';
          OutRec.HPeriod7 := '';
        end
      end;
    end;
  except

  end;
  //Return
  GetBoxscoreData := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// FANTASY STATS FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//NFL Functions
//Passing
function GetFantasyFootballPassingStats: FantasyFootballPassingStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyFootballPassingStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.Attempts[i] := '';
    OutRec.Completions[i] := '';
    OutRec.PassingYards[i] := '';
    OutRec.PassingTDs[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */dbo.p_SI_Get_NFL_Weekly_Leaders ' + QuotedStr('Top QB'));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := FieldByName('Rank').AsString;
          OutRec.Attempts[i] := FieldByName('Att').AsString;
          OutRec.Completions[i] := FieldByName('Comp').AsString;
          OutRec.PassingYards[i] := FieldByName('PassYards').AsString;
          OutRec.PassingTDs[i] := FieldByName('PassTD').AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyFootballPassingStats := OutRec;
end;

//Rushing
function GetFantasyFootballRushingStats: FantasyFootballRushingStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyFootballRushingStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.Rushes[i] := '';
    OutRec.Yards[i] := '';
    OutRec.TDs[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */dbo.p_SI_Get_NFL_Weekly_Leaders ' + QuotedStr('Top RB'));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := FieldByName('Rank').AsString;
          OutRec.Rushes[i] := FieldByName('Rush').AsString;
          OutRec.Yards[i] := FieldByName('RushYards').AsString;
          OutRec.TDs[i] := FieldByName('RushTD').AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyFootballRushingStats := OutRec;
end;

//Receiving
function GetFantasyFootballReceivingStats: FantasyFootballReceivingStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyFootballReceivingStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.Receptions[i] := '';
    OutRec.Yards[i] := '';
    OutRec.TDs[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */dbo.p_SI_Get_NFL_Weekly_Leaders ' + QuotedStr('Top WR'));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := FieldByName('Rank').AsString;
          OutRec.Receptions[i] := FieldByName('Receptions').AsString;
          OutRec.Yards[i] := FieldByName('RecYards').AsString;
          OutRec.TDs[i] := FieldByName('RecTD').AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyFootballReceivingStats := OutRec;
end;

//MLB Functions
//Pitching
function GetFantasyBaseballPitchingStats: FantasyBaseballPitchingStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyBaseballPitchingStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.Hits[i] := '';
    OutRec.Walks[i] := '';
    OutRec.Wins[i] := '';
    OutRec.Losses[i] := '';
    OutRec.Saves[i] := '';
    OutRec.Strikeouts[i] := '';
    OutRec.EarnedRuns[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */[dbo].[p_SI_Get_MLB_Daily_Leaders] ' + QuotedStr('Pitcher Stats') + ', 0,' +
        QuotedStr(''));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := IntToStr(i);
          OutRec.Hits[i] := FieldByName('Hits').AsString;
          OutRec.Walks[i] := FieldByName('Walks').AsString;
          OutRec.Wins[i] := FieldByName('Wins').AsString;
          OutRec.Losses[i] := FieldByName('Losses').AsString;
          OutRec.Saves[i] := FieldByName('Saves').AsString;
          OutRec.Strikeouts[i] := FieldByName('Strikeouts').AsString;
          OutRec.EarnedRuns[i] := FieldByName('EarnedRuns').AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyBaseballPitchingStats := OutRec;
end;

//Batting
function GetFantasyBaseballBattingStats: FantasyBaseballBattingStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyBaseballBattingStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.AtBats[i] := '';
    OutRec.Hits[i] := '';
    OutRec.RBI[i] := '';
    OutRec.Doubles[i] := '';
    OutRec.Triples[i] := '';
    OutRec.HomeRuns[i] := '';
    OutRec.Walks[i] := '';
    OutRec.StolenBases[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */[dbo].[p_SI_Get_MLB_Daily_Leaders] ' + QuotedStr('Batter Stats') + ', 0,' +
        QuotedStr(''));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := IntToStr(i);
          OutRec.AtBats[i] := FieldByName('AtBats').AsString;
          OutRec.Hits[i] := FieldByName('Hits').AsString;
          OutRec.RBI[i] := FieldByName('RBI').AsString;
          OutRec.Doubles[i] := FieldByName('Doubles').AsString;
          OutRec.Triples[i] := FieldByName('Triples').AsString;
          OutRec.HomeRuns[i] := FieldByName('HomeRuns').AsString;
          OutRec.Walks[i] := FieldByName('Walks').AsString;;
          OutRec.StolenBases[i] := FieldByName('StolenBases').AsString;;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyBaseballBattingStats := OutRec;
end;

//NBA Functions
//Points
function GetFantasyBasketballStats(StatType: String): FantasyBasketballStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyBasketballStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.Points[i] := '';
    OutRec.Rebounds[i] := '';
    OutRec.Assists[i] := '';
    OutRec.ThreePtFieldGoalsMade[i] := '';
    OutRec.Blocks[i] := '';
    OutRec.Steals[i] := '';
    OutRec.FieldGoalsMade[i] := '';
    OutRec.FieldGoalAttempts[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */[dbo].[p_SI_Get_NBA_Daily_Leaders] ' + QuotedStr(StatType) + ', 0, ' + QuotedStr(''));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := FieldByName('Rank').AsString;
          if (StatType = 'Points') then
            OutRec.Points[i] := FieldByName('Pts').AsString
          else if (StatType = 'Rebounds') then
            OutRec.Rebounds[i] := FieldByName('Reb').AsString
          else if (StatType = 'Assists') then
            OutRec.Assists[i] := FieldByName('Ast').AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyBasketballStats := OutRec;
end;

//NHL Functions
//Points
function GetFantasyHockeyPointsStats: FantasyHockeyPointsStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyHockeyPointsStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.Points[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */[dbo].[p_SI_Get_NHL_Daily_Leaders]' + QuotedStr('Points') + ', 0, ' + QuotedStr(''));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := FieldByName('Rank').AsString;
          OutRec.Points[i] := FieldByName('Pts').AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyHockeyPointsStats := OutRec;
end;

//Goals
function GetFantasyHockeyGoalsStats: FantasyHockeyGoalsStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyHockeyGoalsStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.Goals[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */[dbo].[p_SI_Get_NHL_Daily_Leaders]' + QuotedStr('Goals') + ', 0, ' + QuotedStr(''));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := FieldByName('Rank').AsString;
          OutRec.Goals[i] := FieldByName('Gls').AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyHockeyGoalsStats := OutRec;
end;

//Assists
function GetFantasyHockeyAssistsStats: FantasyHockeyAssistsStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyHockeyAssistsStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.Assists[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */[dbo].[p_SI_Get_NHL_Daily_Leaders]' + QuotedStr('Assists') + ', 0, ' + QuotedStr(''));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := FieldByName('Rank').AsString;
          OutRec.Assists[i] := FieldByName('Ast').AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyHockeyAssistsStats := OutRec;
end;

end.
