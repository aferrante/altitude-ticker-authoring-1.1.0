object MainForm: TMainForm
  Left = 279
  Top = 90
  Width = 1279
  Height = 912
  Caption = 
    'Gametrak Ticker Authoring Application for Altitude Sports Versio' +
    'n 1.1.0  Video Design Software Inc.'
  Color = clSilver
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyUp = FormKeyUp
  PixelsPerInch = 96
  TextHeight = 13
  object ProgramModePageControl: TPageControl
    Left = 8
    Top = 8
    Width = 430
    Height = 915
    Hint = 'Double-click on a graphic to edit its text'
    ActivePage = TabSheet2
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnChange = ProgramModePageControlChange
    object ScheduleTab: TTabSheet
      Caption = 'Schedule'
      ImageIndex = 6
      object SchedulePanel: TPanel
        Left = 2
        Top = 2
        Width = 419
        Height = 882
        BevelWidth = 2
        Color = clSilver
        TabOrder = 0
        object Label24: TLabel
          Left = 14
          Top = 16
          Width = 130
          Height = 16
          Caption = 'Available Playlists'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label25: TLabel
          Left = 14
          Top = 513
          Width = 138
          Height = 16
          Caption = 'Scheduled Playlists'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Panel27: TPanel
          Left = 13
          Top = 284
          Width = 395
          Height = 225
          BevelWidth = 2
          TabOrder = 0
          object Label13: TLabel
            Left = 8
            Top = 2
            Width = 178
            Height = 16
            Caption = 'Scheduling Time Controls'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label15: TLabel
            Left = 150
            Top = 41
            Width = 34
            Height = 16
            Caption = 'Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label19: TLabel
            Left = 19
            Top = 67
            Width = 125
            Height = 16
            Caption = 'Start Enable Time'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label37: TLabel
            Left = 19
            Top = 124
            Width = 120
            Height = 16
            Caption = 'End Enable Time'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label68: TLabel
            Left = 18
            Top = 88
            Width = 105
            Height = 16
            Caption = '(24 HR Format)'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label2: TLabel
            Left = 260
            Top = 41
            Width = 36
            Height = 16
            Caption = 'Time'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object ScheduleEntryStartDate: TDateTimePicker
            Left = 150
            Top = 62
            Width = 97
            Height = 24
            Date = 37340.801306944400000000
            Time = 37340.801306944400000000
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object ScheduleEntryEndDate: TDateTimePicker
            Left = 150
            Top = 117
            Width = 97
            Height = 24
            Date = 37340.801306944400000000
            Time = 37340.801306944400000000
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object ScheduleEntryStartTime: TDateTimePicker
            Left = 259
            Top = 62
            Width = 113
            Height = 24
            Date = 38760.853983645840000000
            Time = 38760.853983645840000000
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Kind = dtkTime
            ParentFont = False
            TabOrder = 2
          end
          object ScheduleEntryEndTime: TDateTimePicker
            Left = 259
            Top = 117
            Width = 113
            Height = 24
            Date = 38760.853983645840000000
            Time = 38760.853983645840000000
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Kind = dtkTime
            ParentFont = False
            TabOrder = 3
          end
          object BitBtn42: TBitBtn
            Left = 79
            Top = 172
            Width = 238
            Height = 31
            Caption = '&Add Playlist To Schedule'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 4
            OnClick = BitBtn42Click
            Glyph.Data = {
              E6000000424DE60000000000000076000000280000000E0000000E0000000100
              0400000000007000000000000000000000001000000000000000000000000000
              BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              3300333333333333330033333333333333003333300033333300333330F03333
              3300333330F033333300330000F000033300330FFFFFFF033300330000F00003
              3300333330F033333300333330F0333333003333300033333300333333333333
              33003333333333333300}
          end
        end
        object AvailablePlaylistsGrid: TtsDBGrid
          Left = 13
          Top = 34
          Width = 393
          Height = 247
          CellSelectMode = cmNone
          CheckBoxStyle = stCheck
          ColMoving = False
          Cols = 6
          ColSelectMode = csNone
          DatasetType = dstStandard
          DataSource = dmMain.dsTicker_Groups
          DefaultRowHeight = 20
          ExactRowCount = True
          ExportDelimiter = ','
          FieldState = fsCustomized
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          GridMode = gmListBox
          HeadingFont.Charset = DEFAULT_CHARSET
          HeadingFont.Color = clWindowText
          HeadingFont.Height = -13
          HeadingFont.Name = 'MS Sans Serif'
          HeadingFont.Style = [fsBold]
          HeadingHeight = 18
          HeadingParentFont = False
          ParentFont = False
          ParentShowHint = False
          RowChangedIndicator = riAutoReset
          RowMoving = False
          RowSelectMode = rsSingle
          ShowHint = False
          TabOrder = 1
          Version = '2.20.26'
          XMLExport.Version = '1.0'
          XMLExport.DataPacketVersion = '2.0'
          DataBound = True
          ColProperties = <
            item
              DataCol = 1
              FieldName = 'Ticker_Mode'
              Col.FieldName = 'Ticker_Mode'
              Col.Heading = 'Mode'
              Col.Width = 47
              Col.AssignedValues = '?'
            end
            item
              DataCol = 2
              FieldName = 'Station_ID'
              Col.FieldName = 'Station_ID'
              Col.Heading = 'Playout ID'
              Col.Width = 81
              Col.AssignedValues = '?'
            end
            item
              DataCol = 3
              FieldName = 'Playlist_Description'
              Col.FieldName = 'Playlist_Description'
              Col.Heading = 'Playlist Description'
              Col.Width = 242
              Col.AssignedValues = '?'
            end
            item
              DataCol = 4
              FieldName = 'Entry_Date'
              Col.FieldName = 'Entry_Date'
              Col.Heading = 'Entry Date/Time'
              Col.Width = 154
              Col.AssignedValues = '?'
            end
            item
              DataCol = 5
              FieldName = 'Edit_Date'
              Col.FieldName = 'Edit_Date'
              Col.Heading = 'Edit Date/Time'
              Col.Width = 154
              Col.AssignedValues = '?'
            end
            item
              DataCol = 6
              FieldName = 'Playlist_ID'
              Col.FieldName = 'Playlist_ID'
              Col.Heading = 'Playlist ID'
              Col.Width = 133
              Col.AssignedValues = '?'
            end>
        end
        object ScheduledPlaylistsGrid: TtsDBGrid
          Left = 13
          Top = 532
          Width = 393
          Height = 233
          CellSelectMode = cmNone
          CheckBoxStyle = stCheck
          ColMoving = False
          Cols = 7
          ColSelectMode = csNone
          DatasetType = dstStandard
          DataSource = dmMain.dsScheduled_Ticker_Groups
          DefaultRowHeight = 20
          ExactRowCount = True
          ExportDelimiter = ','
          FieldState = fsCustomized
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          GridMode = gmListBox
          HeadingFont.Charset = DEFAULT_CHARSET
          HeadingFont.Color = clWindowText
          HeadingFont.Height = -13
          HeadingFont.Name = 'MS Sans Serif'
          HeadingFont.Style = [fsBold]
          HeadingHeight = 18
          HeadingParentFont = False
          ParentFont = False
          ParentShowHint = False
          RowChangedIndicator = riAutoReset
          RowMoving = False
          RowSelectMode = rsSingle
          ShowHint = False
          TabOrder = 2
          Version = '2.20.26'
          XMLExport.Version = '1.0'
          XMLExport.DataPacketVersion = '2.0'
          OnDblClick = ScheduledPlaylistsGridDblClick
          DataBound = True
          ColProperties = <
            item
              DataCol = 1
              FieldName = 'Ticker_Mode'
              Col.FieldName = 'Ticker_Mode'
              Col.Heading = 'Mode'
              Col.Width = 48
              Col.AssignedValues = '?'
            end
            item
              DataCol = 2
              FieldName = 'Station_ID'
              Col.FieldName = 'Station_ID'
              Col.Heading = 'Playout ID'
              Col.Width = 81
              Col.AssignedValues = '?'
            end
            item
              DataCol = 3
              FieldName = 'Playlist_Description'
              Col.FieldName = 'Playlist_Description'
              Col.Heading = 'Playlist Description'
              Col.Width = 220
              Col.AssignedValues = '?'
            end
            item
              DataCol = 4
              FieldName = 'Start_Enable_Time'
              Col.FieldName = 'Start_Enable_Time'
              Col.Heading = 'Start Time'
              Col.Width = 154
              Col.AssignedValues = '?'
            end
            item
              DataCol = 5
              FieldName = 'End_Enable_Time'
              Col.FieldName = 'End_Enable_Time'
              Col.Heading = 'End Time'
              Col.Width = 154
              Col.AssignedValues = '?'
            end
            item
              DataCol = 6
              FieldName = 'Entry_Date'
              Col.FieldName = 'Entry_Date'
              Col.Heading = 'Entry Date'
              Col.Width = 154
              Col.AssignedValues = '?'
            end
            item
              DataCol = 7
              Col.Heading = 'Playlist ID'
              Col.Width = 130
              Col.AssignedValues = '?'
            end>
        end
        object Panel26: TPanel
          Left = 13
          Top = 771
          Width = 395
          Height = 105
          BevelWidth = 2
          TabOrder = 3
          object Label36: TLabel
            Left = 11
            Top = 7
            Width = 243
            Height = 16
            Caption = 'Scheduled Playlist Editing Controls'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object BitBtn44: TBitBtn
            Left = 79
            Top = 66
            Width = 238
            Height = 30
            Cancel = True
            Caption = '&Delete Playlist from Schedule'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            OnClick = BitBtn44Click
            Glyph.Data = {
              DE010000424DDE01000000000000760000002800000024000000120000000100
              0400000000006801000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              333333333333333333333333000033338833333333333333333F333333333333
              0000333911833333983333333388F333333F3333000033391118333911833333
              38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
              911118111118333338F3338F833338F3000033333911111111833333338F3338
              3333F8330000333333911111183333333338F333333F83330000333333311111
              8333333333338F3333383333000033333339111183333333333338F333833333
              00003333339111118333333333333833338F3333000033333911181118333333
              33338333338F333300003333911183911183333333383338F338F33300003333
              9118333911183333338F33838F338F33000033333913333391113333338FF833
              38F338F300003333333333333919333333388333338FFF830000333333333333
              3333333333333333333888330000333333333333333333333333333333333333
              0000}
            NumGlyphs = 2
          end
          object BitBtn43: TBitBtn
            Left = 80
            Top = 26
            Width = 238
            Height = 32
            Caption = '&Edit Schedule Entry Times'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            OnClick = BitBtn43Click
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000120B0000120B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
              000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
              00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
              F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
              0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
              FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
              FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
              0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
              00333377737FFFFF773333303300000003333337337777777333}
            NumGlyphs = 2
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Playlist Data Entry'
      ImageIndex = 1
      object DataEntryPanel: TPanel
        Left = 0
        Top = 0
        Width = 421
        Height = 884
        BevelWidth = 2
        Color = clSilver
        TabOrder = 0
        object Panel5: TPanel
          Left = 8
          Top = 8
          Width = 407
          Height = 868
          BevelWidth = 2
          TabOrder = 0
          object Label1: TLabel
            Left = 11
            Top = 8
            Width = 64
            Height = 16
            Caption = 'Category'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label11: TLabel
            Left = 16
            Top = 535
            Width = 145
            Height = 16
            Caption = 'Available Templates'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object LeagueTab: TTabControl
            Left = 13
            Top = 27
            Width = 384
            Height = 443
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            HotTrack = True
            ParentFont = False
            Style = tsFlatButtons
            TabOrder = 0
            Tabs.Strings = (
              'MLB'
              'NBA'
              'NCAAB'
              'NCAAF'
              'NFL'
              'NHL')
            TabIndex = 0
            OnChange = LeagueTabChange
            OnChanging = LeagueTabChanging
            object GamesDBGrid: TtsDBGrid
              Left = 1
              Top = 41
              Width = 380
              Height = 403
              Hint = 
                'Select the desired text template by clicking on it; Double-click' +
                ' to add a new graphic'
              AutoInsert = False
              CellSelectMode = cmNone
              CheckBoxStyle = stCheck
              Cols = 10
              ColSelectMode = csNone
              DatasetType = dstStandard
              DataSource = dmMain.dsSportbaseQuery
              DefaultRowHeight = 20
              ExactRowCount = True
              ExportDelimiter = ','
              FieldState = fsCustomized
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              GridMode = gmListBox
              HeadingFont.Charset = DEFAULT_CHARSET
              HeadingFont.Color = clWindowText
              HeadingFont.Height = -13
              HeadingFont.Name = 'MS Sans Serif'
              HeadingFont.Style = [fsBold]
              HeadingHeight = 20
              HeadingParentFont = False
              ParentFont = False
              ParentShowHint = False
              RowChangedIndicator = riAutoReset
              RowMoving = False
              ShowHint = True
              TabOrder = 0
              Version = '2.20.26'
              XMLExport.Version = '1.0'
              XMLExport.DataPacketVersion = '2.0'
              OnDblClick = GamesDBGridDblClick
              OnMouseDown = GamesDBGridMouseDown
              DataBound = True
              ColProperties = <
                item
                  DataCol = 1
                  FieldName = 'GDATE'
                  Col.FieldName = 'GDATE'
                  Col.Heading = 'Date'
                  Col.HorzAlignment = htaRight
                  Col.Width = 76
                  Col.AssignedValues = '?'
                end
                item
                  DataCol = 2
                  FieldName = 'League'
                  Col.FieldName = 'League'
                  Col.Heading = 'League'
                  Col.Width = 59
                  Col.AssignedValues = '?'
                end
                item
                  DataCol = 3
                  FieldName = 'VALIAS'
                  Col.ControlType = ctText
                  Col.FieldName = 'VALIAS'
                  Col.Heading = 'Visitor'
                  Col.MaxLength = 50
                  Col.HorzAlignment = htaLeft
                  Col.Width = 107
                  Col.AssignedValues = '?'
                end
                item
                  DataCol = 4
                  FieldName = 'HALIAS'
                  Col.ControlType = ctText
                  Col.FieldName = 'HALIAS'
                  Col.Heading = 'Home'
                  Col.HeadingHorzAlignment = htaLeft
                  Col.HorzAlignment = htaLeft
                  Col.Width = 107
                  Col.AssignedValues = '?'
                end
                item
                  DataCol = 5
                  FieldName = 'VCITY'
                  Col.FieldName = 'VCITY'
                  Col.Heading = 'Visitor City'
                  Col.Width = 200
                  Col.AssignedValues = '?'
                end
                item
                  DataCol = 6
                  FieldName = 'HCITY'
                  Col.FieldName = 'HCITY'
                  Col.Heading = 'Home City'
                  Col.Width = 200
                  Col.AssignedValues = '?'
                end
                item
                  DataCol = 7
                  FieldName = 'Start'
                  Col.FieldName = 'Start'
                  Col.Heading = 'Start Time'
                  Col.HorzAlignment = htaLeft
                  Col.Width = 147
                  Col.AssignedValues = '?'
                end
                item
                  DataCol = 8
                  FieldName = 'GCode'
                  Col.FieldName = 'GCode'
                  Col.Heading = 'Game Code'
                  Col.Width = 105
                  Col.AssignedValues = '?'
                end
                item
                  DataCol = 9
                  FieldName = 'VTeam'
                  Col.FieldName = 'VTeam'
                  Col.Heading = 'Visitor Nickname'
                  Col.Width = 150
                  Col.AssignedValues = '?'
                end
                item
                  DataCol = 10
                  FieldName = 'HTeam'
                  Col.FieldName = 'HTeam'
                  Col.Heading = 'Home Nickname'
                  Col.Width = 150
                  Col.AssignedValues = '?'
                end>
            end
            object OddsDBGrid: TtsDBGrid
              Left = -1
              Top = 41
              Width = 382
              Height = 402
              Hint = 
                'Select the desired text template by clicking on it; Double-click' +
                ' to add a new graphic'
              AutoInsert = False
              CellSelectMode = cmNone
              CheckBoxStyle = stCheck
              Cols = 6
              ColSelectMode = csNone
              DatasetType = dstStandard
              DataSource = dmMain.dsSportbaseQuery
              DefaultRowHeight = 20
              ExactRowCount = True
              ExportDelimiter = ','
              FieldState = fsCustomized
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              GridMode = gmListBox
              HeadingFont.Charset = DEFAULT_CHARSET
              HeadingFont.Color = clWindowText
              HeadingFont.Height = -13
              HeadingFont.Name = 'MS Sans Serif'
              HeadingFont.Style = [fsBold]
              HeadingHeight = 20
              HeadingParentFont = False
              ParentFont = False
              ParentShowHint = False
              RowChangedIndicator = riAutoReset
              RowMoving = False
              ShowHint = True
              TabOrder = 2
              Version = '2.20.26'
              XMLExport.Version = '1.0'
              XMLExport.DataPacketVersion = '2.0'
              DataBound = True
              ColProperties = <
                item
                  DataCol = 1
                  FieldName = 'GameDate'
                  Col.FieldName = 'GameDate'
                  Col.Heading = 'Date'
                  Col.HorzAlignment = htaRight
                  Col.Width = 68
                  Col.AssignedValues = '?'
                end
                item
                  DataCol = 2
                  FieldName = 'Manual'
                  Col.ControlType = ctCheck
                  Col.FieldName = 'Manual'
                  Col.Heading = 'Manual'
                  Col.Width = 58
                  Col.AssignedValues = '?'
                end
                item
                  DataCol = 3
                  FieldName = 'AwayTeam'
                  Col.ControlType = ctText
                  Col.FieldName = 'AwayTeam'
                  Col.Heading = 'Visitor'
                  Col.MaxLength = 50
                  Col.HorzAlignment = htaLeft
                  Col.Width = 140
                  Col.AssignedValues = '?'
                end
                item
                  DataCol = 4
                  FieldName = 'HomeTeam'
                  Col.ControlType = ctText
                  Col.FieldName = 'HomeTeam'
                  Col.Heading = 'Home'
                  Col.HeadingHorzAlignment = htaLeft
                  Col.HorzAlignment = htaLeft
                  Col.Width = 143
                  Col.AssignedValues = '?'
                end
                item
                  DataCol = 5
                  FieldName = 'GameTime'
                  Col.FieldName = 'GameTime'
                  Col.Heading = 'Start Time'
                  Col.HorzAlignment = htaLeft
                  Col.Width = 139
                  Col.AssignedValues = '?'
                end
                item
                  DataCol = 6
                  FieldName = 'GameID'
                  Col.FieldName = 'GameID'
                  Col.Heading = 'Game Code'
                  Col.Width = 132
                  Col.AssignedValues = '?'
                end>
            end
            object SponsorsDBGrid: TtsDBGrid
              Left = 0
              Top = 41
              Width = 381
              Height = 402
              Hint = 
                'Select the desired text template by clicking on it; Double-click' +
                ' to add a new graphic'
              AutoInsert = False
              CellSelectMode = cmNone
              CheckBoxStyle = stCheck
              Cols = 1
              ColSelectMode = csNone
              DatasetType = dstStandard
              DataSource = dmMain.dsSponsor_Logos
              DefaultRowHeight = 20
              ExactRowCount = True
              ExportDelimiter = ','
              FieldState = fsCustomized
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              GridMode = gmListBox
              HeadingFont.Charset = DEFAULT_CHARSET
              HeadingFont.Color = clWindowText
              HeadingFont.Height = -13
              HeadingFont.Name = 'MS Sans Serif'
              HeadingFont.Style = [fsBold]
              HeadingHeight = 20
              HeadingParentFont = False
              ParentFont = False
              ParentShowHint = False
              RowChangedIndicator = riAutoReset
              RowMoving = False
              RowSelectMode = rsSingle
              ShowHint = True
              TabOrder = 1
              Version = '2.20.26'
              XMLExport.Version = '1.0'
              XMLExport.DataPacketVersion = '2.0'
              DataBound = True
              ColProperties = <
                item
                  DataCol = 1
                  FieldName = 'LogoName'
                  Col.ControlType = ctText
                  Col.FieldName = 'LogoName'
                  Col.Heading = 'Sponsor Logo Name'
                  Col.MaxLength = 50
                  Col.HorzAlignment = htaLeft
                  Col.Width = 364
                  Col.AssignedValues = '?'
                end>
            end
          end
          object AvailableTemplatesDBGrid: TtsDBGrid
            Left = 13
            Top = 556
            Width = 381
            Height = 264
            Hint = 
              'Select the desired text template by clicking on it; Double-click' +
              ' to add a new graphic'
            AutoInsert = False
            CellSelectMode = cmNone
            CheckBoxStyle = stCheck
            Cols = 1
            ColSelectMode = csNone
            DatasetType = dstStandard
            DataSource = dmMain.dsQuery2
            DefaultRowHeight = 20
            ExactRowCount = True
            ExportDelimiter = ','
            FieldState = fsCustomized
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            GridMode = gmListBox
            HeadingFont.Charset = DEFAULT_CHARSET
            HeadingFont.Color = clWindowText
            HeadingFont.Height = -13
            HeadingFont.Name = 'MS Sans Serif'
            HeadingFont.Style = [fsBold]
            HeadingHeight = 20
            HeadingParentFont = False
            ParentFont = False
            ParentShowHint = False
            PopupMenu = TemplateTimeEditPopupMenu
            RowChangedIndicator = riAutoReset
            RowMoving = False
            RowSelectMode = rsSingle
            ShowHint = True
            TabOrder = 1
            Version = '2.20.26'
            XMLExport.Version = '1.0'
            XMLExport.DataPacketVersion = '2.0'
            OnDblClick = AvailableTemplatesDBGridDblClick
            OnRowChanged = AvailableTemplatesDBGridRowChanged
            DataBound = True
            ColProperties = <
              item
                DataCol = 1
                FieldName = 'Template_Description'
                Col.ControlType = ctText
                Col.FieldName = 'Template_Description'
                Col.Heading = 'Template Description'
                Col.MaxLength = 50
                Col.HorzAlignment = htaLeft
                Col.Width = 347
                Col.AssignedValues = '?'
              end>
          end
          object AddToPlaylistBtn: TBitBtn
            Left = 13
            Top = 828
            Width = 184
            Height = 29
            Caption = '&Add Entry to Playlist'
            TabOrder = 2
            OnClick = AddToPlaylistBtnClick
            Glyph.Data = {
              E6000000424DE60000000000000076000000280000000E0000000E0000000100
              0400000000007000000000000000000000001000000000000000000000000000
              BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              3300333333333333330033333333333333003333300033333300333330F03333
              3300333330F033333300330000F000033300330FFFFFFF033300330000F00003
              3300333330F033333300333330F0333333003333300033333300333333333333
              33003333333333333300}
          end
          object InsertIntoPlaylistBtn: TBitBtn
            Left = 207
            Top = 828
            Width = 190
            Height = 29
            Caption = '&Insert Entry Into Playlist'
            TabOrder = 3
            OnClick = InsertIntoPlaylistBtnClick
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              33333333FF33333333FF333993333333300033377F3333333777333993333333
              300033F77FFF3333377739999993333333333777777F3333333F399999933333
              33003777777333333377333993333333330033377F3333333377333993333333
              3333333773333333333F333333333333330033333333F33333773333333C3333
              330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
              993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
              333333333337733333FF3333333C333330003333333733333777333333333333
              3000333333333333377733333333333333333333333333333333}
            NumGlyphs = 2
          end
          object SponsorDwellPanel: TPanel
            Left = 16
            Top = 480
            Width = 373
            Height = 41
            BevelWidth = 2
            TabOrder = 4
            object Label5: TLabel
              Left = 61
              Top = 13
              Width = 187
              Height = 16
              Caption = 'Sponsor Logo Dwell (secs)'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object SponsorLogoDwell: TSpinEdit
              Left = 260
              Top = 9
              Width = 51
              Height = 26
              MaxValue = 30
              MinValue = 1
              TabOrder = 0
              Value = 5
            end
          end
          object PromptForData: TCheckBox
            Left = 260
            Top = 535
            Width = 129
            Height = 17
            Alignment = taLeftJustify
            Caption = 'Prompt for Data'
            Checked = True
            State = cbChecked
            TabOrder = 5
          end
          object ManualLeaguePanel: TPanel
            Left = 16
            Top = 481
            Width = 373
            Height = 41
            BevelWidth = 2
            TabOrder = 6
            object Label3: TLabel
              Left = 88
              Top = 13
              Width = 57
              Height = 16
              Caption = 'Header:'
              OnClick = About1Click
            end
            object ManualLeagueSelect: TComboBox
              Left = 153
              Top = 7
              Width = 208
              Height = 28
              AutoDropDown = True
              Style = csDropDownList
              DropDownCount = 20
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -17
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ItemHeight = 20
              ParentFont = False
              TabOrder = 0
            end
          end
          object ManualOverridePanel: TPanel
            Left = 15
            Top = 481
            Width = 374
            Height = 41
            BevelWidth = 2
            TabOrder = 7
            object ManualOverrideBtn: TBitBtn
              Left = 172
              Top = 4
              Width = 194
              Height = 33
              Caption = '&Manual Game Override'
              TabOrder = 0
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000120B0000120B00001000000000000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000003
                333333333F777773FF333333008888800333333377333F3773F3333077870787
                7033333733337F33373F3308888707888803337F33337F33337F330777880887
                7703337F33337FF3337F3308888000888803337F333777F3337F330777700077
                7703337F33377733337FB3088888888888033373FFFFFFFFFF733B3000000000
                0033333777777777773333BBBB3333080333333333F3337F7F33BBBB707BB308
                03333333373F337F7F3333BB08033308033333337F7F337F7F333B3B08033308
                033333337F73FF737F33B33B778000877333333373F777337333333B30888880
                33333333373FFFF73333333B3300000333333333337777733333}
              NumGlyphs = 2
            end
            object ShowOddsOnly: TCheckBox
              Left = 7
              Top = 14
              Width = 146
              Height = 18
              Caption = 'Show Odds Only'
              TabOrder = 1
              OnClick = ShowOddsOnlyClick
            end
          end
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Database Maintenance'
      ImageIndex = 3
      object DBMaintPanel: TPanel
        Left = 1
        Top = 1
        Width = 421
        Height = 883
        BevelWidth = 2
        Color = clSilver
        TabOrder = 0
        object Panel19: TPanel
          Left = 8
          Top = 7
          Width = 407
          Height = 869
          BevelWidth = 2
          TabOrder = 0
          object Label40: TLabel
            Left = 15
            Top = 13
            Width = 60
            Height = 16
            Caption = 'Playlists'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = About1Click
          end
          object DatabaseMaintPlaylistGrid: TtsDBGrid
            Left = 13
            Top = 37
            Width = 383
            Height = 743
            CellSelectMode = cmNone
            CheckBoxStyle = stCheck
            ColMoving = False
            Cols = 4
            ColSelectMode = csNone
            DatasetType = dstStandard
            DefaultRowHeight = 18
            ExactRowCount = True
            ExportDelimiter = ','
            FieldState = fsCustomized
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            GridMode = gmListBox
            HeadingFont.Charset = DEFAULT_CHARSET
            HeadingFont.Color = clWindowText
            HeadingFont.Height = -13
            HeadingFont.Name = 'MS Sans Serif'
            HeadingFont.Style = [fsBold]
            HeadingHeight = 18
            HeadingParentFont = False
            ParentFont = False
            ParentShowHint = False
            PopupMenu = DBMaintPopupMenu
            RowChangedIndicator = riAutoReset
            RowMoving = False
            Rows = 4
            RowSelectMode = rsSingle
            ShowHint = False
            TabOrder = 0
            Version = '2.20.26'
            XMLExport.Version = '1.0'
            XMLExport.DataPacketVersion = '2.0'
            DataBound = False
            ColProperties = <
              item
                DataCol = 1
                FieldName = 'Station_ID'
                Col.FieldName = 'Station_ID'
                Col.Heading = 'Playout ID'
                Col.Width = 81
                Col.AssignedValues = '?'
              end
              item
                DataCol = 2
                FieldName = 'Playlist_Description'
                Col.FieldName = 'Playlist_Description'
                Col.Heading = 'Playlist Description'
                Col.Width = 369
                Col.AssignedValues = '?'
              end
              item
                DataCol = 3
                FieldName = 'Entry_Date'
                Col.FieldName = 'Entry_Date'
                Col.Heading = 'Entry Date'
                Col.Width = 150
                Col.AssignedValues = '?'
              end
              item
                DataCol = 4
                FieldName = 'Edit_Date'
                Col.FieldName = 'Edit_Date'
                Col.Heading = 'Edit Date'
                Col.Width = 150
                Col.AssignedValues = '?'
              end>
          end
          object DBNavigator1: TDBNavigator
            Left = 124
            Top = 8
            Width = 256
            Height = 24
            VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
            TabOrder = 1
          end
          object BitBtn32: TBitBtn
            Left = 16
            Top = 794
            Width = 183
            Height = 31
            Caption = 'Refresh'
            TabOrder = 2
            OnClick = BitBtn32Click
            Glyph.Data = {
              DE010000424DDE01000000000000760000002800000024000000120000000100
              0400000000006801000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
              33333333333F8888883F33330000324334222222443333388F3833333388F333
              000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
              F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
              223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
              3338888300003AAAAAAA33333333333888888833333333330000333333333333
              333333333333333333FFFFFF000033333333333344444433FFFF333333888888
              00003A444333333A22222438888F333338F3333800003A2243333333A2222438
              F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
              22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
              33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
              3333333333338888883333330000333333333333333333333333333333333333
              0000}
            NumGlyphs = 2
          end
          object BitBtn34: TBitBtn
            Left = 212
            Top = 794
            Width = 185
            Height = 31
            Cancel = True
            Caption = 'Delete Selected'
            TabOrder = 3
            OnClick = BitBtn34Click
            Glyph.Data = {
              DE010000424DDE01000000000000760000002800000024000000120000000100
              0400000000006801000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              333333333333333333333333000033338833333333333333333F333333333333
              0000333911833333983333333388F333333F3333000033391118333911833333
              38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
              911118111118333338F3338F833338F3000033333911111111833333338F3338
              3333F8330000333333911111183333333338F333333F83330000333333311111
              8333333333338F3333383333000033333339111183333333333338F333833333
              00003333339111118333333333333833338F3333000033333911181118333333
              33338333338F333300003333911183911183333333383338F338F33300003333
              9118333911183333338F33838F338F33000033333913333391113333338FF833
              38F338F300003333333333333919333333388333338FFF830000333333333333
              3333333333333333333888330000333333333333333333333333333333333333
              0000}
            NumGlyphs = 2
          end
          object DatabaseMaintBreakingNewsPlaylistGrid: TtsDBGrid
            Left = 13
            Top = 37
            Width = 383
            Height = 743
            CellSelectMode = cmNone
            CheckBoxStyle = stCheck
            ColMoving = False
            Cols = 4
            ColSelectMode = csNone
            DatasetType = dstStandard
            DataSource = dmMain.dsTicker_Groups
            DefaultRowHeight = 18
            ExactRowCount = True
            ExportDelimiter = ','
            FieldState = fsCustomized
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            GridMode = gmListBox
            HeadingFont.Charset = DEFAULT_CHARSET
            HeadingFont.Color = clWindowText
            HeadingFont.Height = -13
            HeadingFont.Name = 'MS Sans Serif'
            HeadingFont.Style = [fsBold]
            HeadingHeight = 18
            HeadingParentFont = False
            ParentFont = False
            ParentShowHint = False
            PopupMenu = DBMaintPopupMenu
            RowChangedIndicator = riAutoReset
            RowMoving = False
            RowSelectMode = rsSingle
            ShowHint = False
            TabOrder = 4
            Version = '2.20.26'
            Visible = False
            XMLExport.Version = '1.0'
            XMLExport.DataPacketVersion = '2.0'
            DataBound = True
            ColProperties = <
              item
                DataCol = 1
                FieldName = 'Playlist_ID'
                Col.FieldName = 'Playlist_ID'
                Col.Heading = 'Playout ID'
                Col.Width = 81
                Col.AssignedValues = '?'
              end
              item
                DataCol = 2
                FieldName = 'Playlist_Description'
                Col.FieldName = 'Playlist_Description'
                Col.Heading = 'Playlist Description'
                Col.Width = 369
                Col.AssignedValues = '?'
              end
              item
                DataCol = 3
                FieldName = 'Entry_Date'
                Col.FieldName = 'Entry_Date'
                Col.Heading = 'Entry Date'
                Col.Width = 150
                Col.AssignedValues = '?'
              end
              item
                DataCol = 4
                FieldName = 'Edit_Date'
                Col.FieldName = 'Edit_Date'
                Col.Heading = 'Edit Date'
                Col.Width = 150
                Col.AssignedValues = '?'
              end>
          end
        end
      end
    end
  end
  object PlaylistSaveControlsPnl: TPanel
    Left = 444
    Top = 746
    Width = 813
    Height = 178
    BevelWidth = 2
    TabOrder = 2
    object Label4: TLabel
      Left = 13
      Top = 12
      Width = 181
      Height = 16
      Caption = 'Playlist Name/Description'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 598
      Top = 16
      Width = 164
      Height = 16
      Caption = 'Last Playlist Save Time'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LastSaveTimeLabel: TLabel
      Left = 598
      Top = 35
      Width = 199
      Height = 25
      Alignment = taCenter
      AutoSize = False
      Color = clBlack
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object RegionLabel: TLabel
      Left = 457
      Top = 93
      Width = 51
      Height = 16
      Caption = 'Region'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object IDLabel: TLabel
      Left = 711
      Top = 93
      Width = 32
      Height = 16
      Caption = 'ID #:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SavePlaylistBtn: TBitBtn
      Left = 394
      Top = 135
      Width = 197
      Height = 31
      Caption = 'Sa&ve Playlist'
      Default = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = SavePlaylistBtnClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333330070
        7700333333337777777733333333008088003333333377F73377333333330088
        88003333333377FFFF7733333333000000003FFFFFFF77777777000000000000
        000077777777777777770FFFFFFF0FFFFFF07F3333337F3333370FFFFFFF0FFF
        FFF07F3FF3FF7FFFFFF70F00F0080CCC9CC07F773773777777770FFFFFFFF039
        99337F3FFFF3F7F777F30F0000F0F09999937F7777373777777F0FFFFFFFF999
        99997F3FF3FFF77777770F00F000003999337F773777773777F30FFFF0FF0339
        99337F3FF7F3733777F30F08F0F0337999337F7737F73F7777330FFFF0039999
        93337FFFF7737777733300000033333333337777773333333333}
      NumGlyphs = 2
    end
    object LoadPlaylistBtn: TBitBtn
      Left = 183
      Top = 135
      Width = 198
      Height = 31
      Caption = '&Load Existing Playlist'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = LoadPlaylistBtnClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
        333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
        0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
        07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
        0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
        33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
        B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
        3BB33773333773333773B333333B3333333B7333333733333337}
      NumGlyphs = 2
    end
    object ExpandButton: TBitBtn
      Left = 13
      Top = 135
      Width = 158
      Height = 31
      Caption = 'Expand Playlist'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = ExpandButtonClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33033333333333333F7F3333333333333000333333333333F777333333333333
        000333333333333F777333333333333000333333333333F77733333333333300
        033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
        33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
        3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
        33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
        333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
        333333773FF77333333333370007333333333333777333333333}
      NumGlyphs = 2
    end
    object SaveClosePlaylistBtn: TBitBtn
      Left = 599
      Top = 135
      Width = 202
      Height = 31
      Caption = 'Save && &Close Playlist'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = SaveClosePlaylistBtnClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333330070
        7700333333337777777733333333008088003333333377F73377333333330088
        88003333333377FFFF7733333333000000003FFFFFFF77777777000000000000
        000077777777777777770FFFFFFF0FFFFFF07F3333337F3333370FFFFFFF0FFF
        FFF07F3FF3FF7FFFFFF70F00F0080CCC9CC07F773773777777770FFFFFFFF039
        99337F3FFFF3F7F777F30F0000F0F09999937F7777373777777F0FFFFFFFF999
        99997F3FF3FFF77777770F00F000003999337F773777773777F30FFFF0FF0339
        99337F3FF7F3733777F30F08F0F0337999337F7737F73F7777330FFFF0039999
        93337FFFF7737777733300000033333333337777773333333333}
      NumGlyphs = 2
    end
    object SingleLoopEnable: TCheckBox
      Left = 214
      Top = 35
      Width = 247
      Height = 26
      Caption = ' Set Ticker to Single Loop Mode'
      Color = clSilver
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 4
      OnClick = SingleLoopEnableClick
    end
    object RegionSelect: TComboBox
      Left = 511
      Top = 88
      Width = 186
      Height = 24
      Style = csDropDownList
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemHeight = 16
      ParentFont = False
      TabOrder = 5
      OnChange = RegionSelectChange
    end
    object RegionSelectMode: TRadioGroup
      Left = 13
      Top = 80
      Width = 433
      Height = 36
      Columns = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemIndex = 0
      Items.Strings = (
        'Save as Global'
        'Save to Specified Region')
      ParentFont = False
      TabOrder = 6
      OnClick = RegionSelectModeClick
    end
    object RegionIDNum: TEdit
      Left = 748
      Top = 88
      Width = 50
      Height = 24
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 50
      ParentFont = False
      ReadOnly = True
      TabOrder = 7
      Text = '0'
    end
    object Edit1: TMaskEdit
      Left = 15
      Top = 35
      Width = 109
      Height = 24
      EditMask = 'TK99999999;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 10
      ParentFont = False
      TabOrder = 8
      Text = 'TK        '
    end
  end
  object PlaylistSelectTabControl: TTabControl
    Left = 444
    Top = 8
    Width = 812
    Height = 738
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Tabs.Strings = (
      'Ticker'
      'Alerts'
      'Templates')
    TabIndex = 0
    OnChange = PlaylistSelectTabControlChange
    object Panel9: TPanel
      Left = 3
      Top = 37
      Width = 801
      Height = 698
      BevelWidth = 2
      TabOrder = 0
      object Label34: TLabel
        Left = 18
        Top = 8
        Width = 104
        Height = 16
        Caption = 'Playlist Entries'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label69: TLabel
        Left = 18
        Top = 23
        Width = 128
        Height = 16
        Caption = 'Number of Entries:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object NumEntries: TLabel
        Left = 147
        Top = 24
        Width = 9
        Height = 16
        Caption = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Image1: TImage
        Left = 624
        Top = 2
        Width = 174
        Height = 40
        Picture.Data = {
          07544269746D6170C6540000424DC6540000000000003600000028000000AF00
          000029000000010018000000000090540000120B0000120B0000000000000000
          0000D8D8D8D5D5D5D5D5D5D5D5D5D4D4D4D5D5D5D5D5D5D4D4D4D4D4D4D4D4D4
          D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4
          D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4
          D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4
          D4D4D4D4D4D4D4D4D4D3D3D3D3D3D3D3D3D3D4D4D4D4D4D4D3D3D3D3D3D3D3D3
          D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3
          D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D2D2D2D2D2D2D2D2D2D2D2D2
          D3D3D3D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2
          D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D1D1D1D1D1D1D2D2D2D2D2D2D1D1D1D1
          D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1
          D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1
          D1D1D1D1D1D1D1D0D0D0D0D0D0D1D1D1D1D1D1D1D1D1D0D0D0D0D0D0D0D0D0D0
          D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0
          D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0
          D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0CFCFCFCFCFCFD0
          D0D0CFCFCFD0D0D0D0D0D0CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF
          CFCFCFCFCFCFCFCFCFCFCFCFD4D4D4110604D3D3D3D0D0D0D0D0D0D0D0D0D0D0
          D0D0D0D0D0D0D0D0D0D0CFCFCFCFCFCFD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0CF
          CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF
          CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCECECECECE
          CECFCFCFCECECECECECECECECECECECECECECECECECECECECECECECECECECECE
          CECECECECECECECECECECECECECECECECECECECECDCDCDCDCDCDCDCDCDCECECE
          CECECECDCDCDCDCDCDCECECECECECECDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCD
          CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCCCCCCCDCDCDCD
          CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCCCCCCCCCCCCCDCDCDCCCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCBCBCBCCCCCCCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCACBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACBCBCBCBCBCBCACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACAC9C9C9CACACACACACACACACACDCDCD58
          D058D4D4D4D1D1D1D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0CFCFCFD0D0D0D0D0D0
          CFCFCFCFCFCFCFCFCFCFD0D0CFCFCFCFCFCFD0CFD0CECFCFCDCFCECFCFCFD0CE
          D0CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCECFCEC8CBCBC5C8C8C5C6C7C3
          C5C6C2C5C6C7C9C9CCCDCDCFCFCFCFCFCFCFCFCFCECECECECECECDCECECDCECE
          CECFCFCDCECECECECECDCFCFCDCECECDCDCDCECDCECDCECECDCECECDCECDCDCE
          CDCDCECECDCECECDCDCDCDCDCDCECECECDCDCDCCCDCDCCCCCCCDCDCDCDCDCDCC
          CDCCCCCDCCCDCCCDCCCDCCCDCDCDCDCCCCCCCDCCCCCDCCCBCDCBCBCDCCCCCDCC
          CDCDCDCCCCCCCDCDCDCCCDCCCDCDCDCCCCCCCBCCCDCBCCCDCCCCCDCBCCCCCCCC
          CACBCCCCCACCCBCCCCCACCCCCCCCCCCBCBCCCBC9CCCBCACCCCCCCCCCCBCCCCCC
          CCCCCBCCCBCCCBCBCCCBCCCCCCCCCBCBCBCCCBCBCBCBCBC9CBCBCACBCBCCCCCB
          CBCCCACBCBCACBCBCACCCCCCCBCBCBCACACACBCBCBCBCACBCACBCBCACBCACACB
          C9CACBCACACBCACACACBC9CACBCACACBC9CACBCACACACBCACACBCACACACACBCB
          C9CAC9CBCAC9CBCACACBC9CACBC9CACBCACACBC9C9CBC9CBCBC9CBCACBCBCAC9
          CACAC8CBCACBCACACACBCBCACACBCBC9CACAC8C9CACBCACACACACACACAC9CACB
          CAC9CAC9C9C9C9C8C9CACACACACAC9CAC9CACACACAC9CACAC8C9CAC8C9CAC8CA
          CAC9CACAC9C9CAC9CACAC9CACAC9CACACAC9CACAC9C9CAC9C9CACACACACACACA
          C9C9C9CACACACACACACACACACFCFCF0DCF02D3D3D3D1D1D1D1D1D1D1D1D1D0D0
          D0D0D0D0D0D0D0CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFD0D0D0CFD0CFCECFD0CD
          CFCFCECFCECFCFCECFCFCFCFCFCFCFCFCFCECECEC4C6C7B4BCBDA7B2B396A3A6
          8390957182885E7A805A747C596F77536D75536E765B747A6A7E8481949A9DAA
          AFB3BABCC3C7C7CECFCFCECECECECECECECECECBCECECDCECECDCDCDCDCECDCD
          CDCECECECDCDCECDCDCECECCCECECDCECECDCECDCDCECDCCCCCBC8C8C9C8C8C9
          CCCCCCCDCDCDCDCDCDCDCDCDCDCDCDCDCDCCCDCDCBCBCCCCC7CDCCC9CCCCCCCB
          CDCCCCCCCCCCCCCBCBCCCBCBCDCCCDCCCDCDCDCDCDCDCDCDCDCBCDCDCACDCDCB
          CCCCCBCBCCCBCACCC9CCCCCACDCCCACCCACBCCCBCCCCCCCDCDCBCCCCCCCCCCCC
          CCCCCCCACCCBCBCCCCCCCCCCCBCCCCCACCCBCBCCCACCCBCBCBCCCBCBCCCBCBCC
          CBCACBC9CBCBCBCACACCCBCACCCBCBCBCACBCBCACBCBCBCBCBCCCCCCCCCBCCCB
          CACBCBCBCBCBCACBCBCACBCBCBCBCACBCBCBCACBCBCBCBC9CBCBC7CACBCACACA
          CACACACACAC8C9CAC9C9CAC9C9CBCBC9CBC9C9CACAC9C9CACACBC8CBCBC9C9CB
          CACACAC9CACBCACBCBCACAC9CACAC9C9C7CAC7CAC8CACAC9C9CACAC8CACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACAC9C8C9C9C8C9C9C9C9CAC9CACAC8C8CAC9C9CACAC9CACAC9
          CAC9CACAC8CACAC8CACAC8CACACACACAC9C9C9CACACACACACACACACACFCFCF49
          EE49D3D3D3D0D0D0D1D1D1D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0CFCFCFCFCFCF
          CFCFCFCFCFCFCFCFCFCFCFCFCECECFCECFCED0D0D0CFCFCFC7CACBB7BCBD919E
          A164787C4A616637545B294A521E414A193A4517384517384417384417374417
          374417374417364217364217394522424F354E5B485F676A7E839BA9ACBFC4C4
          CDCDCDCECECECDCECECBCECDCECDCDCECECECDCDCDCDCDCDCDCDCDCDCDCDCDCD
          CDCDCDCDCECECEC0C3C38E999C6B7D8463798178889099A4A7B0B8BABFC3C4CB
          CCCCCDCDCDCDCDCDCCCDCDCBCDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCDCCCDCDCDCCCCCCCDCD
          CDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCCCCCCCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCA
          CBCACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACBCBCBCBCBCBCACACACACACAC9C9C9C6C6C6C3C3C3C2C2
          C2C1C1C1C1C1C1C1C1C1C1C1C1C3C3C3C8C7C7CACACACACACACACACACACACACA
          CACACACACAC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
          C9C9C9CACACAC9C9C9CACACACFCFCF088D02D2D2D2D0D0D0D0D0D0D0D0D0D0D0
          D0D0D0D0D0D0D0CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCECFCECFCFCFD0
          D0D0C0C5C7919FA3566F7635515A203D4817343E17343E173642173744173A46
          173E48173F4A17404C17434D17444F17455117455017455017465017434D173D
          4B17364817314116323F24424C405D62738588B0B7B9CDCDCDCECECECECECECE
          CECECDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCECECEC9C9C9ADB4B682979D
          466770193E461D37412E4956435F676C7F849FA7ABBFC5C5CDCDCDCDCDCDCDCD
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCDCDCDCDCDCDB5B4B48D8C8C8B8B8B8D8D8D8D8D8D8D8D8D8D8D8D8B8B8B
          898989A0A0A0C8C8C8CCCCCCCBCBCBCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBBCBDBC9292928787878D8D8D8C8C8C8C8C8C8C
          8C8C8C8C8C8C8C8C8C8C8C8C8C8C8B8B8B8C8C8C8D8D8D919191979797A1A1A1
          ADADADB5B5B5BDBDBDC5C5C5CBCBCBCBCBCBCBCBCBCBCBCBCACACACACACACACA
          CACACACACACACACACACAC9C9C9CACACACBCBCBCACACAC4C4C4B9B9B9ADADAD9A
          9A9A8282827070706262625656565050504D4D4D4E4E4E4E4E4E4F4F4F575757
          6868687A7A7A939393ABABABB9B9B9C7C7C7CACACACACACAC9C9C9C9C9C9C9C9
          C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9CACACACACACACACACACFCFCF04
          6404D1D1D1D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0CFCFCFCFCFCFCFCFCF
          CFCFCFCFCFCFD0D0D0D0D0D0C6C9CA8E9CA048626A1E404C183541183744173C
          4819404D1A434E19444D1844501945521A46551A4958184C5B184F5E17516017
          5362175262185364185364175363175263175162174F5F174A5A164051163747
          16374433515A6B7E84AFB4B7CECECECECECECECDCDCDCDCDCDCDCDCDCDCDCDCD
          CDCDCDCECCCECCCDCDCBCDCDCDCDCDCDC5C9C996A7A84B687018324215293A15
          354124435049626B7D8D91B1B8B9CDCDCDCDCDCDCCCDCCCBCCCBCBCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCDCDCDCDCDCD7D7D7D1B1B1B1616
          16191919191919191919191919181818151515282828949494CCCCCCCCCCCCCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCCCCCCB2B2B2
          3B3B3B1313131919191919191919191818181919191818181717171616161616
          161616161616161818181A1A1A2222222E2E2E3A3A3A474747575757797979A1
          A1A1BDBDBDCBCBCBCBCBCBCBCBCBCACACACACACACACACACACACACBCBCBC5C5C5
          ACACAC7E7E7E5757574343432F2F2F1D1D1D1414141313131313131212121212
          121212121212121212121212121212121212121212121919192C2C2C41414161
          6161909090BCBCBCCACACACACACAC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
          C9C9C9CACACACACACACACACACFCFCF5EF060D2D2D2D0D0D0D0D0D0D0D0D0D0D0
          D0D0D0D0D0D0D0D0D0D0CFCFCFCFCFCFD0CFCFCFCFCFCFCFCFAAB4B4536B711E
          3E46173641183E4A1B414D1A414D18424D18444F174752184A56194D5B18505F
          185364195566195868195A6C185D6D195D701B5D721C5F741B5E731A5F741A5F
          72195D71185C6F195A6E18586B185567164E60164252163A47314B587F8C92C1
          C4C5CECECECECECECDCDCDCDCDCDCDCDCDCDCDCDCCCDCDCBCCCCCCCCCCCDCDCD
          CDCDCDCDCDCDC3C8C8798E93233F4C153143153D4A153843173943334C587886
          8DBBBFC0CDCDCDCDCDCCCACDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCBCBCB6D6D6D151515141414141414141414141414141414151515
          141414141414383838AAAAAACCCCCCCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCCCCCCC6C6C65D5D5D13131314141413131313131313
          13131313131313131414141D1D1D2222222323232222221F1F1F1D1D1D1A1A1A
          1515151313131313131313131515152626264242427F7F7FBBBBBBCACACACBCB
          CBCACACACACACACACACACACACABCBCBC54545413131313131312121212121212
          12121515151D1D1D2525252B2B2B2C2C2C2C2C2C2A2A2A282828272727232323
          1F1F1F1B1B1B1616161212121212121212121B1B1B414141858585C0C0C0CACA
          CACACACAC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9CACACACACACACACACACFCFCF68
          4C68D1D1D1D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0CFCFCFCECFCE
          D0D0D0CDCFCF8B9C9E324E5318363E173D4719434E1B414E19414E18444F1848
          55174E5A1853601B54641C57691A596C1B5C701C5F721C62751C64771D667A1E
          697E1F6A811F6C821F6D831F6D841F6E82206D811F6B801E697D1C667B186478
          166173185C6F1754661645561A3E4C486069A2AAAECECECECECECECDCCCCCDCD
          CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCCCDCDCBCECDCDCDCDCDCDCD8B9C9F25
          424F153444154654154655153B4D1638473F586298A2A6CDCDCDCDCDCDCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCBCBCBCDCDCDC5C5C55757571515151414
          141414141515151616161818181919191919191515151414144C4C4CBABABACC
          CCCCCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCCCCCCCCCCCC
          9393932020201313131414141515151515151818182828284141413C3C3C2D2D
          2D2828282525251F1F1F1D1D1D1E1E1E1F1F1F2222222222221D1D1D17171713
          1313131313161616393939808080C2C2C2CBCBCBCBCBCBCACACACBCBCBC7C7C7
          6969691313131212121313131E1E1E2F2F2F3838383535352E2E2E2323231C1C
          1C1818181717171717171717171515151414141414141818181C1C1C1B1B1B16
          1616121212121212171717505050B2B2B2CACACACACACAC9C9C9C9C9C9C9C9C9
          C9C9C9CACACACACACACACACACFCFCF50BC50D0D0D0D0D0D0D0D0D0D0D0D0D0D0
          D0D0D0D0D0D0D0D0D0D0CFCFCFD0D0D0CBCECE798D9025424D18354218434D1A
          434E19414F1742511747551A4E5D1A53621955671A596A1B5C6E1C5E731C6176
          1D657A1E687E206C81216F8523728925758F257892267D962880992882992A81
          982B7F962A7C9227789023748D2070881D6B831B677D19627917607216546516
          3F532E4C5A809197C8CACACDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCD
          CDCDCDCBCDCDCDCCCCCDCDCDCDCDCD7C8F931B3946153948164959165162154F
          5F153E4F25465471868AC0C5C5CDCDCDCDCDCCCBCCCBCBCCCCCCCCCCCCCCCCCB
          CBCBCDCDCDB5B5B53E3E3E1515151414141515151717171919191C1C1C202020
          2020201B1B1B141414141414636363C7C7C7CCCCCCCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCCCCCCB7B7B740404013131313131314141418
          18182B2B2B4646464141413F3F3F4242423C3C3C3C3C3C3838382E2E2E242424
          1C1C1C1414141313131A1A1A2121212121211B1B1B1414141313131818185353
          53A8A8A8CBCBCBCBCBCBCACACACBCBCB9F9F9F2A2A2A1212121C1C1C2E2E2E28
          28281D1D1D1C1C1C2424243B3B3B5454546565656C6C6C717171747474686868
          5656563A3A3A1B1B1B1212121919191E1E1E1C1C1C1616161212121212124D4D
          4DB8B8B8CACACAC9C9C9C9C9C9C9C9C9C9C9C9CACACACACACACACACACFCFCF0B
          A00BD0D0D0D1D1D1D0D0D0D0D0D0D0CFD0D0D0D0CFD0D0CDD0CFD0D0D0D0D0D0
          83959A1D39451836451843501A424D1A424F194555184A5A1851601B56661B5A
          6B1B5C6E1C5F721E63771E667B1F687F206D8521708923768D277E922B849B33
          8BA93C93B1419EBA43A6BF43A8BE42A3BD3C9CB63894AF368DA730869F2C8098
          27799223748C236E861D687D176577165D7116485C1B4353627A80BBC0C0CECE
          CECDCECECACDCECBCECECBCDCDCBCECDCDCECDCDCCCDCDCDCDCCCDCCCDCDCDC3
          C8C856737815323E15424C17505D1A586C1A596E154B5E1540504A6D77ADB8BA
          CDCDCDCBCDCAC9CCCBCCCBCDCCCCCCCCCCCCCDCDCD9E9E9E2A2A2A1414141515
          151616161919191E1E1E2727272F2F2F2E2E2E2727271C1C1C14141419191983
          8383CCCCCCCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCACACACBCBCB
          C6C6C65B5B5B1313131313131313131B1B1B3A3A3A3D3D3D191919747474BEBE
          BEB7B7B7B7B7B7B6B6B6AFAFAFA5A5A59696967C7C7C4545451A1A1A13131319
          19191E1E1E1D1D1D1919191313131212122F2F2F959595CACACACACACACACACA
          B8B8B84646461313131414141B1B1B3232326666668C8C8CA2A2A2BABABAC6C6
          C6CACACACACACACACACACACACACACACAC7C7C7BBBBBB93939345454513131315
          15151F1F1F202020191919121212121212707070CACACACACACAC9C9C9C9C9C9
          C9C9C9CACACACACACACACACACFCFCF4AC24AD0D0D0D1D1D1D0D0D0D0D0D0D0D0
          D0D0D0D0D0D0D0D0D0D0D0D0D09CA7A92A444D1834451A425119424B19424D18
          4653174B5B1951631A57681B5A6C1C5D711D60731F647921687C226B80236F86
          25758C287B902A839A308BA93D9CB74CB1C556C1CD59C9CE4EBFCF36ABCB299D
          C02998B52B93AF2F90A9308EA62E879F2A829A287C9427778F2071891A6C7F1A
          6678186174165165163D4D435E659AA4A6CECECECDCDCDCDCDCDCDCDCDCCCCCD
          CBCDCDCCCDCCCDCDCDCCCDCDCDCDCDCDCDCD82959B173542153D45174E5B1859
          70185D7317607415576B1543573D646FA7B3B7CDCDCDCACDCACBCBCBCCCCCCCD
          CDCDCDCDCD8383831C1C1C1515151515151616161D1D1D353535414141303030
          3333333939392D2D2D1C1C1C1414142B2B2B9B9B9BCCCCCCCCCCCCCBCBCBCACA
          CACBCBCBCBCBCBCBCBCBCBCBCBCCCCCCCBCBCB7979791818181313131313131C
          1C1C383838353535131313656565CACACACBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBBEBEBE8E8E8E4646461414141313131919191E1E1E1B1B1B1515
          15131313292929999999CACACACACACAC6C6C65D5D5D1212123A3A3A838383B3
          B3B3CACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACAAEAEAE3737371212121B1B1B2929292828281C1C1C1212
          12414141B7B7B7CACACAC9C9C9C9C9C9C9C9C9CACACACACACACACACACFCFCF21
          7522D0D0D0D0D0D0D0D0D0D0D0D0CFD0CFCED0CED0D0CFD0D0D0C2C6C751686F
          18313C18424F1B414E18414E1846531A4C5C1852631957691B5A6E1C5F721C62
          751D64791F697F206C81226F8624748C297C942D839A3390A93EA0C151BBCD5D
          CFCF51CBCE31B3C7249CBF2E92AF2F8AA0247D95187089166C891B738F237A94
          277D95287D95257B9224778E2172881E6C821B6579176175165669163D4E2744
          52718288B6BDBECDCDCDCDCDCDCDCDCDCCCDCDCBCDCCCCCCCCCDCDCDCDCDCDC8
          CCCB677F86153342153C47164C5C165B7216627715657916667C165F7615495E
          3F6A77AFBBBDCDCDCDCDCCCACCCCCCCDCDCDC9C9C96868681515151515151515
          151818183131314C4C4C4444443535351D1D1D2929293737372D2D2D19191914
          14143A3A3AAAAAAACCCCCCCBCBCBCACACACBCBCBCBCBCBCBCBCBCBCBCBCCCCCC
          CBCBCBA7A7A73333331313131313131B1B1B3131313434341313133E3E3EB7B7
          B7CBCBCBCACACACACACACACACACACACACACACACACACACBCBCBCBCBCBBBBBBB63
          63631919191313131818181E1E1E1E1E1E171717131313303030A2A2A2CBCBCB
          CBCBCB9E9E9E7A7A7AB0B0B0CACACACACACACACACACACACACACACACACACAC9C9
          C9C9C9C9C9C9C9CACACACACACACACACACACACACACACACACACAB4B4B43D3D3D12
          12121919193030303737372929291212122A2A2AA4A4A4CACACAC9C9C9C9C9C9
          C9C9C9CACACACACACACACACACFCFCF644A64D0D0D0D0D0D0D0D0D0D0D0D0CFD1
          CECFD0CDD0D0CFD0D0D08E9C9E233D49183B4A1B424F1B424E184652184C5B1A
          516219566A195C6E1B5F721D63761E657A1E677F1F6C83206F8624738B267992
          2D819B328BAA3E9FBB55BBCB58CECF3CC0CB24A3C051A2BC8DB3BFADBFC2B3C0
          C0A3B6B982A2A850828E206072165A701A69801F728A22758C25768D24738C22
          70881F6C821C667C1B6275185A6E16465A16394838575F7D8E8FB6BDBDCCCECE
          CDCDCDCDCDCDCDCDCDCDCDCDCDCDCD8A999925474D15364517424F184F61185E
          741A667A1B6B801B6B821B6A8317657B15516455808AC1C6C8CDCDCDCCCCCCCD
          CDCDC0C0C05555551515151515151414142020204848483939393B3B3B959595
          4B4B4B1414142323233232322A2A2A1818181414144D4D4DBEBEBECCCCCCCCCC
          CCCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBC4C4C45F5F5F13131314141418
          18182B2B2B3434341B1B1B1D1D1D949494CBCBCBCBCBCBCBCBCBCACACACACACA
          CACACACACACACACACACBCBCBCBCBCBC5C5C56464641313131313131818181F1F
          1F1E1E1E1616161313134C4C4CBBBBBBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCA
          CACACACACACACACACACACACACACACBCBCBCACACACACACACACACACACACACACACA
          C6C6C6BCBCBC9393934A4A4A1616161212122020203B3B3B4646463737371515
          15282828A3A3A3CACACAC9C9C9C9C9C9C9C9C9CACACACACACACACACACFCFCF45
          9111D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0CFD0CED0D0D0CCCECE5E757D18343F
          18424F194251184353194A591952621B56671B5A6D1A5F711C62751F64791F67
          7E1E6981216E8624738B27788F277E973188A23A98B447B0CA5BC3CF37B9C921
          9DB972ADBFC3CACCCFCFCECFCFCFCFCFCFCECECECFCFCFC8CBCB9BACAD4B717C
          184A5D16546C18697F1C6E851E6F89227089226F87216D821F687D1C62771A5C
          70165060163E4C183A453251585E73778A989AA4B0B1AEB8B8A2ADAD6A7F8224
          434C153641153F4C174A5919576B1A63771D6A80206F862071881D71891C7187
          15657E1B5E767D9DA6CDCDCDCDCDCDCDCDCDB9B9B94848481515151414141616
          162F2F2F4E4E4E222222434343C5C5C5A8A8A82B2B2B14141420202032323228
          2828141414141414636363C3C3C3CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CCCCCCCCCCCC8888881D1D1D1414141515152525253535352525251313135D5D
          5DC4C4C4CBCBCBCACACACACACACACACACACACACACACACACACACACACACACACACB
          CBCBBDBDBD4D4D4D1313131313131A1A1A232323212121141414151515707070
          CACACACBCBCBCACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACAC2C2C2B3B3B39696967171715353533A3A3A1A1A1A1212121212121D
          1D1D3838385353535656563B3B3B121212414141B8B8B8CACACAC9C9C9C9C9C9
          C9C9C9CACACACACACACACACACFCFCF5C8C5CD0D0D0D1D1D1D0D0D0D0D0D0D0D0
          D0CFD0CFD0D0D0BCC0C3415B6518364318434F194250184755184F5E1A55661C
          596C1B5D701B61731D65771F677B1F6A80206C8523728826768D277B932A829B
          338EA7439EBE48B4CF38B3CD1896B660A3B6C7CBCDCFCFCEC8CECBCACECCCDCC
          CECECDCECFCDCECECECECECECEBEC2C4798D94264C5A16465C165F771A687E1D
          6C811E6F841E6F841D6D821C687C1B6477175E6F165567174A5C153A4A153240
          1B39452A4753354F5928454C16354015364615404D164A581756691761751B69
          7F1F6F8622748B2478902079921E78921D7690156480387284AFBBBDCDCDCDCC
          CCCCAEAEAE3939391515151414141B1B1B3B3B3B4949491919195A5A5AC8C8C8
          CCCCCC8686861E1E1E1414142424243131312424241414141A1A1A7E7E7ECBCB
          CBCCCCCCC9C9C9CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBA5A5A528282813131313
          13131F1F1F3232322B2B2B131313353535B3B3B3CBCBCBCACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACBCBCB8F8F8F1B1B1B1313131414
          141F1F1F2828281D1D1D1212123A3A3AB6B6B6CACACACACACACACACACACACACA
          CACACACACACACACAC8C8C8B3B3B38585856060604E4E4E3B3B3B1F1F1F121212
          1212121212121212121616162828284444445B5B5B6060604646461A1A1A1717
          17828282CACACACACACAC9C9C9C9C9C9C9C9C9CACACACACACACACACACFCFCFD0
          CECFD0D0D0D0D0D0D1D1D1D0D0D0D0D0D0CFD0D0D0D0D0A1A9AD2A4651183B48
          19434F184451184C581953611C586A1D5B6F1B60731B63781E667B1F6A7E1F6D
          8220708724758C267990287E962B879F3592AB3FA1BF3FAEC82599BE2087AB96
          B6BDCFCFCFCBCFCDCACECDCECDCECECDCCCECECCCECECCCFCFCDCECECDCECECE
          CECECE97A3A634545F163F5217586D1B63761D697D1E6C831E6E821E6C7E1E68
          7D1C647B195F761B5B6C185666164C5E154152153A4A153745153A4516424E18
          4A5816505E165B6B1862771E6780226F8723768D257A93287E9925829E23839E
          23819C15749518698689A7AFCCCCCCCDCDCD9B9B9B2827281515151516162222
          224343433D3D3D161616757575CCCCCCCCCCCCCACACA68686814141414141424
          24242E2E2E212121141414232323939393CCCCCCCBCBCBCBCBCBCBCBCBCBCBCB
          CACACACBCBCBC0C0C04C4C4C1414141313131919192D2D2D3030301717172424
          249B9B9BCBCBCBCBCBCBCACACACACACACACACACACACACACACACACACACACACAC9
          C9C9CBCBCBB4B4B43B3B3B1313131212121B1B1B2B2B2B2A2A2A1515151F1F1F
          8D8D8DCBCBCBCBCBCBC9C9C9CACACACACACAC0C0C08E8E8E5A5A5A3333331818
          1812121212121212121212121212121219191925252533333342424251515152
          5252444444282828131313282828838383C8C8C8CACACAC9C9C9C9C9C9C9C9C9
          C9C9C9C9C9C9C9C9C9CACACACFCFCF02806FD0D0D0D0D0D0D1D1D1D0D0D0CFD0
          D1CFD0D0D0D0D08A999B203F49183D4B194350184554184D5D1856661B596C1C
          5E711C61761D657B20687C226C80226E8523718A27758F287A912A7F972E88A2
          3693AD3CA0BB36A4C01D8BAE2B87A3A2BFC1CFCFCFCFCECFCFCDCFCECECDCECF
          CCCECFCDCDCECDCDCFCECDCECECACECDCECECECFCFCFA8B3B43D5A6116394919
          53681B5F771B657D1D6A80206C82216A812169811D677D1966751962731A5D70
          1A576B185467165565165565185866195C6B185F721966791D6B812371882677
          8E257B9428809A2B86A02D89A52D8DA8278BA71981A11571916E9DA9CDCDCDCD
          CDCD828482191A1A1515151818182B2B2B4848483333331C1C1C8C8C8CCDCDCD
          CCCCCCCCCCCCB7B7B74949491414141717172626262B2B2B1C1C1C1414143939
          39ACACACCCCCCCCCCCCCCBCBCBCBCBCBCBCBCBCCCCCCCCCCCC82828217171713
          13131616162727273232321F1F1F141414747474CBCBCBCBCBCBCACACACACACA
          CACACACACACACACACACACACACACACAC9C9C9CBCBCBC8C8C86262621313131313
          131818182B2B2B3232321F1F1F131313626262C6C6C6CBCBCBCACACACACACAA8
          A8A84E4E4E1A1A1A1212121212121212121313131414141F1F1F2F2F2F3B3B3B
          4444444747474242423535352626261B1B1B1919193737376D6D6DA6A6A6CACA
          CACACACAC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9CACACACACACACACACACFCFCF08
          804ED0D0D0D1D1D1D0D0D0D0D0D0CFD0D0CFD0D0D0D0D07E8E911D3C48183D4C
          1943501947571950611857691B5C6E1D60721D64781E677C216B7F236D82236F
          8624748C27788F297B942C819B2F88A23591AC3B9AB63099B81881A42C8199A3
          BDC0CFCFCFCFCECFCECECECECECECECECECECECECECECECECECECDCECECCCFCE
          CDCDCDCECECECECECEADB5B5435B64163A49165166195F751C657B1E6981216D
          84216D83206C821D697F1D687D1D667B1E637A1D63781B63761C64781D677A1E
          697C1F6C8220708723748B267992287E982A839D2D89A3318EAA3694AF3799B4
          2D98B61D8CAB1579996299A7CACBCBCCCCCB646665151515141414191A1A3333
          334A4A4A2929292424249C9C9CCCCCCCCCCCCCCCCCCCCCCCCCA4A4A42F2F2F14
          14141818182828282B2B2B1A1A1A1313134A4A4ABABABACCCCCCCACACACACACA
          CBCBCBCBCBCBCBCBCBABABAB3131311313131414142121212F2F2F2727271313
          13444444BABABACBCBCBCACACACACACACACACACACACACACACACACACACACACACA
          CACACBCBCBCBCBCB7B7B7B141414121212161616292929393939292929131313
          494949BBBBBBCACACACBCBCBA7A7A73737371212121212121212121212121616
          162727274040404949494040403131312626261F1F1F1E1E1E2B2B2B4B4B4B6E
          6E6E8D8D8DB7B7B7CACACACACACACACACAC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
          C9C9C9CACACACACACACACACACFCFCF0D802DD0D0D0D0D0D0D0D0D0D0D0D0CFD0
          D0CFD0D0D0D0D08595981E3D49183D4B18435019495819536218596B1A5D701C
          62731C66781F697E216C81236F8523728923768D267A912A7C952C819C2E88A2
          3490AB3596B22E92AE1C7E9D1D728C8DACB2CFCFCFCFCFCFCECECECECECECECE
          CECECECECECECECECECECDCECECDCECECDCDCDCDCDCDCECECECECECEB0B6B843
          5B64163A4A175364195F741C657C1F6A821F6E83206F86206E861F6D851F6D85
          1F6E85216E83216E82226F8422728724758A25778D277B93297F972B849E2E89
          A4318DA83493B0389AB73DA1BE3DA6C433A4C42196B6157FA0649DACCDCDCDC0
          C0BF4A4B491515151414141F1F1F3C3C3C4949492020202B2B2BA8A8A8CCCCCC
          CCCCCCCBCBCBCCCCCCCCCCCC8888881D1D1D1414141B1B1B2B2B2B2828281717
          17141414616161C2C2C2CCCCCCCACACACBCBCBCBCBCBCBCBCBC2C2C24E4E4E13
          13131313131B1B1B2B2B2B2D2D2D1515151F1F1F939393CBCBCBCACACACACACA
          CACACACACACACACACACACACACACACACACACACBCBCBCBCBCB7C7C7C1414141313
          131717172B2B2B404040333333131313404040B5B5B5CACACABCBCBC4B4B4B12
          12121212121212121313131A1A1A3737375252524444442A2A2A262626404040
          5A5A5A6E6E6E8D8D8DAEAEAEC4C4C4CACACACACACACACACACACACACACACAC9C9
          C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9CACACACACACACACACACFCFCF12
          800CD0D0D0D0D0D0D0D0D0D1D1D1D0D0D0CED0D0D0D0D098A6A8254350183B48
          184450194B5A195465185A6C1B60721C65751E687B1F6A81206E842371872474
          8B24788F277B932C7F982D829C2E87A1338DA73491AC2C8DA91D809C1768815D
          8C97C6CACBCFCFCFCCCECCCDCECECFCDCECECECECECECECECECECECECECDCDCD
          CCCECECCCECECCCECECDCDCDCECECEACB2B43B545D1639481654671A61761C67
          7D1C6E831E718621718922738B23768D22758C25758E257690267892277B9428
          7E962B81982D859D2E89A3308DA83392AC3796B13C9CBD42A4C446AECA49B2CD
          3AAFCB229ABE1582A672A9B5CDCDCDB0B0AE3637361515151414142525254444
          44474747191919353535B1B1B1CCCCCCCCCCCCCBCBCBCBCBCBCCCCCCC7C7C75E
          5E5E1414141414141F1F1F2C2C2C262626141414161616777777CBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCB6A6A6A1313131313131717172626262F2F2F1D1D
          1D131313747474CBCBCBCBCBCBCBCBCBCACACACACACACACACACACACACACACACA
          CACACBCBCBC5C5C5585858131313131313191919323232484848393939121212
          474747BABABACACACAA3A3A32525251212121212121212121616163030304D4D
          4D3A3A3A363636646464989898BFBFBFCACACACACACACACACACACACACACACACA
          CACAC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9CACACACACACACACACAC9C9C9C9C9C9
          C9C9C9CACACACACACACACACACFCFCF11310ED0D0D0D0D0D0D0D0D0D1D1D1D0D0
          D0CFD0CFD0D0D0B0B7BA35505C183844194551184C5C1954671B5A6F1C60741D
          66781D697D206C8321708623738A24768E267992297B952B7F992C829C2E879F
          338CA4368EA82C8BA71E819D176D8724667889A2A5CFCFCFCFCFCFCCCDCECECD
          CECECECECECECECECECECECECECECECECDCDCDCECECECECECECACDCECCCECECE
          CECEA2ADAF3A545F16384816586A18647B1A6A8320718822738C26768F257A91
          267C93297D952A7E972980992A839B2D869F3189A2328EA53290AA3696B03A9C
          B63EA2C047ABC94CB2CB4FBCCD55C0CD42B8CD1A98C01C86A992B4BDCDCDCDA2
          A2A22A2A2A1414141616162C2C2C494949434343161616434343BABABACCCCCC
          CCCCCCCBCBCBCCCCCCCBCBCBCCCCCCB0B0B03E3E3E1414141515152020202C2C
          2C2222221313132121218D8D8DCCCCCCCBCBCBCBCBCBCBCBCBCBCBCB97979725
          25251313131414142020202E2E2E252525131313565656C3C3C3CBCBCBCACACA
          CACACACACACACACACACACACACBCBCBCBCBCBCBCBCB9999992323231313131313
          132020203F3F3F535353363636131313666666C8C8C8CACACAA0A0A023232312
          12121212121313131E1E1E4040403131312A2A2A9F9F9FCACACACACACACACACA
          CACACACACACAC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9CACACACACACACACA
          CAC6C6C6BABABAC6C6C6CACACAC9C9C9C9C9C9CACACACACACACACACACFCFCF2D
          0E24D1D1D1D1D1D1D0D0D0D0D0D0D0CFD0D0CFD0D0D0D0C7CACA526B72183440
          18434F184C5D1A54681C5B701D61741E66781F6A7E216D8422728725748B2577
          8E2879922A7D972B80992D839C2E869E318AA2328CA22D8AA224829E1D789317
          5F75295A6792A2A8CFCFCFCFCFCFCECECECECECECECECECECECECECECECECECE
          CECECECECECECECECECDCECECCCDCECECDCDCECECEA9B0B23D5660163C4E1658
          701A65811F6B8520728A247790267C922980972B829C2C839F2D86A12E89A332
          8DA63591AB3695B03998B43C9FBC43A7C64AAFCB50BBCC54C4CB59C9CC54CACD
          39B5CC158FB84798B0BDC5C8CDCDCD9595952020201515151A1A1A3636365050
          503D3D3D141414595959C5C5C5CCCCCCCCCCCCCBCBCBCBCBCBCBCBCBCCCCCCCC
          CCCC9E9E9E2E2E2E1414141515152323232D2D2D212121141414303030A7A7A7
          CBCBCBCBCBCBCBCBCBCBCBCBBFBFBF4D4D4D1313131313131A1A1A2B2B2B2D2D
          2D1313132E2E2EA8A8A8CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCB989898383838131313131313181818313131545454575757232323202020
          9A9A9ACACACACACACAABABAB2D2D2D1313131212121414142525254141412121
          212E2E2EB7B7B7CACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACAABABAB6161613C3C3C9E9E9ECACACACACACA
          C9C9C9CACACACACACACACACACFCFCF48EB39D1D1D1D1D1D1D1D1D1D0D0D0D0CF
          D0D0CFCFD0D0D0CFD0D06F878A18333D183F4D184C5D1B55671B5C701D61741E
          6578206A7E206D8323718826768D26778E2879932A7D972C81992E839C2E859E
          2F88A1308BA02F8AA12C86A1247C971B6F8817556B28505F7B8C91C4C7C7CECE
          CECECECECECECECECECECECECECECECECECECECECECEC3C6C6BDC1C1CCCCCDCD
          CDCECDCECDCECECEADB4B7415D68163F4F16576E19677E1A6D841F758C247B93
          2680982A859E2D88A4308DA83290AC3593AE3998B43D9EBA40A3C145AAC94EB5
          CC52BFCC55C7CD5BCCCD5CCDCD47C7CD1CA2C3258DB49AB8C3CDCDCDCCCCCC84
          84841A1A1A1515151F1F1F404040585858373737151515737373CCCCCCCCCCCC
          CCCCCCCBCBCBCBCBCBCCCCCCCBCBCBCCCCCCCCCCCC8484841818181414141717
          172929293232322020201414144B4B4BBDBDBDCCCCCCCACACACBCBCBCBCBCB79
          79791616161313131515152626263232321D1D1D131313747474C7C7C7C5C5C5
          C2C2C2C1C1C1BBBBBBAAAAAA8F8F8F6363632424241313131313131919193030
          305353536262623C3C3C131313585858C5C5C5CACACACACACAC5C5C562626212
          12121212121414142121213B3B3B2F2F2F161616505050A1A1A1C5C5C5CACACA
          CACACACACACACACACACACACACACACACACACAC3C3C3AAAAAA8787876464642D2D
          2D121212121212707070CACACACACACAC9C9C9CACACACACACACACACACFCFCF64
          C84FD1D1D1D0D0D0D0D0D0D1D1D1CFD1D1CFCFD0D0CFD0D0D0D0A3AFAE304B51
          1838461A4B5B1B5365185B6E1C61751F667A1E697E1F6E8422738824758C2578
          90277B94297F982B819A2D839D2E879F2F89A13189A23189A33089A22A839D21
          78921A6D871752671A414E4F676F8D9A9CB2B9B9CACCCCCFCFCFCECECECECECE
          CECCCBA8AAAD577F858DA8AACECECECECECECCCDCCCDCECDCECECEB4BCBC5A76
          7C19445716506917677D1A718A1E7794247E9828849D2B8BA53191AC3595B039
          9AB63DA1BB44A7C548AFCA4FB8CD57C2CB5AC9C95ECDCC61CDCD50C9CD25AAC8
          1791B876ACBDCACCCCCCCCCCCACACA6C6C6C1515151515152525254B4B4B5C5C
          5C3030301A1A1A8A8A8ACCCCCCCCCCCCCCCCCCCCCCCCCBCBCBCBCBCBCCCCCCCB
          CBCBCCCCCCC5C5C56060601414141414141D1D1D3333333838381E1E1E141414
          676767C7C7C7CBCBCBCACACACBCBCB9292921F1F1F1313131313132020203131
          312A2A2A1414142525254F4F4F5050504B4B4B4848483F3F3F2C2C2C1A1A1A13
          13131313131616162323233B3B3B5454545C5C5C3C3C3C131313444444B3B3B3
          CBCBCBCACACACACACACACACAB0B0B03F3F3F1313131313131717172C2C2C3C3C
          3C2626261212122323235050506F6F6F7C7C7C8080808181817F7F7F79797969
          69694D4D4D2C2C2C151515121212121212121212121212474747BFBFBFCACACA
          C9C9C9CACACACACACACACACACFCFCF677D08D2D2D2D0D0D0D0D0D0D0D0D0CED1
          D1CCD0D0CFD0D0D0D0D0CDCFCF697E8218323B1844521A506318586C1B60731E
          65791E6A7F1F6E8422738825768D277891297C962980992C829B2F849E2F87A0
          3089A1328AA3338CA6328BA5318AA42B859F227D961B7287175E701742521A3A
          45334D565C6F767B888D8590917D8A8A6372772853652D7887ACC2C5CECECECD
          CDCDCDCDCDCDCECDCCCECDCECECEC6C9C97B9096305A66164F64165F79176F8A
          2078922480992888A22F90AC3397B3399EBB42A5C449AECB4FB8CD55C2CD5ACA
          CD61CDCD58CCCC3DBFCC1CA4C51C96B679AEBFCBCBCDCDCDCCCDCDCDBDBDBD4D
          4D4D1515151717173030305757576262622C2C2C2424249E9E9ECDCDCDCCCCCC
          CBCBCBCCCCCCCCCCCCCCCCCCCBCBCBCCCCCCCBCBCBCCCCCCBBBBBB4747471313
          131313132222224141414A4A4A2525251717177F7F7FCCCCCCCBCBCBCBCBCBB2
          B2B23838381313131313131D1D1D2E2E2E3B3B3B353535232323151515131313
          1313131313131414141717171D1D1D2424242A2A2A3737374444444848483C3C
          3C1F1F1F161616555555B4B4B4CACACACACACACACACACACACACACACACACACAA8
          A8A84646461313131212121717172727273434342E2E2E1F1F1F131313131313
          1414141515151515151414141313131212121212121414141D1D1D2626263838
          383A3A3A1616162C2C2CA8A8A8CACACACACACACACACACACACACACACACFCFCF46
          A013D3D3D3D1D1D1D0D0D0D0D0D0CFD0D0CED0D0CFD0D0D0D0D0D0D0D0A9B1B3
          354E54183744194C5F18566A1A5E711C63771E6A7E206F8422738824768C2678
          91297C962B80992C829B2F849E2F879F318AA2328DA53390A93490A93491A934
          8FA9328AA42A859D247C951E6D8617597116485D173E52163C4F164253184F5E
          165B731A7B9B80B6C6CECECECECECDCECECECECECECCCDCDCBCDCCCCCECBCECE
          CCCECECEA8B4B6607E87265D6E155A7315668117738F1E7E9B2589A62A92B032
          9CBB3AA5C542AECD47B7CD4ABECD48C2CC40B9CC23ABC7159BBC3298B988B6C2
          CCCCCCCDCDCCCBCBCBCDCDCDADADAD3131311515151717172A2A2A4242424B4B
          4B2222222D2D2DAAAAAACCCCCCCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCCCCCCCCCCCC9F9F9F2C2C2C1313131414142727274646464A4A4A
          1E1E1E262626909090CBCBCBCBCBCBC8C8C86464641313131313131717172121
          212C2C2C3434343636363434343131312E2E2E2C2C2C2A2A2A2A2A2A2A2A2A2B
          2B2B2A2A2A2828282323231B1B1B1D1D1D3A3A3A818181C4C4C4CACACACBCBCB
          CACACACACACACACACAC9C9C9CACACACACACAB8B8B87373733333331313131212
          121414142424242F2F2F2F2F2F2A2A2A2626262525252525252525252727272B
          2B2B3030303838383F3F3F4545454A4A4A3D3D3D1818181A1A1A8B8B8BCACACA
          CACACACACACACACACACACACACFCFCF25C31ED3D3D3D0D0D0D0D0D0D0D0D0D1D0
          D1D0D0D0D0D0D0CFD0D0D0D0D0D0D0D08A9A9D1F3A46183C4F1A5467195D6E19
          63751C697E1F6E8421728823768C267990287C952B819A2E849D2F869F2F87A1
          328AA5348FA73593AC3895B23B97B73D9DBA40A1BD41A1BE3FA0BC389CB7369A
          B23698AD3595AC3A9AAB3DA4B12EA4C21697C057A9C1C0C8CDCECECDCDCECDCD
          CDCDCDCDCDCCCCCECCCCCECCCECDCBCDCCCDCECDCDCDCDCACCCCA4B1B4688B96
          34718419647F15668415718F157B9A1985A61C8DB11F94B92199BD219ABC1697
          B91A92B93C9BBC76B0BDB6C5C6CDCDCDCBCDCBCBCBCBCBCCCDCDCDCD98989823
          23231515151515151616161717171818181414143A3A3AB3B3B3CCCCCCCCCCCC
          CBCBCBCBCBCBCBCBCBCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCCCCCCCCCCCC8282
          821B1B1B1414141414141717171F1F1F2121211313132929299D9D9DCBCBCBCB
          CBCB949494222222131313141414151515151515151515171717181818171717
          1717171717171717171A1A1A1D1D1D2323232B2B2B3B3B3B5252526E6E6E9B9B
          9BC0C0C0CACACACBCBCBCACACACACACACACACACACACACACACACACACACACACAC9
          C9C9CACACACACACAB6B6B68080805353533030301B1B1B141414121212121212
          1717171919191B1B1B1D1D1D1D1D1D1D1D1D1D1D1D1C1C1C1B1B1B1919191E1E
          1E2F2F2F4C4C4C6B6B6BA6A6A6CACACACACACACACACACACACACACACACFCFCF04
          E629D4D4D4D1D1D1D1D1D1D0D0D0D0D0D0D0D0D0D0D0D0CFCFCFCFCFCFD0D0D0
          C7C9CA6C7D841B3A461846571B5B6B1A61731A677B1D6A821F708721758A2679
          90287D962B819B2D859E2F87A03089A2338BA93590AC3997B23FA0BB4AADC652
          BCCC54C2CF57C4CF5AC7CF5BC9CE5CCCCE60CECF62CFCF56CECE33BCCE189FC4
          4CA2BEB4C4C8CECECDCECCCECDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCD
          CDCDCDCDCDCDCECECECECECECECCCECDB3BEC288A1AC66919F528B9E40869C34
          839B2E84A0308CA53995AC439CB259A4B487B1BCBCC5C8CDCDCBCDCDCBCDCDCD
          CCCCCDCCCCCCCDCCCDCDCDCDB2B2B27D7D7D7777777C7C7C7B7B7B7A7A7A7979
          797373738E8E8EC5C5C5CCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCCCCCCCB
          CBCBCBCBCBCBCBCBCBCBCBCCCCCCC2C2C28A8A8A7575757B7B7B797979787878
          7B7B7B7A7A7A7373739D9D9DCBCBCBCBCBCBBEBEBE8686867474747A7A7A7A7A
          7A7B7B7B7A7A7A7979797979797979797979797878787C7C7C8484848D8D8D98
          9898A8A8A8B9B9B9C6C6C6CBCBCBCBCBCBCBCBCBCBCBCBCACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACBCBCBCACACAC6C6
          C6B1B1B19191917676766464645C5C5C5656565252524F4F4F4F4F4F51515153
          53535656565E5E5E6767677272728A8A8AA9A9A9C2C2C2CACACACACACAC9C9C9
          C9C9C9CACACACACACACACACACFCFCFF50F00D4D4D4D1D1D1D0D0D0D0D0D0D0D0
          D0D0D0D0D0D0D0CFCFCFCFD0CFCDD0CFD0D0D0C6C8C9687B80193A47184A5B1A
          5D701863771B697E1B6F841F748A267890297C962B809A2B859E2D87A0308CA5
          3390AA3793B13F9DBA4AB1C557C6CE57C9CF41B0BD279EB229A5BC35AEC43AB3
          C636B5C72BB2C41DA4BD249ABE68ABC2BCC6C9CECECBCDCDCDCCCDCECDCDCDCD
          CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCECCCECDCDCDC9CDCBCACECC
          CECECECDCDCDCDCDCDC4C8CBB8C2C6AEBDC2A8BCC0ABBEC2B2C4C6BACACAC7CC
          CCCDCDCDCDCDCDCACCCDC8CCCCCACCCCCCCCCCCCCCCCCCCCCCCCCCCCCDCDCDCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCDCDCDCCCCCCCCCCCCCDCDCDCCCCCCCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCCCC
          CCCCCCCCCBCBCBCBCBCBCBCBCBCCCCCCCBCBCBCBCBCBCBCBCBCACACACACACACB
          CBCBCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCACACACACA
          CACBCBCBCBCBCBCACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACAC9C9C9C9C9C9CBCBCBCACACACACACACACACACACACACACACAC8C8C8
          C5C5C5C4C4C4C2C2C2C2C2C2C3C3C3C4C4C4C5C5C5C8C8C8CACACACACACACACA
          CACACACACACACACACACAC9C9C9C9C9C9C9C9C9CACACACACACACACACACFCFCFE9
          0000D4D4D4D0D0D0D0D0D0D1D1D1D0D0D0D0D0D0D0D0D0CFCFCFCFCFCFCFCFCF
          D0D0CDD0D0D0C7CACB6A7D861B3D4C184B5A1862751A677B1B6B801E71892476
          8F277C952A80992A849E2D87A1308CA53491AD3995B342A3BF52BBCD47BDCB38
          9FB475A6B5719BAB458CA1378FAA3493B03499B5439CB869A6B9A5BEC6CACDCD
          CECECACDCCCACCCBCECDCECCCDCECECECECECDCDCDCDCDCDCDCDCDCDCDCDCDCD
          CDCDCDCDCCCECCCCCECCCCCDCECBCDCDCCCDCCCCCECDCDCDCDCDCDCDCDCDCDCD
          CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCCCCCCCBCCCCCCCCCACDCBCBCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCDCDCDCCCCCCCCCCCCCDCDCDCCCCCCCDCDCDCDCD
          CDCCCCCCCCCCCCCCCCCCCBCBCBCBCBCBCCCCCCCBCBCBCCCCCCCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCCCCCCCBCBCBCCCCCCCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACAC9C9C9CACACACACACACACACAC9C9C9CACACACACACAC9C9C9C9C9C9C9C9
          C9C9C9C9CACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACAC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
          C9C9C9CACACACACACACACACACFCFCF7C4D00D4D4D4D0D0D0D1D1D1D0D0D0D0D0
          D0D0D0D0D0D0D0CFCFCFCFCFCFCFCFCECFCFCECFCECED0D0D0CACBCB7F90932B
          4F5A18495B185E731B677E1C6D861D738C207992287F992A839D2D88A2318DA9
          3793B13B99B742A9C14FBCCF45B4CC1B7E9A5F8A91CAC7C5CACCCBBAC5C6B5C3
          C6B7C6C9C4C9CBCECECDCECECECECECDCDCECDCDCDCDCDCDCDCDCDCDCDCDCDCD
          CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCECDCD
          CECDCDCDCDCDCDCDCDCECDCDCCCCCCCBCCCCCDCCCCCDCDCDCDCDCDCCCCCDCCCC
          CCCCCCCCCCCCCCCDCCCCCDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCBCBCBCBCBCBCCCCCCCCCCCCCBCBCB
          CCCCCCCBCBCBCBCBCBCCCCCCCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCACACACBCBCBCBCBCBCBCBCBCACACACACACACA
          CACACACACACACACACACACACACACACACACACBCBCBCACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACAC9C9C9CACACAC9C9C9CACACACACACACACACAC9C9C9C9C9C9C9C9C9
          C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
          C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9CACACACACACACACACACFCFCF00
          0000D4D4D4D1D1D1D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0CFCFCF
          CFCFCFCECECECFCFCFCFCFCFD0D0D09FABAF41636E184D5D17576C18677C196F
          871C7690237D9627839C2C8AA2318EAA3795B23D9CBA44A9C54EB9CE52BFCF30
          97B1175F755F7A85BBBBBCCFCFCFCFCFCFCFCFCFCECECECFCFCFCBCECECCCECE
          CECECECDCDCDCDCDCDCECECECDCDCDCECECECECECECECECDCDCDCDCDCDCDCDCD
          CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCCCCCCCDCDCDCDCDCDCC
          CCCCCCCCCCCCCCCCCCCCCCCDCDCDCDCDCDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCBCB
          CBCCCCCCCBCBCBCCCCCCCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCACACACACACACACACACACACACACACACBCBCBCACACACACACACACA
          CACBCBCBCACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACAC9C9C9C9C9C9CACA
          CACACACAC9C9C9C9C9C9C9C9C9CACACAC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
          C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
          C9C9C9CACACACACACACACACACFCFCF932500D4D4D4D1D1D1D0D0D0D0D0D0D0D0
          D0D0D0D0D0D0D0CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCECFCFCFCFCFD0
          D0D0BCC3C3748E93316270185769175F76196C8A1D7593217F9725869D2B8BA6
          3291B03999BB42A6C54AB6CC54C1CF50BCCD2C92B017556E345560848F96B3B7
          B9C9CAC9CECECECECECECECECECECECECECECECECECECECECECECECECECECECE
          CECEC8C9CAC9C8CACDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCCCCCCCDCDCDCDCDCD
          CDCDCDCDCDCDCDCDCDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCDCDCDCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCBCBCBCBCBCBCBCBCB
          CBCBCBCCCCCCCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCA
          CACACACACACBCBCBCBCBCBCACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACAC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9CACACAC9C9C9C9C9C9
          C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
          C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9CACACACACACACACACACFCFCF80
          8080D4D4D4D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0CFCFCFCFCFCFCFCFCF
          CFCFCFCFCFCFCFCFCFCFCFCFCDCFCFCFCFCFCFCFCFCECFCFAFB9BC708F983A71
          7F1B627717607D176B871877921C809C268AA62E92B0349DBB3DABC64AB8CE57
          C4CF55C6CF40AAB7297A89225A6A365A675A7379838E93969CA09CA5A7A4AFAD
          ACB4B1AAB1B29BABAE91A7AC92ACB39BB4B7A5B5B6B9BFC1CDCDCDCECECECDCD
          CDCDCDCDCDCDCDCCCCCCCDCDCDCDCDCDCCCCCCCDCDCDCDCDCDCCCCCCCDCDCDCC
          CCCCCCCCCCCDCDCDCCCCCCCCCCCCCDCDCDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCBCBCBCBCBCBCCCCCCCCCCCCCCCCCCCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCACACACBCBCBCBCBCBCBCBCBCBCBCBCACACACACACACACA
          CACACACACBCBCBCACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACAC9C9C9C9C9C9CACA
          CACACACAC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
          C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
          C9C9C9CACACACACACACACACACFCFCF94959AD4D4D4D0D0D0D1D1D1D0D0D0D0D0
          D0D0D0D0D0D0D0CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF
          CFCFCFCFCFCFCFCFCFCFCFCECFCFB7C1C487A2A754838E33738B206B86176984
          176F8D1779971A82A31D8AB02795B935A5C243B3C74DC1CE4CC4CE3CAFBD2690
          A117778A17697D1E6376216374216677256B7B37758546819059919D7FA8B1A8
          BDC0BFC6C6C4CAC9CBCDCCCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCD
          CDCDCDCCCCCCCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCBCBCBCCCCCCCCCCCCCCCCCCCCCCCCCBCBCB
          CCCCCCCBCBCBCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCCCCCCCCCCCCCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCA
          CACACBCBCBCBCBCBCBCBCBCACACACACACACACACACBCBCBCACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACAC9C9C9CACACACACACACACACACACACACACACAC9C9C9CACACA
          C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
          C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9CACACACACACACACACACFCFCF54
          565DD4D4D4D0D0D0D0D0D0D0D0D0D0D0D0D1D1D1D0D0D0D0D0D0CFCFCFCFCFCF
          CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCACFCECDCFCFCFCF
          CFCFCFCFC6CACAB2C0C393ADB36E97A251879A408297348197287C9B1F7C9C1C
          80A01B81A51B88AB1F93B1279DB82FA2BE30A3BD34A3BB3CA2B7479FB1599FB4
          71A9B995B7C0AFC0C5BCC7C9C8CDCCCECECDCDCECECACDCECBCDCDCECDCDCDCD
          CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCCCCCCCDCDCDCCCCCCCC
          CCCCCCCCCCCDCDCDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCBCB
          CBCCCCCCCCCCCCCCCCCCCBCBCBCCCCCCCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCCCCCCCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACAC9C9C9CACACACACACACACACACACACACACA
          CAC9C9C9C9C9C9C9C9C9C9C9C9CACACAC9C9C9CACACAC9C9C9C9C9C9C9C9C9C9
          C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
          C9C9C9CACACACACACACACACACFCFCFCACACAD4D4D4D0D0D0D0D0D0D0D0D0D0D0
          D0D0D0D0D0D0D0CFCFCFCFCFCFCECECECECECECECECECECECECFCFCFCECECECE
          CECECECECECFCECFCFCED0CECED0CCCFCFCDCFCFCFCFCFCFCFCFCFCFCFCECECE
          C7CBCBBDC6C6B3C1C3A7B9BF9CB5BA91B0B884ABB781ABB685ACB78FB0BA9BB5
          BEA6BBC3AEC3C9B9C8CCC3CCCECBCECECECECECECECECECECECECECECDCECDCA
          CDCDCACDCDC9CCCCCBCCCCCDCDCDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCACACACBCBCBCACACACACACACACACACBCB
          CBCACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACAC9C9C9CACACAC9C9C9CACACA
          CACACAC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9CACACACACACAC9C9C9C9C9
          C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
          C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
          C9C9C9C9C9C9C9C9C9C9C9C9C8C8C8C8C8C8C9C9C9C9C9C9C9C9C9C9C9C9C8C8
          C8C8C8C8C8C8C8C8C8C8C9C9C9CACACACACACACACACACACACACACACACFCFCFCC
          CFEBD3D3D3D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0CFCFCFCFCFCF
          CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFD0D0CFCFCFCFCECE
          CECECECECECECFCECECECFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF
          CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCECECECFCFCFCECECECECECECECECE
          CECECECDCDCDCDCDCDCECDCECECECECDCDCECECECDCECECDCECDCDCDCDCDCDCD
          CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCD
          CDCDCDCDCDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCBCBCBCCCCCCCCCCCCCBCBCBCBCBCBCC
          CCCCCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCACACACACACACACACACACACACACACACBCB
          CBCACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACAC9C9C9C9C9C9C9C9
          C9C9C9C9C9C9C9C9C9C9C9C9C9CACACACACACAC9C9C9C9C9C9C9C9C9C9C9C9C9
          C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9CACACACACACA
          CACACACACACACACACACACACACFCFCFEFE6DAD3D3D3D1D1D1D0D0D0D0D0D0D0D0
          D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0CFCFCFCF
          CFCFD0D0D0CFCFCFCFCFCFD0D0D0CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF
          CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCECE
          CECECECECFCFCFCFCFCFCECECECECECECFCFCFCECECECECECECECECECECECECD
          CDCDCECECECECECECECECECECECECECECECECECECDCDCDCDCDCDCDCDCDCECECE
          CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCD
          CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCD
          CDCDCDCDCDCCCCCCCDCDCDCDCDCDCCCCCCCDCDCDCDCDCDCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCBCBCBCCCCCCCCCCCCCCCCCCCCCCCCCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCACACACACACACBCBCBCBCBCBCBCBCBCACACACBCBCBCBCBCBCACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACA
          CACACACACACACACACACACACACACACACACACACACACACACACACACACACACDCDCDCE
          CECED8D8D8D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D4D4D4D4D4D4D4D4D4
          D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4
          D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4
          D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4
          D4D4D4D3D3D3D4D4D4D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3
          D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3
          D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D2D2D2D3D3D3D2D2D2D2D2D2
          D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2
          D2D2D2D2D2D2D2D1D1D1D2D2D2D2D2D2D1D1D1D1D1D1D2D2D2D1D1D1D1D1D1D1
          D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1
          D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1
          D1D1D1D1D1D1D1D1D1D1D0D0D0D0D0D0D1D1D1D0D0D0D1D1D1D0D0D0D0D0D0D0
          D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0
          D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0
          D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0CFCFCFD0
          D0D0D0D0D0CFCFCFD0D0D0D0D0D0CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF
          CFCFCFCFCFCFCFCFCFCFCFCFD4D4D4073D00}
        Stretch = True
      end
      object BitBtn20: TBitBtn
        Left = 621
        Top = 46
        Width = 81
        Height = 27
        Caption = 'Next'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        Visible = False
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
          333333333337F33333333333333033333333333333373F333333333333090333
          33333333337F7F33333333333309033333333333337373F33333333330999033
          3333333337F337F33333333330999033333333333733373F3333333309999903
          333333337F33337F33333333099999033333333373333373F333333099999990
          33333337FFFF3FF7F33333300009000033333337777F77773333333333090333
          33333333337F7F33333333333309033333333333337F7F333333333333090333
          33333333337F7F33333333333309033333333333337F7F333333333333090333
          33333333337F7F33333333333300033333333333337773333333}
        NumGlyphs = 2
      end
      object BitBtn18: TBitBtn
        Left = 469
        Top = 47
        Width = 145
        Height = 30
        Caption = 'Search for Text'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        Visible = False
        Kind = bkHelp
      end
      object Panel4: TPanel
        Left = 11
        Top = 455
        Width = 261
        Height = 231
        BevelWidth = 2
        Enabled = False
        TabOrder = 3
        Visible = False
        object Label38: TLabel
          Left = 16
          Top = 85
          Width = 96
          Height = 16
          Caption = 'Editorial Note'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label41: TLabel
          Left = 15
          Top = 131
          Width = 110
          Height = 16
          Caption = 'Start Date/Time'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label42: TLabel
          Left = 15
          Top = 182
          Width = 105
          Height = 16
          Caption = 'End Date/Time'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label43: TLabel
          Left = 18
          Top = 13
          Width = 175
          Height = 16
          Caption = 'Playlist Entry Parameters'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label7: TLabel
          Left = 16
          Top = 61
          Width = 153
          Height = 16
          Caption = 'Dwell Time (Seconds)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object EntryEnable: TCheckBox
          Left = 15
          Top = 36
          Width = 113
          Height = 18
          Alignment = taLeftJustify
          Caption = 'Entry Enable'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          State = cbChecked
          TabOrder = 0
        end
        object EntryNote: TEdit
          Left = 16
          Top = 103
          Width = 229
          Height = 24
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 100
          ParentFont = False
          TabOrder = 1
        end
        object EntryStartEnableDate: TDateTimePicker
          Left = 16
          Top = 149
          Width = 105
          Height = 24
          Date = 38760.853829386570000000
          Time = 38760.853829386570000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object EntryStartEnableTime: TDateTimePicker
          Left = 126
          Top = 149
          Width = 111
          Height = 24
          Date = 38760.853983645840000000
          Time = 38760.853983645840000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Kind = dtkTime
          ParentFont = False
          TabOrder = 3
        end
        object EntryEndEnableDate: TDateTimePicker
          Left = 16
          Top = 199
          Width = 105
          Height = 24
          Date = 38760.855092766210000000
          Time = 38760.855092766210000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
        end
        object EntryEndEnableTime: TDateTimePicker
          Left = 126
          Top = 199
          Width = 111
          Height = 24
          Date = 38760.855219178250000000
          Time = 38760.855219178250000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Kind = dtkTime
          ParentFont = False
          TabOrder = 5
        end
        object EntryDwellTime: TSpinEdit
          Left = 176
          Top = 55
          Width = 56
          Height = 27
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          MaxValue = 600
          MinValue = 2
          ParentFont = False
          TabOrder = 6
          Value = 2
        end
      end
      object Panel16: TPanel
        Left = 11
        Top = 454
        Width = 779
        Height = 239
        BevelWidth = 2
        TabOrder = 4
        object Label14: TLabel
          Left = 13
          Top = 4
          Width = 118
          Height = 16
          Caption = 'Template Fields:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TemplateDescription: TLabel
          Left = 138
          Top = 4
          Width = 371
          Height = 17
          AutoSize = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object EntryFieldsGrid: TtsGrid
          Left = 13
          Top = 23
          Width = 753
          Height = 172
          CheckBoxStyle = stCheck
          Cols = 3
          DefaultRowHeight = 20
          ExportDelimiter = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          GridMode = gmListBox
          HeadingFont.Charset = DEFAULT_CHARSET
          HeadingFont.Color = clWindowText
          HeadingFont.Height = -13
          HeadingFont.Name = 'MS Sans Serif'
          HeadingFont.Style = [fsBold]
          HeadingHeight = 20
          HeadingParentFont = False
          ParentFont = False
          ParentShowHint = False
          Rows = 4
          ShowHint = False
          TabOrder = 0
          Version = '2.20.26'
          XMLExport.Version = '1.0'
          XMLExport.DataPacketVersion = '2.0'
          ColProperties = <
            item
              DataCol = 1
              Col.Heading = 'Index'
              Col.ReadOnly = True
              Col.Width = 49
            end
            item
              DataCol = 2
              Col.Heading = 'Field Description'
              Col.ReadOnly = True
              Col.Width = 205
            end
            item
              DataCol = 3
              Col.Heading = 'Field Contents'
              Col.Width = 482
            end>
        end
      end
      object Panel1: TPanel
        Left = 190
        Top = 3
        Width = 420
        Height = 34
        BevelWidth = 2
        TabOrder = 5
        object PlaylistModeLabel: TLabel
          Left = 2
          Top = 2
          Width = 417
          Height = 31
          Alignment = taCenter
          AutoSize = False
          Caption = 'TICKER PLAYLIST MODE'
          Color = clSilver
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
      end
      object Panel2: TPanel
        Left = 21
        Top = 652
        Width = 758
        Height = 33
        BevelWidth = 2
        TabOrder = 6
        object ApdStatusLight1: TApdStatusLight
          Left = 211
          Top = 8
          Width = 15
          Height = 15
          Glyph.Data = {
            66010000424D660100000000000076000000280000001E0000000F0000000100
            040000000000F0000000C40E0000C40E00001000000000000000006B00001039
            100010AD10001873180018DE180039DE3900424A420052FF5200636363007384
            73008C8C8C0094EF94009C9C9C00D6CEC600E7E7E700FF00F700FFFF9611119F
            FFFFFFFA61166AFFFF00FFF310000111FFFFFF616666666FFF00FF0002000001
            1FFFF68898886666FF00F3222220200001FF699A9A9888666F00C02222222000
            019A6CACAAAA98866A003444444242200068CCCCCCAAAA886600244454544222
            0016CCACCDCCCA9986004447575554222018DDDDDACCCAA98100455777775542
            2218CDDDDDDCDCAA960054777B777552203ADDDDDDDACDCA8600B477BDB77554
            209CDDEEEDDDDACC8A00F577BBBB774423FFCDEEDEEDDDCC6F00FF577B777744
            2FFFFCEEEEDDDCC8FF00FFF757777444FFFFFFCEEEDEDC9FFF00FFFFB75455BF
            FFFFFFFCCCCCCCFFFF00}
          Lit = False
        end
        object Label6: TLabel
          Left = 8
          Top = 8
          Width = 196
          Height = 16
          Caption = 'Graphics Engine Connection'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object ApdStatusLight2: TApdStatusLight
          Left = 240
          Top = 8
          Width = 15
          Height = 15
          Glyph.Data = {
            16060000424D160600000000000036040000280000001E0000000F0000000100
            080000000000E0010000C30E0000C30E000000010000000000006B0003003910
            1100AD10120073181900DE181D00DE393D004A424200FF525500636363008473
            74008C8C8C00EF9495009C9C9C00CEC6D600E7E7E70000F8FF00FFFFFF000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            00000000000000000000000000000000000000000000000000000F0F0F0F0906
            01010101090F0F0F0F0F0F0F0F0A06010106060A0F0F0F0F00000F0F0F030100
            0000000101010F0F0F0F0F0F0601060606060606060F0F0F00000F0F00000002
            000000000001010F0F0F0F06080809080808060606060F0F00000F0302020202
            02000200000000010F0F0609090A090A090808080606060F00000C0002020202
            0202020000000001090A060C0A0C0A0A0A0A09080806060A0000030404040404
            040204020200000006080C0C0C0C0C0C0A0A0A0A080806060000020404040504
            050404020202000001060C0C0A0C0C0D0C0C0C0A090908060000040404070507
            050505040202020001080D0D0D0D0D0A0C0C0C0A0A0908010000040505070707
            070705050402020201080C0D0D0D0D0D0D0C0D0C0A0A0906000005040707070B
            0707070505020200030A0D0D0D0D0D0D0D0A0C0D0C0A080600000B0407070B0D
            0B07070505040200090C0D0D0E0E0E0D0D0D0D0A0C0C080A00000F0507070B0B
            0B0B0707040402030F0F0C0D0E0E0D0E0E0D0D0D0C0C060F00000F0F0507070B
            070707070404020F0F0F0F0C0E0E0E0E0D0D0D0C0C080F0F00000F0F0F070507
            0707070404040F0F0F0F0F0F0C0E0E0E0D0E0D0C090F0F0F00000F0F0F0F0B07
            050405050B0F0F0F0F0F0F0F0F0C0C0C0C0C0C0C0F0F0F0F0000}
          Lit = False
        end
      end
      object PlaylistGrid: TtsGrid
        Left = 13
        Top = 51
        Width = 777
        Height = 404
        AlwaysShowFocus = True
        CheckBoxStyle = stCheck
        ColMoving = False
        Cols = 9
        ColSelectMode = csNone
        DefaultRowHeight = 20
        ExportDelimiter = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        GridMode = gmListBox
        HeadingFont.Charset = DEFAULT_CHARSET
        HeadingFont.Color = clWindowText
        HeadingFont.Height = -13
        HeadingFont.Name = 'MS Sans Serif'
        HeadingFont.Style = [fsBold]
        HeadingHeight = 20
        HeadingParentFont = False
        InactiveButtonState = ibsNone
        ParentFont = False
        ParentShowHint = False
        PopupMenu = PlaylistPopupMenu
        PrintTotals = False
        RowMoving = False
        Rows = 0
        ShowHint = True
        TabOrder = 0
        ThumbTracking = True
        Version = '2.20.26'
        XMLExport.Version = '1.0'
        XMLExport.DataPacketVersion = '2.0'
        OnClick = PlaylistGridClick
        OnDblClick = PlaylistGridDblClick
        OnKeyDown = PlaylistGridKeyDown
        OnKeyUp = PlaylistGridKeyUp
        OnRowChanged = PlaylistGridRowChanged
        ColProperties = <
          item
            DataCol = 1
            Col.Heading = 'Index'
            Col.ReadOnly = True
            Col.Width = 45
          end
          item
            DataCol = 2
            Col.Heading = 'League'
            Col.ReadOnly = True
          end
          item
            DataCol = 3
            Col.Heading = 'Page ID'
            Col.Width = 70
          end
          item
            DataCol = 4
            Col.ControlType = ctCheck
            Col.Heading = 'Enable'
          end
          item
            DataCol = 5
            Col.Heading = 'TID'
            Col.Width = 40
          end
          item
            DataCol = 6
            Col.Heading = 'Template Description'
            Col.ReadOnly = True
            Col.Width = 176
          end
          item
            DataCol = 7
            Col.Heading = 'Additional Information'
            Col.Width = 327
          end
          item
            DataCol = 8
            Col.Heading = 'Notes'
            Col.Width = 293
          end
          item
            DataCol = 9
            Col.Heading = 'Selected'
            Col.Visible = False
          end>
      end
    end
  end
  object Panel3: TPanel
    Left = 692
    Top = 9
    Width = 564
    Height = 27
    BevelWidth = 2
    TabOrder = 3
    object DateTimeLabel: TLabel
      Left = 292
      Top = 1
      Width = 272
      Height = 23
      Alignment = taCenter
      AutoSize = False
      Color = clBlack
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -21
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object PlaylistNameLabel: TLabel
      Left = 2
      Top = 1
      Width = 287
      Height = 23
      Alignment = taCenter
      AutoSize = False
      Color = clBlack
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -21
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
  end
  object DefaultTemplatesGrid: TtsGrid
    Left = 460
    Top = 97
    Width = 777
    Height = 399
    AlwaysShowFocus = True
    CheckBoxStyle = stCheck
    ColMoving = False
    Cols = 7
    ColSelectMode = csNone
    DefaultRowHeight = 20
    ExportDelimiter = ','
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    GridMode = gmListBox
    HeadingFont.Charset = DEFAULT_CHARSET
    HeadingFont.Color = clWindowText
    HeadingFont.Height = -13
    HeadingFont.Name = 'MS Sans Serif'
    HeadingFont.Style = [fsBold]
    HeadingHeight = 20
    HeadingParentFont = False
    InactiveButtonState = ibsNone
    ParentFont = False
    ParentShowHint = False
    PopupMenu = DefaultTemplatesPopupMenu
    PrintTotals = False
    RowMoving = False
    Rows = 0
    ShowHint = True
    TabOrder = 4
    ThumbTracking = True
    Version = '2.20.26'
    Visible = False
    XMLExport.Version = '1.0'
    XMLExport.DataPacketVersion = '2.0'
    ColProperties = <
      item
        DataCol = 1
        Col.Heading = 'Index'
        Col.ReadOnly = True
        Col.Width = 45
      end
      item
        DataCol = 2
        Col.Heading = 'League'
        Col.ReadOnly = True
        Col.Width = 78
      end
      item
        DataCol = 3
        Col.Heading = 'Template ID'
        Col.Width = 104
      end
      item
        DataCol = 4
        Col.Heading = 'Type'
        Col.Width = 88
      end
      item
        DataCol = 5
        Col.Heading = 'Template Description'
        Col.ReadOnly = True
        Col.Width = 364
      end
      item
        DataCol = 6
        Col.ControlType = ctCheck
        Col.Heading = 'Enable'
      end
      item
        DataCol = 7
        Col.Heading = 'Selected'
        Col.Visible = False
      end>
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 924
    Width = 1257
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object MainMenu1: TMainMenu
    AutoHotkeys = maManual
    Left = 712
    Top = 52
    object File1: TMenuItem
      Caption = '&File'
      object Exit1: TMenuItem
        Caption = 'E&xit'
        OnClick = Exit1Click
      end
    end
    object Login1: TMenuItem
      Caption = '&Login'
      Visible = False
      object LogintoSystem1: TMenuItem
        Caption = 'Login to System'
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object LogoutfromSystem1: TMenuItem
        Caption = 'Logout from System'
      end
    end
    object Prefs1: TMenuItem
      Caption = '&Prefs'
      object SetPrefs1: TMenuItem
        Caption = 'Set &Prefs'
        OnClick = SetPrefs1Click
      end
    end
    object Database1: TMenuItem
      Caption = '&Utilities'
      object RepopulateTeamsCollection1: TMenuItem
        Caption = 'Reload Data from Database'
        OnClick = RepopulateTeamsCollection1Click
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object PurgeScheduledEvents1DayOld1: TMenuItem
        Caption = 'Purge Expired Scheduled Events'
        OnClick = PurgeScheduledEvents1DayOld1Click
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object ShowSponsorLogoDataEntryPage1: TMenuItem
        Caption = 'Show Sponsor Logo Data Entry Page'
      end
      object HideSponsorLogoDataEntryPage1: TMenuItem
        Caption = 'Hide Sponsor Logo Data Entry Page'
        Checked = True
      end
      object N11: TMenuItem
        Caption = '-'
      end
      object ReconnecttoGraphicsEngine1: TMenuItem
        Caption = 'Reconnect to Graphics Engine'
        OnClick = ReconnecttoGraphicsEngine1Click
      end
      object N12: TMenuItem
        Caption = '-'
      end
      object ClearGraphicsOutput1: TMenuItem
        Caption = 'Clear All Graphics Output'
        OnClick = ClearGraphicsOutput1Click
      end
      object N14: TMenuItem
        Caption = '-'
      end
      object EditSponsorLogoDefinitions1: TMenuItem
        Caption = 'Edit Sponsor Logo Definitions'
        OnClick = EditSponsorLogoDefinitions1Click
      end
      object EditTeamInformation1: TMenuItem
        Caption = 'Edit Team Information'
        OnClick = EditTeamInformation1Click
      end
      object EditGamePhaseData1: TMenuItem
        Caption = 'Edit Game Phase Data'
        OnClick = EditGamePhaseData1Click
      end
      object EditCustomHeaders1: TMenuItem
        Caption = 'Edit Custom Headers'
        OnClick = EditCustomHeaders1Click
      end
    end
    object Options1: TMenuItem
      Caption = 'Options'
      object UseRankforNCAAB1: TMenuItem
        Caption = 'Use Rank for NCAAB'
        Checked = True
        OnClick = UseRankforNCAAB1Click
      end
      object UseSeedforNCAAB1: TMenuItem
        Caption = 'Use Seed for NCAAB'
        OnClick = UseSeedforNCAAB1Click
      end
    end
    object TickerDatabaseSelection: TMenuItem
      Caption = 'Ticker Database Selection'
      Visible = False
      object TickerDB1: TMenuItem
        Caption = 'Ticker DB #1'
        Checked = True
        OnClick = TickerDB1Click
      end
      object TickerDB2: TMenuItem
        Caption = 'Ticker DB #2'
        OnClick = TickerDB2Click
      end
      object TickerDB3: TMenuItem
        Caption = 'Ticker DB #3'
        OnClick = TickerDB3Click
      end
      object TickerDB4: TMenuItem
        Caption = 'Ticker DB #4'
        OnClick = TickerDB4Click
      end
      object TickerDB5: TMenuItem
        Caption = 'Ticker DB #5'
        OnClick = TickerDB5Click
      end
      object TickerDB6: TMenuItem
        Caption = 'Ticker DB #6'
        OnClick = TickerDB6Click
      end
      object TickerDB7: TMenuItem
        Caption = 'Ticker DB #7'
        OnClick = TickerDB7Click
      end
      object TickerDB8: TMenuItem
        Caption = 'Ticker DB #8'
        OnClick = TickerDB8Click
      end
      object TickerDB9: TMenuItem
        Caption = 'Ticker DB #9'
        OnClick = TickerDB9Click
      end
      object TickerDB10: TMenuItem
        Caption = 'Ticker DB #10'
        OnClick = TickerDB10Click
      end
    end
    object Help1: TMenuItem
      Caption = '&Help'
      object About1: TMenuItem
        Caption = '&About'
        OnClick = About1Click
      end
    end
  end
  object PBJustOne1: TPBJustOne
    Left = 807
    Top = 53
  end
  object TimeOfDayTimer: TTimer
    OnTimer = TimeOfDayTimerTimer
    Left = 743
    Top = 53
  end
  object tsMaskDefs1: TtsMaskDefs
    Masks = <
      item
        AutoFill = [mcOnEdit]
        Evaluate = [mcOnEdit]
        Name = 'Mask0'
        Picture = '###'
      end>
    Left = 775
    Top = 53
  end
  object PlaylistPopupMenu: TPopupMenu
    Left = 647
    Top = 53
    object Enable1: TMenuItem
      Caption = '&Enable'
      OnClick = Enable1Click
    end
    object Disable1: TMenuItem
      Caption = '&Disable'
      OnClick = Disable1Click
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object EditStartEndEnableTimes1: TMenuItem
      Caption = 'Edit &Start/End Enable Times'
      OnClick = EditStartEndEnableTimes1Click
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object SelectAll1: TMenuItem
      Caption = 'Select All'
      OnClick = SelectAll1Click
    end
    object DeSelectAll1: TMenuItem
      Caption = 'De-Select All'
      OnClick = DeSelectAll1Click
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object Cut1: TMenuItem
      Caption = 'Cu&t'
      OnClick = Cut1Click
    end
    object Copy1: TMenuItem
      Caption = '&Copy'
      OnClick = Copy1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Paste1: TMenuItem
      Caption = '&Paste'
      OnClick = Paste1Click
    end
    object AppendtoEnd1: TMenuItem
      Caption = 'P&aste at End'
      OnClick = AppendtoEnd1Click
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object Duplicate1: TMenuItem
      Caption = 'D&uplicate'
      OnClick = Duplicate1Click
    end
    object DuplicateandAppendtoEnd1: TMenuItem
      Caption = 'Dupl&icate and Append to End'
      OnClick = DuplicateandAppendtoEnd1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Delete1: TMenuItem
      Caption = 'De&lete'
      OnClick = Delete1Click
    end
    object N13: TMenuItem
      Caption = '-'
    end
    object ClearExtraLineRegion1: TMenuItem
      Caption = 'Clear Extra Line Region'
      Visible = False
      OnClick = ClearExtraLineRegion1Click
    end
    object ClearGraphicsOutput2: TMenuItem
      Caption = 'Clear All Graphics Output'
      OnClick = ClearGraphicsOutput2Click
    end
  end
  object DBMaintPopupMenu: TPopupMenu
    Left = 618
    Top = 54
    object RenamePlaylist1: TMenuItem
      Caption = 'Rename Playlist'
    end
  end
  object TemplateTimeEditPopupMenu: TPopupMenu
    Left = 678
    Top = 54
    object EditDefaultTemplateStartEndEnableTimes1: TMenuItem
      Caption = '&Edit Default Template Parameters'
      OnClick = EditDefaultTemplateStartEndEnableTimes1Click
    end
  end
  object DefaultTemplatesPopupMenu: TPopupMenu
    Left = 839
    Top = 53
    object MenuItem6: TMenuItem
      Caption = 'Select All'
      OnClick = MenuItem6Click
    end
    object MenuItem7: TMenuItem
      Caption = 'De-Select All'
      OnClick = MenuItem7Click
    end
    object MenuItem19: TMenuItem
      Caption = '-'
    end
    object Enable2: TMenuItem
      Caption = 'Enable by Default'
      OnClick = Enable2Click
    end
    object DisablebyDefault1: TMenuItem
      Caption = 'Disable by Default'
      OnClick = DisablebyDefault1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object MenuItem18: TMenuItem
      Caption = 'De&lete'
      OnClick = MenuItem18Click
    end
  end
  object PlaylistAutoSaveTimer: TTimer
    Enabled = False
    Interval = 60000
    OnTimer = PlaylistAutoSaveTimerTimer
    Left = 871
    Top = 53
  end
end
