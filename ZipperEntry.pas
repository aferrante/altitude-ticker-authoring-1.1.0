unit ZipperEntry;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, AdStatLt, ExtCtrls, {ad3SpellBase, ad3Spell,} Grids_ts,
  TSGrid, OoMisc, ad3SpellBase, ad3Spell, Globals, Menus, ExtDlgs;

type
  TZipperEntryDlg = class(TForm)
    Panel11: TPanel;
    Label1: TLabel;
    SpellCheckIndicator: TApdStatusLight;
    Label35: TLabel;
    BitBtn2: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn1: TBitBtn;
    RecordGrid: TtsGrid;
    TemplateName: TLabel;
    AddictSpell31: TAddictSpell3;
    HiddenEdit: TEdit;
    Label2: TLabel;
    StyleChipsGrid: TtsGrid;
    BitBtn3: TBitBtn;
    EntryPopup: TPopupMenu;
    Paste1: TMenuItem;
    CopyfromClipboard1: TMenuItem;
    N1: TMenuItem;
    CuttoClipboard1: TMenuItem;
    OpenPictureDialog: TOpenPictureDialog;
    procedure BitBtn2Click(Sender: TObject);
    procedure RecordGridCellChanged(Sender: TObject; OldCol, NewCol, OldRow, NewRow: Integer);
    procedure RecordGridCellEdit(Sender: TObject; DataCol, DataRow: Integer; ByUser: Boolean);
    procedure ProcessFKey(Key: Word);
    procedure PostKeyEx32( key: Word; Const shift: TShiftState; specialkey: Boolean );
    procedure RecordGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BitBtn3Click(Sender: TObject);
    procedure AddStyleChipCode;
    procedure StyleChipsGridDblClick(Sender: TObject);
    procedure Paste1Click(Sender: TObject);
    procedure CopyfromClipboard1Click(Sender: TObject);
    procedure CuttoClipboard1Click(Sender: TObject);
    procedure RecordGridClick(Sender: TObject);
    procedure RecordGridComboInit(Sender: TObject; Combo: TtsComboGrid; DataCol, DataRow: Integer);
    procedure RecordGridComboDropDown(Sender: TObject; Combo: TtsComboGrid; DataCol, DataRow: Integer);
    procedure RecordGridComboCellLoaded(Sender: TObject;
      Combo: TtsComboGrid; DataCol, DataRow: Integer; var Value: Variant);
    procedure AddictSpell31CompleteCheck(Sender: TObject);
    procedure RecordGridButtonClick(Sender: TObject; DataCol,
      DataRow: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ZipperEntryDlg: TZipperEntryDlg;

implementation

uses Main;

{$R *.DFM}

//Perform spell check on entry
procedure TZipperEntryDlg.BitBtn2Click(Sender: TObject);
begin
  //Set spellchecker data dictionary
  AddictSpell31.ConfigDictionaryDir.Add (SpellCheckerDictionaryDir);
  //Launch the spell checker
  HiddenEdit.Text := RecordGrid.Cell[3, RecordGrid.CurrentDataRow];
  AddictSpell31.CheckWinControl(HiddenEdit, ctAll);
  //Light the indicator
  SpellCheckIndicator.Lit := TRUE;
end;

//Handlers to turn off spell check indicator
procedure TZipperEntryDlg.RecordGridCellChanged(Sender: TObject; OldCol,
  NewCol, OldRow, NewRow: Integer);
begin
  //Extinguish spell check light
  SpellCheckIndicator.Lit := FALSE;
end;
procedure TZipperEntryDlg.RecordGridCellEdit(Sender: TObject; DataCol,
  DataRow: Integer; ByUser: Boolean);
begin
  //Extinguish spell check light
  SpellCheckIndicator.Lit := FALSE;
end;

//Procedure to process function keys
procedure TZipperEntryDlg.ProcessFKey(Key: Word);
var
  StyleChipRecPtr: ^StyleChipRec;
  MyShiftState: TShiftState;
begin
  if (Key = VK_F1) then
  begin
    if (StyleChip_Collection.Count >= 1) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(0);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F2) then
  begin
    if (StyleChip_Collection.Count >= 2) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(1);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F3) then
  begin
    if (StyleChip_Collection.Count >= 3) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(2);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F4) then
  begin
    if (StyleChip_Collection.Count >= 4) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(3);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F5) then
  begin
    if (StyleChip_Collection.Count >= 5) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(4);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F6) then
  begin
    if (StyleChip_Collection.Count >= 6) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(5);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F7) then
  begin
    if (StyleChip_Collection.Count >= 7) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(6);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F8) then
  begin
    if (StyleChip_Collection.Count >= 8) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(7);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F9) then
  begin
    if (StyleChip_Collection.Count >= 9) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(8);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F10) then
  begin
    if (StyleChip_Collection.Count >= 10) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(9);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F11) then
  begin
    if (StyleChip_Collection.Count >= 11) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(10);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F12) then
  begin
    if (StyleChip_Collection.Count >= 12) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(11);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end;
end;

//Handler for key down on grid - for processing function keys
procedure TZipperEntryDlg.RecordGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  ProcessFKey(Key);
end;

//General procedure to send the specified key to the keyboard buffer
Procedure TZipperEntryDlg.PostKeyEx32( key: Word; Const shift: TShiftState;
            specialkey: Boolean );
Type
  TShiftKeyInfo = Record
                    shift: Byte;
                    vkey : Byte;
                  End;
  byteset = Set of 0..7;
Const
  shiftkeys: Array [1..3] of TShiftKeyInfo =
    ((shift: Ord(ssCtrl); vkey: VK_CONTROL ),
     (shift: Ord(ssShift); vkey: VK_SHIFT ),
     (shift: Ord(ssAlt); vkey: VK_MENU ));
Var
  flag: DWORD;
  bShift: ByteSet absolute shift;
  i: Integer;
Begin
  For i := 1 To 3 Do Begin
    If shiftkeys[i].shift In bShift Then
      keybd_event( shiftkeys[i].vkey,
                   MapVirtualKey(shiftkeys[i].vkey, 0),
                   0, 0);
  End; { For }
  If specialkey Then
    flag := KEYEVENTF_EXTENDEDKEY
  Else
    flag := 0;


  keybd_event( key, MapvirtualKey( key, 0 ), flag, 0 );
  flag := flag or KEYEVENTF_KEYUP;
  keybd_event( key, MapvirtualKey( key, 0 ), flag, 0 );

  For i := 3 DownTo 1 Do Begin
    If shiftkeys[i].shift In bShift Then
      keybd_event( shiftkeys[i].vkey,
                   MapVirtualKey(shiftkeys[i].vkey, 0),
                   KEYEVENTF_KEYUP, 0);
  End; { For }
End; { PostKeyEx32 }

//Handlers for adding style chip code
procedure TZipperEntryDlg.BitBtn3Click(Sender: TObject);
begin
  AddStyleChipCode;
end;
procedure TZipperEntryDlg.StyleChipsGridDblClick(Sender: TObject);
begin
  AddStyleChipCode;
end;

//Procedure for adding style chip code
procedure TZipperEntryDlg.AddStyleChipCode;
var
  i: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  MyShiftState: TShiftState;
  Buffer1, Buffer2: PChar;
  Size: Integer;
  OutText: String;
begin
  if (StyleChip_Collection.Count > 0) then
  begin
    i := StyleChipsGrid.CurrentDataRow-1;
    if (i < 0) then i := 0;
    //Get the style chip code
    StyleChipRecPtr := StyleChip_Collection.At(i);
    //Add the style chip code to the text
    if (RecordGrid.CellReadOnly[3, RecordGrid.CurrentDataRow]= roOff) then
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
    //Set focus back to grid
    RecordGrid.SetFocus;
    PostKeyEx32(VK_END, MyShiftState, FALSE);
  end;
end;

//Handler to paste the text from the windows clipboard into the current cell of the grid
procedure TZipperEntryDlg.Paste1Click(Sender: TObject);
begin
  RecordGrid.CurrentCell.PasteFromClipboard;
end;

//Handler to Copy the text from to the windows clipboard from the current cell of the grid
procedure TZipperEntryDlg.CopyfromClipboard1Click(Sender: TObject);
begin
  //Select all text and copy to the clipboard
  RecordGrid.CurrentCell.SelectAll;
  RecordGrid.CurrentCell.CopyToClipboard;
end;

//Handler for cut to clipboard
procedure TZipperEntryDlg.CuttoClipboard1Click(Sender: TObject);
begin
  //Select all text and copy to the clipboard
  RecordGrid.CurrentCell.SelectAll;
  RecordGrid.CurrentCell.CopyToClipboard;
  //Clear the text
  RecordGrid.CurrentCell.SelText := '';
end;

//Check to see if any winner/loser boxes need to be checked/unchecked
procedure TZipperEntryDlg.RecordGridClick(Sender: TObject);
var
  i: SmallInt;
begin
  //If checking new visitor winner, check all visitor boxes and un-check home boxes
  if (RecordGrid.Cell[4, RecordGrid.CurrentDataRow] = 5) AND
     (RecordGrid.CellCheckBoxState[3,RecordGrid.CurrentDataRow] = cbChecked) then
  begin
    for i := 1 to RecordGrid.Rows do
      if (RecordGrid.Cell[4, i] = 5) then
        RecordGrid.CellCheckBoxState[3, i] := cbChecked;
    for i := 1 to RecordGrid.Rows do
      if (RecordGrid.Cell[4, i] = 6) then
        RecordGrid.CellCheckBoxState[3, i] := cbUnchecked;
  end
  //If unchecking visitor winner, uncheck all visitor winners
  else if (RecordGrid.Cell[4, RecordGrid.CurrentDataRow] = 5) AND
     (RecordGrid.CellCheckBoxState[3,RecordGrid.CurrentDataRow] = cbUnChecked) then
  begin
    for i := 1 to RecordGrid.Rows do
      if (RecordGrid.Cell[4, i] = 5) then
        RecordGrid.CellCheckBoxState[3, i] := cbUnChecked;
  end
  //If checking new home winner, check all home boxes and un-check visitor boxes
  else if (RecordGrid.Cell[4, RecordGrid.CurrentDataRow] = 6) AND
     (RecordGrid.CellCheckBoxState[3,RecordGrid.CurrentDataRow] = cbChecked) then
  begin
    for i := 1 to RecordGrid.Rows do
      if (RecordGrid.Cell[4, i] = 6) then
        RecordGrid.CellCheckBoxState[3, i] := cbChecked;
    for i := 1 to RecordGrid.Rows do
      if (RecordGrid.Cell[4, i] = 5) then
        RecordGrid.CellCheckBoxState[3, i] := cbUnchecked;
  end
  //If unchecking home winner, uncheck all home winners
  else if (RecordGrid.Cell[4, RecordGrid.CurrentDataRow] = 6) AND
     (RecordGrid.CellCheckBoxState[3,RecordGrid.CurrentDataRow] = cbUnChecked) then
  begin
    for i := 1 to RecordGrid.Rows do
      if (RecordGrid.Cell[4, i] = 6) then
        RecordGrid.CellCheckBoxState[3, i] := cbUnChecked;
  end;
end;

//Init the combo grid
procedure TZipperEntryDlg.RecordGridComboInit(Sender: TObject; Combo: TtsComboGrid; DataCol, DataRow: Integer);
begin
    //Set list type
    Combo.DropDownStyle := ddDropDownList;
    //Set the number of rows displayed to 12 and the return value column to 3
    Combo.DropDownRows := 12;
    Combo.ValueCol := 1;
    //Set AutoSearch on so user input or drop down causes automatic positioning
    Combo.AutoSearch := asNone;
    //Set the number of columns displayed to 1
    Combo.DropDownCols := 1;
    Combo.Grid.Cols := 1;
    Combo.Grid.HeadingOn := False;
    Combo.Grid.RowBarOn := False;
    //Set row properties
    Combo.Grid.Rows := Weather_Icon_Collection.Count;
    Combo.Grid.DefaultRowHeight := 20;
end;

//Init the combo grid
procedure TZipperEntryDlg.RecordGridComboDropDown(Sender: TObject;
  Combo: TtsComboGrid; DataCol, DataRow: Integer);
begin
    //Set list type
    Combo.DropDownStyle := ddDropDownList;
    //Set the number of rows displayed to 12 and the return value column to 3
    Combo.DropDownRows := 12;
    Combo.ValueCol := 1;
    //Set AutoSearch on so user input or drop down causes automatic positioning
    Combo.AutoSearch := asNone;
    //Set the number of columns displayed to 2
    Combo.DropDownCols := 1;
    Combo.Grid.Cols := 1;
    Combo.Grid.HeadingOn := False;
    Combo.Grid.RowBarOn := False;
    //Set row properties
    Combo.Grid.Rows := Weather_Icon_Collection.Count;
    Combo.Grid.DefaultRowHeight := 20;
end;

//Populate the combo grid
procedure TZipperEntryDlg.RecordGridComboCellLoaded(Sender: TObject;
  Combo: TtsComboGrid; DataCol, DataRow: Integer; var Value: Variant);
var
  WeatherIconRecPtr: ^WeatherIconRec;
begin
  if (Weather_Icon_Collection.Count > 0) then
  begin
    WeatherIconRecPtr := Weather_Icon_Collection.At(DataRow-1);
    Value := WeatherIconRecPtr^.Icon_Name;
  end;
end;

//Change complete - copy back to cell
procedure TZipperEntryDlg.AddictSpell31CompleteCheck(Sender: TObject);
begin
  RecordGrid.Cell[3, RecordGrid.CurrentDataRow] := HiddenEdit.Text;
end;

//Added for Altitude Sports
procedure TZipperEntryDlg.RecordGridButtonClick(Sender: TObject; DataCol, DataRow: Integer);
var
  RemoteFilename: String;
  RemoteFileDrive: String;
  HostDrive: String;
begin
  RecordGrid.StoreData := TRUE;

  try
    //Team Logo
    //Chip A
    if (RecordGrid.Cell[DataCol+1, DataRow] = FIELD_TYPE_TEAM_LOGO_1A) or
      (RecordGrid.Cell[DataCol+1, DataRow] = FIELD_TYPE_TEAM_LOGO_2A) then
    begin
      OpenPictureDialog.InitialDir := Team_Logo_Mapped_Drive + '\' + Team_Logo_Base_Path;
      OpenPictureDialog.Filter := 'All (*.tga;*.tif;*)|*.tga;*.tif|TGA Format (*.tga)|*.tga|TIF Format (*.tif)|*.tif';
      HostDrive := Team_Logo_Host_Drive;
    end
    //Chip B
    else if (RecordGrid.Cell[DataCol+1, DataRow] = FIELD_TYPE_TEAM_LOGO_1B) or
      (RecordGrid.Cell[DataCol+1, DataRow] = FIELD_TYPE_TEAM_LOGO_2B) then
    begin
      OpenPictureDialog.InitialDir := Team_Logo_Mapped_Drive + '\' + Team_Logo_Base_Path;
      OpenPictureDialog.Filter := 'All (*.tga;*.tif;*)|*.tga;*.tif|TGA Format (*.tga)|*.tga|TIF Format (*.tif)|*.tif';
      HostDrive := Team_Logo_Host_Drive;
    end;

    //If a file was selected, store only the base name & extension
    if (OpenPictureDialog.Execute) then
    begin
      //RecordGrid.Cell[DataCol, DataRow] := ExtractFileName(OpenPictureDialog.Files[0]);
      RemoteFilename := ExtractFilename(OpenPictureDialog.Files[0]);
      //RemoteFileDrive := ExtractFileDrive (RemoteFilename);
      //RemoteFilename := StringReplace(RemoteFilename, RemoteFileDrive, HostDrive,[]);
      RecordGrid.Cell[DataCol, DataRow] := RemoteFilename;
    end;
  except

  end;
end;

end.
