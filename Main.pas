////////////////////////////////////////////////////////////////////////////////
// Authoring application for 2007 Comcast Ticker
// V1.0  12/27/07  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added additional comments on ticker mode controls
// V1.0.3  03/04/08  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support INI-based offset for game start times to support
// regional sports nets; Modified to support INI-based timezone suffix (e.g. ET,
// PT)
// V1.0.4  03/31/08  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support authoring for multiple playout locations.
// V1.2.0.0  03/24/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support switch for enabling/disabling template field format
// prefixes and suffixes
// V2.0.0.2  04/27/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support game final glyph and fix for suspended & postponed games.
// V2.0.0.3  05/08/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support new formatting functions for game start time, etc.
// V2.0.0.4  05/17/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support new behavior for blanking line-ups while displaying
// sponsor pages.
// V2.0.0.5  05/21/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support "compound" field definitions required for new look
// 2-line mode.
// V2.1.0.0  06/01/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to provide INI setting to turn off local playlist authoring
// controls.
// V2.1.0.1  09/07/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support full-page sponsor logos and templates with rankings
// V2.2.0.0  12/12/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support additional templates for lower-right sponsors
// V2.2.1.0  12/17/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support Stats Inc. for basic game scores/schedules data; most
// references to Sportsticker data removed for clarity
// V2.3.0.0  03/19/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to include DEBUGMODE boolean INI setting to simplify testing;
// removes game data criteria from Games query
// V2.3.0.1  03/31/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to show VCITY and HCITY fields in games grid - was showing team
// nicknames; modified to correct bug with game start time fields showing day
// of week instead of game start time; added symbolic field description for game
// final highlighted in gold.
// V2.3.0.2  04/05/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to include SNY specific feature for copying 2 consecutive 1-line
// game notes to a single 2-line game note when switching modes.
// V2.3.0.3  04/06/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added support for additional data fields used for MLB boxscores (hits,
// errors).
// V2.3.0.4  04/09/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added support for baseball baserunner situation and current batter templates.
// V2.4.0.0  05/25/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Mod to fix issue where inserted pages did not have correct dwell time.
// V2.4.1.0  05/27/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Mod to implement 1-page to 2-page note conversions as previously supported
// by SNY ticker.
// V2.4.2.0  06/18/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added support for look-ahead past current day for Stats Inc. game schedules.
// V2.4.3.0  07/09/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modifications to support Phase 2 Stats Inc. data integration
// V3.0.0.0  09/01/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to correct text placement when showing game start Data + Time in
// 2-line templates.
// V3.0.1.0  11/12/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to fix bug with NFL post-game stats (RB stats used QB data).
// V3.0.2.0  11/29/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support mixed-case abbreviations for stats.
// V3.0.5.0  02/03/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to fix misc. MLB Bugs - Wrong pitcher last name in hero note when
// visitor was winner; added runs to batter hero note; modified Game Phase code
// to handle Grapefruit and Cactus leagues.
// V3.0.7.0  04/04/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Fixed behavior for rain delays in MLB and other special cases.
// V3.0.8.0  04/08/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Fixed issues with MLB games where score did not show up until the bottom of
// the 1st inning; also issue with inning indicator for inter-league games.
// V3.0.9.0  06/20/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Fixed bug where stored proc to get game data was looking for Seeding for
// CFB teams.
// V3.0.11  09/12/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added INI setting for enabling/disabling playlist save functions; added
// support for selecting Ticker database connection from one or more options.
// V3.0.12  01/13/12  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added support to not show playlist save confirm dialog on exit if playlist
// save functions are disabled. Modified logic to prevent schedule overlaps when
// scheduling playlists.
// V3.0.14  02/12/12  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to fix issue where Day of Week + Start Time was getting split into
// two fields for 1-line templates ($Start_Time_No_Timezone).
// V3.0.15  10/02/13  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support Altitude Sports
// V1.0.0  10/25/13  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support preview of playlist records.
// V1.0.1  11/11/13  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added option for allowing specific extended characters.
// V1.0.2  11/15/13  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added support for ticker IDs of up to 8 characters.
// V1.0.3  12/03/13  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added safeguards to prevent entry of blank team logo filenames for alert
// templates.
// V1.0.4  12/05/13  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to not use "GetMixedCase" function for MLS games due to issues with
// specific team names.
// V1.1.0  06/13/14  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, Buttons, ExtCtrls, Menus, Mask, ComCtrls, StBase,
  TSImageList, Grids_ts, TSGrid, TSDBGrid, DBCtrls, JPEG, StColl, MPlayer, OleCtrls,
  OleServer, Excel97, Grids, PBJustOne, ClipBrd, TSMask, DBGrids, OoMisc, AdStatLt,
  Globals, INIFiles, Variants;

{H+} //ANSI strings

//Record type defs
type
  //Main progam object def
  TMainForm = class(TForm)
    ProgramModePageControl: TPageControl;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    Prefs1: TMenuItem;
    SetPrefs1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    TabSheet2: TTabSheet;
    TabSheet4: TTabSheet;
    DBMaintPanel: TPanel;
    Login1: TMenuItem;
    LogintoSystem1: TMenuItem;
    N2: TMenuItem;
    LogoutfromSystem1: TMenuItem;
    PBJustOne1: TPBJustOne;
    Database1: TMenuItem;
    RepopulateTeamsCollection1: TMenuItem;
    TimeOfDayTimer: TTimer;
    ScheduleTab: TTabSheet;
    SchedulePanel: TPanel;
    N3: TMenuItem;
    PurgeScheduledEvents1DayOld1: TMenuItem;
    N4: TMenuItem;
    ShowSponsorLogoDataEntryPage1: TMenuItem;
    HideSponsorLogoDataEntryPage1: TMenuItem;
    tsMaskDefs1: TtsMaskDefs;
    PlaylistSaveControlsPnl: TPanel;
    Label4: TLabel;
    SavePlaylistBtn: TBitBtn;
    LoadPlaylistBtn: TBitBtn;
    ExpandButton: TBitBtn;
    PlaylistSelectTabControl: TTabControl;
    Panel9: TPanel;
    Label34: TLabel;
    Label69: TLabel;
    NumEntries: TLabel;
    BitBtn20: TBitBtn;
    BitBtn18: TBitBtn;
    Panel4: TPanel;
    Label38: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    EntryEnable: TCheckBox;
    EntryNote: TEdit;
    EntryStartEnableDate: TDateTimePicker;
    EntryStartEnableTime: TDateTimePicker;
    EntryEndEnableDate: TDateTimePicker;
    EntryEndEnableTime: TDateTimePicker;
    Panel16: TPanel;
    Label14: TLabel;
    TemplateDescription: TLabel;
    EntryFieldsGrid: TtsGrid;
    PlaylistGrid: TtsGrid;
    PlaylistPopupMenu: TPopupMenu;
    Copy1: TMenuItem;
    Paste1: TMenuItem;
    Duplicate1: TMenuItem;
    Delete1: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    Cut1: TMenuItem;
    Enable1: TMenuItem;
    Disable1: TMenuItem;
    N7: TMenuItem;
    AppendtoEnd1: TMenuItem;
    DuplicateandAppendtoEnd1: TMenuItem;
    N8: TMenuItem;
    EditStartEndEnableTimes1: TMenuItem;
    Panel27: TPanel;
    Label13: TLabel;
    Label15: TLabel;
    Label19: TLabel;
    Label37: TLabel;
    Label68: TLabel;
    ScheduleEntryStartDate: TDateTimePicker;
    ScheduleEntryEndDate: TDateTimePicker;
    Label24: TLabel;
    AvailablePlaylistsGrid: TtsDBGrid;
    Label25: TLabel;
    ScheduledPlaylistsGrid: TtsDBGrid;
    Panel26: TPanel;
    Label36: TLabel;
    BitBtn44: TBitBtn;
    BitBtn43: TBitBtn;
    Panel19: TPanel;
    Label40: TLabel;
    DatabaseMaintPlaylistGrid: TtsDBGrid;
    DBNavigator1: TDBNavigator;
    BitBtn32: TBitBtn;
    BitBtn34: TBitBtn;
    Panel1: TPanel;
    PlaylistModeLabel: TLabel;
    DataEntryPanel: TPanel;
    Panel5: TPanel;
    Label1: TLabel;
    Label11: TLabel;
    LeagueTab: TTabControl;
    GamesDBGrid: TtsDBGrid;
    SponsorsDBGrid: TtsDBGrid;
    AvailableTemplatesDBGrid: TtsDBGrid;
    AddToPlaylistBtn: TBitBtn;
    InsertIntoPlaylistBtn: TBitBtn;
    SponsorDwellPanel: TPanel;
    Label5: TLabel;
    SponsorLogoDwell: TSpinEdit;
    N9: TMenuItem;
    SelectAll1: TMenuItem;
    N10: TMenuItem;
    DeSelectAll1: TMenuItem;
    Panel2: TPanel;
    ApdStatusLight1: TApdStatusLight;
    Label6: TLabel;
    ApdStatusLight2: TApdStatusLight;
    N11: TMenuItem;
    ReconnecttoGraphicsEngine1: TMenuItem;
    ScheduleEntryStartTime: TDateTimePicker;
    ScheduleEntryEndTime: TDateTimePicker;
    Label2: TLabel;
    BitBtn42: TBitBtn;
    PromptForData: TCheckBox;
    Label7: TLabel;
    EntryDwellTime: TSpinEdit;
    Panel3: TPanel;
    DateTimeLabel: TLabel;
    PlaylistNameLabel: TLabel;
    DBMaintPopupMenu: TPopupMenu;
    RenamePlaylist1: TMenuItem;
    SaveClosePlaylistBtn: TBitBtn;
    Label8: TLabel;
    LastSaveTimeLabel: TLabel;
    ManualLeaguePanel: TPanel;
    Label3: TLabel;
    ManualLeagueSelect: TComboBox;
    Image1: TImage;
    ManualOverridePanel: TPanel;
    ManualOverrideBtn: TBitBtn;
    ShowOddsOnly: TCheckBox;
    OddsDBGrid: TtsDBGrid;
    N12: TMenuItem;
    ClearGraphicsOutput1: TMenuItem;
    N13: TMenuItem;
    ClearGraphicsOutput2: TMenuItem;
    ClearExtraLineRegion1: TMenuItem;
    TemplateTimeEditPopupMenu: TPopupMenu;
    EditDefaultTemplateStartEndEnableTimes1: TMenuItem;
    N14: TMenuItem;
    EditSponsorLogoDefinitions1: TMenuItem;
    EditTeamInformation1: TMenuItem;
    EditGamePhaseData1: TMenuItem;
    EditCustomHeaders1: TMenuItem;
    SingleLoopEnable: TCheckBox;
    RegionSelect: TComboBox;
    RegionSelectMode: TRadioGroup;
    RegionLabel: TLabel;
    RegionIDNum: TEdit;
    IDLabel: TLabel;
    DatabaseMaintBreakingNewsPlaylistGrid: TtsDBGrid;
    DefaultTemplatesGrid: TtsGrid;
    DefaultTemplatesPopupMenu: TPopupMenu;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem18: TMenuItem;
    MenuItem19: TMenuItem;
    Enable2: TMenuItem;
    DisablebyDefault1: TMenuItem;
    N1: TMenuItem;
    PlaylistAutoSaveTimer: TTimer;
    Options1: TMenuItem;
    UseRankforNCAAB1: TMenuItem;
    UseSeedforNCAAB1: TMenuItem;
    TickerDatabaseSelection: TMenuItem;
    TickerDB1: TMenuItem;
    TickerDB2: TMenuItem;
    TickerDB3: TMenuItem;
    TickerDB4: TMenuItem;
    TickerDB5: TMenuItem;
    TickerDB6: TMenuItem;
    TickerDB7: TMenuItem;
    TickerDB8: TMenuItem;
    TickerDB9: TMenuItem;
    TickerDB10: TMenuItem;
    StatusBar: TStatusBar;
    Edit1: TMaskEdit;
    //General program functions
    procedure FormActivate(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    //Dialog functions
    procedure About1Click(Sender: TObject);
    procedure SetPrefs1Click(Sender: TObject);
    procedure LoadPrefs;
    //Handlers for UI objects
    procedure ProgramModePageControlChange(Sender: TObject);
    procedure ScheduledPlaylistsGridDblClick(Sender: TObject);
    procedure PlaylistGridDblClick(Sender: TObject);
    procedure PurgeScheduledEvents1DayOld1Click(Sender: TObject);
    procedure LeagueTabChange(Sender: TObject);
    procedure ExpandButtonClick(Sender: TObject);
    procedure AddToPlaylistBtnClick(Sender: TObject);
    procedure InsertIntoPlaylistBtnClick(Sender: TObject);
    procedure AvailableTemplatesDBGridDblClick(Sender: TObject);
    procedure PlaylistGridRowChanged(Sender: TObject; OldRow, NewRow: Integer);
    procedure ScheduleSelectTabChange(Sender: TObject);
    procedure DatabaseMaintSelectTabChange(Sender: TObject);
    procedure PlaylistSelectTabControlChange(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure SavePlaylistBtnClick(Sender: TObject);
    procedure LoadPlaylistBtnClick(Sender: TObject);
    procedure BitBtn32Click(Sender: TObject);
    procedure BitBtn34Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn42Click(Sender: TObject);
    procedure BitBtn44Click(Sender: TObject);
    procedure BitBtn43Click(Sender: TObject);
    //Playlist operations
    procedure AddPlaylistEntry(CurrentTemplateID: SmallInt; AddAllDefaultTemplates: Boolean);
    procedure InsertPlaylistEntry(CurrentTemplateID: SmallInt; AddAllDefaultTemplates: Boolean);
    procedure LoadZipperCollection;
    procedure RefreshEntryFieldsGrid(TickerMode: SmallInt);
    procedure RefreshPlaylistGrid;
    procedure DeleteGraphicFromZipperPlaylist(Confirm: Boolean);
    procedure DeleteZipperPlaylist (PlaylistType: SmallInt; Playlist_ID: Double; Playlist_Description: String);
    function SaveTickerPlaylist(PlaylistType: SmallInt; ClearCollection: Boolean): Boolean;
    procedure DeletePlaylistFromSchedule(PlaylistType: SmallInt; Start_Enable_Time: Double; Station_ID: SmallInt);
    procedure RefreshScheduleGrid;
    procedure EditCurrentScheduleEntry;
    procedure AddScheduleEntry;
    //Utility functions
    procedure ReloadDataFromDatabase;
    function GetPlaylistCount(PlaylistType: SmallInt; SearchStr: String): SmallInt;
    procedure EditZipperPlaylistEntry;
    procedure LoadPlaylistCollection(PlaylistType: SmallInt; AppendMode: SmallInt; PlaylistID: Double);
    procedure RepopulateTeamsCollection1Click(Sender: TObject);
    procedure ReloadGamesfromGametrak1Click(Sender: TObject);
    function GetGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
    function GetGameTimeString(GTimeStr: String): String;
    function GetStatInfo (StoredProcedureName: String): StatRec;
    function GetAutomatedLeagueDisplayMnemonic (League: String): String;
    function GetGameState(GTIME: String; GPHASE: SmallInt): Integer;
    function GetTemplateFieldsCount(Template_ID: SmallInt): SmallInt;
    function GetRecordTypeFromTemplateID(Template_ID: SmallInt): SmallInt;
    function GetTemplateInformation(TemplateID: SmallInt): TemplateDefsRec;
    procedure Delete1Click(Sender: TObject);
    procedure Enable1Click(Sender: TObject);
    procedure Disable1Click(Sender: TObject);
    procedure Copy1Click(Sender: TObject);
    procedure SelectForCopyOrCut;
    procedure Cut1Click(Sender: TObject);
    procedure Paste1Click(Sender: TObject);
    procedure AppendtoEnd1Click(Sender: TObject);
    procedure PasteEntries(AppendToEnd: Boolean);
    procedure DuplicateEntries(AppendToEnd: Boolean; DuplicationCount: SmallInt);
    procedure EditStartEndEnableTimes1Click(Sender: TObject);
    procedure Duplicate1Click(Sender: TObject);
    procedure DuplicateandAppendtoEnd1Click(Sender: TObject);
    function GetRecordTypeDescription (Playlist_Type: SmallInt; Record_Type: SmallInt): String;
    procedure TimeOfDayTimerTimer(Sender: TObject);
    //Procedures for hotkey operations
    procedure Cut;
    procedure Copy;
    procedure Paste;
    procedure PasteEnd;
    procedure Duplicate;
    procedure PlaylistGridKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SelectAll1Click(Sender: TObject);
    procedure DeSelectAll1Click(Sender: TObject);
    procedure SelectAll;
    procedure DeSelectAll;
    procedure MoveRecord(Direction: SmallInt);
    procedure PlaylistGridClick(Sender: TObject);
    procedure ReconnecttoGraphicsEngine1Click(Sender: TObject);
    procedure PlaylistGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure AvailableTemplatesDBGridRowChanged(Sender: TObject; OldRow, NewRow: Variant);
    procedure PreviewCurrentPlaylistRecord;
    procedure SaveClosePlaylistBtnClick(Sender: TObject);
    function ScrubText (InText: String):String;
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ShowOddsOnlyClick(Sender: TObject);
    procedure ClearGraphicsOutput1Click(Sender: TObject);
    procedure ClearGraphicsOutput2Click(Sender: TObject);
    procedure ClearExtraLineRegion1Click(Sender: TObject);
    procedure ClearExtraLineRegion2Click(Sender: TObject);
    procedure EditDefaultTemplateStartEndEnableTimes1Click(Sender: TObject);
    procedure EditSponsorLogoDefinitions1Click(Sender: TObject);
    procedure EditTeamInformation1Click(Sender: TObject);
    procedure EditGamePhaseData1Click(Sender: TObject);
    procedure EditCustomHeaders1Click(Sender: TObject);
    procedure StorePrefs;
    procedure GamesDBGridDblClick(Sender: TObject);
    procedure ManualGameOverride;
    procedure SingleLoopEnableClick(Sender: TObject);
    function CheckForInvalidTemplates(CurrentDisplayMode: SmallInt): Boolean;
    procedure ReAssignTemplateIDsOnModeChange;
    procedure RegionSelectModeClick(Sender: TObject);
    procedure RegionSelectChange(Sender: TObject);
    function GetRecordTypeForTemplate(TemplateID: SmallInt): SmallInt;
    procedure GamesDBGridMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

    //Default templates functions
    procedure RefreshDefaultTemplatesGrid;
    procedure AddDefaultTemplateEntry(CurrentTemplateID: SmallInt);
    procedure InsertDefaultTemplateEntry(CurrentTemplateID: SmallInt);
    procedure MenuItem7Click(Sender: TObject);
    procedure LeagueTabChanging(Sender: TObject; var AllowChange: Boolean);
    procedure SaveDefaultTemplatesList(Category_ID: SmallInt; ClearCollection: Boolean);
    procedure LoadTemplateListBtnClick(Sender: TObject);
    procedure SaveTemplateListBtnClick(Sender: TObject);
    procedure SaveCloseTemplateListBtnClick(Sender: TObject);
    procedure MenuItem18Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure Enable2Click(Sender: TObject);
    procedure DisablebyDefault1Click(Sender: TObject);
    procedure PlaylistAutoSaveTimerTimer(Sender: TObject);
    procedure SavePlaylistWithoutClear;
    procedure UseRankforNCAAB1Click(Sender: TObject);
    procedure UseSeedforNCAAB1Click(Sender: TObject);

    //For database selection
    procedure SetCurrentTickerDatabase(DatabaseIndex: SmallInt);
    procedure SetStatusBar(StatusText: String; HighlightText: Boolean);
    procedure TickerDB1Click(Sender: TObject);
    procedure TickerDB2Click(Sender: TObject);
    procedure TickerDB3Click(Sender: TObject);
    procedure TickerDB4Click(Sender: TObject);
    procedure TickerDB5Click(Sender: TObject);
    procedure TickerDB6Click(Sender: TObject);
    procedure TickerDB7Click(Sender: TObject);
    procedure TickerDB8Click(Sender: TObject);
    procedure TickerDB9Click(Sender: TObject);
    procedure TickerDB10Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

uses Preferences,     //Prefs dialog
     AboutBox,        //About box
     DataModule,      //Data module
     SearchDataEntry, //Dialog used to enter text to search for
     GeneralFunctions,//General purpose functions
     PlaylistSelect,  //Dialog for selecting playlist to load
     PlaylistGraphicsViewer,  //Viewer for Zipper playlists
     ZipperEntryEditor,  //Editor dialog for playlist entries
     ScheduleEntryTimeEditor, //Editor for schedule entry times
     NoteEntryEditor, //Dialog for game note entry & editing
     NCAAManualGameEntry, //Dialog for manual NCAA entries
     ZipperEntry, //Dialog for basic playlist element entry
     DuplicationCountSelect, //Dialog for selecting the number of duplicate records to generate
     EnableDateTimeEditor,
     EngineConnectionPreferences,
     EngineIntf,
     ManualGameEditor,
     SponsorLogoEditor,
     DatabaseEditor,
     GamePhaseEditor,
     CustomCategoryEditor,
     BreakingNewsPlaylistSelect,
     GameDataFunctions, TemplateSelect;

{$R *.DFM}
////////////////////////////////////////////////////////////////////////////////
//Procedures to delete data pointers in each collection node
////////////////////////////////////////////////////////////////////////////////
procedure Ticker_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TickerRec));
end;
procedure Sponsor_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(BreakingNewsRec));
end;
procedure Temp_Ticker_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TickerRec));
end;
procedure Temp_Sponsor_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(BreakingNewsRec));
end;
procedure Team_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TeamRec));
end;
procedure Ticker_Playout_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TickerRec));
end;
procedure SponsorLogo_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(SponsorLogoRec));
end;
procedure PromoLogo_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(PromoLogoRec));
end;
procedure Stat_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(StatRec));
end;
procedure Temp_Stat_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(StatRec));
end;
procedure Game_Phase_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(GamePhaseRec));
end;
procedure Template_Defs_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateDefsRec));
end;
procedure Child_Template_ID_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(ChildTemplateIDRec));
end;
procedure Temporary_Fields_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateFieldsRec));
end;
procedure Template_Fields_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateFieldsRec));
end;
procedure Categories_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(CategoryRec));
end;
procedure Category_Templates_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(CategoryTemplatesRec));
end;
procedure RecordType_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(RecordTypeRec));
end;
procedure StyleChip_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(StyleChipRec));
end;
procedure LeagueCode_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(LeagueCodeRec));
end;
procedure Automated_League_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(AutomatedLeagueRec));
end;
procedure PlayoutStationInfo_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(PlayoutStationInfoRec));
end;
procedure Default_Templates_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(DefaultTemplatesRec));
end;
procedure Selected_Templates_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(DefaultTemplatesRec));
end;
//Added for Altitude Sports
procedure Template_Commands_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateCommandRec));
end;
procedure Temporary_Template_Commands_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateCommandRec));
end;

////////////////////////////////////////////////////////////////////////////////
// GENERAL PROGRAM PROCEDURES, FUNCTIONS AND HANDLERS
////////////////////////////////////////////////////////////////////////////////
//Handler for form activiation - inits done here
procedure TMainForm.FormActivate(Sender: TObject);
var
  Year, Month, Day: Word;
begin
  //Set size
  //MainForm.Height := 1024;
  MainForm.Height := 1001;
  MainForm.Width := 1290;

  //Set defaults
  //Hide scheduling tab for Altitude
  ProgramModePageControl.Pages[0].TabVisible := FALSE;
  LastPageIndex := 0;
  ProgramModePageControl.ActivePageIndex := 1;
  LeagueTab.TabIndex := 0;
  //Init for 2-line mode ticker, looping
  TickerDisplayMode := 4;

  //Load pereferences file
  LoadPrefs;

  //Set loop mode control enable
  if (EnableSingleLoopModeControls) then
    SingleLoopEnable.Enabled := TRUE
  else
    SingleLoopEnable.Enabled := FALSE;

  //Enable/disable playlist save buttons accordingly
  if (EnablePlaylistSaveFunctions) then
  begin
    SavePlaylistBtn.Visible := TRUE;
    SaveClosePlaylistBtn.Visible := TRUE;
    SavePlaylistBtn.Enabled := TRUE;
    SaveClosePlaylistBtn.Enabled := TRUE;
  end
  else begin
    SavePlaylistBtn.Visible := FALSE;
    SaveClosePlaylistBtn.Visible := FALSE;
    SavePlaylistBtn.Enabled := FALSE;
    SaveClosePlaylistBtn.Enabled := FALSE;
  end;

  //Create collections
  Ticker_Collection := TStCollection.Create(1500);
  BreakingNews_Collection := TStCollection.Create(500);
  Temp_Ticker_Collection := TStCollection.Create(1500);
  Temp_BreakingNews_Collection := TStCollection.Create(500);
  Team_Collection := TStCollection.Create(5000);
  Ticker_Playout_Collection := TStCollection.Create(1500);
  Overlay_Collection := TStCollection.Create(250);
  SponsorLogo_Collection := TStCollection.Create(100);
  PromoLogo_Collection := TStCollection.Create(100);
  Stat_Collection := TStCollection.Create(250);
  Temp_Stat_Collection := TStCollection.Create(250);
  Game_Phase_Collection := TStCollection.Create(500);
  Template_Defs_Collection := TStCollection.Create(500);
  Child_Template_ID_Collection := TStCollection.Create(250);
  Template_Fields_Collection := TStCollection.Create(5000);
  Temporary_Fields_Collection := TStCollection.Create(100);
  Categories_Collection := TStCollection.Create(50);
  Category_Templates_Collection := TStCollection.Create(500);
  Sports_Collection := TStCollection.Create(50);
  RecordType_Collection := TStCollection.Create(250);
  StyleChip_Collection := TStCollection.Create(100);
  LeagueCode_Collection := TStCollection.Create(1000);
  Automated_League_Collection := TStCollection.Create(50);
  PlayoutStationInfo_Collection := TStCollection.Create(10);
  Default_Templates_Collection := TStCollection.Create(500);
  Selected_Templates_Collection := TStCollection.Create(250);
  //Added for Altitude Sports
  Template_Commands_Collection := TStCollection.Create(500);
  Temporary_Template_Commands_Collection := TStCollection.Create(50);

  Ticker_Collection.DisposeData := Ticker_Collection_DisposeData;
  BreakingNews_Collection.DisposeData := Sponsor_Collection_DisposeData;
  Temp_Ticker_Collection.DisposeData := Temp_Ticker_Collection_DisposeData;
  Temp_BreakingNews_Collection.DisposeData := Temp_Sponsor_Collection_DisposeData;
  Team_Collection.DisposeData := Team_Collection_DisposeData;
  Ticker_Playout_Collection.DisposeData := Ticker_Playout_Collection_DisposeData;
  SponsorLogo_Collection.DisposeData := SponsorLogo_Collection_DisposeData;
  PromoLogo_Collection.DisposeData := PromoLogo_Collection_DisposeData;
  Stat_Collection.DisposeData := Stat_Collection_DisposeData;
  Temp_Stat_Collection.DisposeData := Temp_Stat_Collection_DisposeData;
  Game_Phase_Collection.DisposeData := Game_Phase_Collection_DisposeData;
  Template_Defs_Collection.DisposeData := Template_Defs_Collection_DisposeData;
  Child_Template_ID_Collection.DisposeData := Child_Template_ID_Collection_DisposeData;
  Template_Fields_Collection.DisposeData := Template_Fields_Collection_DisposeData;
  Temporary_Fields_Collection.DisposeData := Temporary_Fields_Collection_DisposeData;
  Categories_Collection.DisposeData := Categories_Collection_DisposeData;
  Category_Templates_Collection.DisposeData := Category_Templates_Collection_DisposeData;
  RecordType_Collection.DisposeData := RecordType_Collection_DisposeData;
  StyleChip_Collection.DisposeData := StyleChip_Collection_DisposeData;
  LeagueCode_Collection.DisposeData := LeagueCode_Collection_DisposeData;
  Automated_League_Collection.DisposeData := Automated_League_Collection_DisposeData;
  PlayoutStationInfo_Collection.DisposeData := PlayoutStationInfo_Collection_DisposeData;
  Default_Templates_Collection.DisposeData := Default_Templates_Collection_DisposeData;
  Selected_Templates_Collection.DisposeData := Selected_Templates_Collection_DisposeData;
  //Added for Altitude Sports
  Template_Commands_Collection.DisposeData := Template_Commands_Collection_DisposeData;
  Temporary_Template_Commands_Collection.DisposeData := Temporary_Template_Commands_Collection_DisposeData;

  //Activate database & tables
  With dmMain do
  begin
    dbTicker.Connected := FALSE;
    dbTicker.ConnectionString := TickerDatabaseConnectionString[1];
    dbTicker.Connected := TRUE;
    tblTicker_Groups.Active := TRUE;
    tblTicker_Elements.Active := TRUE;
    tblScheduled_Ticker_Groups.Active := TRUE;
    //tblScheduled_BreakingNews_Groups.Active := TRUE;
    tblSponsor_Logos.Active := TRUE;
    dbSportbase.Connected := FALSE;
    dbSportbase.ConnectionString := SportbaseConnectionString;
    dbSportbase.Connected := TRUE;
    //Added for Altitude Sports
    dbLogoImages.Connected := FALSE;
    dbLogoImages.ConnectionString := LogoImagesDatabaseConnectionString;
    dbLogoImages.Connected := TRUE;
  end;

  //Call procedure to load data from database
  ReloadDataFromDatabase;

  //Init checkbox for one loop through if enabled
  if (DefaultToOneLoopThrough) then SingleLoopEnable.Checked := TRUE
  else SingleLoopEnable.Checked := FALSE;

  //Init local playlist controls
  if (EnableLocalPlaylistControls) then
  begin
    RegionSelectMode.Visible := TRUE;
    RegionLabel.Visible := TRUE;
    IDLabel.Visible := TRUE;
    RegionSelect.Visible := TRUE;
    RegionIDNum.Visible := TRUE;
  end
  else begin
    RegionSelectMode.Visible := FALSE;
    RegionLabel.Visible := FALSE;
    IDLabel.Visible := FALSE;
    RegionSelect.Visible := FALSE;
    RegionIDNum.Visible := FALSE;
  end;

  //Init date/time pickers & combo boxes
  ScheduleEntryStartDate.Date := Now;
  ScheduleEntryEndDate.Date := Now+1;
  ScheduleEntryStartTime.Time := 0;
  ScheduleEntryEndTime.Time := 0;
  EntryStartEnableDate.Date := Now;
  EntryStartEnableTime.Time := 0;
  DecodeDate(Now, Year, Month, Day);
  //Allow for leap year
  if (Month = 2) AND (Day = 29) then Day := 28;
  EntryEndEnableDate.Date := EncodeDate(Year+10, Month, Day);
  EntryEndEnableTime.Time := 1-(1/(24*60));

  //Force update of games grid
  LeagueTab.OnChange(self);

  //Set playlist type tab names
  PlaylistSelectTabControl.Tabs.Clear;
  PlaylistSelectTabControl.Tabs.Add('Ticker');
  //Disabled for Altitude Sports
  //PlaylistSelectTabControl.Tabs.Add('Alerts');
  //if (EnableDefaultTemplates) then
  //  PlaylistSelectTabControl.Tabs.Add('Templates');

  //Connect to the graphics engine if it's enabled
  if (EngineParameters.Enabled = TRUE) then
  begin
    //Check for serial port enabled
    if (EngineParameters.UseSerial) then
    begin
      try
        EngineInterface.EngineCOMPort.ComNumber := EngineParameters.COMPort;
        EngineInterface.EngineCOMPort.Baud := EngineParameters.BaudRate;
        EngineInterface.EngineCOMPort.Open := TRUE;
      except
        if (ErrorLoggingEnabled = True) then
        begin
          Error_Condition := True;
          //Label16.Caption := 'ERROR';
          //WriteToErrorLog
          EngineInterface.WriteToErrorLog('Error occurred while trying connect to graphics engine via COM port');
        end;
      end;
    end
    //Check for TCP/IP port enabled
    else begin
      try
        EngineInterface.EnginePort.Address := EngineParameters.IPAddress;
        EngineInterface.EnginePort.Port := StrToIntDef(EngineParameters.Port, 1337);
        EngineInterface.EnginePort.Active := TRUE;
      except
        if (ErrorLoggingEnabled = True) then
        begin
          Error_Condition := True;
          //Label16.Caption := 'ERROR';
          //WriteToErrorLog
          EngineInterface.WriteToErrorLog('Error occurred while trying connect to graphics engine via IP port');
        end;
      end;
    end;
  end;

  //Setup playlist auto-save
  if (PlaylistAutoSaveEnable) then
  begin
    PlaylistAutoSaveTimer.Interval := PlaylistAutoSaveInterval*60000;
    PlaylistAutoSaveTimer.Enabled := TRUE;
  end;

  //Set menu options for seeding
  if (UseSeedingForNCAABGames) then
  begin
    UseRankforNCAAB1.Checked := FALSE;
    UseSeedforNCAAB1.Checked := TRUE;
  end
  else begin
    UseRankforNCAAB1.Checked := TRUE;
    UseSeedforNCAAB1.Checked := FALSE;
  end;

  //Setup for Ticker database selection
  //Top-level menu selection enable
  if (EnableTickerDatabaseSelection) then
  begin
    TickerDatabaseSelection.Visible := TRUE;
    //Set status bar
    StatusBar.Visible := TRUE;
    SetStatusBar('SELECTED DATABASE: ' + TickerDatabaseDescription[1], FALSE);
  end
  else begin
    StatusBar.Visible := FALSE;
    TickerDatabaseSelection.Visible := FALSE;
  end;
  //Enable specifci menu entries
  if (TickerDatabaseConnectionEnable[1]) then
  begin
    TickerDB1.Caption := TickerDatabaseDescription[1];
    TickerDB1.Visible := TRUE;
  end
  else TickerDB1.Visible := FALSE;
  if (TickerDatabaseConnectionEnable[2]) then
  begin
    TickerDB2.Caption := TickerDatabaseDescription[2];
    TickerDB2.Visible := TRUE;
  end
  else TickerDB2.Visible := FALSE;
  if (TickerDatabaseConnectionEnable[3]) then
  begin
    TickerDB3.Caption := TickerDatabaseDescription[3];
    TickerDB3.Visible := TRUE;
  end
  else TickerDB3.Visible := FALSE;
  if (TickerDatabaseConnectionEnable[4]) then
  begin
    TickerDB4.Caption := TickerDatabaseDescription[4];
    TickerDB4.Visible := TRUE;
  end
  else TickerDB4.Visible := FALSE;
  if (TickerDatabaseConnectionEnable[5]) then
  begin
    TickerDB5.Caption := TickerDatabaseDescription[5];
    TickerDB5.Visible := TRUE;
  end
  else TickerDB5.Visible := FALSE;
  if (TickerDatabaseConnectionEnable[6]) then
  begin
    TickerDB6.Caption := TickerDatabaseDescription[6];
    TickerDB6.Visible := TRUE;
  end
  else TickerDB6.Visible := FALSE;
  if (TickerDatabaseConnectionEnable[7]) then
  begin
    TickerDB7.Caption := TickerDatabaseDescription[7];
    TickerDB7.Visible := TRUE;
  end
  else TickerDB7.Visible := FALSE;
  if (TickerDatabaseConnectionEnable[8]) then
  begin
    TickerDB8.Caption := TickerDatabaseDescription[8];
    TickerDB8.Visible := TRUE;
  end
  else TickerDB8.Visible := FALSE;
  if (TickerDatabaseConnectionEnable[9]) then
  begin
    TickerDB9.Caption := TickerDatabaseDescription[9];
    TickerDB9.Visible := TRUE;
  end
  else TickerDB9.Visible := FALSE;
  if (TickerDatabaseConnectionEnable[10]) then
  begin
    TickerDB10.Caption := TickerDatabaseDescription[10];
    TickerDB10.Visible := TRUE;
  end
  else TickerDB10.Visible := FALSE;

  //Delay
  Sleep(1000);

  //Load the layer on the Icon Station
  EngineInterface.LoadIconStationLayer(IconStationLayerName, IconStationTemplateLayer);

  //Make sure graphic is cleared from engine
  //Set controls to abort current event locally
  EngineInterface.InitTicker(TICKER_BACKPLATE_OUT);
  TickerbackplateIn := FALSE;
end;

//Handler for program exit from main menu
procedure TMainForm.Exit1Click(Sender: TObject);
begin
  Close;
end;

//Handler for main program form close
procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  OKToClose: Boolean;
begin
  //Init
  OkToClose := TRUE;
  //Confirm with operator}
  if (MessageDlg('Are you sure you want to exit the Gametrak Ticker Authoring application?',
                  mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
    //Modified for V3_0_14 to only show prompts if playlist save functions enabled
    if (EnablePlaylistSaveFunctions) then
    begin
      //Prompt for save of playlist before exiting
      if (Ticker_Collection.Count > 0) then
      begin
        if (MessageDlg('Do you wish to save the contents of the current ticker playlist before exiting?',
                        mtWarning, [mbYes, mbNo], 0) = mrYes) then
        begin
          //Save the playlist
          if (SaveTickerPlaylist(TICKER, TRUE) = FALSE) then OKToCLose := FALSE;
        end;
      end;
      //Prompt for save of playlist before exiting
      if (BreakingNews_Collection.Count > 0) AND (OKToClose = TRUE) then
      begin
        if (MessageDlg('Do you wish to save the contents of the current Alerts playlist before exiting?',
                        mtWarning, [mbYes, mbNo], 0) = mrYes) then
        begin
          //Save the playlist
          if (SaveTickerPlaylist(BREAKINGNEWS, TRUE) = FALSE) then OKToCLose := FALSE;
        end;
      end;
    end;
    //Set action for main form
    if (OKToClose) then begin
      //Close database connections
      dmMain.dbTicker.Connected := FALSE;
      dmMain.dbSportbase.Connected := FALSE;
      Action := caFree;
    end
    else Action := caNone;
  end
  else
     Action := caNone;
end;

//Procedure to load user preferences
procedure TMainForm.LoadPrefs;
var
  PrefsINI : TINIFile;
begin
  try
    //Create the INI file
    PrefsINI := TINIFile.Create(ExtractFilePath(Application.EXEName) + 'Gametrak_Ticker_Authoring.ini');
    //Read the settings
    StationID := PrefsINI.ReadInteger('General Settings', 'StationID', 1);
    ForceUpperCase := PrefsINI.ReadBool('General Settings', 'Force Upper Case', FALSE);
    AutoTrimText := PrefsINI.ReadBool('General Settings', 'Auto Trim Edited Text', TRUE);
    ForceUpperCaseGameInfo := PrefsINI.ReadBool('General Settings', 'Force Upper Case Game Info', TRUE);
    SpellCheckerDictionaryDir := PrefsINI.ReadString('General Settings', 'Spellchecker Dictionary Path',
      ExtractFilePath(Application.EXEName) + '\Dictionaries');
    //Get game start time offset - expressed in hours, so divide by 24 to get value
    GameStartTimeOffset := (PrefsINI.ReadInteger('General Settings', 'Game Start Time Offset from ET (hours)', 0))/24;
    TimeZoneSuffix := PrefsINI.ReadString('General Settings', 'Time Zone Suffix', 'ET');
    EnableTemplateFieldFormatting := PrefsINI.ReadBool('General Settings', 'Enable Template Field Formatting', TRUE);
    DefaultToOneLoopThrough  := PrefsINI.ReadBool('General Settings', 'Default to One Loop Through', TRUE);
    EnableLocalPlaylistControls := PrefsINI.ReadBool('General Settings', 'Enable Local Playlist Controls', TRUE);
    DebugMode := PrefsINI.ReadBool('General Settings', 'Debug Mode', FALSE);
    EnableGameScheduleLookAhead := PrefsINI.ReadBool('General Settings', 'Enable Game Schedule Look-Ahead', TRUE);
    EnableDefaultTemplates := PrefsINI.ReadBool('General Settings', 'Enable Default Templates', TRUE);
    ShowTimeAndDay := PrefsINI.ReadBool('General Settings', 'Show Time and Day for Schedule', FALSE);
    EnableCrawlTemplates := PrefsINI.ReadBool('General Settings', 'Enable Crawl Templates', FALSE);

    PlaylistAutoSaveEnable := PrefsINI.ReadBool('General Settings', 'Playlist Auto-Save Enable', TRUE);
    PlaylistAutoSaveInterval := PrefsINI.ReadInteger('General Settings', 'Playlist Auto-Save Interval (Minutes)', 2);
    UseSeedingForNCAABGames := PrefsINI.ReadBool('General Settings', 'Use Seeding for NCAAB Games', FALSE);

    EnablePlaylistSaveFunctions := PrefsINI.ReadBool('General Settings', 'Enable Playlist Save Functions', TRUE);

    EnableSingleLoopModeControls := PrefsINI.ReadBool('General Settings', 'Enable Single Loop Mode Controls', FALSE);

    //Do database connections
    EnableTickerDatabaseSelection := PrefsINI.ReadBool('Database Connections', 'Enable Ticker Database Connection Selection', FALSE);
    //Main ticker database #1
    TickerDatabaseConnectionString[1] := PrefsINI.ReadString('Database Connections', 'Ticker Database Connection String',
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=True;User ID=sa;' +
      'Initial Catalog=Altitude_Sports_Ticker;Data Source=OWNER-PC\SQLEXPRESS');
    TickerDatabaseDescription[1] := PrefsINI.ReadString('Database Connections', 'Ticker Database Description', 'Ticker Database 1');
    TickerDatabaseConnectionEnable[1] := PrefsINI.ReadBool('Database Connections', 'Ticker Database Connection Enable', TRUE);
    //Ticker database #2
    TickerDatabaseConnectionString[2] := PrefsINI.ReadString('Database Connections', 'Ticker Database Connection String #2',
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;' +
      'Initial Catalog=SNYTicker;Data Source=(local)');
    TickerDatabaseDescription[2] := PrefsINI.ReadString('Database Connections', 'Ticker Database Description #2', 'Ticker Database 2');
    TickerDatabaseConnectionEnable[2] := PrefsINI.ReadBool('Database Connections', 'Ticker Database Connection Enable #2', FALSE);
    //Ticker database #3
    TickerDatabaseConnectionString[3] := PrefsINI.ReadString('Database Connections', 'Ticker Database Connection String #3',
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;' +
      'Initial Catalog=SNYTicker;Data Source=(local)');
    TickerDatabaseDescription[3] := PrefsINI.ReadString('Database Connections', 'Ticker Database Description #3', 'Ticker Database 3');
    TickerDatabaseConnectionEnable[3] := PrefsINI.ReadBool('Database Connections', 'Ticker Database Connection Enable #3', FALSE);
    //Ticker database #4
    TickerDatabaseConnectionString[4] := PrefsINI.ReadString('Database Connections', 'Ticker Database Connection String #4',
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;' +
      'Initial Catalog=SNYTicker;Data Source=(local)');
    TickerDatabaseDescription[4] := PrefsINI.ReadString('Database Connections', 'Ticker Database Description #4', 'Ticker Database 4');
    TickerDatabaseConnectionEnable[4] := PrefsINI.ReadBool('Database Connections', 'Ticker Database Connection Enable #4', FALSE);
    //Ticker database #5
    TickerDatabaseConnectionString[5] := PrefsINI.ReadString('Database Connections', 'Ticker Database Connection String #5',
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;' +
      'Initial Catalog=SNYTicker;Data Source=(local)');
    TickerDatabaseDescription[5] := PrefsINI.ReadString('Database Connections', 'Ticker Database Description #5', 'Ticker Database 5');
    TickerDatabaseConnectionEnable[5] := PrefsINI.ReadBool('Database Connections', 'Ticker Database Connection Enable #5', FALSE);
    //Ticker database #6
    TickerDatabaseConnectionString[6] := PrefsINI.ReadString('Database Connections', 'Ticker Database Connection String #6',
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;' +
      'Initial Catalog=SNYTicker;Data Source=(local)');
    TickerDatabaseDescription[6] := PrefsINI.ReadString('Database Connections', 'Ticker Database Description #6', 'Ticker Database 6');
    TickerDatabaseConnectionEnable[6] := PrefsINI.ReadBool('Database Connections', 'Ticker Database Connection Enable #6', FALSE);
    //Ticker database #7
    TickerDatabaseConnectionString[7] := PrefsINI.ReadString('Database Connections', 'Ticker Database Connection String #7',
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;' +
      'Initial Catalog=SNYTicker;Data Source=(local)');
    TickerDatabaseDescription[7] := PrefsINI.ReadString('Database Connections', 'Ticker Database Description #7', 'Ticker Database 7');
    TickerDatabaseConnectionEnable[7] := PrefsINI.ReadBool('Database Connections', 'Ticker Database Connection Enable #7', FALSE);
    //Ticker database #8
    TickerDatabaseConnectionString[8] := PrefsINI.ReadString('Database Connections', 'Ticker Database Connection String #8',
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;' +
      'Initial Catalog=SNYTicker;Data Source=(local)');
    TickerDatabaseDescription[8] := PrefsINI.ReadString('Database Connections', 'Ticker Database Description #8', 'Ticker Database 8');
    TickerDatabaseConnectionEnable[8] := PrefsINI.ReadBool('Database Connections', 'Ticker Database Connection Enable #8', FALSE);
    //Ticker database #9
    TickerDatabaseConnectionString[9] := PrefsINI.ReadString('Database Connections', 'Ticker Database Connection String #9',
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;' +
      'Initial Catalog=SNYTicker;Data Source=(local)');
    TickerDatabaseDescription[9] := PrefsINI.ReadString('Database Connections', 'Ticker Database Description #9', 'Ticker Database 9');
    TickerDatabaseConnectionEnable[9] := PrefsINI.ReadBool('Database Connections', 'Ticker Database Connection Enable #9', FALSE);
    //Ticker database #10
    TickerDatabaseConnectionString[10] := PrefsINI.ReadString('Database Connections', 'Ticker Database Connection String #10',
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;' +
      'Initial Catalog=SNYTicker;Data Source=(local)');
    TickerDatabaseDescription[10] := PrefsINI.ReadString('Database Connections', 'Ticker Database Description #10', 'Ticker Database 10');
    TickerDatabaseConnectionEnable[10] := PrefsINI.ReadBool('Database Connections', 'Ticker Database Connection Enable #10', FALSE);
    //Sportbase
    SportbaseConnectionString := PrefsINI.ReadString('Database Connections', 'Sportbase Database Connection String',
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;' +
      'Initial Catalog=Sportbase;Data Source=(local)');
    LogoImagesDatabaseConnectionString := PrefsINI.ReadString('Database Connections', 'Logo Filesnames Database Connection String',
      'Provider=SQLOLEDB.1;Persist Security Info=True;User ID=sa;Password=Vds@dmin1;Initial Catalog=Altitude_Authoring_Images;Data Source=OWNER-PC\SQLEXPRESS');

    EngineParameters.Enabled := PrefsINI.ReadBool('Ticker Engine Connection', 'Enabled', FALSE);
    EngineParameters.UseSerial := PrefsINI.ReadBool('Ticker Engine Connection', 'Use Serial Port', FALSE);
    EngineParameters.BaudRate := PrefsINI.ReadInteger('Ticker Engine Connection', 'Baud Rate', 9600);
    EngineParameters.COMPort := PrefsINI.ReadInteger('Ticker Engine Connection', 'COM Port', 1);
    EngineParameters.IPAddress := PrefsINI.ReadString('Ticker Engine Connection', 'IP Address', '172.28.0.5');
    EngineParameters.Port := PrefsINI.ReadString('Ticker Engine Connection', 'Port', '1337');

    //Added for Altitude Sports
    IconStationLayerName := PrefsINI.ReadString('Icon Station Settings', 'Layer Name', 'AS_Ticker_Final2');
    IconStationTemplateLayer := PrefsINI.ReadString('Icon Station Settings', 'Layer', 'A');
    Sponsor_Logo_Base_Path := PrefsINI.ReadString('Icon Station Settings', 'Sponsor Logo Base Path', 'D:/Altitude/altitude/Ticker graphics/Sales logos');
    Team_Logo_Mapped_Drive := PrefsINI.ReadString('Icon Station Settings', 'Team Logo Mapped Drive', 'C:');
    Team_Logo_Base_Path := PrefsINI.ReadString('Icon Station Settings', 'Team Logo Base Path', 'VDS_Projects\Altitude Sports\Graphics from Altitude\Team chips');
    LEAGUE_CHIP_TEMPLATE_ID := PrefsINI.ReadInteger('Icon Station Settings', 'League Chip Template ID', 0);
    TICKER_IN_TEMPLATE := PrefsINI.ReadInteger('Icon Station Settings', 'Ticker In Template ID', -1);
    TICKER_OUT_TEMPLATE := PrefsINI.ReadInteger('Icon Station Settings', 'Ticker Out Template ID', -2);
    WEBSITE_IN_TEMPLATE := PrefsINI.ReadInteger('Icon Station Settings', 'Website URL In Template ID', -4);
    WEBSITE_OUT_TEMPLATE := PrefsINI.ReadInteger('Icon Station Settings', 'Website URL Out Template ID', -5);
    SPDR_TAGLINE_TEMPLATE := PrefsINI.ReadInteger('Icon Station Settings', 'SPDR Tagline Template ID', 6);
    UseKillAllCommandForClear := PrefsINI.ReadBool('Icon Station Settings', 'Use Kill All Command for Clear', TRUE);

  finally
    //Free up the object
    PrefsINI.Free;
  end;
end;

//Procedure to store user preferences
procedure TMainForm.StorePrefs;
var
  PrefsINI : TINIFile;
begin
  try
    //Create the INI file
    PrefsINI := TINIFile.Create(ExtractFilePath(Application.EXEName) + 'Gametrak_Ticker_Authoring.ini');
    //Save the settings
    PrefsINI.WriteInteger('General Settings', 'StationID', StationID);
    PrefsINI.WriteBool('General Settings', 'Force Upper Case', ForceUpperCase);
    PrefsINI.WriteBool('General Settings', 'Force Upper Case Game Info', ForceUpperCaseGameInfo);
    PrefsINI.WriteBool('General Settings', 'Auto Trim Edited Text', AutoTrimText);
    PrefsINI.WriteString('General Settings', 'Spellchecker Dictionary Path', SpellCheckerDictionaryDir);
    PrefsINI.WriteInteger('General Settings', 'Game Start Time Offset from ET (hours)', Trunc(GameStartTimeOffset*24));
    PrefsINI.WriteString('General Settings', 'Time Zone Suffix', TimeZoneSuffix);
    PrefsINI.WriteBool('General Settings', 'Enable Template Field Formatting', EnableTemplateFieldFormatting);
    PrefsINI.WriteBool('General Settings', 'Default to One Loop Through', DefaultToOneLoopThrough);
    PrefsINI.WriteBool('General Settings', 'Enable Local Playlist Controls', EnableLocalPlaylistControls);
    PrefsINI.WriteBool('General Settings', 'Debug Mode', DebugMode);
    PrefsINI.WriteBool('General Settings', 'Enable Game Schedule Look-Ahead', EnableGameScheduleLookAhead);
    PrefsINI.WriteBool('General Settings', 'Enable Default Templates', EnableDefaultTemplates);
    PrefsINI.WriteBool('General Settings', 'Show Time and Day for Schedule', ShowTimeAndDay);
    PrefsINI.WriteBool('General Settings', 'Enable Crawl Templates', EnableCrawlTemplates);

    PrefsINI.WriteBool('General Settings', 'Playlist Auto-Save Enable', PlaylistAutoSaveEnable);
    PrefsINI.WriteInteger('General Settings', 'Playlist Auto-Save Interval (Minutes)', PlaylistAutoSaveInterval);
    PrefsINI.WriteBool('General Settings', 'Use Seeding for NCAAB Games', UseSeedingForNCAABGames);

    PrefsINI.WriteBool('General Settings', 'Enable Playlist Save Functions', EnablePlaylistSaveFunctions);

    PrefsINI.WriteBool('General Settings', 'Enable Single Loop Mode Controls', EnableSingleLoopModeControls);

    //Do database connections
    PrefsINI.WriteBool('Database Connections', 'Enable Ticker Database Connection Selection', EnableTickerDatabaseSelection);
    //Main ticker database #1
    PrefsINI.WriteString('Database Connections', 'Ticker Database Connection String', TickerDatabaseConnectionString[1]);
    PrefsINI.WriteString('Database Connections', 'Ticker Database Description', TickerDatabaseDescription[1]);
    PrefsINI.WriteBool('Database Connections', 'Ticker Database Connection Enable', TickerDatabaseConnectionEnable[1]);
    //Ticker database #2
    PrefsINI.WriteString('Database Connections', 'Ticker Database Connection String #2', TickerDatabaseConnectionString[2]);
    PrefsINI.WriteString('Database Connections', 'Ticker Database Description #2', TickerDatabaseDescription[2]);
    PrefsINI.WriteBool('Database Connections', 'Ticker Database Connection Enable #2', TickerDatabaseConnectionEnable[2]);
    //Ticker database #3
    PrefsINI.WriteString('Database Connections', 'Ticker Database Connection String #3', TickerDatabaseConnectionString[3]);
    PrefsINI.WriteString('Database Connections', 'Ticker Database Description #3', TickerDatabaseDescription[3]);
    PrefsINI.WriteBool('Database Connections', 'Ticker Database Connection Enable #3', TickerDatabaseConnectionEnable[3]);
    //Ticker database #4
    PrefsINI.WriteString('Database Connections', 'Ticker Database Connection String #4', TickerDatabaseConnectionString[4]);
    PrefsINI.WriteString('Database Connections', 'Ticker Database Description #4', TickerDatabaseDescription[4]);
    PrefsINI.WriteBool('Database Connections', 'Ticker Database Connection Enable #4', TickerDatabaseConnectionEnable[4]);
    //Ticker database #5
    PrefsINI.WriteString('Database Connections', 'Ticker Database Connection String #5', TickerDatabaseConnectionString[5]);
    PrefsINI.WriteString('Database Connections', 'Ticker Database Description #5', TickerDatabaseDescription[5]);
    PrefsINI.WriteBool('Database Connections', 'Ticker Database Connection Enable #5', TickerDatabaseConnectionEnable[5]);
    //Ticker database #6
    PrefsINI.WriteString('Database Connections', 'Ticker Database Connection String #6', TickerDatabaseConnectionString[6]);
    PrefsINI.WriteString('Database Connections', 'Ticker Database Description #6', TickerDatabaseDescription[6]);
    PrefsINI.WriteBool('Database Connections', 'Ticker Database Connection Enable #6', TickerDatabaseConnectionEnable[6]);
    //Ticker database #7
    PrefsINI.WriteString('Database Connections', 'Ticker Database Connection String #7', TickerDatabaseConnectionString[7]);
    PrefsINI.WriteString('Database Connections', 'Ticker Database Description #7', TickerDatabaseDescription[7]);
    PrefsINI.WriteBool('Database Connections', 'Ticker Database Connection Enable #7', TickerDatabaseConnectionEnable[7]);
    //Ticker database #8
    PrefsINI.WriteString('Database Connections', 'Ticker Database Connection String #8', TickerDatabaseConnectionString[8]);
    PrefsINI.WriteString('Database Connections', 'Ticker Database Description #8', TickerDatabaseDescription[8]);
    PrefsINI.WriteBool('Database Connections', 'Ticker Database Connection Enable #8', TickerDatabaseConnectionEnable[8]);
    //Ticker database #9
    PrefsINI.WriteString('Database Connections', 'Ticker Database Connection String #9', TickerDatabaseConnectionString[9]);
    PrefsINI.WriteString('Database Connections', 'Ticker Database Description #9', TickerDatabaseDescription[9]);
    PrefsINI.WriteBool('Database Connections', 'Ticker Database Connection Enable #9', TickerDatabaseConnectionEnable[9]);
    //Ticker database #10
    PrefsINI.WriteString('Database Connections', 'Ticker Database Connection String #10', TickerDatabaseConnectionString[10]);
    PrefsINI.WriteString('Database Connections', 'Ticker Database Description #10', TickerDatabaseDescription[10]);
    PrefsINI.WriteBool('Database Connections', 'Ticker Database Connection Enable #10', TickerDatabaseConnectionEnable[10]);
    //Sportbase
    PrefsINI.WriteString('Database Connections', 'Sportbase Database Connection String', SportbaseConnectionString);
    PrefsINI.WriteString('Database Connections', 'Logo Filesnames Database Connection String', LogoImagesDatabaseConnectionString);

    PrefsINI.WriteBool('Ticker Engine Connection', 'Enabled', EngineParameters.Enabled);
    PrefsINI.WriteBool('Ticker Engine Connection', 'Use Serial Port', EngineParameters.UseSerial);
    PrefsINI.WriteInteger('Ticker Engine Connection', 'Baud Rate', EngineParameters.BaudRate);
    PrefsINI.WriteInteger('Ticker Engine Connection', 'COM Port', EngineParameters.COMPort);
    PrefsINI.WriteString('Ticker Engine Connection', 'IP Address', EngineParameters.IPAddress);
    PrefsINI.WriteString('Ticker Engine Connection', 'Port', EngineParameters.Port);

    //Added for Altitude Sports
    PrefsINI.WriteString('Icon Station Settings', 'Layer Name', IconStationLayerName);
    PrefsINI.WriteString('Icon Station Settings', 'Layer', IconStationTemplateLayer);
    PrefsINI.WriteString('Icon Station Settings', 'Sponsor Logo Base Path', Sponsor_Logo_Base_Path);
    PrefsINI.WriteString('Icon Station Settings', 'Team Logo Mapped Drive', Team_Logo_Mapped_Drive);
    PrefsINI.WriteString('Icon Station Settings', 'Team Logo Base Path', Team_Logo_Base_Path);
    PrefsINI.WriteInteger('Icon Station Settings', 'League Chip Template ID', LEAGUE_CHIP_TEMPLATE_ID);
    PrefsINI.WriteInteger('Icon Station Settings', 'Ticker In Template ID', TICKER_IN_TEMPLATE);
    PrefsINI.WriteInteger('Icon Station Settings', 'Ticker Out Template ID', TICKER_OUT_TEMPLATE);
    PrefsINI.WriteInteger('Icon Station Settings', 'Website URL In Template ID', WEBSITE_IN_TEMPLATE);
    PrefsINI.WriteInteger('Icon Station Settings', 'Website URL Out Template ID', WEBSITE_OUT_TEMPLATE);
    PrefsINI.WriteInteger('Icon Station Settings', 'SPDR Tagline Template ID', SPDR_TAGLINE_TEMPLATE);
    PrefsINI.WriteBool('Icon Station Settings', 'Use Kill All Command for Clear', UseKillAllCommandForClear);

  finally
    //Free up the object
    PrefsINI.Free;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES FOR LAUNCHING VARIOUS PROGRAM DIALOGS
////////////////////////////////////////////////////////////////////////////////
//Display about box
procedure TMainForm.About1Click(Sender: TObject);
var
  Modal: TAbout;
begin
  Modal := TAbout.Create(Application);
  try
    Modal.ShowModal;
  finally
    Modal.Free
  end;
end;

//Handler to display dialog for setting user preferences
procedure TMainForm.SetPrefs1Click(Sender: TObject);
var
  Modal: TPrefs;
  Control: Word;
begin
  Modal := TPrefs.Create(Application);
  try
    Modal.SpinEdit1.Value := StationID;
    Modal.Edit1.Text := SpellCheckerDictionaryDir;
    Modal.Edit2.Text := TickerDatabaseConnectionString[1];
    Modal.Edit3.Text := SportbaseConnectionString;
    Modal.Edit4.Text := LogoImagesDatabaseConnectionString;
    Modal.EngineEnable.Checked := EngineParameters.Enabled;
    if (EngineParameters.UseSerial) then
      Modal.RadioGroup1.ItemIndex := 1
    else
      Modal.RadioGroup1.ItemIndex := 0;
    Modal.EngineIPAddress.Text := EngineParameters.IPAddress;
    Modal.EnginePort.Text := EngineParameters.Port;
    Modal.EngineComPortNumber.ItemIndex := EngineParameters.COMPort-1;
    if (EngineParameters.BaudRate = 9600) then
      Modal.EngineBaudRate.ItemIndex := 0
    else
      Modal.EngineBaudRate.ItemIndex := 1;
    Modal.ForceUpperCaseCheckBox.Checked := ForceUpperCase;
    Modal.ForceUpperCaseGameInfoCheckBox.Checked := ForceUpperCaseGameInfo;
    Modal.DefaultToSingleLoopCheckBox.Checked := DefaultToOneLoopThrough;

    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    //Set new values if user didn't cancel out
    if (Control = mrOK) then
    begin
      ForceUpperCase := Modal.ForceUpperCaseCheckbox.Checked;
      ForceUpperCaseGameInfo := Modal.ForceUpperCaseGameInfoCheckBox.Checked;
      DefaultToOneLoopThrough := Modal.DefaultToSingleLoopCheckBox.Checked;
      StationID := Modal.SpinEdit1.Value;
      TickerDatabaseConnectionString[1] := Modal.Edit2.Text;
      SportbaseConnectionString := Modal.Edit3.Text;
      LogoImagesDatabaseConnectionString := Modal.Edit4.Text;
      SpellCheckerDictionaryDir := Modal.Edit1.Text;
      EngineParameters.Enabled := Modal.EngineEnable.Checked;
      if (Modal.RadioGroup1.ItemIndex = 1) then
        EngineParameters.UseSerial := TRUE
      else
        EngineParameters.UseSerial := FALSE;
      EngineParameters.IPAddress := Modal.EngineIPAddress.Text;
      EngineParameters.Port := Modal.EnginePort.Text;
      EngineParameters.COMPort := Modal.EngineComPortNumber.ItemIndex+1;
      if (Modal.EngineBaudRate.ItemIndex = 0) then
        EngineParameters.BaudRate := 9600
      else
        EngineParameters.BaudRate := 19200;
      DefaultTickerMode := Modal.RadioGroup1.ItemIndex+1;
      //Activate database & tables
      With dmMain do
      begin
        dbTicker.Connected := FALSE;
        dbTicker.ConnectionString := TickerDatabaseConnectionString[1];
        dbTicker.Connected := TRUE;
        tblTicker_Elements.Active := TRUE;
        tblTicker_Groups.Active := TRUE;
        tblScheduled_Ticker_Groups.Active := TRUE;
        //tblBreakingNews_Elements.Active := TRUE;
        //tblBreakingNews_Groups.Active := TRUE;
        //tblScheduled_BreakingNews_Groups.Active := TRUE;
        tblSponsor_Logos.Active := TRUE;
        dbSportbase.Connected := FALSE;
        dbSportbase.ConnectionString := SportbaseConnectionString;
        dbSportbase.Connected := TRUE;
        //Added for Altitude Sports
        dbLogoImages.Connected := FALSE;
        dbLogoImages.ConnectionString := LogoImagesDatabaseConnectionString;
        dbLogoImages.Connected := TRUE;
      end;
    end;
    Modal.Free
  end;
  //Store the preferences
  StorePrefs;
end;

////////////////////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS
// Various functions used for collection lookup, data processing, etc.
////////////////////////////////////////////////////////////////////////////////
function TMainForm.ScrubText (InText: String) : String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := '';
  for i := 1 to Length(InText) do
    //Filter out control characters, etc.
    if ((Ord(InText[i]) >= 32) AND (Ord(InText[i]) <= 126)) OR
    //V1.0.2 added support for allowing extended characters
    (Ord(InText[i]) = 169) OR (Ord(InText[i]) = 174) OR (Ord(InText[i]) = 177) then
      OutStr := OutStr + InText[i];
  //Remove leading and trailing blanks from story
  ScrubText := OutStr;
end;

//Handler to reload collections
procedure TMainForm.RepopulateTeamsCollection1Click(Sender: TObject);
begin
  ReloadDataFromDatabase;
end;
//General procedure to cache in data from database into collections
procedure TMainForm.ReloadDataFromDatabase;
var
  TeamPtr: ^TeamRec; //Pointer to team collection object
  SponsorLogoPtr: ^SponsorLogoRec; //Pointer to sponsor logo object
  GamePhasePtr: ^GamePhaseRec;
  TemplateDefRecPtr: ^TemplateDefsRec;
  TemplateCommandRecPtr: ^TemplateCommandRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  ChildTemplateIDRecPtr: ^ChildTemplateIDRec;
  CategoryRecPtr: ^CategoryRec;
  CategoryTemplatesRecPtr: ^CategoryTemplatesRec;
  RecordTypeRecPtr: ^RecordTypeRec;
  StyleChipRecPtr: ^StyleChipRec;
  LeagueCodeRecPtr: ^LeagueCodeRec;
  AutomatedLeagueRecPtr: ^AutomatedLeagueRec;
  PlayoutStationInfoRecPtr: ^PlayoutStationInfoRec;
begin
  //Load collections for users and block subcategories
  Team_Collection.Clear;
  Team_Collection.Pack;
  //Load in teams information
  dmMain.Query1.SQL.Clear;
  dmMain.Query1.SQL.Add('SELECT * FROM Teams');
  dmMain.Query1.Open;
  if (dmMain.Query1.RecordCount > 0) then
  begin
    dmMain.Query1.First;
    repeat
      //Add item to teams collection
      GetMem (TeamPtr, SizeOf(TeamRec));
      With TeamPtr^ do
      begin
        League := dmMain.Query1.FieldByName('League').AsString;
        //OldSTTeamCode := dmMain.Query1.FieldByName('OldSTTeamCode').AsString;
        NewSTTeamCode := dmMain.Query1.FieldByName('NewSTTeamCode').AsString;
        StatsIncID := dmMain.Query1.FieldByName('StatsIncID').AsFloat;
        LongName := dmMain.Query1.FieldByName('LongName').AsString;
        ShortName := dmMain.Query1.FieldByName('ShortName').AsString;
        BaseName := dmMain.Query1.FieldByName('BaseName').AsString;
        DisplayName1 := dmMain.Query1.FieldByName('DisplayName1').AsString;
        DisplayName2 := dmMain.Query1.FieldByName('DisplayName2').AsString;
        If (Team_Collection.Count <= 5000) then
        begin
          Team_Collection.Insert(TeamPtr);
          Team_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.Query1.Next;
    until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
  end
  else begin
    MessageDlg('No data found in Teams database table!', mtError, [mbOk], 0);
  end;
  //Close query
  dmMain.Query1.Active := FALSE;

  //Load in the data from the game phase codes table; iterate for all records in table
  Game_Phase_Collection.Clear;
  Game_Phase_Collection.Pack;
  dmMain.Query1.SQL.Clear;
  dmMain.Query1.SQL.Add('SELECT * FROM Game_Phase_Codes');
  dmMain.Query1.Open;
  if (dmMain.Query1.RecordCount > 0) then
  begin
    dmMain.Query1.First;
    repeat
      //Add item to scripts collection
      GetMem (GamePhasePtr, SizeOf(GamePhaseRec));
      With GamePhasePtr^ do
      begin
        League := dmMain.Query1.FieldByName('League').AsString;
        ST_Phase_Code := dmMain.Query1.FieldByName('ST_Phase_Code').AsInteger;
        SI_Phase_Code := dmMain.Query1.FieldByName('SI_Phase_Code').AsInteger;
        Display_Period := dmMain.Query1.FieldByName('Display_Period').AsString;
        End_Is_Half := dmMain.Query1.FieldByName('End_Is_Half').AsBoolean;
        Display_Half := dmMain.Query1.FieldByName('Display_Half').AsString;
        Display_Final := dmMain.Query1.FieldByName('Display_Final').AsString;
        Display_Extended1 := dmMain.Query1.FieldByName('Display_Extended1').AsString;
        Display_Extended2 := dmMain.Query1.FieldByName('Display_Extended2').AsString;
        If (Game_Phase_Collection.Count <= 500) then
        begin
          //Add to collection
          Game_Phase_Collection.Insert(GamePhasePtr);
          Game_Phase_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.Query1.Next;
    until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
  end
  else begin
    MessageDlg('No data found in Game Phase database table!', mtError, [mbOk], 0);
  end;
  //Close query
  dmMain.Query1.Active := FALSE;

  //Load in the data from the sponsor logos table; iterate for all records in table
  SponsorLogo_Collection.Clear;
  SponsorLogo_Collection.Pack;
  dmMain.Query1.SQL.Clear;
  dmMain.Query1.SQL.Add('SELECT * FROM Sponsor_Logos');
  dmMain.Query1.Open;
  if (dmMain.Query1.RecordCount > 0) then
  begin
    dmMain.Query1.First;
    repeat
      //Add item to scripts collection
      GetMem (SponsorLogoPtr, SizeOf(SponsorLogoRec));
      With SponsorLogoPtr^ do
      begin
        SponsorLogoIndex := dmMain.Query1.FieldByName('LogoIndex').AsInteger;
        SponsorLogoName := dmMain.Query1.FieldByName('LogoName').AsString;
        SponsorLogoFilename := dmMain.Query1.FieldByName('LogoFilename').AsString;
        If (SponsorLogo_Collection.Count <= 100) then
        begin
          //Add to collection
          SponsorLogo_Collection.Insert(SponsorLogoPtr);
          SponsorLogo_Collection.Pack;
          //Add to comboboxes
          //ComboBox4.Items.Add(SponsorLogoName);
        end;
      end;
      //Got to next record
      dmMain.Query1.Next;
    until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
  end;
  //Close query
  dmMain.Query1.Active := FALSE;

  //Load in the data from the template definitions table; iterate for all records in table
  Template_Defs_Collection.Clear;
  Template_Defs_Collection.Pack;
  dmMain.Query1.SQL.Clear;
  dmMain.Query1.SQL.Add('SELECT * FROM Template_Defs');
  dmMain.Query1.Open;
  if (dmMain.Query1.RecordCount > 0) then
  begin
    dmMain.Query1.First;
    repeat
      //Add item to scripts collection
      GetMem (TemplateDefRecPtr, SizeOf(TemplateDefsRec));
      With TemplateDefRecPtr^ do
      begin
        Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
        Template_Type := dmMain.Query1.FieldByName('Template_Type').AsInteger;
        AlternateModeTemplateID := dmMain.Query1.FieldByName('AlternateModeTemplateID').AsInteger;
        Template_Description := dmMain.Query1.FieldByName('Template_Description').AsString;
        Template_Has_Children := dmMain.Query1.FieldByName('Template_Has_Children').AsBoolean;
        Template_Is_Child := dmMain.Query1.FieldByName('Template_Is_Child').AsBoolean;
        Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
        TemplateSponsorType := dmMain.Query1.FieldByName('TemplateSponsorType').AsInteger;
        Engine_Template_ID := dmMain.Query1.FieldByName('Engine_Template_ID').AsInteger;
        Default_Dwell := dmMain.Query1.FieldByName('Default_Dwell').AsInteger;
        ManualLeague := dmMain.Query1.FieldByName('ManualLeague').AsBoolean;
        UsesGameData := dmMain.Query1.FieldByName('UsesGameData').AsBoolean;
        HideWebsiteURL := dmMain.Query1.FieldByName('HideWebsiteURL').AsBoolean;
        ShowLeagueChip := dmMain.Query1.FieldByName('ShowLeagueChip').AsBoolean;
        RequiredGameState := dmMain.Query1.FieldByName('RequiredGameState').AsInteger;
        //Special feature for SNY
        if (AllowAlertBackgroundsForNews) then
          Use_Alert_Background := dmMain.Query1.FieldByName('Use_Alert_Background').AsBoolean;
        StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
        EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
        Template_Layer_Name := dmMain.Query1.FieldByName('Template_Layer_Name').AsString;
        If (Template_Defs_Collection.Count <= 250) then
        begin
          //Add to collection
          Template_Defs_Collection.Insert(TemplateDefRecPtr);
          Template_Defs_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.Query1.Next;
    until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
  end
  else begin
    MessageDlg('No data found in Template Definitions database table!', mtError, [mbOk], 0);
  end;
  //Close query
  dmMain.Query1.Active := FALSE;

  //Added for Altitude Sports
  //Load in the data from the template commands table; iterate for all records in table
  Template_Commands_Collection.Clear;
  Template_Commands_Collection.Pack;
  dmMain.Query1.SQL.Clear;
  dmMain.Query1.SQL.Add('SELECT * FROM Template_Commands');
  dmMain.Query1.Open;
  if (dmMain.Query1.RecordCount > 0) then
  begin
    dmMain.Query1.First;
    repeat
      //Add item to scripts collection
      GetMem (TemplateCommandRecPtr, SizeOf(TemplateCommandRec));
      With TemplateCommandRecPtr^ do
      begin
        Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
        Command_Type := dmMain.Query1.FieldByName('Command_Type').AsInteger;
        Command_Index := dmMain.Query1.FieldByName('Command_Index').AsInteger;
        Command_Text := dmMain.Query1.FieldByName('Command_Text').AsString;
        if (Template_Commands_Collection.Count <= 500) then
        begin
          //Add to collection
          Template_Commands_Collection.Insert(TemplateCommandRecPtr);
          Template_Commands_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.Query1.Next;
    until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
  end
  else begin
    MessageDlg('No data found in Template Commands database table!', mtError, [mbOk], 0);
  end;
  //Close query
  dmMain.Query1.Active := FALSE;

  //Load in the data from the child template definitions table; iterate for all records in table
  Child_Template_ID_Collection.Clear;
  Child_Template_ID_Collection.Pack;
  dmMain.Query1.SQL.Clear;
  dmMain.Query1.SQL.Add('SELECT * FROM Child_Template_IDs');
  dmMain.Query1.Open;
  if (dmMain.Query1.RecordCount > 0) then
  begin
    dmMain.Query1.First;
    repeat
      //Add item to scripts collection
      GetMem (ChildTemplateIDRecPtr, SizeOf(ChildTemplateIDRec));
      With ChildTemplateIDRecPtr^ do
      begin
        Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
        Child_Template_ID := dmMain.Query1.FieldByName('Child_Template_ID').AsInteger;
        Child_Default_Enable_State := dmMain.Query1.FieldByName('Child_Default_Enable_State').AsBoolean;
        If (Child_Template_ID_Collection.Count <= 250) then
        begin
          //Add to collection
          Child_Template_ID_Collection.Insert(ChildTemplateIDRecPtr);
          Child_Template_ID_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.Query1.Next;
    until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
  end;
  //Close query
  dmMain.Query1.Active := FALSE;

  //Load in the data from the template fieldss table; iterate for all records in table
  Template_Fields_Collection.Clear;
  Template_Fields_Collection.Pack;
  dmMain.Query1.SQL.Clear;
  dmMain.Query1.SQL.Add('SELECT * FROM Template_Fields');
  dmMain.Query1.Open;
  if (dmMain.Query1.RecordCount > 0) then
  begin
    dmMain.Query1.First;
    repeat
      //Add item to scripts collection
      GetMem (TemplateFieldsRecPtr, SizeOf(TemplateFieldsRec));
      With TemplateFieldsRecPtr^ do
      begin
        Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
        Field_ID := dmMain.Query1.FieldByName('Field_ID').AsInteger;
        Field_Type := dmMain.Query1.FieldByName('Field_Type').AsInteger;
        Field_Is_Manual := dmMain.Query1.FieldByName('Field_Is_Manual').AsBoolean;
        Field_Label := dmMain.Query1.FieldByName('Field_Label').AsString;
        Field_Contents := dmMain.Query1.FieldByName('Field_Contents').AsString;
        Field_Format_Prefix := dmMain.Query1.FieldByName('Field_Format_Prefix').AsString;
        Field_Format_Suffix := dmMain.Query1.FieldByName('Field_Format_Suffix').AsString;
        Field_Max_Length := dmMain.Query1.FieldByName('Field_Max_Length').AsInteger;
        Engine_Field_ID := dmMain.Query1.FieldByName('Engine_Field_ID').AsInteger;
        Layout_Region_Name := dmMain.Query1.FieldByName('Layout_Region_Name').AsString;
        Layout_Field_Name := dmMain.Query1.FieldByName('Layout_Field_Name').AsString;
        Field_Is_Crawl := dmMain.Query1.FieldByName('Field_Is_Crawl').AsBoolean;
        Crawl_Pad_Leading := dmMain.Query1.FieldByName('Crawl_Pad_Leading').AsInteger;
        Crawl_Pad_Trailing := dmMain.Query1.FieldByName('Crawl_Pad_Trailing').AsInteger;
        If (Template_Fields_Collection.Count <= 5000) then
        begin
          //Add to collection
          Template_Fields_Collection.Insert(TemplateFieldsRecPtr);
          Template_Fields_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.Query1.Next;
    until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
  end
  else begin
    MessageDlg('No data found in Template Fields database table!', mtError, [mbOk], 0);
  end;
  //Close query
  dmMain.Query1.Active := FALSE;

  //Load in the data from the categories table; iterate for all records in table
  Categories_Collection.Clear;
  Categories_Collection.Pack;
  dmMain.Query1.SQL.Clear;
  dmMain.Query1.SQL.Add('SELECT * FROM Categories');
  dmMain.Query1.Open;
  if (dmMain.Query1.RecordCount > 0) then
  begin
    dmMain.Query1.First;
    //Clear the categories tab control string list
    LeagueTab.Tabs.Clear;
    repeat
      //Add item to scripts collection
      GetMem (CategoryRecPtr, SizeOf(CategoryRec));
      With CategoryRecPtr^ do
      begin
        Category_Type := dmMain.Query1.FieldByName('Category_Type').AsInteger;
        Category_ID := dmMain.Query1.FieldByName('Category_ID').AsInteger;
        Category_Name := dmMain.Query1.FieldByName('Category_Name').AsString;
        Category_Label := dmMain.Query1.FieldByName('Category_Label').AsString;
        Category_Description := dmMain.Query1.FieldByName('Category_Description').AsString;
        Category_Is_Sport := dmMain.Query1.FieldByName('Category_Is_Sport').AsBoolean;
        Category_Sport := dmMain.Query1.FieldByName('Category_Sport').AsString;
        Sport_Games_Table_Name := dmMain.Query1.FieldByName('Sport_Games_Table_Name').AsString;
            If (Categories_Collection.Count <= 50) then
        begin
          //Add to collection
          Categories_Collection.Insert(CategoryRecPtr);
          Categories_Collection.Pack;
          //Add the category name to the tab list if it's the first category (init)
          if (Category_Type = 1) then LeagueTab.Tabs.Add (Category_Name);
        end;
      end;
      //Got to next record
      dmMain.Query1.Next;
    until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
  end
  else begin
    MessageDlg('No data found in Ticker Categories database table!', mtError, [mbOk], 0);
  end;
  //Close query
  dmMain.Query1.Active := FALSE;

  //Load in the data from the categories table; iterate for all records in table
  Category_Templates_Collection.Clear;
  Category_Templates_Collection.Pack;
  dmMain.Query1.SQL.Clear;
  dmMain.Query1.SQL.Add('SELECT * FROM Category_Templates');
  dmMain.Query1.Open;
  if (dmMain.Query1.RecordCount > 0) then
  begin
    dmMain.Query1.First;
    repeat
      //Add item to scripts collection
      GetMem (CategoryTemplatesRecPtr, SizeOf(CategoryTemplatesRec));
      With CategoryTemplatesRecPtr^ do
      begin
        Category_ID := dmMain.Query1.FieldByName('Category_ID').AsInteger;
        Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
        Template_Type := dmMain.Query1.FieldByName('Template_Type').AsInteger;
        Template_Description := dmMain.Query1.FieldByName('Template_Description').AsString;
        Template_IsOddsOnly := dmMain.Query1.FieldByName('Template_IsOddsOnly').AsBoolean;
        If (Category_Templates_Collection.Count <= 500) then
        begin
          //Add to collection
          Category_Templates_Collection.Insert(CategoryTemplatesRecPtr);
          Category_Templates_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.Query1.Next;
    until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
  end
  else begin
    MessageDlg('No data found in Category Templates database table!', mtError, [mbOk], 0);
  end;
  //Close query
  dmMain.Query1.Active := FALSE;

  //Load in the data from the record types table; iterate for all records in table
  RecordType_Collection.Clear;
  RecordType_Collection.Pack;
  dmMain.Query1.SQL.Clear;
  dmMain.Query1.SQL.Add('SELECT * FROM Record_Types');
  dmMain.Query1.Open;
  if (dmMain.Query1.RecordCount > 0) then
  begin
    dmMain.Query1.First;
    repeat
      //Add item to scripts collection
      GetMem (RecordTypeRecPtr, SizeOf(RecordTypeRec));
      With RecordTypeRecPtr^ do
      begin
        Playlist_Type := dmMain.Query1.FieldByName('Playlist_Type').AsInteger;
        Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
        Record_Description := dmMain.Query1.FieldByName('Record_Description').AsString;
        If (RecordType_Collection.Count <= 250) then
        begin
          //Add to collection
          RecordType_Collection.Insert(RecordTypeRecPtr);
          RecordType_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.Query1.Next;
    until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
  end
  else begin
    MessageDlg('No data found in Record Type database table!', mtError, [mbOk], 0);
  end;
  //Close query
  dmMain.Query1.Active := FALSE;

  //Load in the data from the style chips table; iterate for all records in table
  {
  StyleChip_Collection.Clear;
  StyleChip_Collection.Pack;
  dmMain.Query1.SQL.Clear;
  dmMain.Query1.SQL.Add('SELECT * FROM Style_Chips');
  dmMain.Query1.Open;
  if (dmMain.Query1.RecordCount > 0) then
  begin
    dmMain.Query1.First;
    repeat
      //Add item to style chip collection
      GetMem (StyleChipRecPtr, SizeOf(StyleChipRec));
      With StyleChipRecPtr^ do
      begin
        StyleChip_Index := dmMain.Query1.FieldByName('StyleChip_Index').AsInteger;
        StyleChip_Description := dmMain.Query1.FieldByName('StyleChip_Description').AsString;
        StyleChip_Code := dmMain.Query1.FieldByName('StyleChip_Code').AsString;
        StyleChip_Type := dmMain.Query1.FieldByName('StyleChip_Type').AsInteger;
        StyleChip_String := dmMain.Query1.FieldByName('StyleChip_String').AsString;
        StyleChip_FontCode := dmMain.Query1.FieldByName('StyleChip_FontCode').AsInteger;
        StyleChip_CharacterCode := dmMain.Query1.FieldByName('StyleChip_CharacterCode').AsInteger;
        If (StyleChip_Collection.Count <= 100) then
        begin
          //Add to collection
          StyleChip_Collection.Insert(StyleChipRecPtr);
          StyleChip_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.Query1.Next;
    until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
  end;
  //Close query
  dmMain.Query1.Active := FALSE;
  }

  //Load in the data from the custome headers table; iterate for all records in table
  LeagueCode_Collection.Clear;
  LeagueCode_Collection.Pack;
  dmMain.Query1.SQL.Clear;
  dmMain.Query1.SQL.Add('SELECT * FROM Custom_Headers');
  dmMain.Query1.Open;
  if (dmMain.Query1.RecordCount > 0) then
  begin
    //Clear the league select combo box entries
    ManualLeagueSelect.Items.Clear;
    dmMain.Query1.First;
    repeat
      //Add item to league codes collection
      GetMem (LeagueCodeRecPtr, SizeOf(LeagueCodeRec));
      With LeagueCodeRecPtr^ do
      begin
        Heading := dmMain.Query1.FieldByName('Heading').AsString;
        If (LeagueCode_Collection.Count <= 1000) then
        begin
          //Add to collection
          LeagueCode_Collection.Insert(LeagueCodeRecPtr);
          LeagueCode_Collection.Pack;
          //Add to manual league selection combo box
          ManualLeagueSelect.Items.Add(LeagueCodeRecPtr^.Heading);
        end;
      end;
      //Got to next record
      dmMain.Query1.Next;
    until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
  end;
  //Close query
  dmMain.Query1.Active := FALSE;
  //Set top of combo box
  ManualLeagueSelect.ItemIndex := 0;

  //Load in the data from the automated leagues table; iterate for all records in table
  Automated_League_Collection.Clear;
  Automated_League_Collection.Pack;
  dmMain.Query1.SQL.Clear;
  dmMain.Query1.SQL.Add('SELECT * FROM Automated_Leagues');
  dmMain.Query1.Open;
  if (dmMain.Query1.RecordCount > 0) then
  begin
    dmMain.Query1.First;
    repeat
      //Add item to league codes collection
      GetMem (AutomatedLeagueRecPtr, SizeOf(AutomatedLeagueRec));
      With AutomatedLeagueRecPtr^ do
      begin
        SI_LeagueCode := dmMain.Query1.FieldByName('SI_LeagueCode').AsString;
        ST_LeagueCode := dmMain.Query1.FieldByName('ST_LeagueCode').AsString;
        Display_Mnemonic := dmMain.Query1.FieldByName('Display_Mnemonic').AsString;
        If (Automated_League_Collection.Count <= 50) then
        begin
          //Add to collection
          Automated_League_Collection.Insert(AutomatedLeagueRecPtr);
          Automated_League_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.Query1.Next;
    until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
  end;
  //Close query
  dmMain.Query1.Active := FALSE;

  //Load in the data from the playout location info table; iterate for all records in table
  if (EnableTickerDatabaseSelection) then
  begin
    PlayoutStationInfo_Collection.Clear;
    PlayoutStationInfo_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Playout_Station_IDs');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      RegionSelect.Items.Clear;
      repeat
        //Add item to league codes collection
        GetMem (PlayoutStationInfoRecPtr, SizeOf(PlayoutStationInfoRec));
        With PlayoutStationInfoRecPtr^ do
        begin
          Station_ID := dmMain.Query1.FieldByName('Station_ID').AsInteger;
          Station_Description := dmMain.Query1.FieldByName('Station_Description').AsString;
          if (PlayoutStationInfo_Collection.Count <= 10) then
          begin
            //Add to collection
            PlayoutStationInfo_Collection.Insert(PlayoutStationInfoRecPtr);
            PlayoutStationInfo_Collection.Pack;
            //Add to combo box
            RegionSelect.Items.Add(Station_Description);
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
      RegionSelect.ItemIndex := 0;
    end;
    //Close query
    dmMain.Query1.Active := FALSE;
  end;  
end;

//Handler to request complete reload of games from Database
procedure TMainForm.ReloadGamesfromGametrak1Click(Sender: TObject);
begin
  //Refresh games tables
  dmMain.SportbaseQuery.Active := FALSE;
  dmMain.SportbaseQuery.Active := TRUE;
end;

//Function to return number of zipper playlists in the database corresponding to
//the specified Cart ID
function TMainForm.GetPlaylistCount(PlaylistType: SmallInt; SearchStr: String): SmallInt;
var
  OutCount: SmallInt;
begin
  dmMain.Query1.Close;
  dmMain.Query1.SQL.Clear;
  Case PlaylistType of
   0: dmMain.Query1.SQL.Add('SELECT * FROM Ticker_Groups WHERE Playlist_Description ' +
                        'LIKE ' + QuotedStr('%' + SearchStr + '%'));
   1: dmMain.Query1.SQL.Add('SELECT * FROM Bug_Groups WHERE Playlist_Description ' +
                        'LIKE ' + QuotedStr('%' + SearchStr + '%'));
   2: dmMain.Query1.SQL.Add('SELECT * FROM ExtraLine_Groups WHERE Playlist_Description ' +
                        'LIKE ' + QuotedStr('%' + SearchStr + '%'));
  end;
  dmMain.Query1.Open;
  //Get the count
  OutCount := dmMain.Query1.RecordCount;
  //Close the query and return
  dmMain.Query1.Close;
  GetPlaylistCount := OutCount;
end;

//Function to take a template ID & return its description
function TMainForm.GetTemplateInformation(TemplateID: SmallInt): TemplateDefsRec;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutRec: TemplateDefsRec;
begin
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = TemplateID) then
      with OutRec do
      begin
        Template_ID := TemplateDefsRecPtr^.Template_ID;
        Template_Type := TemplateDefsRecPtr^.Template_Type;
        Template_Description := TemplateDefsRecPtr^.Template_Description;
        Record_Type := TemplateDefsRecPtr^.Record_Type;
        Engine_Template_ID := TemplateDefsRecPtr^.Engine_Template_ID;
        Default_Dwell := TemplateDefsRecPtr^.Default_Dwell;
        ManualLeague := TemplateDefsRecPtr^.ManualLeague;
        UsesGameData := TemplateDefsRecPtr^.UsesGameData;
        HideWebsiteURL := TemplateDefsRecPtr^.HideWebsiteURL;
        ShowLeagueChip := TemplateDefsRecPtr^.ShowLeagueChip;
        Template_Has_Children := TemplateDefsRecPtr^.Template_Has_Children;
        Template_Is_Child := TemplateDefsRecPtr^.Template_Is_Child;
        StartEnableDateTime := TemplateDefsRecPtr^.StartEnableDateTime;
        EndEnableDateTime := TemplateDefsRecPtr^.EndEnableDateTime;
      end;
    end
  end;
  GetTemplateInformation := OutRec;
end;

//Function to take a league and a game phase code, and return a game phase
//record
function TMainForm.GetGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
var
  i: SmallInt;
  GamePhaseRecPtr: ^GamePhaseRec; //Pointer to game phase record type
  OutRec: GamePhaseRec;
  FoundRecord: Boolean;
begin
  FoundRecord := FALSE;
  OutRec.League := ' ';
  OutRec.ST_Phase_Code := 0;
  OutRec.SI_Phase_Code := 0;
  OutRec.Display_Period := ' ';
  OutRec.Display_Half := ' ';
  OutRec.Display_Final := ' ';
  OutRec.Display_Extended1 := ' ';
  OutRec.Display_Extended2 := ' ';
  if (Game_Phase_Collection.Count > 0) then
  begin
    i := 0;
    repeat
      GamePhaseRecPtr := Game_Phase_Collection.At(i);
      if (GamePhaseRecPtr^.League = League) AND (GamePhaseRecPtr^.SI_Phase_Code = GPhase) then
      begin
        OutRec.League := League;
        OutRec.ST_Phase_Code := GPhase;
        OutRec.SI_Phase_Code := GPhase;
        OutRec.Label_Period := GamePhaseRecPtr^.Label_Period;
        OutRec.Display_Period := GamePhaseRecPtr^.Display_Period;
        OutRec.End_Is_Half := GamePhaseRecPtr^.End_Is_Half;
        OutRec.Display_Half := GamePhaseRecPtr^.Display_Half;
        OutRec.Display_Final := GamePhaseRecPtr^.Display_Final;
        OutRec.Display_Extended1 := GamePhaseRecPtr^.Display_Extended1;
        OutRec.Display_Extended2 := GamePhaseRecPtr^.Display_Extended2;
        OutRec.Game_OT := GamePhaseRecPtr^.Game_OT;
      end;
      Inc(i);
    until (i = Game_Phase_Collection.Count) OR (FoundRecord = TRUE);
  end;
  GetGamePhaseRec := OutRec;
end;

//Function to get game time string from GT Server time value
function TMainForm.GetGameTimeString(GTimeStr: String): String;
var
  Min, Sec: String;
begin
  //Blank out case where time = 9999 - for MLB
  if (GTimeStr = '9999') then Result := ' '
  //Actual clock time
  else if (StrToIntDef(GTimeStr, -1) <> -1) AND (Length(GTimeStr) = 4) then
  begin
    Min := GTimeStr[1] + GTimeStr[2];
    Sec := GTimeStr[3] + GTimeStr[4];
    Min := IntToStr(StrToInt(GTimeStr[1]+GTimeStr[2]));
    Result := Min + ':' + Sec;
  end
  else if (GTimeStr = 'END-') then Result := 'End'
  else if (GTimeStr = 'POST') then Result := 'PPD'
  else if (GTimeStr = 'SUSP') then Result := 'PPD'
  else if (GTimeStr = 'DELA') then Result := 'DLY'
  else if (GTimeStr = 'RAIN') then Result := 'RD'
  else if (GTimeStr = 'CANC') then Result := 'PPD'
  else Result := ' ';
end;

//Function to take the stat stored procedure name and return the stat information record
function TMainForm.GetStatInfo (StoredProcedureName: String): StatRec;
var
  i: SmallInt;
  StatRecPtr: ^StatRec; //Pointer to stat record type
  OutRec: StatRec;
begin
  OutRec.StatLeague := ' ';
  OutRec.StatType := 0;
  OutRec.StatDescription := ' ';
  OutRec.StatHeader := ' ';
  OutRec.StatStoredProcedure := ' ';
  OutRec.StatDataField := ' ';
  OutRec.StatAbbreviation := ' ';
  OutRec.UseStatQualifier := FALSE;
  OutRec.StatQualifier := ' ';
  OutRec.StatQualifierValue := 0;
  if (Stat_Collection.Count > 0) then
  begin
    for i := 0 to Stat_Collection.Count-1 do
    begin
      StatRecPtr := Stat_Collection.At(i);
      if (Trim(StatRecPtr^.StatStoredProcedure) = Trim(StoredProcedureName)) then
      begin
        OutRec.StatLeague := StatRecPtr^.StatLeague;
        OutRec.StatType := StatRecPtr^.StatType;
        OutRec.StatDescription := StatRecPtr^.StatDescription;
        OutRec.StatHeader := StatRecPtr^.StatHeader;
        OutRec.StatStoredProcedure := StatRecPtr^.StatStoredProcedure;
        OutRec.StatDataField := StatRecPtr^.StatDataField;
        OutRec.StatAbbreviation := StatRecPtr^.StatAbbreviation;
        OutRec.UseStatQualifier := StatRecPtr^.UseStatQualifier;
        OutRec.StatQualifier := StatRecPtr^.StatQualifier;
        OutRec.StatQualifierValue := StatRecPtr^.StatQualifierValue;
      end;
    end;
  end;
  GetStatInfo := OutRec;
end;

//Function to take the Stats Inc. league code and return the display mnemonic
function TMainForm.GetAutomatedLeagueDisplayMnemonic (League: String): String;
var
  i: SmallInt;
  AutomatedLeagueRecPtr: ^AutomatedLeagueRec; //Pointer to team record type
  OutStr: String;
  FoundRecord: Boolean;
begin
  //Init to passed in variable; will return if no substitution found
  OutStr := League;
  if (Automated_League_Collection.Count > 0) then
  begin
    FoundRecord := FALSE;
    i := 0;
    Repeat
      AutomatedLeagueRecPtr := Automated_League_Collection.At(i);
      if (Trim(AutomatedLeagueRecPtr^.SI_LeagueCode) = League) then
      begin
        OutStr := AutomatedLeagueRecPtr^.Display_Mnemonic;
      end;
      Inc(i);
    Until (FoundRecord = TRUE) OR (i = Automated_League_Collection.Count);
  end;
  GetAutomatedLeagueDisplayMnemonic := OutStr;
end;

//Function to get game state
function TMainForm.GetGameState(GTIME: String; GPHASE: SmallInt): Integer;
begin
  if (GTIME = '9999') OR (GPHASE = 0) then
    //Game not started
    Result := 0
  else if (GTIME = 'FINA') OR (GTIME = 'SUM-') then
    //Game is final
    Result := 3
  else if (GTIME = 'END-') then
    //End of period/quarter
    Result := 2
  else if (GPHASE <> 0) AND (StrToIntDef(GTIME, -1) <> -1) then
    //Game in progress
    Result := 1
  else
    //All other conditions
    Result := -1; // Game is suspended due to rain or cancelled etc. (GTIME = 'RAIN' , GTIME = 'SUSP', GTIME = 'CANC')
end;

//Function to get the number of fields in a template
function TMainForm.GetTemplateFieldsCount(Template_ID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  OutVal: SmallInt;
begin
  OutVal := 0;
  If (Template_Fields_Collection.Count > 0) then
  begin
    for i := 0 to Template_Fields_Collection.Count-1 do
    begin
      TemplateFieldsRecPtr := Template_Fields_Collection.At(i);
      if (TemplateFieldsRecPtr^.Template_ID = Template_ID) {AND
         (TemplateFieldsRecPtr^.Field_Label <> 'Unused')} then Inc(OutVal);
    end;
  end;
  GetTemplateFieldsCount := OutVal;
end;

//Function to get the record type based on the template ID
function TMainForm.GetRecordTypeFromTemplateID(Template_ID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutVal: SmallInt;
begin
  OutVal := 0;
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = Template_ID) then
        OutVal := TemplateDefsRecPtr^.Record_Type;
    end;
  end;
  GetRecordTypeFromTemplateID := OutVal;
end;

//Function to return a record type description base on playlist type and record type code
function TMainForm.GetRecordTypeDescription (Playlist_Type: SmallInt; Record_Type: SmallInt): String;
var
  i: SmallInt;
  RecordTypeRecPtr: ^RecordTypeRec;
  OutStr: String;
begin
  OutStr := '';
  if (RecordType_Collection.Count > 1) then
  begin
    for i := 0 to RecordType_Collection.Count-1 do
    begin
      RecordTypeRecPtr := RecordType_Collection.At(i);
      if (RecordTypeRecPtr^.Playlist_Type = Playlist_Type) AND (RecordTypeRecPtr^.Record_Type = Record_Type) then
        OutStr := RecordTypeRecPtr^.Record_Description;
    end;
  end;
  GetRecordTypeDescription := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
// HANDLERS FOR GENERAL PROGRAM OPERATIONS
////////////////////////////////////////////////////////////////////////////////
//Event handler for page control change of selection
procedure TMainForm.ProgramModePageControlChange(Sender: TObject);
var
  CategoryRecPtr: ^CategoryRec;
begin
  //Handle page change
  Case ProgramModePageControl.ActivePageIndex of
    //Playlist scheduling mode
    0: Begin
         Case PlaylistSelectTabControl.TabIndex of
             //Ticker
          0: begin
               dmMain.tblTicker_Groups.Active := FALSE;
               dmMain.tblTicker_Groups.Active := TRUE;
               dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
               dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
               AvailablePlaylistsGrid.DataSource := dmMain.dsTicker_Groups;
               ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_Ticker_Groups;
             end;
             //Alerts
          1: begin
               dmMain.tblBreakingNews_Groups.Active := FALSE;
               dmMain.tblBreakingNews_Groups.Active := TRUE;
               //dmMain.tblScheduled_BreakingNews_Groups.Active := FALSE;
               //dmMain.tblScheduled_BreakingNews_Groups.Active := TRUE;
               AvailablePlaylistsGrid.DataSource := dmMain.dsBreakingNews_Groups;
               ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_BreakingNews_Groups;
             end;
             //Default Templates (by sport)
          2: begin
               dmMain.tblTicker_Groups.Active := FALSE;
               dmMain.tblTicker_Groups.Active := TRUE;
               dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
               dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
               AvailablePlaylistsGrid.DataSource := dmMain.dsTicker_Groups;
               ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_Ticker_Groups;
             end;
         end;
       end;
    //Ticker authoring mode
    1: Begin
         //Check to see if category tab is a sport; if so, show and populate games grid
         CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
         //Now, populate the templates grid by querying the database
         dmMain.Query2.Active := FALSE;
         dmMain.Query2.SQL.Clear;
         //Only show games if in ticker mode (not alerts)
         dmMain.Query2.Filtered := TRUE;
         //Filter for templates applicable to current playlist type
         dmMain.Query2.Filter := 'Template_Type = ' + IntToStr(PlaylistSelectTabControl.TabIndex+1);
         dmMain.Query2.SQL.Add ('SELECT * FROM Category_Templates WHERE ' +
           'Category_ID = ' + IntToStr(CategoryRecPtr^.Category_ID));
         dmMain.Query2.Active := TRUE;
       end;
    //DB Maint
    2: Begin
         Case PlaylistSelectTabControl.TabIndex of
             //Ticker
          0: begin
               dmMain.tblTicker_Groups.Active := FALSE;
               dmMain.tblTicker_Groups.Active := TRUE;
               DatabaseMaintPlaylistGrid.DataSource := dmMain.dsTicker_Groups;
               DatabaseMaintBreakingNewsPlaylistGrid.Visible := FALSE;
             end;
             //Alerts
          1: begin
               dmMain.tblBreakingNews_Groups.Active := FALSE;
               dmMain.tblBreakingNews_Groups.Active := TRUE;
               DatabaseMaintBreakingNewsPlaylistGrid.DataSource := dmMain.dsBreakingNews_Groups;
               DatabaseMaintBreakingNewsPlaylistGrid.Visible := TRUE;
             end;
         end;
       end;
  end;
  //Set new page index
  LastPageIndex := ProgramModePageControl.ActivePageIndex;
  //Load current templates
  LeagueTab.OnChange(self);
end;

//Handler for PlaylistSelectTabControl tab change
procedure TMainForm.PlaylistSelectTabControlChange(Sender: TObject);
var
  i: SmallInt;
  CategoryRecPtr: ^CategoryRec;
  TemplateType: SmallInt;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        //Show tab sheet for schedule
        ProgramModePageControl.Pages[0].TabVisible := TRUE;
        //Hide default template grid/list and save panel
        DefaultTemplatesGrid.Visible := FALSE;
        //Show playlist grid
        PlaylistGrid.Visible := TRUE;
        DatabaseMaintPlaylistGrid.DataSource := dmMain.dsTicker_Groups;
        AvailablePlaylistsGrid.DataSource := dmMain.dsTicker_Groups;
        ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_Ticker_Groups;
        LastSaveTimeLabel.Caption :=
          PlaylistInfo[PlaylistSelectTabControl.TabIndex+1].LastPlaylistSaveTimeString;
        //Show playlist name entry dialog, hide immediate mode checkbox
        Label4.Visible := TRUE;
        Edit1.Visible := TRUE;
        SingleLoopEnable.Visible := TRUE;
        DatabaseMaintPlaylistGrid.DataSource := dmMain.dsTicker_Groups;
        DatabaseMaintBreakingNewsPlaylistGrid.Visible := FALSE;
      end;
   {
      //Alerts
   1: begin
        //Hide tab sheet for schedule
        ProgramModePageControl.Pages[0].TabVisible := FALSE;
        //Hide default template grid/list save panel
        TemplateListSaveControlsPnl.Visible := FALSE;
        DefaultTemplatesGrid.Visible := FALSE;
        //Show playlist grid
        PlaylistGrid.Visible := TRUE;
        DatabaseMaintPlaylistGrid.DataSource := dmMain.dsBreakingNews_Groups;
        AvailablePlaylistsGrid.DataSource := dmMain.dsBreakingNews_Groups;
        ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_BreakingNews_Groups;
        //Hide playlist name entry dialog, show immediate mode checkbox
        Label4.Visible := FALSE;
        Edit1.Visible := FALSE;
        Label9.Visible := TRUE;
        ImmediateModeBreakingNews.Visible := TRUE;
        EnableBreakingNews.Visible := TRUE;
        BreakingNewsIterations.Visible := TRUE;
        SingleLoopEnable.Visible := FALSE;
        DatabaseMaintBreakingNewsPlaylistGrid.DataSource := dmMain.dsBreakingNews_Groups;
        DatabaseMaintBreakingNewsPlaylistGrid.Visible := TRUE;
      end;
      //Default template specificaton
   2: begin
        //Show tab sheet for schedule
        ProgramModePageControl.Pages[0].TabVisible := TRUE;
        //Hide playlist grid
        PlaylistGrid.Visible := FALSE;
        //Show default template list/grid & save panel
        TemplateListSaveControlsPnl.Visible := TRUE;
        DefaultTemplatesGrid.Visible := TRUE;
        DatabaseMaintPlaylistGrid.DataSource := dmMain.dsTicker_Groups;
        AvailablePlaylistsGrid.DataSource := dmMain.dsTicker_Groups;
        ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_Ticker_Groups;
        LastSaveTimeLabel.Caption :=
          PlaylistInfo[PlaylistSelectTabControl.TabIndex+1].LastPlaylistSaveTimeString;
        //Hide playlist name entry dialog, hide immediate mode checkbox
        Label4.Visible := FALSE;
        Edit1.Visible := FALSE;
        Label9.Visible := FALSE;
        ImmediateModeBreakingNews.Visible := FALSE;
        EnableBreakingNews.Visible := FALSE;
        BreakingNewsIterations.Visible := FALSE;
        SingleLoopEnable.Visible := FALSE;
        DatabaseMaintPlaylistGrid.DataSource := dmMain.dsTicker_Groups;
        DatabaseMaintBreakingNewsPlaylistGrid.Visible := FALSE;
      end;
  }
  end;
  //Set playlist name
  Edit1.Text := PlaylistInfo[PlaylistSelectTabControl.TabIndex+1].PlaylistName;
  PlaylistNameLabel.Caption := PlaylistInfo[PlaylistSelectTabControl.TabIndex+1].PlaylistName;
  LastSaveTimeLabel.Caption :=
    PlaylistInfo[PlaylistSelectTabControl.TabIndex+1].LastPlaylistSaveTimeString;
  //Set labels and control attributes
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        SchedulePanel.Color := clSilver;
        DataEntryPanel.Color := clSilver;
        DBMaintPanel.Color := clSilver;
        PlaylistModeLabel.Color := clSilver;
        PlaylistModeLabel.Caption := 'TICKER PLAYLIST MODE';
      end;
   {
      //Alerts
   1: begin
        Label16.Visible := FALSE;
        DoubleLineCheck.Visible := FALSE;
        SingleLineCheck.Visible := FALSE;
        SchedulePanel.Color := clSkyBlue;
        DataEntryPanel.Color := clSkyBlue;
        DBMaintPanel.Color := clSkyBLue;
        PlaylistModeLabel.Color := clSkyBlue;
        PlaylistModeLabel.Caption := 'ALERTS';
        //Check if immediate mode is checked
        if (ImmediateModeBreakingNews.Checked) then
        begin
          ImmediateModeBreakingNews.Color := clRed;
          PlaylistModeLabel.Color := clRed;
          PlaylistModeLabel.Caption := 'IMMEDIATE MODE';
        end
        else begin
          ImmediateModeBreakingNews.Color := clBtnFace;
          PlaylistModeLabel.Color := clSkyBlue;
          PlaylistModeLabel.Caption := 'ALERTS';
        end;
      end;
      //Default templates
   2: begin
        Label16.Visible := FALSE;
        DoubleLineCheck.Visible := TRUE;
        SingleLineCheck.Visible := TRUE;
        SchedulePanel.Color := clGray;
        DataEntryPanel.Color := clGray;
        DBMaintPanel.Color := clGray;
        PlaylistModeLabel.Color := clGray;
        PlaylistModeLabel.Caption := 'DEFAULT TEMPLATES MODE';
      end;
  }
  end;

  //Populate the league tab control based on ticker/alerts selection
  if (Categories_Collection.Count > 0) then
  begin
    LeagueTab.Tabs.Clear;
    for i := 0 to Categories_Collection.Count-1 do
    begin
      CategoryRecPtr := Categories_Collection.At(i);
      With CategoryRecPtr^ do
      begin
        //Modified to handle default templates tab page
        if (Category_Type = PlaylistSelectTabControl.TabIndex+1) OR
           //This causes Default Templates page to be populated
           (Category_Type = PlaylistSelectTabControl.TabIndex-1) then LeagueTab.Tabs.Add (Category_Name);
      end;
   end;
  end;

  //Check to see if category tab is a sport; if so, show and populate games grid
  CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
  //Now, populate the templates grid by querying the database
  dmMain.Query2.Active := FALSE;
  dmMain.Query2.SQL.Clear;
  dmMain.Query2.Filtered := TRUE;
  //Filter for templates applicable to current playlist type
  if (PlaylistSelectTabControl.TabIndex = 1) then
    //Alerts
    TemplateType := 2
  else
    //Ticker or Default Templates
    TemplateType := 1;
  dmMain.Query2.Filter := 'Template_Type = ' + IntToStr(TemplateType);
  dmMain.Query2.SQL.Add ('SELECT * FROM Category_Templates WHERE ' +
    'Category_ID = ' + IntToStr(CategoryRecPtr^.Category_ID));
  dmMain.Query2.Active := TRUE;
  //Refresh the grids
  RefreshPlaylistGrid;
  RefreshEntryFieldsGrid(TickerDisplayMode);
  //Force refresh of available templates
  LeagueTabChange(Self);
end;

//Handler for change in schedule selection
procedure TMainForm.ScheduleSelectTabChange(Sender: TObject);
begin
end;

//Handler for change in database maintenance selection
procedure TMainForm.DatabaseMaintSelectTabChange(Sender: TObject);
begin
end;

//Handler for check/uncheck of odds only checkbox
procedure TMainForm.ShowOddsOnlyClick(Sender: TObject);
begin
  if (ShowOddsOnly.Checked) then
    ManualOverridePanel.Color := clYellow
  else
    ManualOverridePanel.Color := clBtnFace;
  LeagueTabChange(Self);
end;

//Handler for a change in the league select tab
procedure TMainForm.LeagueTabChange(Sender: TObject);
var
  CategoryRecPtr: ^CategoryRec;
  Day, Month, Year: Word;
  TodayStr, YesterdayStr: String;
  QueryString: String;
  TableName: String;
  OddsOnly: SmallInt;
  LeagueStr: String;
  QueryStr: String;
  TemplateType: SmallInt;
begin
  //Check to see if category tab is a sport; if so, show and populate games grid
  CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
  //First, check to see if sponsors. If so, show sponsor grid for ticker mode (but not for alerts)
  if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') AND (PlaylistSelectTabControl.TabIndex = 0) then
  begin
    SponsorsDBGrid.Visible := TRUE;
    SponsorDwellPanel.Visible := TRUE;
    GamesDBGrid.Visible := FALSE;
    OddsDBGrid.Visible := FALSE;
    ManualOverridePanel.Visible := FALSE;
    ManualLeaguePanel.Visible := FALSE;
  end
  //Show games
  else if (CategoryRecPtr^.Category_Is_Sport = TRUE) AND (PlaylistSelectTabControl.TabIndex = 0) then
  begin
    SponsorsDBGrid.Visible := FALSE;
    SponsorDwellPanel.Visible := FALSE;
    if (ShowOddsOnly.Checked) then
    begin
      OddsDBGrid.Visible := TRUE;
      GamesDBGrid.Visible := FALSE;
    end
    else begin
      OddsDBGrid.Visible := FALSE;
      GamesDBGrid.Visible := TRUE;
    end;
    //ManualOverridePanel.Visible := TRUE;
    ManualOverridePanel.Visible := FALSE;
    ManualLeaguePanel.Visible := FALSE;
    //Setup query to run stored procedure
    //Show games grid
    dmMain.SportbaseQuery.Active := FALSE;
    dmMain.SportbaseQuery.SQL.Clear;

    //Setup query
    TableName := CategoryRecPtr^.Sport_Games_Table_Name;
    if (Trim(TableName) <> '') then
    begin
      //Show games grid
      dmMain.SportbaseQuery.Active := FALSE;
      dmMain.SportbaseQuery.SQL.Clear;

      QueryString := 'SELECT * FROM ' + TableName;
      //Now, add so that only today's and yesterday's games are shown
      DecodeDate(Now, Year, Month, Day);
      TodayStr := IntToStr(Month) + '/' + IntToStr(Day) + '/' + IntToStr(Year);
      DecodeDate(Now-1, Year, Month, Day);
      YesterdayStr := IntToStr(Month) + '/' + IntToStr(Day) + '/' + IntToStr(Year);
      if not (DebugMode) then
      begin
        if (EnableGameScheduleLookAhead) then
          QueryString := QueryString + ' WHERE ((GDate = ' + QuotedStr(YesterdayStr) +
            ') OR (GDate >= ' + QuotedStr(TodayStr) + '))'
        else
          QueryString := QueryString + ' WHERE ((GDate = ' + QuotedStr(TodayStr) +
            ') OR (GDate = ' + QuotedStr(YesterdayStr) + '))';
      end;
      //Set to order by date
      QueryString := QueryString + ' ORDER BY GDATE ASC';
      //Add additional sort criteria for Subleague if MLB
      if (CategoryRecPtr^.Category_Sport = 'MLB') then
        QueryString := QueryString + ', Subleague ASC';
      //Set to order by date
      QueryString := QueryString + ', CAST(START AS DATETIME) ASC';
      dmMain.SportbaseQuery.SQL.Add(QueryString);
      dmMain.SportbaseQuery.Active := TRUE;
    end;
  end
  //Manual templates
  else begin
    SponsorsDBGrid.Visible := FALSE;
    SponsorDwellPanel.Visible := FALSE;
    GamesDBGrid.Visible := FALSE;
    ManualOverridePanel.Visible := FALSE;
    //Refresh the league comabo box
    ManualLeaguePanel.Visible := TRUE;
  end;

  //Now, populate the templates grid by querying the database
  dmMain.Query2.Active := FALSE;
  dmMain.Query2.SQL.Clear;
  dmMain.Query2.Filtered := TRUE;
  //Filter for templates applicable to current playlist type
  if (PlaylistSelectTabControl.TabIndex = 1) then
    //Alerts
    TemplateType := 2
  else
    //Ticker or Default Templates
    TemplateType := 1;
  dmMain.Query2.Filter := 'Template_Type = ' + IntToStr(TemplateType);
  //Add filter to query to ensure that only applicable templates are displayed (odds only)
  if (ShowOddsOnly.Checked) then OddsOnly := 1
  else OddsOnly := 0;
  QueryStr := 'SELECT * FROM Category_Templates WHERE ' +
    'Category_ID = ' + IntToStr(CategoryRecPtr^.Category_ID) + ' AND Template_IsOddsOnly = ' + IntToStr(OddsOnly);
  //Add to query to filter templates based on 1 or 2 line display modes
  if (TickerDisplayMode = 1) OR (TickerDisplayMode = 2) then
    //1-line ticker
    QueryStr := QueryStr + ' AND ((Template_ID < 1000) OR (Template_ID >= 10000))'
  else
    //2-line ticker
    QueryStr := QueryStr + ' AND ((Template_ID >= 1000) AND (Template_ID < 10000))';
  //Check for crawl templates enabled
  if not (EnableCrawlTemplates) then
    QueryStr := QueryStr + ' AND (Template_UsesCrawl = 0)';
  dmMain.Query2.SQL.Add (QueryStr);
  dmMain.Query2.Active := TRUE;

  //Refresh the Default Templates grid
  RefreshDefaultTemplatesGrid;
end;

//Handler for playlist collapse/expand button
procedure TMainForm.ExpandButtonClick(Sender: TObject);
begin
  if (ExpandButton.Caption = 'Expand Playlist') then
  begin
    ExpandButton.Caption := 'Collapse Playlist';
    PlaylistGrid.Top := 43;
    PlaylistGrid.Height := 650;
  end
  else begin
    ExpandButton.Caption := 'Expand Playlist';
    PlaylistGrid.Top := 43;
    PlaylistGrid.Height := 406;
  end;
end;

//Handler for 1x loop enable checkbox
procedure TMainForm.SingleLoopEnableClick(Sender: TObject);
begin
  //Loop 1X
  if (SingleLoopEnable.Checked = TRUE) then
  begin
    TickerDisplayMode := 1;
    SingleLoopEnable.Color := clYellow;
  end
  //Loop continuously
  else begin
    //1-line, loop
    TickerDisplayMode := 2;
    SingleLoopEnable.Color := clSilver;
  end;
end;

//Procedure to switch to alternate template IDs on mode change
//Initial version supports 1-Line -> 2-Line conversion
procedure TMainForm.ReAssignTemplateIDsOnModeChange;
var
  i,j,k: SmallInt;
  TickerCollectionCount: SmallInt;
  TickerRecPtr, TickerRecPtr2, NewTickerRecPtr: ^TickerRec;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  Temp_Template_ID: SmallInt;
  MaxCount: SmallInt;
  SaveString1, SaveString2: String;
  EventGUID: TGUID;
begin
  //Check for collection entries and substitute alternate template IDs
  if (Ticker_Collection.Count > 0) then
  begin
    //Check to see if successive 1-line entries need to be combined into a single 2-line entry
    //Only act if saving out in 2-line mode FROM 1-line mode
    if (TickerDisplayMode = 1) OR (TickerDisplayMode = 2) then
    begin
      //Iterate through collection; if template ID is for 1-line and saving in 2-line mode,
      //combine successive 1-line notes into single 2-line note
      MaxCount := Ticker_Collection.Count-1;
      i := 0;
      While i <= MaxCount do
      begin
        //Point to collection object
        TickerRecPtr := Ticker_Collection.At(i);

        //Check for record type > 1000 indicating successive notes should be combined; > 10000 is a special case
        //for stats related pages
        if (GetRecordTypeForTemplate(TickerRecPtr^.Template_ID) > 1000) AND
           (GetRecordTypeForTemplate(TickerRecPtr^.Template_ID) < 10000) AND
           (i < Ticker_Collection.Count-1) then
        begin
          //Save template ID of first note
          Temp_Template_ID :=  TickerRecPtr^.Template_ID;
          //Save first user defined string value; will be combined
          SaveString1 := TickerRecPtr^.UserData[1];
          //Increment to next record
          Inc(i);
          //Point to collection object
          TickerRecPtr := Ticker_Collection.At(i);
          //Check template ID and record type for next record; if also of same type and record type > 1000,
          //combine notes into single template and proceed
          if (GetRecordTypeForTemplate(TickerRecPtr^.Template_ID) > 1000) AND
             (GetRecordTypeForTemplate(TickerRecPtr^.Template_ID) < 10000) AND
             (TickerRecPtr^.Template_ID = Temp_Template_ID) then
          begin
            SaveString2 := TickerRecPtr^.UserData[1];
            TickerRecPtr^.UserData[1] := SaveString1;
            TickerRecPtr^.UserData[2] := SaveString2;
            //Now, set the prior record for deletion - will be deleted below
            TickerRecPtr2 := Ticker_Collection.At(i-1);
            TickerRecPtr2^.UserData[1] := 'DELETE';
          end
          //Not of same type, so don't combine; decrement ticker collection counter to make sure separate
          //record is inserted
          else Dec(i);
        end;
        inc(i);
      end;
    end

    //Check to see if single 2-line entry should be expanded into 2 separate 1-line entries
    //Only act if saving out in 1-line mode FROM 2-line mode
    else if (TickerDisplayMode = 3) OR (TickerDisplayMode = 4) then
    begin
      //Iterate through collection; if template ID is for 1-line and saving in 2-line mode,
      //combine successive 1-line notes into single 2-line note
      MaxCount := Ticker_Collection.Count-1;
      i := 0;
      While i <= MaxCount do
      begin
        //Point to collection object
        TickerRecPtr := Ticker_Collection.At(i);

        //Check for record type > 1000 indicating notes should be expanded
        //Note: Record types > 10000 reserved for stats related functions
        if (GetRecordTypeForTemplate(TickerRecPtr^.Template_ID) > 1000) AND
           (GetRecordTypeForTemplate(TickerRecPtr^.Template_ID) < 10000) AND (i <= Ticker_Collection.Count-1) then
        begin
          //Save second user defined string values; will be expanded into two templates if there is text
          //in the second field
          SaveString1 := TickerRecPtr^.UserData[2];
          if (Trim(SaveString1) <> '') then
          begin
            //Insert new record
            GetMem (NewTickerRecPtr, SizeOf(TickerRec));
            With NewTickerRecPtr^ do
            begin
              Event_Index := TickerRecPtr^.Event_Index+1;
              CreateGUID(EventGUID);
              Event_GUID := EventGUID;
              Enabled := TickerRecPtr^.Enabled;
              Is_Child := TickerRecPtr^.Is_Child;
              League := TickerRecPtr^.League;
              Subleague_Mnemonic_Standard := TickerRecPtr^.Subleague_Mnemonic_Standard;
              Mnemonic_LiveEvent := TickerRecPtr^.Mnemonic_LiveEvent;
              Template_ID := TickerRecPtr^.Template_ID;
              Record_Type := TickerRecPtr^.Record_Type;
              GameID := TickerRecPtr^.GameID;
              SponsorLogo_Name := TickerRecPtr^.SponsorLogo_Name;
              SponsorLogo_Dwell := TickerRecPtr^.SponsorLogo_Dwell;
              StatStoredProcedure := TickerRecPtr^.StatStoredProcedure;
              StatType := TickerRecPtr^.StatType;
              StatTeam := TickerRecPtr^.StatTeam;
              StatLeague := TickerRecPtr^.StatLeague;
              StatRecordNumber := TickerRecPtr^.StatRecordNumber;
              UserData[1] := SaveString1;
              for k := 2 to 50 do UserData[k] := '';
              StartEnableDateTime := TickerRecPtr^.StartEnableDateTime;
              EndEnableDateTime := TickerRecPtr^.EndEnableDateTime;
              Selected := TickerRecPtr^.Selected;
              DwellTime := TickerRecPtr^.DwellTime;
              Description := TickerRecPtr^.Description;
              Comments := TickerRecPtr^.Comments;
              If (Ticker_Collection.Count <= 1500) then
              begin
                //If last entry, append to end of collection; otherwise insert
                if (i = MaxCount) then
                  Ticker_Collection.Insert(NewTickerRecPtr)
                else
                  Ticker_Collection.AtInsert(i+1, NewTickerRecPtr);
                Ticker_Collection.Pack;
                //Increment the Max Count variable
                Inc(MaxCount);
              end;
            end;
          end;
        end;
        //Go to next record
        inc(i);
      end;
    end;

    //Re-assign the template IDs 1-Line <-> 2-Line
    //Set ticker collection count - may be decremented if entry needs to be discarded
    TickerCollectionCount := Ticker_Collection.Count;
    i := 0;
    //Walk through ticker collection and switch template IDs
    while i < TickerCollectionCount do
    begin
      //Get ticker element and current template ID
      TickerRecPtr := Ticker_Collection.At(i);
      Temp_Template_ID := TickerRecPtr^.Template_ID;
      //Only act if there are template definitions
      if (Template_Defs_Collection.Count > 0) then
      begin
        //Walk through templates to find alternate mode ID
        for j := 0 to Template_Defs_Collection.Count-1 do
        begin
          TemplateDefsRecPtr := Template_Defs_Collection.At(j);
          //Check for match between ticker element template ID and entry in template defs
          if (TemplateDefsRecPtr^.Template_ID = Temp_Template_ID) then
          begin
            //Alternate template ID found, so re-assign template ID
            if (TemplateDefsRecPtr^.AlternateModeTemplateID > 0) then
            begin
              TickerRecPtr^.Template_ID := TemplateDefsRecPtr^.AlternateModeTemplateID;
              TickerRecPtr^.Record_Type := GetRecordTypeForTemplate(TemplateDefsRecPtr^.AlternateModeTemplateID);
            end
            //No alternate template ID, so delete record (invalid alternate mode template IDs set to -1)
            else begin
              //Delete record
              Ticker_Collection.AtDelete(i);
              //Decrement collection count if less than max
              if (i < TickerCollectionCount-1) then
              begin
                i := i-1;
                Dec(TickerCollectionCount);
              end;
            end;
          end;
        end;
      end;
      Inc(i);
    end;

    //Delete any records marked for deletion after combining notes from consecutive 1-line entries
    //into a single 2-line entry
    MaxCount := Ticker_Collection.Count-1;
    i := 0;
    while i < MaxCount do
    begin
      TickerRecPtr := Ticker_Collection.At(i);
      if (TickerRecPtr^.UserData[1] = 'DELETE') then
      begin
        Ticker_Collection.AtDelete(i);
        Ticker_Collection.Pack;
        Dec(MaxCount);
      end;
      inc(i);
    end;

    //Added for Phase 2 Stats Inc. mods
    //Check to see if single stat entry should be expanded into 2 separate 1-line entries
    //The re-assigned template will be the first line; 2nd template added below
    //e.g. First template = Visitor; Second template = Home
    if (TickerDisplayMode = 3) OR (TickerDisplayMode = 4) then
    begin
      MaxCount := Ticker_Collection.Count-1;
      i := 0;
      While i <= MaxCount do
      begin
        //Point to collection object
        TickerRecPtr := Ticker_Collection.At(i);
        //Added for Phase 2 Stats Inc. Mod; expands stats related 2-line templates to two single-line templates
        //Check for record types > 20000 reserved for 1st template of 2 template expansion
        if (GetRecordTypeForTemplate(TickerRecPtr^.Template_ID) > 20000) AND (i <= Ticker_Collection.Count-1) then
        begin
          //Bump template ID and record type for single-line mode
          //Insert new record
          GetMem (NewTickerRecPtr, SizeOf(TickerRec));
          With NewTickerRecPtr^ do
          begin
            Event_Index := TickerRecPtr^.Event_Index+1;
            CreateGUID(EventGUID);
            Event_GUID := EventGUID;
            Enabled := TickerRecPtr^.Enabled;
            Is_Child := TickerRecPtr^.Is_Child;
            League := TickerRecPtr^.League;
            Subleague_Mnemonic_Standard := TickerRecPtr^.Subleague_Mnemonic_Standard;
            Mnemonic_LiveEvent := TickerRecPtr^.Mnemonic_LiveEvent;
            Template_ID := TickerRecPtr^.Template_ID + 10000; //Add 10000 to get home team template
            Record_Type := TickerRecPtr^.Record_Type + 10000; //Add 10000 to get home team template record type
            GameID := TickerRecPtr^.GameID;
            SponsorLogo_Name := TickerRecPtr^.SponsorLogo_Name;
            SponsorLogo_Dwell := TickerRecPtr^.SponsorLogo_Dwell;
            StatStoredProcedure := TickerRecPtr^.StatStoredProcedure;
            StatType := TickerRecPtr^.StatType;
            StatTeam := TickerRecPtr^.StatTeam;
            StatLeague := TickerRecPtr^.StatLeague;
            StatRecordNumber := TickerRecPtr^.StatRecordNumber;
            UserData[1] := SaveString1;
            for k := 2 to 50 do UserData[k] := '';
            StartEnableDateTime := TickerRecPtr^.StartEnableDateTime;
            EndEnableDateTime := TickerRecPtr^.EndEnableDateTime;
            Selected := TickerRecPtr^.Selected;
            DwellTime := TickerRecPtr^.DwellTime;
            Description := TickerRecPtr^.Description;
            Comments := TickerRecPtr^.Comments;
            If (Ticker_Collection.Count <= 1500) then
            begin
              //If last entry, append to end of collection; otherwise insert
              if (i = MaxCount) then
                Ticker_Collection.Insert(NewTickerRecPtr)
              else
                Ticker_Collection.AtInsert(i+1, NewTickerRecPtr);
              Ticker_Collection.Pack;
              //Increment the Max Count variable
              Inc(MaxCount);
            end;
          end;
          inc(i); //Jump past inserted record
        end;
        Inc(i);
      end;
    end;

    //Re-order event indices in collection; may have been affected by conversions
    if (Ticker_Collection.Count > 0) then
    begin
      for i := 0 to Ticker_Collection.Count-1 do
      begin
        TickerRecPtr := Ticker_Collection.At(i);
        TickerRecPtr^.Event_Index := i+1;
      end;
    end;

    //Refresh the grid
    RefreshPlaylistGrid;
  end;
end;

//Function to return the record type for a specific template ID
function TMainForm.GetRecordTypeForTemplate(TemplateID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutVal: SmallInt;
begin
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = TemplateID) then
        OutVal := TemplateDefsRecPtr^.Record_Type;
    end;
  end;
  GetRecordTypeForTemplate := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES AND HANDLERS FOR ZIPPER PLAYLIST CREATION AND EDITING
////////////////////////////////////////////////////////////////////////////////
//General procedure to clear and refresh the trial playlist grid
procedure TMainForm.RefreshPlaylistGrid;
var
  i: SmallInt;
  TickerPtr: ^TickerRec;
  BreakingNewsPtr: ^BreakingNewsRec;
  CollectionCount: SmallInt;
  AutoLeagueMnemonic: String;
  CurrentRow, CurrentTopRow: SmallInt;
begin
  //Get current row
  CurrentRow := PlaylistGrid.CurrentDataRow;
  CurrentTopRow := PlaylistGrid.TopRow;
  //Clear grid values
  if (PlaylistGrid.Rows > 0) then
    PlaylistGrid.DeleteRows (1, PlaylistGrid.Rows);
  //Init values
  Case PlaylistSelectTabControl.TabIndex of
   0: CollectionCount := Ticker_Collection.Count;
   1: CollectionCount := BreakingNews_Collection.Count;
  end;
  if (CollectionCount > 0) then
  begin
    PlaylistGrid.StoreData := TRUE;
    PlaylistGrid.Cols := 7;
    PlaylistGrid.Rows := CollectionCount;
    //Populate the grid
    for i := 0 to CollectionCount-1 do
    begin
      Case PlaylistSelectTabControl.TabIndex of
          //Ticker
       0: begin
            TickerPtr := Ticker_Collection.At(i);
            PlaylistGrid.Cell[1,i+1] := IntToStr(i+1); //Index
            //Check if automated league; if so, lookup display mnemonic
            //PlaylistGrid.Cell[2,i+1] := TickerPtr^.Subleague_Mnemonic_Standard; //Subleague name
            PlaylistGrid.Cell[2,i+1] := TickerPtr^.League; //League name
            PlaylistGrid.Cell[3,i+1] := IntToStr(TickerPtr^.Event_GUID.D2); //League name
            PlaylistGrid.Cell[4,i+1] := TickerPtr^.Enabled; //Record enable
            PlaylistGrid.Cell[5,i+1] := TickerPtr^.Template_ID; //Record enable
            PlaylistGrid.Cell[6,i+1] :=
              GetRecordTypeDescription(PlaylistSelectTabControl.TabIndex+1, TickerPtr^.Record_Type);
            if (TickerPtr^.Record_Type = 1) OR (TickerPtr^.Record_Type = 45) then
              PlaylistGrid.Cell[7,i+1] := TickerPtr^.SponsorLogo_Name
            else if (GetTemplateInformation(TickerPtr^.Template_ID).UsesGameData = TRUE) then
              PlaylistGrid.Cell[7,i+1] := TickerPtr^.Description
//            else if (TickerPtr^.Record_Type = 2) OR (TickerPtr^.Record_Type = 3) then
//              PlaylistGrid.Cell[7,i+1] := TickerPtr^.Comments
            else
              PlaylistGrid.Cell[7,i+1] := TickerPtr^.UserData[1];
            PlaylistGrid.Cell[8,i+1] := TickerPtr^.Comments; //Record enable
            //By default, show item over white background except if child template and added as group
            if (GetTemplateInformation(TickerPtr^.Template_ID).Template_Is_Child = TRUE) AND
               (TickerPtr^.Is_Child = TRUE) then
              PlaylistGrid.RowColor[i+1] := clGray
            else
              PlaylistGrid.RowColor[i+1] := clWindow;
            //Update number fo entries label
            NumEntries.Caption := IntToStr(Ticker_Collection.Count);
          end;
          //Alerts
       1: begin
            BreakingNewsPtr := BreakingNews_Collection.At(i);
            PlaylistGrid.Cell[1,i+1] := IntToStr(i+1); //Index
            PlaylistGrid.Cell[2,i+1] := BreakingNewsPtr^.League; //League name
            PlaylistGrid.Cell[3,i+1] := IntToStr(BreakingNewsPtr^.Event_GUID.D2); //League name
            PlaylistGrid.Cell[4,i+1] := BreakingNewsPtr^.Enabled; //Record enable
            PlaylistGrid.Cell[5,i+1] := BreakingNewsPtr^.Template_ID; //Record enable
            PlaylistGrid.Cell[6,i+1] :=
              GetRecordTypeDescription(PlaylistSelectTabControl.TabIndex+1, BreakingNewsPtr^.Record_Type);
            PlaylistGrid.Cell[7,i+1] := BreakingNewsPtr^.UserData[1];
            PlaylistGrid.Cell[8,i+1] := BreakingNewsPtr^.Comments; //Record enable
            PlaylistGrid.RowColor[i+1] := clWindow;
            //Update number fo entries label
            NumEntries.Caption := IntToStr(BreakingNews_Collection.Count);
          end;
      end;
    end;
  end
  else begin
    NumEntries.Caption := IntToStr(CollectionCount);
  end;
  //Refresh grid
  if (CurrentTopRow > 0) AND (CollectionCount > 0) then
  begin
    if (CurrentTopRow > CollectionCount) then CurrentTopRow := CollectionCount;
    PlaylistGrid.TopRow := CurrentTopRow;
  end;
  if (CurrentRow > 0) AND (CollectionCount > 0) then
  begin
    if (CurrentRow > CollectionCount) then CurrentRow := CollectionCount;
    PlaylistGrid.CurrentDataRow := CurrentRow;
    PlaylistGrid.RowSelected[CurrentRow] := TRUE;
  end;
end;

//Handler to refresh the entries grid when a new playlist record is selected
procedure TMainForm.PlaylistGridRowChanged(Sender: TObject; OldRow,
  NewRow: Integer);
begin
  if (PlaylistGrid.Rows > 0) then RefreshEntryFieldsGrid(TickerDisplayMode);
end;
//Procedure to update the entry fields grid with the data for the currently selected
//record in the playlist; also updates other entry specific data entry controls
procedure TMainForm.RefreshEntryFieldsGrid (TickerMode: SmallInt);
var
  i, j, k: SmallInt;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  CurrentTemplateID: SmallInt;
  NumTemplateFields: SmallInt;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CollectionCount: SmallInt;
  UserDataBias: SmallInt;
  TemplateInfo: TemplateDefsRec;
  CurrentGameData: GameRec;
begin
  Case PlaylistSelectTabControl.TabIndex of
   0: CollectionCount := Ticker_Collection.Count;
   1: CollectionCount := BreakingNews_Collection.Count;
  end;
  //Get data for current record in grid
  if (CollectionCount > 0) then
  begin
    Case PlaylistSelectTabControl.TabIndex of
        //Ticker
     0: begin
          TickerRecPtr := Ticker_Collection.At(PlaylistGrid.CurrentDataRow-1);
          CurrentTemplateID := TickerRecPtr^.Template_ID;
        end;
        //Alerts
     1: begin
          BreakingNewsRecPtr := BreakingNews_Collection.At(PlaylistGrid.CurrentDataRow-1);
          CurrentTemplateID := BreakingNewsRecPtr^.Template_ID;
        end;
    end;
    TemplateDescription.Caption := GetTemplateInformation(CurrentTemplateID).Template_Description;
    NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);
    if (NumTemplateFields > 0) then
    begin
      //Clear grid values
      if (EntryFieldsGrid.Rows > 0) then
        EntryFieldsGrid.DeleteRows (1, EntryFieldsGrid.Rows);
      EntryFieldsGrid.StoreData := TRUE;
      EntryFieldsGrid.Cols := 3;
      EntryFieldsGrid.Rows := NumTemplateFields;
      //Populate the grid
      i := 0;
      k := 0;
      //Get game data if template requires it
      Case PlaylistSelectTabControl.TabIndex of
          //Ticker
       0: begin
            TemplateInfo := EngineInterface.LoadTempTemplateFields(TickerRecPtr^.Template_ID);
            if (TemplateInfo.UsesGameData) then
            begin
              //Use full game data
              CurrentGameData := GameDataFunctions.GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
            end;
          end;
          //Alerts
       1: begin
            TemplateInfo := EngineInterface.LoadTempTemplateFields(BreakingNewsRecPtr^.Template_ID);
          end;
      end;
      //Get the manually entered fields
      for j := 0 to Template_Fields_Collection.Count-1 do
      begin
        TemplateFieldsRecPtr := Template_Fields_Collection.At(j);
        if (TemplateFieldsRecPtr^.Template_ID = CurrentTemplateID) then
        begin
          EntryFieldsGrid.Cell[1,i+1] := TemplateFieldsRecPtr^.Field_ID; //Index
          EntryFieldsGrid.Cell[2,i+1] := TemplateFieldsRecPtr^.Field_Label; //Description
          if (TemplateFieldsRecPtr^.Field_Is_Manual = TRUE) then
          begin
            //Set bias
            //Standard mode
            UserDataBias := 0;
            EntryFieldsGrid.RowColor[i+1] := clWindow;
            Case PlaylistSelectTabControl.TabIndex of
                //Ticker
             0: begin
                  //Insert league mnemonic and apply offset to account for league value being
                  //stored explicitly in ticker record
                  if (TemplateFieldsRecPtr^.Field_Type = 8) then
                  begin
                    EntryFieldsGrid.Cell[3,i+1] := TickerRecPtr^.League;
                    Dec(k);
                  end
                  //Check for image files
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1B) then
                    EntryFieldsGrid.Cell[3,i+1] := TickerRecPtr^.UserData[21]
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2B) then
                    EntryFieldsGrid.Cell[3,i+1] := TickerRecPtr^.UserData[22]
                  else
                    EntryFieldsGrid.Cell[3,i+1] := TickerRecPtr^.UserData[UserDataBias + k+1];
                end;
                //Alerts
             1: begin
                  //Insert league mnemonic and apply offset to account for league value being
                  //stored explicitly in ticker record
                  if (TemplateFieldsRecPtr^.Field_Type = 8) then
                  begin
                    EntryFieldsGrid.Cell[3,i+1] := BreakingNewsRecPtr^.League;
                    Dec(k);
                  end
                  else
                    EntryFieldsGrid.Cell[3,i+1] := BreakingNewsRecPtr^.UserData[k+1];
                end;
            end;
            Inc(k);
          end
          else begin
            //Set cell value
            Case PlaylistSelectTabControl.TabIndex of
                //Ticker
             0: begin
                  //Get game data if template requires it
                  EntryFieldsGrid.Cell[3,i+1] :=
                    GetValueOfSymbol(TICKER, TemplateFieldsRecPtr^.Field_Contents,
                      PlaylistGrid.CurrentDataRow-1, CurrentGameData, TickerDisplayMode).SymbolValue;
                end;
                //Alerts
             1: begin
                  EntryFieldsGrid.Cell[3,i+1] :=
                    GetValueOfSymbol(BREAKINGNEWS, TemplateFieldsRecPtr^.Field_Contents,
                      PlaylistGrid.CurrentDataRow-1, CurrentGameData, TickerDisplayMode).SymbolValue;
                end;
            end;
            EntryFieldsGrid.RowColor[i+1] := clAqua;
          end;
          //Increment the manual fields counter
          Inc (i);
        end;
      end;
    end;
    //Update other fields
    Case PlaylistSelectTabControl.TabIndex of
        //Ticker
     0: begin
          EntryNote.Text := TickerRecPtr^.Comments;
          EntryEnable.Checked := TickerRecPtr^.Enabled;
          EntryStartEnableDate.Date := TickerRecPtr^.StartEnableDateTime;
          EntryStartEnableTime.Time := TickerRecPtr^.StartEnableDateTime;
          EntryEndEnableDate.Date := TickerRecPtr^.EndEnableDateTime;
          EntryEndEnableTime.Time := TickerRecPtr^.EndEnableDateTime;
          EntryDwellTime.Value := Trunc(TickerRecPtr^.DwellTime/1000);
        end;
        //Alerts
     1: begin
          EntryNote.Text := BreakingNewsRecPtr^.Comments;
          EntryEnable.Checked := BreakingNewsRecPtr^.Enabled;
          EntryStartEnableDate.Date := BreakingNewsRecPtr^.StartEnableDateTime;
          EntryStartEnableTime.Time := BreakingNewsRecPtr^.StartEnableDateTime;
          EntryEndEnableDate.Date := BreakingNewsRecPtr^.EndEnableDateTime;
          EntryEndEnableTime.Time := BreakingNewsRecPtr^.EndEnableDateTime;
          EntryDwellTime.Value := Trunc(BreakingNewsRecPtr^.DwellTime/1000);
        end;
    end;
  end
  else begin
    if (EntryFieldsGrid.Rows > 0) then
      EntryFieldsGrid.DeleteRows (1, EntryFieldsGrid.Rows);
  end;
end;

//Handler for append entry to playlist button
procedure TMainForm.AddToPlaylistBtnClick(Sender: TObject);
begin
  Case PlaylistSelectTabControl.TabIndex of
    0..1: AddPlaylistEntry(dmMain.Query2.FieldByName('Template_ID').AsInteger, SINGLETEMPLATEONLY);
    2: AddDefaultTemplateEntry(dmMain.Query2.FieldByName('Template_ID').AsInteger);
  end;
end;
//Handler for double-click on template select grid
procedure TMainForm.AvailableTemplatesDBGridDblClick(Sender: TObject);
begin
  Case PlaylistSelectTabControl.TabIndex of
    0..1: AddPlaylistEntry(dmMain.Query2.FieldByName('Template_ID').AsInteger, SINGLETEMPLATEONLY);
    2: AddDefaultTemplateEntry(dmMain.Query2.FieldByName('Template_ID').AsInteger);
  end;
end;
//General procedure for appending an entry to the main ticker playlist
procedure TMainForm.AddPlaylistEntry(CurrentTemplateID: SmallInt; AddAllDefaultTemplates: Boolean);
var
  i,j,k,m,n: SmallInt;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  SaveRow: SmallInt;
  Control: Word;
  Modal: TZipperEntryDlg;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CurrentTemplateDefs: TemplateDefsRec;
  ChildTemplateIDRecPtr: ^ChildTemplateIDRec;
  NumTemplateFields: SmallInt;
  //CurrentTemplateID: SmallInt;
  User_Data: Array[1..50] of String[255];
  CollectionCount: SmallInt;
  CategoryRecPtr: ^CategoryRec;
  EventGUID: TGUID;
  OKToGo: Boolean;
  CurrentGameData: GameRec;
  NumGamesSelected: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  LeagueCodeRecPtr: ^LeagueCodeRec;
  WeatherIconRecPtr: ^WeatherIconRec;
  RowSelected: Boolean;
  RecordCount: SmallInt;
  SelectedTemplatesRecPtr: ^DefaultTemplatesRec;
begin
  //Init
  OKToGo := TRUE;

  //////////////////////////////////////////////////////////////////////////////
  //Execute this block of code if selecting individual templates; code for
  //Default Template list below
  //////////////////////////////////////////////////////////////////////////////
  if (AddAllDefaultTemplates = SINGLETEMPLATEONLY) then
  begin
    //Check to see if category tab is a sport; if so, show and populate games grid
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //First, check to see if sponsors. If so, show sponsor grid
    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
    begin
      //Get number of games selected
      if (dmMain.SportbaseQuery.RecordCount > 0) then
      begin
        NumGamesSelected := GamesDBGrid.SelectedRows.Count;
        if (NumGamesSelected = 0) then
        begin
          GamesDBGrid.SelectRows(GamesDBGrid.CurrentDataRow, GamesDBGrid.CurrentDataRow, TRUE);
          NumGamesSelected := 1;
        end;
      end
      else NumGamesSelected := 0;
    end
    else NumGamesSelected := 0;

    //Init fields
    for i := 1 to 50 do User_Data[i] := ' ';

    //Get collection count
    Case PlaylistSelectTabControl.TabIndex of
         //Ticker
      TICKER: CollectionCount := Ticker_Collection.Count;
         //Alerts
      BREAKINGNEWS: CollectionCount := BreakingNews_Collection.Count;
    end;
    if (CollectionCount < 1500) then
    begin
      CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
      //CurrentTemplateID := dmMain.Query2.FieldByName('Template_ID').AsInteger;
      NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);
      //First, check to make sure that there are games listed if the template requires one
      CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
      if (CurrentTemplateDefs.Record_Type = 1) AND
        (CategoryRecPtr^.Category_Is_Sport = TRUE) AND (dmMain.SportbaseQuery.RecordCount = 0) then
      begin
        MessageDlg('There are no games available or a game has not been selected. The record ' +
                   'cannot be added to the playlist.', mtWarning, [mbOK], 0);
      end
      //Check to see if odds only selected and user is selecting a template that requires game data
      //else if (CurrentTemplateDefs.EnableForOddsOnly = FALSE) AND (ShowOddsOnly.Checked) then
      //begin
      //  MessageDlg('The selected template cannot be used when game data is not available. Please select ' +
      //    'an "Odds Only" template.', mtError, [mbOK], 0);
      //end
      //OK to proceed
      else begin
        //Only proceed if fields defined, prompt for data enabled and NOT a parent template; if more than one
        //game selected, don't prompt operator
        if (NumTemplateFields > 0) AND (PromptForData.Checked) AND
           (CurrentTemplateDefs.Template_Has_Children = FALSE) AND
           (((NumGamesSelected = 1) AND (GetTemplateInformation(CurrentTemplateID).UsesGameData=TRUE) AND
             (dmMain.SportbaseQuery.RecordCount > 0)) OR (GetTemplateInformation(CurrentTemplateID).UsesGameData=FALSE)) then
        begin
          try
            Modal := TZipperEntryDlg.Create(Application);
            //Add style chips
            if (StyleChip_Collection.Count > 0) then
            begin
              Modal.StyleChipsGrid.StoreData := TRUE;
              Modal.StyleChipsGrid.Cols := 2;
              Modal.StyleChipsGrid.Rows := StyleChip_Collection.Count;
              for i := 0 to StyleChip_Collection.Count-1 do
              begin
                StyleChipRecPtr := StyleChip_Collection.At(i);
                Modal.StyleChipsGrid.Cell[1,i+1] := StyleChipRecPtr^.StyleChip_Code;
                Modal.StyleChipsGrid.Cell[2,i+1] := StyleChipRecPtr^.StyleChip_Description;
              end;
            end;
            //Set initial values for grid
            //Clear grid values
            if (Modal.RecordGrid.Rows > 0) then
              Modal.RecordGrid.DeleteRows (1, Modal.RecordGrid.Rows);
            Modal.RecordGrid.StoreData := TRUE;
            Modal.RecordGrid.Cols := 4;
            Modal.RecordGrid.Rows := NumTemplateFields;
            //Populate the grid
            i := 0;
            //Get the game data
            if (GetTemplateInformation(CurrentTemplateID).UsesGameData) then
            begin
              CurrentGameData := GameDataFunctions.GetGameData(CategoryRecPtr^.Category_Sport,
                Trim(dmMain.SportbaseQuery.FieldByname('GCode').AsString));
            end;
            //Iterate through the fields
            for j := 0 to Template_Fields_Collection.Count-1 do
            begin
              TemplateFieldsRecPtr := Template_Fields_Collection.At(j);
              if (TemplateFieldsRecPtr^.Template_ID = CurrentTemplateID) then
              begin
                Modal.RecordGrid.Cell[1,i+1] := TemplateFieldsRecPtr^.Field_ID; //Index
                Modal.RecordGrid.Cell[2,i+1] := TemplateFieldsRecPtr^.Field_Label; //Description
                //Set color & read only property
                Modal.RecordGrid.CellReadOnly[1,i+1] := roOn;
                Modal.RecordGrid.CellReadOnly[2,i+1] := roOn;
                Modal.RecordGrid.Cell[4,i+1] := TemplateFieldsRecPtr^.Field_Type;
                Modal.RecordGrid.CellReadOnly[4,i+1] := roOn;
                if (TemplateFieldsRecPtr^.Field_Is_Manual = TRUE) then
                begin
                  //First check to see if it's a manual game winner indicator or animation
                  if (TemplateFieldsRecPtr^.Field_Type = 5) OR (TemplateFieldsRecPtr^.Field_Type = 6) then
                  begin
                    Modal.RecordGrid.CellControlType[3,i+1] := ctCheck;
                    Modal.RecordGrid.CellCheckBoxState[3,i+1] := cbUnchecked;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Next, check to see if it's a manual next page icon animation
                  else if (TemplateFieldsRecPtr^.Field_Type = 9) then
                  begin
                    Modal.RecordGrid.CellControlType[3,i+1] := ctCheck;
                    Modal.RecordGrid.CellCheckBoxState[3,i+1] := cbUnchecked;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Next, check to see if it's a weather icon; if so, display flipbooks in dropdown
                  else if (TemplateFieldsRecPtr^.Field_Type = 7) then
                  begin
                    Modal.RecordGrid.AssignCellCombo(3,i+1);
                    Modal.RecordGrid.CellButtonType[3,i+1] := btCombo;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                    //Init first combo box entry
                    WeatherIconRecPtr := Weather_Icon_Collection.At(0);
                    Modal.RecordGrid.Cell[3, i+1] := WeatherIconRecPtr^.Icon_Name;
                  end
                  //Next, check to see if it's the manual league entry; if so, display choices in dropdown
                  else if (TemplateFieldsRecPtr^.Field_Type = 8) then
                  begin
                    //Init combo box entry; fixed for breaking news
                    LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                    Modal.RecordGrid.Cell[3,i+1] :=  LeagueCodeRecPtr^.Heading;
                  end
                  //Added for Altitude sports
                  //Next, check to see if it's automatic league entry; if so, use category tab to get league
                  else if (TemplateFieldsRecPtr^.Field_Type = 10) then
                  begin
                    //Init combo box entry; fixed for breaking news
                    CategoryRecPtr := Categories_Collection.At(LeagueTab.TabIndex);
                    Modal.RecordGrid.Cell[3,i+1] :=  CategoryRecPtr^.Category_Label;
                  end
                  //Added for Altitude sports
                  //Next, check for team logo
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1B) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2B) then
                  begin
                    Modal.RecordGrid.CellButtonType[3,i+1] := btNormal;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  else begin
                    Modal.RecordGrid.RowColor[i+1] := clWindow;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end;
                end
                //Not a manual field, so populate automatically
                else begin
                  //Set cell value
                  if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) <> 0) then
                  begin
                    //Check if manual template; if so put league into first field
                    if (TemplateFieldsRecPtr^.Field_Contents = '$League') then
                    begin
                      LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                      Modal.RecordGrid.Cell[3,i+1] := LeagueCodeRecPtr^.Heading;
                    end
                    else
                      Modal.RecordGrid.Cell[3,i+1] := GetValueOfSymbol(PlaylistSelectTabControl.TabIndex+1,
                        TemplateFieldsRecPtr^.Field_Contents, NOENTRYINDEX, CurrentGameData, 0).SymbolValue;
                  end
                  else
                    Modal.RecordGrid.Cell[3,i+1] := TemplateFieldsRecPtr^.Field_Contents;
                  Modal.RecordGrid.RowColor[i+1] := clAqua;
                  Modal.RecordGrid.CellReadOnly[3,i+1] := roOn;
                end;
                //Increment the template fields match counter
                Inc (i);
              end;
            end;
            Modal.TemplateName.Caption := CurrentTemplateDefs.Template_Description;

            //Show the dialog
            Control := Modal.ShowModal;
          finally
            //Set new values if user didn't cancel out
            if (Control = mrOK) then OKToGo := TRUE
            else OKToGo := FALSE;
            //Set values based on data in grid
            if (Modal.RecordGrid.Rows > 0) then
            begin
              i := 1;
              for j := 1 to Modal.RecordGrid.Rows do
              begin
                if (Modal.RecordGrid.CellReadOnly[3,j] = roOff) then
                begin
                  //Check for visitor manual winner indication record type
                  if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                  begin
                    User_Data[24] := 'TRUE';
                    User_Data[49] := 'TRUE';
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                  begin
                    User_Data[24] := 'FALSE';
                    User_Data[49] := 'FALSE';
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                  begin
                    User_Data[25] := 'TRUE';
                    User_Data[50] := 'TRUE';
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                  begin
                    User_Data[25] := 'FALSE';
                    User_Data[50] := 'FALSE';
                    Dec(i);
                  end
                  //Check for next page animated icon
                  else if (Modal.RecordGrid.Cell[4,j] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                  begin
                    User_Data[23] := 'TRUE';
                    User_Data[48] := 'TRUE';
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                  begin
                    User_Data[23] := 'FALSE';
                    User_Data[48] := 'FALSE';
                    Dec(i);
                  end
                  //Check for image file types
                  else if (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_1A) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_1B) then
                  begin
                    User_Data[21] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    User_Data[46] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    //Alert operator if blank and dump out
                    if (Trim(User_Data[21]) = '') then
                    begin
                      MessageDlg('One or both team logos were not specified - please try adding the page again.', mtError, [mbOK], 0);
                      OkToGo := FALSE;
                    end;
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_2A) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_2B) then
                  begin
                    User_Data[22] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    User_Data[47] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    //Alert operator if blank and dump out
                    if (Trim(User_Data[22]) = '') then
                    begin
                      MessageDlg('One or both team logos were not specified - please try adding the page again.', mtError, [mbOK], 0);
                      OkToGo := FALSE;
                    end;
                    Dec(i);
                  end
                  else begin
                    //Set user value text and increment
                    User_Data[i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    //Set default live mode values - can be edited later
                    User_Data[25+i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                  end;
                  Inc(i);
                end;
              end;
            end;
            //Save current grid row
            SaveRow := PlaylistGrid.CurrentDataRow;
            Modal.Free;
          end;
        end
        else begin
          //Init fields
          for i := 1 to 50 do User_Data[i] := ' ';
        end;

        //Append record; falls through to here whether or not user dialog is displayed
        if (OKToGo) then
        begin
          ////////////////////////////////////////////////////////////////////////
          // THIS BLOCK EXECUTED FOR CATEGORIES THAT ARE AUTOMATED SPORTS AND FOR
          // TEMPLATES THAT REQUIRE GAME DATA
          ////////////////////////////////////////////////////////////////////////
          //Check to see if at least one game in sportbase database
          if (CategoryRecPtr^.Category_Is_Sport = TRUE) AND
             (dmMain.SportbaseQuery.RecordCount > 0) AND
             (CurrentTemplateDefs.UsesGameData = TRUE) then
          begin
            //Loop through for all selected games
            if (ShowOddsOnly.Checked) then
            begin
              //Go to first record in dataset
              dmMain.SportbaseQuery3.First;
              RecordCount := dmMain.SportbaseQuery3.RecordCount
            end
            else begin
              //Go to first record in dataset
              dmMain.SportbaseQuery.First;
              RecordCount := dmMain.SportbaseQuery.RecordCount;
            end;
            for m := 1 to RecordCount do
            begin
              //Check to see if game is selected
              if (ShowOddsOnly.Checked) then
                RowSelected := OddsDBGrid.RowSelected[OddsDBGrid.CurrentDataRow]
              else
                RowSelected := GamesDBGrid.RowSelected[GamesDBGrid.CurrentDataRow];
              if (RowSelected = TRUE) then
              begin
                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR PARENT TEMPLATES - ADDS CHILDREN
                ////////////////////////////////////////////////////////////////////////
                //Check to see if template is a "parent" template; if so, add all child templates
                if (CurrentTemplateDefs.Template_Has_Children) then
                begin
                  if (Child_Template_ID_Collection.Count > 0) then
                  begin
                    for i := 0 to Child_Template_ID_Collection.Count-1 do
                    begin
                      ChildTemplateIDRecPtr := Child_Template_ID_Collection.At(i);
                      //Check for child, and append if match
                      if (ChildTemplateIDRecPtr^.Template_ID = CurrentTemplateID) then
                      begin
                        //Allocate memory for the object
                        Case PlaylistSelectTabControl.TabIndex of
                             //Ticker
                         0: begin
                              GetMem (TickerRecPtr, SizeOf(TickerRec));
                              With TickerRecPtr^ do
                              begin
                                //Set values for collection record
                                Event_Index := Ticker_Collection.Count+1;
                                CreateGUID(EventGUID);
                                Event_GUID := EventGUID;
                                Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                                Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                                Subleague_Mnemonic_Standard := '';
                                Mnemonic_LiveEvent := '';
                                Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                                //Set league ID
                                if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                                begin
                                  League := CategoryRecPtr^.Category_Sport;
                                  //League := Trim(dmMain.SportbaseQuery.FieldByname('League').AsString);
                                  GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                                  Description := GameDataFunctions.GetGameData(TRIM(League), TRIM(GameID)).Description;
                                end
                                else begin
                                  League := 'NONE';
                                  GameID := '-1';
                                  Description := '';
                                end;
                                Selected := FALSE;
                                //Check to see if sponsor logo
                                CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                                if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                                begin
                                  SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                                  SponsorLogo_Dwell := SponsorLogoDwell.Value;
                                end
                                else begin
                                  SponsorLogo_Name := ' ';
                                  SponsorLogo_Dwell := 0;
                                end;
                                //Set Is Child bit
                                Is_Child := TRUE;
                                //For stats
                                StatStoredProcedure := ' ';
                                StatType := 0;;
                                StatTeam := ' ';
                                StatLeague := ' ';
                                StatRecordNumber := 0;
                                for k := 1 to 50 do UserData[k] := User_Data[k];
                                //Set the start/end enable times to the template defaults
                                StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                                EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                                DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                                Comments := ' ';
                                //Insert object to collection
                                Ticker_Collection.Insert(TickerRecPtr);
                                Ticker_Collection.Pack;
                              end;
                            end;
                             //Alerts
                         1: begin
                              GetMem (BreakingNewsRecPtr, SizeOf(BreakingNewsRec));
                              With BreakingNewsRecPtr^ do
                              begin
                                //Set values for collection record
                                Event_Index := BreakingNews_Collection.Count+1;
                                CreateGUID(EventGUID);
                                Event_GUID := EventGUID;
                                Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                                LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                                League := LeagueCodeRecPtr^.Heading;
                                Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                                Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                                Description := '';
                                Selected := FALSE;
                                //Set Is Child bit
                                for k := 1 to 2 do UserData[k] := User_Data[k];
                                //Set the start/end enable times to the template defaults
                                StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                                EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                                DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                                Comments := ' ';
                                //Insert object to collection
                                BreakingNews_Collection.Insert(BreakingNewsRecPtr);
                                BreakingNews_Collection.Pack;
                              end;
                            end;
                        end;
                      end;
                    end;
                  end;
                end
                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR NON-PARENT TEMPLATES IN SPORTS CATEGORIES
                ////////////////////////////////////////////////////////////////////////
                //Not a parent template, so just append record for selected record type
                else begin
                  //Allocate memory for the object
                  Case PlaylistSelectTabControl.TabIndex of
                       //Ticker
                   0: begin
                        GetMem (TickerRecPtr, SizeOf(TickerRec));
                        With TickerRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := Ticker_Collection.Count+1;
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := EntryEnable.Checked;
                          Template_ID := CurrentTemplateID;
                          Subleague_Mnemonic_Standard := '';
                          Mnemonic_LiveEvent := '';
                          Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                          //Set league ID
                          if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                          begin
                            League := CategoryRecPtr^.Category_Sport;
                            //League := Trim(dmMain.SportbaseQuery.FieldByname('League').AsString);
                            GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                            Description := GameDataFunctions.GetGameData(TRIM(League), TRIM(GameID)).Description;
                          end
                          else begin
                            League := 'NONE';
                            GameID := '-1';
                            Description := '';
                          end;
                          //Check to see if sponsor logo
                          CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                          if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                          begin
                            SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                            SponsorLogo_Dwell := SponsorLogoDwell.Value;
                          end
                          else begin
                            SponsorLogo_Name := ' ';
                            SponsorLogo_Dwell := 0;
                          end;
                          //Clear Is Child bit
                          Is_Child := FALSE;
                          Selected := FALSE;
                          //For stats
                          StatStoredProcedure := '';
                          StatType := 0;;
                          StatTeam := ' ';
                          StatLeague := ' ';
                          StatRecordNumber := 0;
                          for k := 1 to 50 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                          Comments := ' ';
                          //Insert object to collection
                          Ticker_Collection.Insert(TickerRecPtr);
                          Ticker_Collection.Pack;
                        end;
                      end;
                      //Alerts
                   1: begin
                        GetMem (BreakingNewsRecPtr, SizeOf(BreakingNewsRec));
                        With BreakingNewsRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := BreakingNews_Collection.Count+1;
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := EntryEnable.Checked;
                          LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                          League := LeagueCodeRecPtr^.Heading;
                          Template_ID := CurrentTemplateID;
                          Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                          Description := '';
                          for k := 1 to 2 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                          Selected := FALSE;
                          Comments := ' ';
                          //Insert object to collection
                          BreakingNews_Collection.Insert(BreakingNewsRecPtr);
                          BreakingNews_Collection.Pack;
                        end;
                      end;
                  end;
                end; //Single template type (NOT a parent record)
              end;
              //Go to next record
              if (ShowOddsOnly.Checked) then
                dmMain.SportbaseQuery3.Next
              else
                dmMain.SportbaseQuery.Next;
            end; //Loop for selected games
          end
          ////////////////////////////////////////////////////////////////////////
          // THIS BLOCK EXECUTED FOR TEMPLATES THAT DO NOT REQUIRE GAME DATA
          ////////////////////////////////////////////////////////////////////////
          //No games in Sportbase query - if not a game specific record type, just add one record
          else if (CurrentTemplateDefs.UsesGameData = FALSE) then
          begin
            //Allocate memory for the object
            Case PlaylistSelectTabControl.TabIndex of
                 //Ticker
             0: begin
                  GetMem (TickerRecPtr, SizeOf(TickerRec));
                  With TickerRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := Ticker_Collection.Count+1;
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    Template_ID := CurrentTemplateID;
                    Subleague_Mnemonic_Standard := '';
                    Mnemonic_LiveEvent := '';
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Get category & league information
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
                    begin
                      League := CategoryRecPtr^.Category_Sport;
                    end
                    else begin
                      //Check if manual league; if so, first user text field will be league
                      if (CurrentTemplateDefs.ManualLeague) then
                      begin
                        LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                        League := LeagueCodeRecPtr^.Heading;
                      end
                      else begin
                        League := 'NONE';
                      end;
                    end;
                    GameID := '0';
                    Description := '';
                    //Check to see if sponsor logo
                    if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                    begin
                      SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                      SponsorLogo_Dwell := SponsorLogoDwell.Value;
                    end
                    else begin
                      SponsorLogo_Name := '';
                      SponsorLogo_Dwell := 0;
                    end;
                    //Clear Is Child bit
                    Is_Child := FALSE;
                    //For stats
                    StatStoredProcedure := '';
                    StatType := 0;;
                    StatTeam := '';
                    StatLeague := '';
                    StatRecordNumber := 0;
                    for k := 1 to 50 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Selected := FALSE;
                    Comments := ' ';
                    //Insert object to collection
                    Ticker_Collection.Insert(TickerRecPtr);
                    Ticker_Collection.Pack;
                  end;
                end;
                 //Alerts
             1: begin
                  GetMem (BreakingNewsRecPtr, SizeOf(BreakingNewsRec));
                  With BreakingNewsRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := BreakingNews_Collection.Count+1;
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                    League := LeagueCodeRecPtr^.Heading;
                    Template_ID := CurrentTemplateID;
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Get category & league information
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    Description := '';
                    for k := 1 to 22 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Selected := FALSE;
                    Comments := ' ';
                    //Insert object to collection
                    BreakingNews_Collection.Insert(BreakingNewsRecPtr);
                    BreakingNews_Collection.Pack;
                  end;
                end;
            end; //Single template type (NOT a parent record)
          end;
        end;
        //Refresh grid
        RefreshPlaylistGrid;
        //Refresh the fields grid
        RefreshEntryFieldsGrid(TickerDisplayMode);
        //Restore current grid row
        if (SaveRow < 1) then SaveRow := 1;
        PlaylistGrid.CurrentDataRow := SaveRow;
      end;
    end;
  end
  //////////////////////////////////////////////////////////////////////////////
  //Execute this block of code if using Default Template list
  //////////////////////////////////////////////////////////////////////////////
  else begin
    //Save current grid row
    SaveRow := PlaylistGrid.CurrentDataRow;
    OKToGo := TRUE;
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //First, check to see if sponsors. If so, show sponsor grid
    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
    begin
      //Get number of games selected
      if (dmMain.SportbaseQuery.RecordCount > 0) then
      begin
        NumGamesSelected := GamesDBGrid.SelectedRows.Count;
        if (NumGamesSelected = 0) then
        begin
          GamesDBGrid.SelectRows(GamesDBGrid.CurrentDataRow, GamesDBGrid.CurrentDataRow, TRUE);
          NumGamesSelected := 1;
        end;
      end
      else begin
        MessageDlg('There are no games available or a game has not been selected. The records ' +
                   'cannot be added to the playlist.', mtWarning, [mbOK], 0);
        NumGamesSelected := 0;
        OKToGo := FALSE;
      end;
    end
    else begin
      NumGamesSelected := 0;
      OKToGo := FALSE;
    end;

    //Proceed if OK
    if (OKToGo) AND (CollectionCount < 1500) AND (Selected_Templates_Collection.Count > 0) then
    begin
      //Go to first record in dataset
      dmMain.SportbaseQuery.First;
      RecordCount := dmMain.SportbaseQuery.RecordCount;
      for m := 1 to RecordCount do
      begin
        RowSelected := GamesDBGrid.RowSelected[GamesDBGrid.CurrentDataRow];
        if (RowSelected = TRUE) then
        begin
          //Iterate through all default templates
          for n := 0 to Selected_Templates_Collection.Count-1 do
          begin
            SelectedTemplatesRecPtr := Selected_Templates_Collection.At(n);
            //Only proceed if template enabled
            if (SelectedTemplatesRecPtr^.Enabled) then
            begin
              CurrentTemplateID := SelectedTemplatesRecPtr^.Template_ID;
              CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
              ////////////////////////////////////////////////////////////////////////
              // THIS BLOCK EXECUTED FOR CATEGORIES THAT ARE AUTOMATED SPORTS AND FOR
              // TEMPLATES THAT REQUIRE GAME DATA
              ////////////////////////////////////////////////////////////////////////
              if (CurrentTemplateDefs.UsesGameData = TRUE) then
              begin
                //Init fields
                for i := 1 to 50 do User_Data[i] := ' ';
                //CurrentTemplateID := dmMain.Query2.FieldByName('Template_ID').AsInteger;
                NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);

                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR PARENT TEMPLATES - ADDS CHILDREN
                ////////////////////////////////////////////////////////////////////////
                //Check to see if template is a "parent" template; if so, add all child templates
                if (CurrentTemplateDefs.Template_Has_Children) then
                begin
                  if (Child_Template_ID_Collection.Count > 0) then
                  begin
                    for i := 0 to Child_Template_ID_Collection.Count-1 do
                    begin
                      ChildTemplateIDRecPtr := Child_Template_ID_Collection.At(i);
                      //Check for child, and append if match
                      if (ChildTemplateIDRecPtr^.Template_ID = CurrentTemplateID) then
                      begin
                        //Allocate memory for the object
                        GetMem (TickerRecPtr, SizeOf(TickerRec));
                        With TickerRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := Ticker_Collection.Count+1;
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                          Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                          Subleague_Mnemonic_Standard := '';
                          Mnemonic_LiveEvent := '';
                          Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                          //Set league ID
                          if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                          begin
                            League := CategoryRecPtr^.Category_Sport;
                            //League := Trim(dmMain.SportbaseQuery.FieldByname('League').AsString);
                            GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                            Description := GameDataFunctions.GetGameData(TRIM(League), TRIM(GameID)).Description;
                          end
                          else begin
                            League := 'NONE';
                            GameID := '-1';
                            Description := '';
                          end;
                          Selected := FALSE;
                          //Check to see if sponsor logo
                          CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                          if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                          begin
                            SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                            SponsorLogo_Dwell := SponsorLogoDwell.Value;
                          end
                          else begin
                            SponsorLogo_Name := ' ';
                            SponsorLogo_Dwell := 0;
                          end;
                          //Set Is Child bit
                          Is_Child := TRUE;
                          //For stats
                          StatStoredProcedure := ' ';
                          StatType := 0;;
                          StatTeam := ' ';
                          StatLeague := ' ';
                          StatRecordNumber := 0;
                          for k := 1 to 50 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                          Comments := ' ';
                          //Insert object to collection
                          Ticker_Collection.Insert(TickerRecPtr);
                          Ticker_Collection.Pack;
                        end;
                      end;
                    end;
                  end;
                end
                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR NON-PARENT TEMPLATES IN SPORTS CATEGORIES
                ////////////////////////////////////////////////////////////////////////
                //Not a parent template, so just append record for selected record type
                else begin
                  //Allocate memory for the object
                  GetMem (TickerRecPtr, SizeOf(TickerRec));
                  With TickerRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := Ticker_Collection.Count+1;
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    Template_ID := CurrentTemplateID;
                    Subleague_Mnemonic_Standard := '';
                    Mnemonic_LiveEvent := '';
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Set league ID
                    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                    begin
                      League := CategoryRecPtr^.Category_Sport;
                      //League := Trim(dmMain.SportbaseQuery.FieldByname('League').AsString);
                      GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                      Description := GameDataFunctions.GetGameData(TRIM(League), TRIM(GameID)).Description;
                    end
                    else begin
                      League := 'NONE';
                      GameID := '-1';
                      Description := '';
                    end;
                    //Check to see if sponsor logo
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                    begin
                      SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                      SponsorLogo_Dwell := SponsorLogoDwell.Value;
                    end
                    else begin
                      SponsorLogo_Name := ' ';
                      SponsorLogo_Dwell := 0;
                    end;
                    //Clear Is Child bit
                    Is_Child := FALSE;
                    Selected := FALSE;
                    //For stats
                    StatStoredProcedure := '';
                    StatType := 0;;
                    StatTeam := ' ';
                    StatLeague := ' ';
                    StatRecordNumber := 0;
                    for k := 1 to 50 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Comments := ' ';
                    //Insert object to collection
                    Ticker_Collection.Insert(TickerRecPtr);
                    Ticker_Collection.Pack;
                  end;
                end; //Single template type (NOT a parent record)
              end //End of block for templates that use game data

              //////////////////////////////////////////////////////////////////
              //THIS BLOCK OF CODE FOR TEMPLATES THAT DO NOT REQUIRE GAME DATA
              //////////////////////////////////////////////////////////////////
              else begin
                //Allocate memory for the object
                GetMem (TickerRecPtr, SizeOf(TickerRec));
                With TickerRecPtr^ do
                begin
                  //Set values for collection record
                  Event_Index := Ticker_Collection.Count+1;
                  CreateGUID(EventGUID);
                  Event_GUID := EventGUID;
                  Enabled := EntryEnable.Checked;
                  Template_ID := CurrentTemplateID;
                  Subleague_Mnemonic_Standard := '';
                  Mnemonic_LiveEvent := '';
                  Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                  //Get category & league information
                  CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                  if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
                  begin
                    League := CategoryRecPtr^.Category_Sport;
                  end
                  else begin
                    //Check if manual league; if so, first user text field will be league
                    if (CurrentTemplateDefs.ManualLeague) then
                    begin
                      LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                      League := LeagueCodeRecPtr^.Heading;
                    end
                    else begin
                      League := 'NONE';
                    end;
                  end;
                  GameID := '0';
                  Description := '';
                  //Check to see if sponsor logo
                  if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                  begin
                    SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                    SponsorLogo_Dwell := SponsorLogoDwell.Value;
                  end
                  else begin
                    SponsorLogo_Name := '';
                    SponsorLogo_Dwell := 0;
                  end;
                  //Clear Is Child bit
                  Is_Child := FALSE;
                  //For stats
                  StatStoredProcedure := '';
                  StatType := 0;;
                  StatTeam := '';
                  StatLeague := '';
                  StatRecordNumber := 0;
                  for k := 1 to 50 do UserData[k] := User_Data[k];
                  //Set the start/end enable times to the template defaults
                  StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                  EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                  DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                  Selected := FALSE;
                  Comments := ' ';
                  //Insert object to collection
                  Ticker_Collection.Insert(TickerRecPtr);
                  Ticker_Collection.Pack;
                end; //Single template type (NOT a parent record)
              end;
            end;
          end;
        end;
        //Next game
        dmMain.SportbaseQuery.Next;
      end;
    end;
  end;
  //Refresh grid
  RefreshPlaylistGrid;
  //Refresh the fields grid
  RefreshEntryFieldsGrid(TickerDisplayMode);
  //Restore current grid row
  if (SaveRow < 1) then SaveRow := 1;
  PlaylistGrid.CurrentDataRow := SaveRow;
end;

//Handler for insert entry into playlist button
procedure TMainForm.InsertIntoPlaylistBtnClick(Sender: TObject);
begin
  Case PlaylistSelectTabControl.TabIndex of
    0..1:   InsertPlaylistEntry(dmMain.Query2.FieldByName('Template_ID').AsInteger, SINGLETEMPLATEONLY);
    2: InsertDefaultTemplateEntry(dmMain.Query2.FieldByName('Template_ID').AsInteger);
  end;

end;
//General procedure to insert an entry into the main ticker playlist
procedure TMainForm.InsertPlaylistEntry(CurrentTemplateID: SmallInt; AddAllDefaultTemplates: Boolean);
var
  OKToGo: Boolean;
  SaveRow, SaveTopRow: SmallInt;
  SaveGamesRow, SaveGamesTopRow: Variant;
  i,j,k,m,n: SmallInt;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  Control: Word;
  Modal: TZipperEntryDlg;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  ChildTemplateIDRecPtr: ^ChildTemplateIDRec;
  NumTemplateFields: SmallInt;
  //CurrentTemplateID: SmallInt;
  CurrentTemplateDefs: TemplateDefsRec;
  User_Data: Array[1..50] of String[255];
  CollectionCount: SmallInt;
  CategoryRecPtr: ^CategoryRec;
  CurrentGameData: GameRec;
  EventGUID: TGUID;
  NumGamesSelected: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  LeagueCodeRecPtr: ^LeagueCodeRec;
  WeatherIconRecPtr: ^WeatherIconRec;
  RowSelected: Boolean;
  RecordCount: SmallInt;
  SelectedTemplatesRecPtr: ^DefaultTemplatesRec;
begin
  //Init
  OKToGo := TRUE;
  for i := 1 to 50 do User_Data[i] := '';

  //////////////////////////////////////////////////////////////////////////////
  //Execute this block of code if selecting individual templates; code for
  //Default Template list below
  //////////////////////////////////////////////////////////////////////////////
  if (AddAllDefaultTemplates = SINGLETEMPLATEONLY) then
  begin
    //Check to see if category tab is a sport; if so, show and populate games grid
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //First, check to see if sponsors. If so, show sponsor grid
    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
    begin
      //Get number of games selected
      if (dmMain.SportbaseQuery.RecordCount > 0) then
      begin
        NumGamesSelected := GamesDBGrid.SelectedRows.Count;
        if (NumGamesSelected = 0) then
        begin
          GamesDBGrid.SelectRows(GamesDBGrid.CurrentDataRow, GamesDBGrid.CurrentDataRow, TRUE);
          NumGamesSelected := 1;
        end;
      end
      else NumGamesSelected := 0;
    end
    else NumGamesSelected := 0;

    //Init fields
    for i := 1 to 50 do User_Data[i] := ' ';

    //Get collection count
    Case PlaylistSelectTabControl.TabIndex of
         //Ticker
      0: CollectionCount := Ticker_Collection.Count;
        //Alerts
      1: CollectionCount := BreakingNews_Collection.Count;
    end;
    if (CollectionCount < 1500) then
    begin
      CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
      //Save current grid row
      SaveRow := PlaylistGrid.CurrentDataRow;
      SaveTopRow := PlaylistGrid.TopRow;
      SaveGamesRow := GamesDBGrid.CurrentDataRow;
      SaveGamesTopRow := GamesDBGrid.TopRow;

      //Get template information
      //CurrentTemplateID := dmMain.Query2.FieldByName('Template_ID').AsInteger;
      NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);
      CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
      //First, check to make sure that there are games listed if the template requires one
      if (CurrentTemplateDefs.Record_Type = 1) AND
        (CategoryRecPtr^.Category_Is_Sport = TRUE) AND (dmMain.SportbaseQuery.RecordCount = 0) then
      begin
        MessageDlg('There are no games available or a game has not been selected. The record ' +
                   'cannot be added to the playlist.', mtWarning, [mbOK], 0);
      end
      //Check to see if odds only selected and user is selecting a template that requires game data
      //else if (CurrentTemplateDefs.EnableForOddsOnly = FALSE) AND (ShowOddsOnly.Checked) then
      // begin
      //   MessageDlg('The selected template cannot be used when game data is not available (odds only).', mtError, [mbOK], 0);
      //end
      //OK to proceed
      else begin
        //Only proceed if fields defined, prompt for data enabled and NOT a parent template
        if (NumTemplateFields > 0) AND (PromptForData.Checked) AND
           (CurrentTemplateDefs.Template_Has_Children = FALSE) AND
           (((NumGamesSelected = 1) AND (GetTemplateInformation(CurrentTemplateID).UsesGameData=TRUE) AND
             (dmMain.SportbaseQuery.RecordCount > 0)) OR (GetTemplateInformation(CurrentTemplateID).UsesGameData=FALSE)) then
        begin
          try
            Modal := TZipperEntryDlg.Create(Application);
            //Set initial values for grid
            //Add style chips
            if (StyleChip_Collection.Count > 0) then
            begin
              Modal.StyleChipsGrid.StoreData := TRUE;
              Modal.StyleChipsGrid.Cols := 2;
              Modal.StyleChipsGrid.Rows := StyleChip_Collection.Count;
              for i := 0 to StyleChip_Collection.Count-1 do
              begin
                StyleChipRecPtr := StyleChip_Collection.At(i);
                Modal.StyleChipsGrid.Cell[1,i+1] := StyleChipRecPtr^.StyleChip_Code;
                Modal.StyleChipsGrid.Cell[2,i+1] := StyleChipRecPtr^.StyleChip_Description;
              end;
            end;
            //Clear grid values
            if (Modal.RecordGrid.Rows > 0) then
              Modal.RecordGrid.DeleteRows (1, Modal.RecordGrid.Rows);
            Modal.RecordGrid.StoreData := TRUE;
            Modal.RecordGrid.Cols := 4;
            Modal.RecordGrid.Rows := NumTemplateFields;
            //Populate the grid
            i := 0;
            //Get the game data
            if (GetTemplateInformation(CurrentTemplateID).UsesGameData) then
            begin
              //Query for Game data
              CurrentGameData := GameDataFunctions.GetGameData(CategoryRecPtr^.Category_Sport,
                TRIM(dmMain.SportbaseQuery.FieldByname('GCode').AsString));
            end;
            //Iterate through the fields
            for j := 0 to Template_Fields_Collection.Count-1 do
            begin
              TemplateFieldsRecPtr := Template_Fields_Collection.At(j);
              if (TemplateFieldsRecPtr^.Template_ID = CurrentTemplateID) then
              begin
                Modal.RecordGrid.Cell[1,i+1] := TemplateFieldsRecPtr^.Field_ID; //Index
                Modal.RecordGrid.Cell[2,i+1] := TemplateFieldsRecPtr^.Field_Label; //Description
                Modal.RecordGrid.Cell[4,i+1] := TemplateFieldsRecPtr^.Field_Type;
                //Set color & read only property
                Modal.RecordGrid.CellReadOnly[1,i+1] := roOn;
                Modal.RecordGrid.CellReadOnly[2,i+1] := roOn;
                Modal.RecordGrid.CellReadOnly[4,i+1] := roOn;
                if (TemplateFieldsRecPtr^.Field_Is_Manual = TRUE) then
                begin
                  //Firt check to see if it's a manual game winner indicator or animation
                  if (TemplateFieldsRecPtr^.Field_Type = 5) OR (TemplateFieldsRecPtr^.Field_Type = 6) then
                  begin
                    Modal.RecordGrid.CellControlType[3,i+1] := ctCheck;
                    Modal.RecordGrid.CellCheckBoxState[3,i+1] := cbUnchecked;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Next, check to see if it's a manual next page icon animation
                  else if (TemplateFieldsRecPtr^.Field_Type = 9) then
                  begin
                    Modal.RecordGrid.CellControlType[3,i+1] := ctCheck;
                    Modal.RecordGrid.CellCheckBoxState[3,i+1] := cbUnchecked;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Next, check to see if it's a weather icon; if so, display flipbooks in dropdown
                  else if (TemplateFieldsRecPtr^.Field_Type = 7) then
                  begin
                    Modal.RecordGrid.AssignCellCombo(3,i+1);
                    Modal.RecordGrid.CellButtonType[3,i+1] := btCombo;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                    //Init first combo box entry
                    WeatherIconRecPtr := Weather_Icon_Collection.At(0);
                    Modal.RecordGrid.Cell[3, i+1] := WeatherIconRecPtr^.Icon_Name;
                  end
                  //Next, check to see if it's the manual league entry; if so, display choices in dropdown
                  else if (TemplateFieldsRecPtr^.Field_Type = 8) then
                  begin
                    //Init combo box entry; fixed for breaking news
                    LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                    Modal.RecordGrid.Cell[3,i+1] :=  LeagueCodeRecPtr^.Heading;
                  end
                  //Added for Altitude sports
                  //Next, check to see if it's automatic league entry; if so, use category tab to get league
                  else if (TemplateFieldsRecPtr^.Field_Type = 10) then
                  begin
                    //Init combo box entry; fixed for breaking news
                    CategoryRecPtr := Categories_Collection.At(LeagueTab.TabIndex);
                    Modal.RecordGrid.Cell[3,i+1] :=  CategoryRecPtr^.Category_Label;
                  end
                  //Added for Altitude sports
                  //Next, check for team logo
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1B) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2B) then
                  begin
                    Modal.RecordGrid.CellButtonType[3,i+1] := btNormal;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  else begin
                    Modal.RecordGrid.RowColor[i+1] := clWindow;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end;
                end
                else begin
                  //Set cell value
                  Modal.RecordGrid.Cell[3,i+1] := GetValueOfSymbol(PlaylistSelectTabControl.TabIndex+1,
                    TemplateFieldsRecPtr^.Field_Contents, NOENTRYINDEX, CurrentGameData, 0).SymbolValue;
                  Modal.RecordGrid.RowColor[i+1] := clAqua;
                  Modal.RecordGrid.CellReadOnly[3,i+1] := roOn;
                end;
                //Increment the template fields match counter
                Inc (i);
              end;
            end;
            Modal.TemplateName.Caption := CurrentTemplateDefs.Template_Description;
            //Show the dialog
            Control := Modal.ShowModal;
          finally
            //Set new values if user didn't cancel out
            //Set new values if user didn't cancel out
            if (Control = mrOK) then OKToGo := TRUE
            else OKToGo := FALSE;
            //Set values based on data in grid
            if (Modal.RecordGrid.Rows > 0) then
            begin
              i := 1;
              for j := 1 to Modal.RecordGrid.Rows do
              begin
                if (Modal.RecordGrid.CellReadOnly[3,j] = roOff) then
                begin
                  //Check for visitor manual winner indication record type
                  if (Modal.RecordGrid.Cell[4,i] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,i] = cbChecked) then
                  begin
                    User_Data[24] := 'TRUE';
                    User_Data[49] := 'TRUE';
                  end
                  else if (Modal.RecordGrid.Cell[4,i] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,i] = cbUnChecked) then
                  begin
                    User_Data[24] := 'FALSE';
                    User_Data[49] := 'FALSE';
                  end
                  else if (Modal.RecordGrid.Cell[4,i] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,i] = cbChecked) then
                  begin
                    User_Data[25] := 'TRUE';
                    User_Data[50] := 'TRUE';
                  end
                  else if (Modal.RecordGrid.Cell[4,i] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,i] = cbUnChecked) then
                  begin
                    User_Data[25] := 'FALSE';
                    User_Data[50] := 'FALSE';
                  end
                  //Check for next page animated icon
                  else if (Modal.RecordGrid.Cell[4,i] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,i] = cbChecked) then
                  begin
                    User_Data[23] := 'TRUE';
                    User_Data[48] := 'TRUE';
                  end
                  else if (Modal.RecordGrid.Cell[4,i] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,i] = cbUnChecked) then
                  begin
                    User_Data[23] := 'FALSE';
                    User_Data[48] := 'FALSE';
                  end
                  //Check for image file types
                  else if (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_1A) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_1B) then
                  begin
                    User_Data[21] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    User_Data[46] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    //Alert operator if blank and dump out
                    if (Trim(User_Data[21]) = '') then
                    begin
                      MessageDlg('One or both team logos were not specified - please try adding the page again.', mtError, [mbOK], 0);
                      OkToGo := FALSE;
                    end;
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_2A) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_2B) then
                  begin
                    User_Data[22] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    User_Data[47] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    //Alert operator if blank and dump out
                    if (Trim(User_Data[22]) = '') then
                    begin
                      MessageDlg('One or both team logos were not specified - please try adding the page again.', mtError, [mbOK], 0);
                      OkToGo := FALSE;
                    end;
                    Dec(i);
                  end
                  else begin
                    //Set user value text and increment
                    User_Data[i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    //Set default live mode values - can be edited later
                    User_Data[25+i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                  end;
                  Inc(i);
                end;
              end;
            end;
            //Save current grid row
            SaveRow := PlaylistGrid.CurrentDataRow;
            Modal.Free;
          end;
        end
        else begin
          //Init fields
          for i := 1 to 50 do User_Data[i] := ' ';
        end;
        if (OKToGo) then
        begin
          ////////////////////////////////////////////////////////////////////////
          // THIS BLOCK EXECUTED FOR CATEGORIES THAT ARE AUTOMATED SPORTS AND FOR
          // TEMPLATES THAT REQUIRE GAME DATA
          ////////////////////////////////////////////////////////////////////////
          //Check to see if at least one game in sportbase database
          if (CategoryRecPtr^.Category_Is_Sport = TRUE) AND (dmMain.SportbaseQuery.RecordCount > 0) AND
             (CurrentTemplateDefs.UsesGameData = TRUE) then
          begin
            //Loop through for all selected games
            if (ShowOddsOnly.Checked) then
            begin
              //Go to first record in dataset
              //dmMain.SportbaseQuery3.First;
              dmMain.SportbaseQuery3.Last;
              RecordCount := dmMain.SportbaseQuery3.RecordCount
            end
            else begin
              //Go to first record in dataset
              //dmMain.SportbaseQuery.First;
              dmMain.SportbaseQuery.Last;
              RecordCount := dmMain.SportbaseQuery.RecordCount;
            end;
            for m := 1 to RecordCount do
            begin
              //Check to see if game is selected
              if (ShowOddsOnly.Checked) then
                RowSelected := OddsDBGrid.RowSelected[OddsDBGrid.CurrentDataRow]
              else
                RowSelected := GamesDBGrid.RowSelected[GamesDBGrid.CurrentDataRow];
              if (RowSelected = TRUE) then
              begin
                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR PARENT TEMPLATES - ADDS CHILDREN
                ////////////////////////////////////////////////////////////////////////
                //Check to see if template is a "parent" template; if so, add all child templates
                if (CurrentTemplateDefs.Template_Has_Children) then
                begin
                  if (Child_Template_ID_Collection.Count > 0) then
                  begin
                    for i := Child_Template_ID_Collection.Count-1 downto 0 do
                    begin
                      //Need to go backwards for insert to make it work for record ordering
                      ChildTemplateIDRecPtr := Child_Template_ID_Collection.At(i);
                      //Check for child, and append if match
                      if (ChildTemplateIDRecPtr^.Template_ID = CurrentTemplateID) then
                      begin
                        //Allocate memory for the object
                        Case PlaylistSelectTabControl.TabIndex of
                            //Ticker
                         0: begin
                              //Allocate memory for the object
                              GetMem (TickerRecPtr, SizeOf(TickerRec));
                              With TickerRecPtr^ do
                              begin
                                //Set values for collection record
                                Event_Index := Ticker_Collection.Count+1;
                                CreateGUID(EventGUID);
                                Event_GUID := EventGUID;
                                Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                                Subleague_Mnemonic_Standard := '';
                                Mnemonic_LiveEvent := '';
                                Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                                Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                                //Set league ID
                                if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                                begin
                                  League := CategoryRecPtr^.Category_Sport;
                                  //League := Trim(dmMain.SportbaseQuery.FieldByname('League').AsString);
                                  GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                                  Description := GameDataFunctions.GetGameData(TRIM(League), TRIM(GameID)).Description;
                                end
                                else begin
                                  League := 'NONE';
                                  GameID := '-1';
                                  Description := '';
                                end;
                                if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                                begin
                                  SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                                  SponsorLogo_Dwell := SponsorLogoDwell.Value;
                                end
                                else begin
                                  SponsorLogo_Name := '';
                                  SponsorLogo_Dwell := 0;
                                end;
                                //Set Is Child bit
                                Is_Child := TRUE;
                                //For stats
                                StatStoredProcedure := '';
                                StatType := 0;;
                                StatTeam := '';
                                StatLeague := '';
                                StatRecordNumber := 0;
                                for k := 1 to 50 do UserData[k] := User_Data[k];
                                //Set the start/end enable times to the template defaults
                                StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                                EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                                //DwellTime := EntryDwellTime.Value*1000;
                                DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                                Selected := FALSE;
                                Comments := ' ';
                                //Insert object to collection
                                if (PlaylistGrid.CurrentDataRow > 0) then
                                begin
                                  Ticker_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, TickerRecPtr);
                                end
                                else
                                  Ticker_Collection.Insert(TickerRecPtr);
                                Ticker_Collection.Pack;
                              end;
                            end;
                            //Alerts
                         1: begin
                              //Allocate memory for the object
                              GetMem (BreakingNewsRecPtr, SizeOf(BreakingNewsRec));
                              With BreakingNewsRecPtr^ do
                              begin
                                //Set values for collection record
                                Event_Index := BreakingNews_Collection.Count+1;
                                CreateGUID(EventGUID);
                                Event_GUID := EventGUID;
                                Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                                LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                                League := LeagueCodeRecPtr^.Heading;
                                Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                                Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                                Description := 'BreakingNews';
                                for k := 1 to 2 do UserData[k] := User_Data[k];
                                //Set the start/end enable times to the template defaults
                                StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                                EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                                //DwellTime := EntryDwellTime.Value*1000;
                                DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                                Selected := FALSE;
                                Comments := ' ';
                                //Insert object to collection
                                if (PlaylistGrid.CurrentDataRow > 0) then
                                  BreakingNews_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, BreakingNewsRecPtr)
                                else
                                  BreakingNews_Collection.Insert(BreakingNewsRecPtr);
                                BreakingNews_Collection.Pack;
                              end;
                            end;
                        end;
                      end;
                    end;
                  end;
                end
                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR NON-PARENT TEMPLATES IN SPORTS CATEGORIES
                ////////////////////////////////////////////////////////////////////////
                //Not a parent template, so just append record for selected record type
                else begin
                  Case PlaylistSelectTabControl.TabIndex of
                      //Ticker
                   0: begin
                        //Allocate memory for the object
                        GetMem (TickerRecPtr, SizeOf(TickerRec));
                        With TickerRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := Ticker_Collection.Count+1;
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := EntryEnable.Checked;
                          Template_ID := CurrentTemplateID;
                          Subleague_Mnemonic_Standard := '';
                          Mnemonic_LiveEvent := '';
                          Record_Type := GetRecordTypeFromTemplateID(dmMain.Query2.FieldByName('Template_ID').AsInteger);
                          //Set league ID
                          if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                          begin
                            League := CategoryRecPtr^.Category_Sport;
                            //League := Trim(dmMain.SportbaseQuery.FieldByname('League').AsString);
                            GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                            Description := GameDataFunctions.GetGameData(TRIM(League), TRIM(GameID)).Description;
                          end
                          else begin
                            League := 'NONE';
                            GameID := '-1';
                            Description := '';
                          end;
                          if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                          begin
                            SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                            SponsorLogo_Dwell := SponsorLogoDwell.Value;
                          end
                          else begin
                            SponsorLogo_Name := '';
                            SponsorLogo_Dwell := 0;
                          end;
                          //Clear Is Child bit
                          Is_Child := FALSE;
                          //For stats
                          StatStoredProcedure := '';
                          StatType := 0;;
                          StatTeam := '';
                          StatLeague := '';
                          StatRecordNumber := 0;
                          for k := 1 to 50 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          //DwellTime := EntryDwellTime.Value*1000;
                          DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                          Selected := FALSE;
                          Comments := ' ';
                          //Insert object to collection
                          if (PlaylistGrid.CurrentDataRow > 0) then
                            Ticker_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, TickerRecPtr)
                          else
                            Ticker_Collection.Insert(TickerRecPtr);
                          Ticker_Collection.Pack;
                        end;
                      end;
                      //Alerts
                   1: begin
                        //Allocate memory for the object
                        GetMem (BreakingNewsRecPtr, SizeOf(BreakingNewsRec));
                        With BreakingNewsRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := BreakingNews_Collection.Count+1;
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := EntryEnable.Checked;
                          LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                          League := LeagueCodeRecPtr^.Heading;
                          Template_ID := CurrentTemplateID;
                          Record_Type := GetRecordTypeFromTemplateID(dmMain.Query2.FieldByName('Template_ID').AsInteger);
                          Description := 'BreakingNews';
                          for k := 1 to 2 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          //DwellTime := EntryDwellTime.Value*1000;
                          DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                          Selected := FALSE;
                          Comments := ' ';
                          //Insert object to collection
                          if (PlaylistGrid.CurrentDataRow > 0) then
                            BreakingNews_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, BreakingNewsRecPtr)
                          else
                            BreakingNews_Collection.Insert(BreakingNewsRecPtr);
                          BreakingNews_Collection.Pack;
                        end;
                      end;
                  end;
                end;
              end;
              //Go to next record
             if (ShowOddsOnly.Checked) then
               //dmMain.SportbaseQuery3.Next
               dmMain.SportbaseQuery3.Prior
             else
               //dmMain.SportbaseQuery.Next;
               dmMain.SportbaseQuery.Prior;
            end;
          end
          ////////////////////////////////////////////////////////////////////////
          // THIS BLOCK EXECUTED FOR TRMPLATES THAT DO NOT REQUIRE GAME DATA
          ////////////////////////////////////////////////////////////////////////
          //No games in Sportbase query - if not a game specific record type, just add one record
          else if (CurrentTemplateDefs.UsesGameData = FALSE) then
          begin
            //Allocate memory for the object
            Case PlaylistSelectTabControl.TabIndex of
                 //Ticker
             0: begin
                  GetMem (TickerRecPtr, SizeOf(TickerRec));
                  With TickerRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := Ticker_Collection.Count+1;
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    Subleague_Mnemonic_Standard := '';
                    Mnemonic_LiveEvent := '';
                    Template_ID := CurrentTemplateID;
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Get category & league information
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
                    begin
                      League := CategoryRecPtr^.Category_Sport;
                    end
                    else begin
                      //Check if manual league; if so, first user text field will be league
                      if (CurrentTemplateDefs.ManualLeague) then
                      begin
                        LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                        League := LeagueCodeRecPtr^.Heading;
                      end
                      else begin
                        League := 'NONE';
                      end;
                    end;
                    GameID := '0';
                    Description := '';
                    //Check to see if sponsor logo
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                    begin
                      SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                      SponsorLogo_Dwell := SponsorLogoDwell.Value;
                    end
                    else begin
                      SponsorLogo_Name := '';
                      SponsorLogo_Dwell := 0;
                    end;
                    //Clear Is Child bit
                    Is_Child := FALSE;
                    //For stats
                    StatStoredProcedure := '';
                    StatType := 0;;
                    StatTeam := '';
                    StatLeague := '';
                    StatRecordNumber := 0;
                    for k := 1 to 50 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Selected := FALSE;
                    Comments := ' ';
                    //Insert object to collection
                    if (PlaylistGrid.CurrentDataRow > 0) then
                      Ticker_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, TickerRecPtr)
                    else
                      Ticker_Collection.Insert(TickerRecPtr);
                    Ticker_Collection.Pack;
                  end;
                end;
                 //Alerts
             1: begin
                  GetMem (BreakingNewsRecPtr, SizeOf(BreakingNewsRec));
                  With BreakingNewsRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := BreakingNews_Collection.Count+1;
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                    League := LeagueCodeRecPtr^.Heading;
                    Template_ID := CurrentTemplateID;
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Get category & league information
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    Description := '';
                    for k := 1 to 2 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Selected := FALSE;
                    Comments := ' ';
                    //Insert object to collection
                    if (PlaylistGrid.CurrentDataRow > 0) then
                      BreakingNews_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, BreakingNewsRecPtr)
                    else
                      BreakingNews_Collection.Insert(BreakingNewsRecPtr);
                    BreakingNews_Collection.Pack;
                  end;
                end;
            end; //Single template type (NOT a parent record)
          end;
        end;
      end;
    end;
  end
  //////////////////////////////////////////////////////////////////////////////
  //Execute this block of code if using Default Template list
  //////////////////////////////////////////////////////////////////////////////
  else begin
    OKToGo := TRUE;
    //Save current grid row
    SaveRow := PlaylistGrid.CurrentDataRow;
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //First, check to see if sponsors. If so, show sponsor grid
    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
    begin
      //Get number of games selected
      if (dmMain.SportbaseQuery.RecordCount > 0) then
      begin
        NumGamesSelected := GamesDBGrid.SelectedRows.Count;
        if (NumGamesSelected = 0) then
        begin
          GamesDBGrid.SelectRows(GamesDBGrid.CurrentDataRow, GamesDBGrid.CurrentDataRow, TRUE);
          NumGamesSelected := 1;
        end;
      end
      else begin
        MessageDlg('There are no games available or a game has not been selected. The records ' +
                   'cannot be added to the playlist.', mtWarning, [mbOK], 0);
        NumGamesSelected := 0;
        OKToGo := FALSE;
      end;
    end
    else begin
      NumGamesSelected := 0;
      OKToGo := FALSE;
    end;

    //Proceed if OK
    if (OKToGo) AND (CollectionCount < 1500) AND (Selected_Templates_Collection.Count > 0) then
    begin
      //Go to LAST record in dataset - required to get inserts in correct order
      dmMain.SportbaseQuery.Last;
      RecordCount := dmMain.SportbaseQuery.RecordCount;
      for m := 1 to RecordCount do
      begin
        RowSelected := GamesDBGrid.RowSelected[GamesDBGrid.CurrentDataRow];
        if (RowSelected = TRUE) then
        begin
          //Iterate through all default templates; iterate from bottom up to keep templates in correct
          //order upon insert
          for n := Selected_Templates_Collection.Count-1 downto 0 do
          begin
            SelectedTemplatesRecPtr := Selected_Templates_Collection.At(n);
            //Only proceed if template enabled
            if (SelectedTemplatesRecPtr^.Enabled) then
            begin
              CurrentTemplateID := SelectedTemplatesRecPtr^.Template_ID;
              CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
              ////////////////////////////////////////////////////////////////////////
              // THIS BLOCK EXECUTED FOR CATEGORIES THAT ARE AUTOMATED SPORTS AND FOR
              // TEMPLATES THAT REQUIRE GAME DATA
              ////////////////////////////////////////////////////////////////////////
              if (CurrentTemplateDefs.UsesGameData = TRUE) then
              begin
                //Init fields
                for i := 1 to 50 do User_Data[i] := ' ';
                //CurrentTemplateID := dmMain.Query2.FieldByName('Template_ID').AsInteger;
                NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);

                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR PARENT TEMPLATES - ADDS CHILDREN
                ////////////////////////////////////////////////////////////////////////
                //Check to see if template is a "parent" template; if so, add all child templates
                if (CurrentTemplateDefs.Template_Has_Children) then
                begin
                  if (Child_Template_ID_Collection.Count > 0) then
                  begin
                    //Count DOWN template list - required to get inserts in correct order
                    for i := Child_Template_ID_Collection.Count-1 downto 0 do
                    begin
                      ChildTemplateIDRecPtr := Child_Template_ID_Collection.At(i);
                      //Check for child, and append if match
                      if (ChildTemplateIDRecPtr^.Template_ID = CurrentTemplateID) then
                      begin
                        //Allocate memory for the object
                        GetMem (TickerRecPtr, SizeOf(TickerRec));
                        With TickerRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := Ticker_Collection.Count+1;
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                          Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                          Subleague_Mnemonic_Standard := '';
                          Mnemonic_LiveEvent := '';
                          Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                          //Set league ID
                          if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                          begin
                            League := CategoryRecPtr^.Category_Sport;
                            //League := Trim(dmMain.SportbaseQuery.FieldByname('League').AsString);
                            GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                            Description := GameDataFunctions.GetGameData(TRIM(League), TRIM(GameID)).Description;
                          end
                          else begin
                            League := 'NONE';
                            GameID := '-1';
                            Description := '';
                          end;
                          Selected := FALSE;
                          //Check to see if sponsor logo
                          CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                          if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                          begin
                            SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                            SponsorLogo_Dwell := SponsorLogoDwell.Value;
                          end
                          else begin
                            SponsorLogo_Name := ' ';
                            SponsorLogo_Dwell := 0;
                          end;
                          //Set Is Child bit
                          Is_Child := TRUE;
                          //For stats
                          StatStoredProcedure := ' ';
                          StatType := 0;;
                          StatTeam := ' ';
                          StatLeague := ' ';
                          StatRecordNumber := 0;
                          for k := 1 to 50 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                          Comments := ' ';
                          //Insert object to collection
                          if (PlaylistGrid.CurrentDataRow > 0) then
                          begin
                            Ticker_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, TickerRecPtr);
                          end
                          else
                            Ticker_Collection.Insert(TickerRecPtr);
                          Ticker_Collection.Pack;
                        end;
                      end;
                    end;
                  end;
                end
                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR NON-PARENT TEMPLATES IN SPORTS CATEGORIES
                ////////////////////////////////////////////////////////////////////////
                //Not a parent template, so just append record for selected record type
                else begin
                  //Allocate memory for the object
                  GetMem (TickerRecPtr, SizeOf(TickerRec));
                  With TickerRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := Ticker_Collection.Count+1;
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    Template_ID := CurrentTemplateID;
                    Subleague_Mnemonic_Standard := '';
                    Mnemonic_LiveEvent := '';
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Set league ID
                    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                    begin
                      League := CategoryRecPtr^.Category_Sport;
                      //League := Trim(dmMain.SportbaseQuery.FieldByname('League').AsString);
                      GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                      Description := GameDataFunctions.GetGameData(TRIM(League), TRIM(GameID)).Description;
                    end
                    else begin
                      League := 'NONE';
                      GameID := '-1';
                      Description := '';
                    end;
                    //Check to see if sponsor logo
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                    begin
                      SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                      SponsorLogo_Dwell := SponsorLogoDwell.Value;
                    end
                    else begin
                      SponsorLogo_Name := ' ';
                      SponsorLogo_Dwell := 0;
                    end;
                    //Clear Is Child bit
                    Is_Child := FALSE;
                    Selected := FALSE;
                    //For stats
                    StatStoredProcedure := '';
                    StatType := 0;;
                    StatTeam := ' ';
                    StatLeague := ' ';
                    StatRecordNumber := 0;
                    for k := 1 to 50 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Comments := ' ';
                    //Insert object to collection
                    if (PlaylistGrid.CurrentDataRow > 0) then
                    begin
                      Ticker_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, TickerRecPtr);
                    end
                    else
                      Ticker_Collection.Insert(TickerRecPtr);
                    Ticker_Collection.Pack;
                  end;
                end; //Single template type (NOT a parent record)
              end //End of block for templates that use game data

              //////////////////////////////////////////////////////////////////
              //THIS BLOCK OF CODE FOR TEMPLATES THAT DO NOT REQUIRE GAME DATA
              //////////////////////////////////////////////////////////////////
              else begin
                //Allocate memory for the object
                GetMem (TickerRecPtr, SizeOf(TickerRec));
                With TickerRecPtr^ do
                begin
                  //Set values for collection record
                  Event_Index := Ticker_Collection.Count+1;
                  CreateGUID(EventGUID);
                  Event_GUID := EventGUID;
                  Enabled := EntryEnable.Checked;
                  Template_ID := CurrentTemplateID;
                  Subleague_Mnemonic_Standard := '';
                  Mnemonic_LiveEvent := '';
                  Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                  //Get category & league information
                  CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                  if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
                  begin
                    League := CategoryRecPtr^.Category_Sport;
                  end
                  else begin
                    //Check if manual league; if so, first user text field will be league
                    if (CurrentTemplateDefs.ManualLeague) then
                    begin
                      LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                      League := LeagueCodeRecPtr^.Heading;
                    end
                    else begin
                      League := 'NONE';
                    end;
                  end;
                  GameID := '0';
                  Description := '';
                  //Check to see if sponsor logo
                  if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                  begin
                    SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                    SponsorLogo_Dwell := SponsorLogoDwell.Value;
                  end
                  else begin
                    SponsorLogo_Name := '';
                    SponsorLogo_Dwell := 0;
                  end;
                  //Clear Is Child bit
                  Is_Child := FALSE;
                  //For stats
                  StatStoredProcedure := '';
                  StatType := 0;;
                  StatTeam := '';
                  StatLeague := '';
                  StatRecordNumber := 0;
                  for k := 1 to 50 do UserData[k] := User_Data[k];
                  //Set the start/end enable times to the template defaults
                  StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                  EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                  DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                  Selected := FALSE;
                  Comments := ' ';
                  //Insert object to collection
                  if (PlaylistGrid.CurrentDataRow > 0) then
                  begin
                    Ticker_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, TickerRecPtr);
                  end
                  else
                    Ticker_Collection.Insert(TickerRecPtr);
                  Ticker_Collection.Pack;
                end; //Single template type (NOT a parent record)
              end;
            end;
          end;
        end;
        //Go to PRIOR game - required to get inserts in correct order
        dmMain.SportbaseQuery.Prior;
      end;
    end;
  end;
  //Refresh grid
  RefreshPlaylistGrid;
  //Refresh the fields grid
  RefreshEntryFieldsGrid(TickerDisplayMode);
  //Restore current grid rows
  if (SaveRow < 1) then SaveRow := 1;
  PlaylistGrid.CurrentDataRow := SaveRow;
end;

//Handler for Enable selected playlist records
procedure TMainForm.Enable1Click(Sender: TObject);
var
  TickerRecPtr, TickerRecPtr2: ^TickerRec;
  BreakingNewsRecPtr, BreakingNewsRecPtr2: ^BreakingNewsRec;
  i,j: SmallInt;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        if (Ticker_Collection.Count > 0) then
          for i := 1 to Ticker_Collection.Count do
          begin
            TickerRecPtr := Ticker_Collection.At(i-1);
            if (PlaylistGrid.RowSelected[i] = TRUE) then
              TickerRecPtr^.Enabled := TRUE;
            //Now, perform same operation on all copies of pages
            for j := 1 to Ticker_Collection.Count do
            begin
              TickerRecPtr2 := Ticker_Collection.At(j-1);
              if (IsEqualGUID(TickerRecPtr^.Event_GUID, TickerRecPtr2^.Event_GUID)) then
              begin
                if (PlaylistGrid.RowSelected[i] = TRUE) then
                  TickerRecPtr2^.Enabled := TRUE;
              end;
            end;
          end;
      end;
      //Alerts
   1: begin
        if (BreakingNews_Collection.Count > 0) then
          for i := 1 to BreakingNews_Collection.Count do
          begin
            BreakingNewsRecPtr := BreakingNews_Collection.At(i-1);
            if (PlaylistGrid.RowSelected[i] = TRUE) then
              BreakingNewsRecPtr^.Enabled := TRUE;
            //Now, perform same operation on all copies of pages
            for j := 1 to BreakingNews_Collection.Count do
            begin
              BreakingNewsRecPtr2 := BreakingNews_Collection.At(j-1);
              if (IsEqualGUID(BreakingNewsRecPtr^.Event_GUID, BreakingNewsRecPtr2^.Event_GUID)) then
              begin
                if (PlaylistGrid.RowSelected[i] = TRUE) then
                  BreakingNewsRecPtr2^.Enabled := TRUE;
              end;
            end;
          end;
      end;
  end;
  //Refresh the grid
  RefreshPlaylistGrid;
end;

//Handler for Disable selected playlist records
procedure TMainForm.Disable1Click(Sender: TObject);
var
  TickerRecPtr, TickerRecPtr2: ^TickerRec;
  BreakingNewsRecPtr, BreakingNewsRecPtr2: ^BreakingNewsRec;
  i,j: SmallInt;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        if (Ticker_Collection.Count > 0) then
          for i := 1 to Ticker_Collection.Count do
          begin
            TickerRecPtr := Ticker_Collection.At(i-1);
            if (PlaylistGrid.RowSelected[i] = TRUE) then
              TickerRecPtr^.Enabled := FALSE;
            //Now, perform same operation on all copies of pages
            for j := 1 to Ticker_Collection.Count do
            begin
              TickerRecPtr2 := Ticker_Collection.At(j-1);
              if (IsEqualGUID(TickerRecPtr^.Event_GUID, TickerRecPtr2^.Event_GUID)) then
              begin
                if (PlaylistGrid.RowSelected[i] = TRUE) then
                  TickerRecPtr2^.Enabled := FALSE;
              end;
            end;
          end;
      end;
      //Alerts
   1: begin
        if (BreakingNews_Collection.Count > 0) then
          for i := 1 to BreakingNews_Collection.Count do
          begin
            BreakingNewsRecPtr := BreakingNews_Collection.At(i-1);
            if (PlaylistGrid.RowSelected[i] = TRUE) then
              BreakingNewsRecPtr^.Enabled := FALSE;
            //Now, perform same operation on all copies of pages
            for j := 1 to BreakingNews_Collection.Count do
            begin
              BreakingNewsRecPtr2 := BreakingNews_Collection.At(j-1);
              if (IsEqualGUID(BreakingNewsRecPtr^.Event_GUID, BreakingNewsRecPtr2^.Event_GUID)) then
              begin
                if (PlaylistGrid.RowSelected[i] = TRUE) then
                  BreakingNewsRecPtr2^.Enabled := FALSE;
              end;
            end;
          end;
      end;
  end;
  //Refresh the grid
  RefreshPlaylistGrid;
end;

//Handler for popup menu entry to allow editing of start/end enable times
procedure TMainForm.EditStartEndEnableTimes1Click(Sender: TObject);
var
  i,j,k,m: SmallInt;
  CurrentRow, CurrentTopRow: SmallInt;
  Modal: TEnableDateTimeEditorDlg;
  Control: Word;
  TickerRecPtr, TickerRecPtr2: ^TickerRec;
  BreakingNewsRecPtr, BreakingNewsRecPtr2: ^BreakingNewsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  User_Data: Array[1..50] of String[255];
  CurrentTemplateID: SmallInt;
  NumTemplateFields: SmallInt;
  CollectionCount: SmallInt;
  EventGUID: TGUID;
begin
  Case PlaylistSelectTabControl.TabIndex of
       //Ticker
    0: CollectionCount := Ticker_Collection.Count;
       //Alerts
    1: CollectionCount := BreakingNews_Collection.Count;
  end;
  If ((CollectionCount > 0) AND (PlaylistGrid.Rows >= 0)) then
  begin
    //Get current row
    CurrentRow := PlaylistGrid.CurrentDataRow;
    CurrentTopRow := PlaylistGrid.TopRow;
    //Only proceed if a row was selected
    if (CurrentRow <> -1) then
    begin
      Case PlaylistSelectTabControl.TabIndex of
          //Ticker
       0: begin
            TickerRecPtr := Ticker_Collection.At(CurrentRow-1);
            //Setup and launch editing dialog if applicable
            try
              Modal := TEnableDateTimeEditorDlg.Create(Application);
              Modal.EntryNote.Text := TickerRecPtr^.Comments;
              Modal.EntryStartEnableDate.Date := TickerRecPtr^.StartEnableDateTime;
              Modal.EntryStartEnableTime.Time := TickerRecPtr^.StartEnableDateTime;
              Modal.EntryEndEnableDate.Date := TickerRecPtr^.EndEnableDateTime;
              Modal.EntryEndEnableTime.Time := TickerRecPtr^.EndEnableDateTime;
              //If a sponsor dwell was specified, it's a sponsor logo
              if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
                Modal.EntryDwellTime.Value := Trunc(TickerRecPtr^.SponsorLogo_Dwell)
              else
                Modal.EntryDwellTime.Value := Trunc(TickerRecPtr^.DwellTime/1000);

              //Show the dialog
              Control := Modal.ShowModal;
            finally
              //Set new values if user didn't cancel out
              if (Control = mrOK) then
              begin
                //Edit all matching objects using comparsion of GUIDs
                for j := 0 to Ticker_Collection.Count-1 do
                begin
                  if (PlaylistGrid.RowSelected[j+1] = TRUE) then
                  begin
                    TickerRecPtr := Ticker_Collection.At(j);
                    EventGUID := TickerRecPtr^.Event_GUID;
                    for m := 0 to Ticker_Collection.Count-1 do
                    begin
                      TickerRecPtr2 := Ticker_Collection.At(m);
                      if (IsEqualGUID(TickerRecPtr2^.Event_GUID, EventGUID)) then
                      With TickerRecPtr2^ do
                      begin
                        StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                            (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                        EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                            (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                        if (Modal.NoteChangeEnable.Checked) then
                          Comments := Modal.EntryNote.Text;
                        //If a sponsor dwell was specified, it's a sponsor logo
                        if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
                          SponsorLogo_Dwell := Modal.EntryDwellTime.Value
                        else
                          DwellTime := Modal.EntryDwellTime.Value*1000;
                      end;
                    end;
                  end;
                end;
              end;
              Modal.Free;
            end;
          end;
          //Alerts
       1: begin
            BreakingNewsRecPtr := BreakingNews_Collection.At(CurrentRow-1);
            //Setup and launch editing dialog if applicable
            try
              Modal := TEnableDateTimeEditorDlg.Create(Application);
              Modal.EntryNote.Text := BreakingNewsRecPtr^.Comments;
              Modal.EntryStartEnableDate.Date := BreakingNewsRecPtr^.StartEnableDateTime;
              Modal.EntryStartEnableTime.Time := BreakingNewsRecPtr^.StartEnableDateTime;
              Modal.EntryEndEnableDate.Date := BreakingNewsRecPtr^.EndEnableDateTime;
              Modal.EntryEndEnableTime.Time := BreakingNewsRecPtr^.EndEnableDateTime;
              Modal.EntryDwellTime.Value := Trunc(BreakingNewsRecPtr^.DwellTime/1000);
              //Show the dialog
              Control := Modal.ShowModal;
            finally
              //Set new values if user didn't cancel out
              if (Control = mrOK) then
              begin
                //Edit all matching objects using comparsion of GUIDs
                for j := 0 to BreakingNews_Collection.Count-1 do
                begin
                  if (PlaylistGrid.RowSelected[j+1] = TRUE) then
                  begin
                    BreakingNewsRecPtr := BreakingNews_Collection.At(j);
                    EventGUID := BreakingNewsRecPtr^.Event_GUID;
                    for m := 0 to BreakingNews_Collection.Count-1 do
                    begin
                      BreakingNewsRecPtr2 := BreakingNews_Collection.At(m);
                      if (IsEqualGUID(BreakingNewsRecPtr2^.Event_GUID, EventGUID)) then
                      With BreakingNewsRecPtr2^ do
                      begin
                        StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                            (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                        EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                            (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                        if (Modal.NoteChangeEnable.Checked) then
                          Comments := Modal.EntryNote.Text;
                        //if (Modal.DwellTimeEnable.Checked) then
                        DwellTime := Modal.EntryDwellTime.Value*1000;
                      end;
                    end;
                  end;
                end;
              end;
              Modal.Free;
            end;
          end;
      end;
    end;
    //Refresh grid
    RefreshPlaylistGrid;
    PlaylistGrid.CurrentDataRow := CurrentRow;
    PlaylistGrid.TopRow := CurrentTopRow;
    PlaylistGrid.RowSelected[CurrentRow] := TRUE;
  end;
end;

//Handler for cut playlist entry
procedure TMainForm.Cut1Click(Sender: TObject);
begin
  Cut;
end;
procedure TMainForm.Cut;
begin
  //Copy records to temp collection
  SelectForCopyOrCut;
  //Set flag
  SelectForCut := TRUE;
  //Delete records from main collection
  DeleteGraphicFromZipperPlaylist(FALSE);
end;

//Handler for copy playlist entry
procedure TMainForm.Copy1Click(Sender: TObject);
begin
  Copy;
end;
procedure TMainForm.Copy;
begin
  SelectForCopyOrCut;
  SelectForCut := FALSE;
end;

//General procedure to set the record to be copied or cut
procedure TMainForm.SelectForCopyOrCut;
var
  i,j: SmallInt;
  CutCopyRecordCount: SmallInt;
  SelectedRecordFound: Boolean;
  TickerRecPtr, TempTickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr, TempBreakingNewsRecPtr: ^BreakingNewsRec;
  Save_Cursor: TCursor;
begin
  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor  Screen.Cursor := crHourGlass;
  SelectedRecordFound := FALSE;
  CutCopyRecordCount := 0;
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        if (Ticker_Collection.Count > 0) then
        begin
          //Clear the temp collection
          Temp_Ticker_Collection.Clear;
          Temp_Ticker_Collection.Pack;
          for i := 0 to Ticker_Collection.Count-1 do
          begin
            TickerRecPtr := Ticker_Collection.At(i);
            if (PlaylistGrid.RowSelected[i+1] = TRUE) then
            begin
              //Set selected flag to true
              TickerRecPtr^.Selected := TRUE;
              //Inset record into temporary collection - will be used for copy/paste operation
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //GetMem (TempTickerRecPtr, SizeOf(TickerRec));
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //With TempTickerRecPtr^ do
              With TemporaryTickerDataArray[CutCopyRecordCount+1] do
              begin
                Event_Index := TickerRecPtr^.Event_Index;
                Event_GUID := TickerRecPtr^.Event_GUID;
                Is_Child := TickerRecPtr^.Is_Child;
                Enabled := TickerRecPtr^.Enabled;
                League := TickerRecPtr^.League;
                Subleague_Mnemonic_Standard := TickerRecPtr^.Subleague_Mnemonic_Standard;
                Mnemonic_LiveEvent := TickerRecPtr^.Mnemonic_LiveEvent;
                Template_ID := TickerRecPtr^.Template_ID;
                Record_Type := TickerRecPtr^.Record_Type;
                GameID := TickerRecPtr^.GameID;
                SponsorLogo_Name := TickerRecPtr^.SponsorLogo_Name;
                SponsorLogo_Dwell := TickerRecPtr^.SponsorLogo_Dwell;
                StatStoredProcedure := TickerRecPtr^.StatStoredProcedure;
                StatType := TickerRecPtr^.StatType;
                StatTeam := TickerRecPtr^.StatTeam;
                StatLeague := TickerRecPtr^.StatLeague;
                StatRecordNumber := TickerRecPtr^.StatRecordNumber;
                for j := 1 to 50 do UserData[j] := TickerRecPtr^.UserData[j];
                StartEnableDateTime := TickerRecPtr^.StartEnableDateTime;
                EndEnableDateTime := TickerRecPtr^.EndEnableDateTime;
                Description := TickerRecPtr^.Description;
                Comments := TickerRecPtr^.Comments;
                Selected := TickerRecPtr^.Selected;
                DwellTime := TickerRecPtr^.DwellTime;
              end;
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //Insert the new record into the collection
              //Temp_Ticker_Collection.Insert(TempTickerRecPtr);
              //Temp_Ticker_Collection.Pack;
              Inc(CutCopyRecordCount);
            end
            else TickerRecPtr^.Selected := FALSE;
          end;
          TemporaryTickerDataCount := CutCopyRecordCount;
        end;
      end;
      //Alerts
   1: begin
        if (BreakingNews_Collection.Count > 0) then
        begin
          //Clear the temp collection
          Temp_BreakingNews_Collection.Clear;
          Temp_BreakingNews_Collection.Pack;
          for i := 0 to BreakingNews_Collection.Count-1 do
          begin
            BreakingNewsRecPtr := BreakingNews_Collection.At(i);
            if (PlaylistGrid.RowSelected[i+1] = TRUE) then
            begin
              BreakingNewsRecPtr^.Selected := TRUE;
              //Insert record into temporary collection - will be used for copy/paste operation
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //GetMem (TempBugRecPtr, SizeOf(BugRec));
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //With TempBugRecPtr^ do
              With TemporaryBreakingNewsDataArray[CutCopyRecordCount+1] do
              begin
                Event_Index := BreakingNewsRecPtr^.Event_Index;
                Event_GUID := BreakingNewsRecPtr^.Event_GUID;
                Enabled := BreakingNewsRecPtr^.Enabled;
                Template_ID := BreakingNewsRecPtr^.Template_ID;
                Record_Type := BreakingNewsRecPtr^.Record_Type;
                for j := 1 to 2 do UserData[j] := BreakingNewsRecPtr^.UserData[j];
                StartEnableDateTime := BreakingNewsRecPtr^.StartEnableDateTime;
                EndEnableDateTime := BreakingNewsRecPtr^.EndEnableDateTime;
                Description := BreakingNewsRecPtr^.Description;
                Comments := BreakingNewsRecPtr^.Comments;
                Selected := BreakingNewsRecPtr^.Selected;
                DwellTime := BreakingNewsRecPtr^.DwellTime;
              end;
              //Insert the new record into the collection
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //Insert the new record into the collection
              //Temp_BreakingNews_Collection.Insert(TempBreakingNewsRecPtr);
              //Temp_BreakingNews_Collection.Pack;
              Inc(CutCopyRecordCount);
            end
            else BreakingNewsRecPtr^.Selected := FALSE;
          end;
          TemporaryBreakingNewsDataCount := CutCopyRecordCount;
        end;
      end;
  end;
  //Restore cursor
  Screen.Cursor := Save_Cursor;
end;

////////////////////////////////////////////////////////////////////////////////
//General procedure for moving a record up or down in the playlist
procedure TMainForm.MoveRecord(Direction: SmallInt);
var
  i,j: SmallInt;
  CutCopyRecordCount: SmallInt;
  SelectedRecordFound: Boolean;
  TickerRecPtr, TempTickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr, TempBreakingNewsRecPtr: ^BreakingNewsRec;
  Save_Cursor: TCursor;
  FoundRecord: Boolean;
  SaveGridPosition: SmallInt;
  SaveDataRow: SmallInt;
  SaveTopRow: SmallInt;
begin
  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor
  Screen.Cursor := crHourGlass;
  SelectedRecordFound := FALSE;
  CutCopyRecordCount := 0;
  SaveDataRow := PlaylistGrid.CurrentDataRow;
  SaveTopRow := PlaylistGrid.TopRow;
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        if (Ticker_Collection.Count > 0) then
        begin
          //Clear the temp collection
          for i := 0 to Ticker_Collection.Count-1 do
          begin
            TickerRecPtr := Ticker_Collection.At(i);
            if (PlaylistGrid.CurrentDataRow-1 = i) AND (SelectedRecordFound = FALSE) then
            begin
              //Set selected flag to true
              SelectedRecordFound := TRUE;
              //Allocate memory for the record
              GetMem (TempTickerRecPtr, SizeOf(TickerRec));
              //Copy the data to the temporary array
              With TempTickerRecPtr^ do
              begin
                Event_Index := TickerRecPtr^.Event_Index;
                Event_GUID := TickerRecPtr^.Event_GUID;
                Is_Child := TickerRecPtr^.Is_Child;
                Enabled := TickerRecPtr^.Enabled;
                League := TickerRecPtr^.League;
                Subleague_Mnemonic_Standard := TickerRecPtr^.Subleague_Mnemonic_Standard;
                Mnemonic_LiveEvent := TickerRecPtr^.Mnemonic_LiveEvent;
                Template_ID := TickerRecPtr^.Template_ID;
                Record_Type := TickerRecPtr^.Record_Type;
                GameID := TickerRecPtr^.GameID;
                SponsorLogo_Name := TickerRecPtr^.SponsorLogo_Name;
                SponsorLogo_Dwell := TickerRecPtr^.SponsorLogo_Dwell;
                StatStoredProcedure := TickerRecPtr^.StatStoredProcedure;
                StatType := TickerRecPtr^.StatType;
                StatTeam := TickerRecPtr^.StatTeam;
                StatLeague := TickerRecPtr^.StatLeague;
                StatRecordNumber := TickerRecPtr^.StatRecordNumber;
                for j := 1 to 50 do UserData[j] := TickerRecPtr^.UserData[j];
                StartEnableDateTime := TickerRecPtr^.StartEnableDateTime;
                EndEnableDateTime := TickerRecPtr^.EndEnableDateTime;
                Description := TickerRecPtr^.Description;
                Comments := TickerRecPtr^.Comments;
                Selected := TickerRecPtr^.Selected;
                DwellTime := TickerRecPtr^.DwellTime;
              end;
              //Delete the current record
              Ticker_Collection.AtDelete(i);
              Ticker_Collection.Pack;
              //Insert the record at the next position up in the grid
              if (i > 0) AND (Direction = UP) then
              begin
                Ticker_Collection.AtInsert(i-1, TempTickerRecPtr);
                Dec(SaveDataRow);
                if (SaveTopRow > 1) then Dec(SaveTopRow);
              end
              else if (i = 0) AND (Direction = UP) then
              begin
                Ticker_Collection.AtInsert(0, TempTickerRecPtr);
                SaveDataRow := 1;
                SaveTopRow := 1;
              end
              else if (i < PlaylistGrid.Rows-2) AND (Direction = DOWN) then
              begin
                Ticker_Collection.AtInsert(i+1, TempTickerRecPtr);
                Inc(SaveDataRow);
                if (SaveTopRow < PlaylistGrid.Rows) then Inc(SaveTopRow);
              end
              else if ((i = PlaylistGrid.Rows-2) OR (i = PlaylistGrid.Rows-1)) AND (Direction = DOWN) then
              begin
                Ticker_Collection.Insert(TempTickerRecPtr);
                SaveDataRow := PlaylistGrid.Rows;
              end;
            end;
          end;
        end;
      end;
      //Alerts
   1: begin
        if (BreakingNews_Collection.Count > 0) then
        begin
          //Clear the temp collection
          for i := 0 to BreakingNews_Collection.Count-1 do
          begin
            BreakingNewsRecPtr := BreakingNews_Collection.At(i);
            if (PlaylistGrid.CurrentDataRow-1 = i) AND (SelectedRecordFound = FALSE) then
            begin
              //Set selected flag to true
              SelectedRecordFound := TRUE;
              //Allocate memory for the record
              GetMem (TempBreakingNewsRecPtr, SizeOf(BreakingNewsRec));
              //Copy the data to the temporary array
              With TempBreakingNewsRecPtr^ do
              begin
                Event_Index := BreakingNewsRecPtr^.Event_Index;
                Event_GUID := BreakingNewsRecPtr^.Event_GUID;
                Enabled := BreakingNewsRecPtr^.Enabled;
                Template_ID := BreakingNewsRecPtr^.Template_ID;
                Record_Type := BreakingNewsRecPtr^.Record_Type;
                for j := 1 to 2 do UserData[j] := BreakingNewsRecPtr^.UserData[j];
                StartEnableDateTime := BreakingNewsRecPtr^.StartEnableDateTime;
                EndEnableDateTime := BreakingNewsRecPtr^.EndEnableDateTime;
                Description := BreakingNewsRecPtr^.Description;
                Comments := BreakingNewsRecPtr^.Comments;
                Selected := BreakingNewsRecPtr^.Selected;
                DwellTime := BreakingNewsRecPtr^.DwellTime;
              end;
              //Delete the current record
              BreakingNews_Collection.AtDelete(i);
              BreakingNews_Collection.Pack;
              //Insert the record at the next position up in the grid
              if (i > 0) AND (Direction = UP) then
              begin
                BreakingNews_Collection.AtInsert(i-1, TempBreakingNewsRecPtr);
                Dec(SaveDataRow);
              end
              else if (i = 0) AND (Direction = UP) then
              begin
                BreakingNews_Collection.AtInsert(0, TempBreakingNewsRecPtr);
                SaveDataRow := 1;
              end
              else if (i < PlaylistGrid.Rows-2) AND (Direction = DOWN) then
              begin
                BreakingNews_Collection.AtInsert(i+1, TempBreakingNewsRecPtr);
                Inc(SaveDataRow);
              end
              else if ((i = PlaylistGrid.Rows-2) OR (i = PlaylistGrid.Rows-1)) AND (Direction = DOWN) then
              begin
                BreakingNews_Collection.Insert(TempBreakingNewsRecPtr);
                SaveDataRow := PlaylistGrid.Rows;
              end;
            end;
          end;
        end;
      end;
  end;
  //Refresh the grid
  RefreshPlaylistGrid;
  //Restore cursor & selected record
  Screen.Cursor := Save_Cursor;
  PlaylistGrid.CurrentDataRow := SaveDataRow;
  PlaylistGrid.SelectRows(1, PlaylistGrid.Rows, FALSE);
  PlaylistGrid.SelectRows(PlaylistGrid.CurrentDataRow, PlaylistGrid.CurrentDataRow, TRUE);
  PlaylistGrid.TopRow := SaveTopRow;
end;

////////////////////////////////////////////////////////////////////////////////

//Handler for paste selected record(s) at specified record location (not last)
procedure TMainForm.Paste1Click(Sender: TObject);
begin
  Paste;
end;
procedure TMainForm.Paste;
begin
  //Call paste entries procedure WITHOUT append and one copy
  PasteEntries(FALSE);
end;
//Handler for paste selected record(s) at end of playlist
procedure TMainForm.AppendtoEnd1Click(Sender: TObject);
begin
  PasteEnd;
end;
procedure TMainForm.PasteEnd;
begin
  //Call paste entries procedure WITH append and one copy
  PasteEntries(TRUE);
end;
//Handler for duplicate selected record at specified record location (not last)
procedure TMainForm.Duplicate1Click(Sender: TObject);
begin
  Duplicate;
end;
procedure TMainForm.Duplicate;
var
  DuplicationCount: SmallInt;
  Modal: TDuplicationCountSelectDlg;
  Control: Word;
begin
  try
    Modal := TDuplicationCountSelectDlg.Create(Application);
    Modal.DuplicationCount.Value := LastDuplicationCount;
    Control := Modal.ShowModal;
  finally
    if (Control = mrOK) then
    begin
      DuplicationCount := Modal.DuplicationCount.Value;
      LastDuplicationCount := Modal.DuplicationCount.Value;
      //Call paste entries procedure WITHOUT append and required number of copies
      DuplicateEntries(FALSE, DuplicationCount);
    end;
    Modal.Free;
  end;
end;
//Handler for duplicate selected record at end of playlist
procedure TMainForm.DuplicateandAppendtoEnd1Click(Sender: TObject);
var
  DuplicationCount: SmallInt;
  Modal: TDuplicationCountSelectDlg;
  Control: Word;
begin
  try
    Modal := TDuplicationCountSelectDlg.Create(Application);
    Modal.DuplicationCount.Value := LastDuplicationCount;
    Control := Modal.ShowModal;
  finally
    if (Control = mrOK) then
    begin
      DuplicationCount := Modal.DuplicationCount.Value;
      LastDuplicationCount := Modal.DuplicationCount.Value;
      //Call paste entries procedure WITH append and required number of copies
      DuplicateEntries(TRUE, DuplicationCount);
    end;
    Modal.Free;
  end;
end;

//General procedure for pasting selected records
procedure TMainForm.PasteEntries(AppendToEnd: Boolean);
var
  i,j,k, m: SmallInt;
  InsertPoint: SmallInt;
  ExistingTickerRecPtr, TickerRecPtr, NewTickerRecPtr: ^TickerRec;
  ExistingBreakingNewsRecPtr, BreakingNewsRecPtr, NewBreakingNewsRecPtr: ^BreakingNewsRec;
  EventGUID: TGUID;
  UpdateOnlyMode: Boolean;
  Control: Word;
  CurrentEventGUID: TGUID;
  Save_Cursor: TCursor;
  ArrayIndex: SmallInt;
begin
  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor
  Screen.Cursor := crHourGlass;
  //Init
  UpdateOnlyMode := FALSE;

  //Prompt for data update only mode
  Control := MessageDlg('Do you wish to OVERWRITE the data for matching records (' +
      'Selecting NO will paste new records)?', mtWarning, [mbYes, mbNo], 0);
  if (Control = mrYes) then UpDateOnlyMode := TRUE;

  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        //Set the paste/insertion point
        InsertPoint := PlaylistGrid.CurrentDataRow-1;
        if (InsertPoint < 0) then InsertPoint := 0;
        //Iterate through temporary collection starting at last element and working to
        //top to keep in correct order
        if (TemporaryTickerDataCount > 0) then
        begin
          for i := 0 to TemporaryTickerDataCount-1 do
          begin
            if (AppendToEnd) then
              ArrayIndex := i+1
            else
              ArrayIndex := TemporaryTickerDataCount-i;
            CurrentEventGUID := TemporaryTickerDataArray[ArrayIndex].Event_GUID;
            //Edit all matching objects using comparsion of GUIDs
            for j := 0 to Ticker_Collection.Count-1 do
            begin
              ExistingTickerRecPtr := Ticker_Collection.At(j);
              if (IsEqualGUID(ExistingTickerRecPtr^.Event_GUID, CurrentEventGUID)) then
              begin
                With ExistingTickerRecPtr^ do
                begin
                  //Set values for collection record
                  for m := 1 to 50 do ExistingTickerRecPtr^.UserData[m] := TemporaryTickerDataArray[ArrayIndex].UserData[m];
                  ExistingTickerRecPtr^.StartEnableDateTime := TemporaryTickerDataArray[ArrayIndex].StartEnableDateTime;
                  ExistingTickerRecPtr^.EndEnableDateTime := TemporaryTickerDataArray[ArrayIndex].EndEnableDateTime;
                  ExistingTickerRecPtr^.Enabled := TemporaryTickerDataArray[ArrayIndex].Enabled;
                  ExistingTickerRecPtr^.Description := TemporaryTickerDataArray[ArrayIndex].Description;
                  ExistingTickerRecPtr^.Comments := TemporaryTickerDataArray[ArrayIndex].Comments;
                  ExistingTickerRecPtr^.DwellTime := TemporaryTickerDataArray[ArrayIndex].DwellTime;
                end;
              end;
            end;
            //Append the new record if not data update only mode
            if (UpdateOnlyMode = FALSE) then
            begin
              GetMem (NewTickerRecPtr, SizeOf(TickerRec));
              With NewTickerRecPtr^ do
              begin
                Event_Index := TemporaryTickerDataArray[ArrayIndex].Event_Index;
                Event_GUID := TemporaryTickerDataArray[ArrayIndex].Event_GUID;
                Is_Child := TemporaryTickerDataArray[ArrayIndex].Is_Child;
                Enabled := TemporaryTickerDataArray[ArrayIndex].Enabled;
                League := TemporaryTickerDataArray[ArrayIndex].League;
                Subleague_Mnemonic_Standard := TemporaryTickerDataArray[ArrayIndex].Subleague_Mnemonic_Standard;
                Mnemonic_LiveEvent := TemporaryTickerDataArray[ArrayIndex].Mnemonic_LiveEvent;
                Template_ID := TemporaryTickerDataArray[ArrayIndex].Template_ID;
                Record_Type := TemporaryTickerDataArray[ArrayIndex].Record_Type;
                GameID := TemporaryTickerDataArray[ArrayIndex].GameID;
                SponsorLogo_Name := TemporaryTickerDataArray[ArrayIndex].SponsorLogo_Name;
                SponsorLogo_Dwell := TemporaryTickerDataArray[ArrayIndex].SponsorLogo_Dwell;
                StatStoredProcedure := TemporaryTickerDataArray[ArrayIndex].StatStoredProcedure;
                StatType := TemporaryTickerDataArray[ArrayIndex].StatType;
                StatTeam := TemporaryTickerDataArray[ArrayIndex].StatTeam;
                StatLeague := TemporaryTickerDataArray[ArrayIndex].StatLeague;
                StatRecordNumber := TemporaryTickerDataArray[ArrayIndex].StatRecordNumber;
                for j := 1 to 50 do UserData[j] := TemporaryTickerDataArray[ArrayIndex].UserData[j];
                StartEnableDateTime := TemporaryTickerDataArray[ArrayIndex].StartEnableDateTime;
                EndEnableDateTime := TemporaryTickerDataArray[ArrayIndex].EndEnableDateTime;
                Description := TemporaryTickerDataArray[ArrayIndex].Description;
                Comments := TemporaryTickerDataArray[ArrayIndex].Comments;
                Selected := TemporaryTickerDataArray[ArrayIndex].Selected;
                DwellTime := TemporaryTickerDataArray[ArrayIndex].DwellTime;
              end;
              //Insert the new record into the collection
              if (AppendToEnd) then
                Ticker_Collection.Insert(NewTickerRecPtr)
              else
                Ticker_Collection.AtInsert(InsertPoint, NewTickerRecPtr);
            end;
          end;
        end;
        RefreshPlaylistGrid;
      end;
      //Alerts
   1: begin
        //Set the paste/insertion point
        InsertPoint := PlaylistGrid.CurrentDataRow-1;
        if (InsertPoint < 0) then InsertPoint := 0;
        //Iterate through temporary collection starting at last element and working to
        //top to keep in correct order
        if (TemporaryBreakingNewsDataCount > 0) then
        begin
          for i := 0 to TemporaryBreakingNewsDataCount-1 do
          begin
            if (AppendToEnd) then
              ArrayIndex := i+1
            else
              ArrayIndex := TemporaryBreakingNewsDataCount-i;
            //Append the new record
            GetMem (NewBreakingNewsRecPtr, SizeOf(BreakingNewsRec));
            With NewBreakingNewsRecPtr^ do
            begin
              Event_Index := TemporaryBreakingNewsDataArray[ArrayIndex].Event_Index;
              Event_GUID := TemporaryBreakingNewsDataArray[ArrayIndex].Event_GUID;
              Enabled := TemporaryBreakingNewsDataArray[ArrayIndex].Enabled;
              Template_ID := TemporaryBreakingNewsDataArray[ArrayIndex].Template_ID;
              Record_Type := TemporaryBreakingNewsDataArray[ArrayIndex].Record_Type;
              for j := 1 to 2 do UserData[j] := TemporaryBreakingNewsDataArray[ArrayIndex].UserData[j];
              StartEnableDateTime := TemporaryBreakingNewsDataArray[ArrayIndex].StartEnableDateTime;
              EndEnableDateTime := TemporaryBreakingNewsDataArray[ArrayIndex].EndEnableDateTime;
              Description := TemporaryBreakingNewsDataArray[ArrayIndex].Description;
              Comments := TemporaryBreakingNewsDataArray[ArrayIndex].Comments;
              Selected := TemporaryBreakingNewsDataArray[ArrayIndex].Selected;
              DwellTime := TemporaryBreakingNewsDataArray[ArrayIndex].DwellTime;
            end;
            //Insert the new record into the collection
            if (AppendToEnd) then
              BreakingNews_Collection.Insert(NewBreakingNewsRecPtr)
            else
              BreakingNews_Collection.AtInsert(InsertPoint, NewBreakingNewsRecPtr)
          end;
        end;
        RefreshPlaylistGrid;
      end;
  end;
  //Restore cursor
  Screen.Cursor := Save_Cursor;
end;

//General procedure for pasting selected records
procedure TMainForm.DuplicateEntries(AppendToEnd: Boolean; DuplicationCount: SmallInt);
var
  i,j,k, m: SmallInt;
  InsertPoint: SmallInt;
  ExistingTickerRecPtr, TickerRecPtr, NewTickerRecPtr: ^TickerRec;
  ExistingBreakingNewsRecPtr, BreakingNewsRecPtr, NewBreakingNewsRecPtr: ^BreakingNewsRec;
  EventGUID: TGUID;
  UpdateOnlyMode: Boolean;
  Control: Word;
  CurrentEventGUID: TGUID;
  Save_Cursor: TCursor;
  ArrayIndex: SmallInt;
  SaveCurrentDataRow, SaveTopRow: SmallInt;
begin
  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor
  Screen.Cursor := crHourGlass;
  //Init
  UpdateOnlyMode := FALSE;
  //Save grid position
  SaveCurrentDataRow := PlaylistGrid.CurrentDataRow;
  SaveTopRow := PlaylistGrid.TopRow;

  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        //Set the paste/insertion point
        InsertPoint := PlaylistGrid.CurrentDataRow-1;
        if (InsertPoint < 0) then InsertPoint := 0;
        //Iterate through temporary collection starting at last element and working to
        //top to keep in correct order
        TickerRecPtr := Ticker_Collection.At(InsertPoint);
        for k := 1 to DuplicationCount do
        begin
          GetMem (NewTickerRecPtr, SizeOf(TickerRec));
          With NewTickerRecPtr^ do
          begin
            Event_Index := TickerRecPtr.Event_Index;
            CreateGUID(EventGUID);
            Event_GUID := EventGUID;
            Is_Child := TickerRecPtr.Is_Child;
            Enabled := TickerRecPtr.Enabled;
            League := TickerRecPtr.League;
            Subleague_Mnemonic_Standard := TickerRecPtr^.Subleague_Mnemonic_Standard;
            Mnemonic_LiveEvent := TickerRecPtr^.Mnemonic_LiveEvent;
            Template_ID := TickerRecPtr.Template_ID;
            Record_Type := TickerRecPtr.Record_Type;
            GameID := TickerRecPtr^.GameID;
            SponsorLogo_Name := TickerRecPtr.SponsorLogo_Name;
            SponsorLogo_Dwell := TickerRecPtr.SponsorLogo_Dwell;
            StatStoredProcedure := TickerRecPtr.StatStoredProcedure;
            StatType := TickerRecPtr.StatType;
            StatTeam := TickerRecPtr.StatTeam;
            StatLeague := TickerRecPtr.StatLeague;
            StatRecordNumber := TickerRecPtr.StatRecordNumber;
            for j := 1 to 50 do UserData[j] := TickerRecPtr.UserData[j];
            StartEnableDateTime := TickerRecPtr.StartEnableDateTime;
            EndEnableDateTime := TickerRecPtr.EndEnableDateTime;
            Description := TickerRecPtr^.Description;
            Comments := TickerRecPtr.Comments;
            Selected := TickerRecPtr.Selected;
            DwellTime := TickerRecPtr.DwellTime;
          end;
          //Insert the new record into the collection
          if (AppendToEnd) then
            Ticker_Collection.Insert(NewTickerRecPtr)
          else
            Ticker_Collection.AtInsert(InsertPoint, NewTickerRecPtr);
        end;
        RefreshPlaylistGrid;
      end;
      //Alerts
   1: begin
        //Set the paste/insertion point
        InsertPoint := PlaylistGrid.CurrentDataRow-1;
        if (InsertPoint < 0) then InsertPoint := 0;
        //Iterate through temporary collection starting at last element and working to
        //top to keep in correct order
        BreakingNewsRecPtr := BreakingNews_Collection.At(InsertPoint);
        for k := 1 to DuplicationCount do
        begin
          GetMem (NewBreakingNewsRecPtr, SizeOf(BreakingNewsRec));
          With NewBreakingNewsRecPtr^ do
          begin
            Event_Index := BreakingNewsRecPtr.Event_Index;
            CreateGUID(EventGUID);
            Event_GUID := EventGUID;
            Enabled := BreakingNewsRecPtr.Enabled;
            Template_ID := BreakingNewsRecPtr.Template_ID;
            Record_Type := BreakingNewsRecPtr.Record_Type;
            for j := 1 to 2 do UserData[j] := BreakingNewsRecPtr.UserData[j];
            StartEnableDateTime := BreakingNewsRecPtr.StartEnableDateTime;
            EndEnableDateTime := BreakingNewsRecPtr.EndEnableDateTime;
            Description := BreakingNewsRecPtr.Description;
            Comments := BreakingNewsRecPtr.Comments;
            Selected := BreakingNewsRecPtr.Selected;
            DwellTime := BreakingNewsRecPtr.DwellTime;
          end;
          //Insert the new record into the collection
          if (AppendToEnd) then
            BreakingNews_Collection.Insert(NewBreakingNewsRecPtr)
          else
            BreakingNews_Collection.AtInsert(InsertPoint, NewBreakingNewsRecPtr);
        end;
        RefreshPlaylistGrid;
      end;
  end;
  //Set current row to bottom duplication row
  PlaylistGrid.CurrentDataRow := SaveCurrentDataRow + DuplicationCount;
  PlaylistGrid.TopRow := SaveTopRow + DuplicationCount;
  PlaylistGrid.RowSelected[SaveCurrentDataRow] := FALSE;
  PlaylistGrid.RowSelected[SaveCurrentDataRow+DuplicationCount] := TRUE;
  //Restore cursor
  Screen.Cursor := Save_Cursor;
end;

//Handler for delete element from zipper playlist button
procedure TMainForm.BitBtn5Click(Sender: TObject);
begin
  DeleteGraphicFromZipperPlaylist(TRUE);
end;
//Handler for delete from pop-up menu
procedure TMainForm.Delete1Click(Sender: TObject);
begin
  DeleteGraphicFromZipperPlaylist(TRUE);
end;
//General procedure to delete an element from the zipper playlist
procedure TMainForm.DeleteGraphicFromZipperPlaylist(Confirm: Boolean);
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  CollectionCount: SmallInt;
  Control: Word;
begin
  //Alert operator
  if (Confirm) then
    Control := MessageDlg('Are you sure you want to delete the selected records?', mtConfirmation, [mbYes, mbNo], 0)
  else
    Control := mrYes;
  if (Control = mrYes) then
  begin
    Case PlaylistSelectTabControl.TabIndex of
         //Ticker
      0: CollectionCount := Ticker_Collection.Count;
         //Alerts
      1: CollectionCount := BreakingNews_Collection.Count;
    end;
    If ((CollectionCount > 0) AND (PlaylistGrid.Rows >= 0)) then
    begin
      //Point to current record & get game id
      //Delete the item from the stack
      Case PlaylistSelectTabControl.TabIndex of
          //Ticker
       0: begin
            for i := 0 to Ticker_Collection.Count-1 do
            begin
              TickerRecPtr := Ticker_Collection.At(i);
              if (PlaylistGrid.RowSelected[i+1] = TRUE) then TickerRecPtr^.Selected := TRUE
              else TickerRecPtr^.Selected := FALSE;
            end;
            i := 0;
            Repeat
              TickerRecPtr := Ticker_Collection.At(i);
              if (TickerRecPtr^.Selected = TRUE) then
              begin
                Ticker_Collection.AtDelete(i);
                Ticker_Collection.Pack;
              end
              else Inc(i);
            Until (i = Ticker_Collection.Count);
          end;
          //Alerts
       1: begin
            for i := 0 to BreakingNews_Collection.Count-1 do
            begin
              BreakingNewsRecPtr := BreakingNews_Collection.At(i);
              if (PlaylistGrid.RowSelected[i+1] = TRUE) then BreakingNewsRecPtr^.Selected := TRUE
              else BreakingNewsRecPtr^.Selected := FALSE;
            end;
            i := 0;
            Repeat
              BreakingNewsRecPtr := BreakingNews_Collection.At(i);
              if (BreakingNewsRecPtr^.Selected = TRUE) then
              begin
                BreakingNews_Collection.AtDelete(i);
                BreakingNews_Collection.Pack;
              end
              else Inc(i);
            Until (i = BreakingNews_Collection.Count);
          end;
      end;
      //Refresh the grid
      RefreshPlaylistGrid;
    end;
  end;
end;

//Handler for edit current zipper entry button
procedure TMainForm.BitBtn3Click(Sender: TObject);
begin
  EditZipperPlaylistEntry;
  //Preview the edited record
  PreviewCurrentPlaylistRecord;
end;
//Handler for double-click on zipper entry in grid - edit entry
procedure TMainForm.PlaylistGridDblClick(Sender: TObject);
begin
  EditZipperPlaylistEntry;
  //Preview the edited record
  PreviewCurrentPlaylistRecord;
end;

//Procedure to launch dialog for editing currently selected zipper entry
procedure TMainForm.EditZipperPlaylistEntry;
var
  i,j,k: SmallInt;
  CurrentRow: SmallInt;
  Modal: TZipperEntryEditorDlg;
  Control: Word;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  User_Data: Array[1..50] of String[255];
  CurrentTemplateID: SmallInt;
  NumTemplateFields: SmallInt;
  CollectionCount: SmallInt;
  CurrentEventGUID: TGUID;
  CurrentGameData: GameRec;
  StyleChipRecPtr: ^StyleChipRec;
  CurrentTemplateDefs: TemplateDefsRec;
  SaveTopRow: SmallInt;
begin
  Case PlaylistSelectTabControl.TabIndex of
       //Ticker
    0: CollectionCount := Ticker_Collection.Count;
       //Alerts
    1: CollectionCount := BreakingNews_Collection.Count;
  end;

  //Init fields
  for i := 1 to 50 do User_Data[i] := '';

  If ((CollectionCount > 0) AND (PlaylistGrid.Rows >= 0)) then
  begin
    //Get current row
    CurrentRow := PlaylistGrid.CurrentDataRow;
    SaveTopRow := PlaylistGrid.TopRow;
    //Only proceed if a row was selected
    if (CurrentRow <> -1) then
    begin
      Case PlaylistSelectTabControl.TabIndex of
          //Ticker
       0: begin
            TickerRecPtr := Ticker_Collection.At(CurrentRow-1);
            CurrentEventGUID := TickerRecPtr^.Event_GUID;
            //Setup and launch editing dialog if applicable
            try
              Modal := TZipperEntryEditorDlg.Create(Application);
              //Add style chips
              if (StyleChip_Collection.Count > 0) then
              begin
                Modal.StyleChipsGrid.StoreData := TRUE;
                Modal.StyleChipsGrid.Cols := 2;
                Modal.StyleChipsGrid.Rows := StyleChip_Collection.Count;
                for i := 0 to StyleChip_Collection.Count-1 do
                begin
                  StyleChipRecPtr := StyleChip_Collection.At(i);
                  Modal.StyleChipsGrid.Cell[1,i+1] := StyleChipRecPtr^.StyleChip_Code;
                  Modal.StyleChipsGrid.Cell[2,i+1] := StyleChipRecPtr^.StyleChip_Description;
                end;
              end;
              Modal.EntryEnable.Checked := TickerRecPtr^.Enabled;
              Modal.EntryNote.Text := TickerRecPtr^.Comments;
              Modal.EntryStartEnableDate.Date := TickerRecPtr^.StartEnableDateTime;
              Modal.EntryStartEnableTime.Time := TickerRecPtr^.StartEnableDateTime;
              Modal.EntryEndEnableDate.Date := TickerRecPtr^.EndEnableDateTime;
              Modal.EntryEndEnableTime.Time := TickerRecPtr^.EndEnableDateTime;
              //If a sponsor dwell was specified, it's a sponsor logo
              if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
                Modal.EntryDwellTime.Value := Trunc(TickerRecPtr^.SponsorLogo_Dwell)
              else
                Modal.EntryDwellTime.Value := Trunc(TickerRecPtr^.DwellTime/1000);
              Modal.BitBtn4.Caption := 'Replace Existing Entry';
              //Set data mode label
              //Check for 2-line mode
              if (TickerDisplayMode = 3) OR (TickerDisplayMode = 4) then
                Modal.DataModeLabel.Caption := '2-LINE MODE'
              //1-line mode
              else if (TickerDisplayMode = 1) OR (TickerDisplayMode = 2) then
                Modal.DataModeLabel.Caption := '1-LINE MODE';
              //Set initial values for grid
              CurrentTemplateID := TickerRecPtr^.Template_ID;
              NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);
              CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
              Modal.TemplateName.Caption := CurrentTemplateDefs.Template_Description;
              if (NumTemplateFields > 0) then
              begin
                //Clear grid values
                if (Modal.RecordGrid.Rows > 0) then
                  Modal.RecordGrid.DeleteRows (1, Modal.RecordGrid.Rows);
                Modal.RecordGrid.StoreData := TRUE;
                Modal.RecordGrid.Cols := 4;
                Modal.RecordGrid.Rows := NumTemplateFields;
                Modal.HasWeather := FALSE;
                Modal.HasManualLeague := FALSE;
                //Populate the grid
                i := 1; k := 1;
                //Check if manual league; if not, get the game data
                if (CurrentTemplateDefs.ManualLeague = FALSE) then
                begin
                  CurrentGameData := GameDataFunctions.GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
                end;
                for j := 0 to Template_Fields_Collection.Count-1 do
                begin
                  TemplateFieldsRecPtr := Template_Fields_Collection.At(j);
                  if (TemplateFieldsRecPtr^.Template_ID = CurrentTemplateID) then
                  begin
                    Modal.RecordGrid.Cell[1,i] := TemplateFieldsRecPtr^.Field_ID; //Index
                    Modal.RecordGrid.Cell[2,i] := TemplateFieldsRecPtr^.Field_Label; //Description
                    Modal.RecordGrid.Cell[4,i] := TemplateFieldsRecPtr^.Field_Type;
                    //Set color & read only property
                    Modal.RecordGrid.CellReadOnly[1,i] := roOn;
                    Modal.RecordGrid.CellReadOnly[2,i] := roOn;
                    Modal.RecordGrid.CellReadOnly[4,i] := roOn;
                    if (TemplateFieldsRecPtr^.Field_Is_Manual = TRUE) then
                    begin
                      //Firt check to see if it's a manual game winner indicator or animation
                      if (TemplateFieldsRecPtr^.Field_Type = 5) then
                      begin
                        Modal.RecordGrid.CellControlType[3,i] := ctCheck;
                        if (TickerRecPtr^.UserData[24] = 'TRUE') then
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbChecked
                        else
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbUnchecked;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      else if (TemplateFieldsRecPtr^.Field_Type = 6) then
                      begin
                        Modal.RecordGrid.CellControlType[3,i] := ctCheck;
                        if (TickerRecPtr^.UserData[25] = 'TRUE') then
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbChecked
                        else
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbUnchecked;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Next, check to see if it's a manual next page icon animation
                      else if (TemplateFieldsRecPtr^.Field_Type = 9) then
                      begin
                        Modal.RecordGrid.CellControlType[3,i] := ctCheck;
                        if (TickerRecPtr^.UserData[23] = 'TRUE') then
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbChecked
                        else
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbUnchecked;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Next, check to see if it's a weather icon; if so, display flipbooks in dropdown
                      else if (TemplateFieldsRecPtr^.Field_Type = 7) then
                      begin
                        Modal.RecordGrid.AssignCellCombo(3,i);
                        Modal.RecordGrid.CellButtonType[3,i] := btCombo;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Init first combo box entry
                        Modal.RecordGrid.Cell[3,i] := TickerRecPtr^.UserData[k];
                        //Set flag
                        Modal.HasWeather := TRUE;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Next, check to see if it's the manual league entry; if so, display choices in dropdown
                      else if (TemplateFieldsRecPtr^.Field_Type = 8) then
                      begin
                        Modal.RecordGrid.AssignCellCombo(3,i);
                        Modal.RecordGrid.CellButtonType[3,i] := btCombo;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Init combo box entry
                        //Modal.RecordGrid.Cell[3,i] := TickerRecPtr^.League + '/' +
                        //  TickerRecPtr^.Subleague_Mnemonic_Standard;
                        Modal.RecordGrid.Cell[3,i] := TickerRecPtr^.League;
                        //Set flag
                        Modal.HasManualLeague := TRUE;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Added for Altitude sports
                      //Next, check to see if it's automatic league entry; if so, use category tab to get league
                      else if (TemplateFieldsRecPtr^.Field_Type = 10) then
                      begin
                        //Init combo box entry; fixed for breaking news
                        Modal.RecordGrid.Cell[3,i] := TickerRecPtr^.League;
                        //Set flag
                        Modal.HasManualLeague := FALSE;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Added for Altitude sports
                      //Next, check for team logo
                      else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1A) or
                        (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1B) then
                      begin
                        Modal.RecordGrid.CellButtonType[3,i] := btNormal;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        Modal.RecordGrid.Cell[3,i] := Trim(TickerRecPtr^.UserData[21]);
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Next, check for team logo
                      else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2A) or
                        (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2B) then
                      begin
                        Modal.RecordGrid.CellButtonType[3,i] := btNormal;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        Modal.RecordGrid.Cell[3,i] := Trim(TickerRecPtr^.UserData[22]);
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      else begin
                        Modal.RecordGrid.RowColor[i] := clWindow;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        if (AutoTrimText) then
                          Modal.RecordGrid.Cell[3,i] := Trim(TickerRecPtr^.UserData[k])
                        else
                          Modal.RecordGrid.Cell[3,i] := TickerRecPtr^.UserData[k];
                      end;
                      Inc(k);
                    end
                    else begin
                      //Set cell value
                      Modal.RecordGrid.Cell[3,i] := GetValueOfSymbol(PlaylistSelectTabControl.TabIndex+1,
                        TemplateFieldsRecPtr^.Field_Contents, CurrentRow-1, CurrentGameData, TickerDisplayMode).SymbolValue;
                      Modal.RecordGrid.RowColor[i] := clAqua;
                      Modal.RecordGrid.CellReadOnly[3,i] := roOn;
                    end;
                    //Increment the template fields match counter
                    Inc (i);
                  end;
                end;
              end;
              //Show the dialog
              Control := Modal.ShowModal;
            finally
              //Set new values if user didn't cancel out
              if (Control = mrOK) then
              begin
                //Set values based on data in grid
                if (Modal.RecordGrid.Rows > 0) then
                begin
                  i := 1;
                  for j := 1 to Modal.RecordGrid.Rows do
                  begin
                    if (Modal.RecordGrid.CellReadOnly[3,j] = roOff) then
                    begin
                      //Check for visitor manual winner indication record type
                      if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                      begin
                        User_Data[24] := 'TRUE';
                        User_Data[49] := 'TRUE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                      begin
                        User_Data[24] := 'FALSE';
                        User_Data[49] := 'FALSE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                      begin
                        User_Data[25] := 'TRUE';
                        User_Data[50] := 'TRUE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                      begin
                        User_Data[25] := 'FALSE';
                        User_Data[50] := 'FALSE';
                        Dec(i);
                      end
                      //Check for next page icon
                      else if (Modal.RecordGrid.Cell[4,j] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                      begin
                        User_Data[23] := 'TRUE';
                        User_Data[48] := 'TRUE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                      begin
                        User_Data[23] := 'FALSE';
                        User_Data[48] := 'FALSE';
                        Dec(i);
                      end
                      //Check for manual league
                      else if (Modal.RecordGrid.Cell[4,j] = 8) then
                      begin
                        //If User changed the entry via dropdown, get the new values
                        if (Modal.League <> '') then
                        begin
                          TickerRecPtr^.League := Modal.League;
                        end;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(i);
                      end
                      //Check for image file types
                      else if (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_1A) or
                              (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_1B) then
                      begin
                        User_Data[21] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                        User_Data[46] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_2A) or
                              (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_2B) then
                      begin
                        User_Data[22] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                        User_Data[47] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                        Dec(i);
                      end
                      else begin
                        //Standard mode, so change all text values
                        //Set user value text and increment
                        User_Data[i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                        User_Data[25+i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                      end;
                      Inc(i);
                    end;
                  end;
                end;
                //Set other values
                TickerRecPtr^.StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                    (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                TickerRecPtr^.EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                    (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                TickerRecPtr^.Enabled := Modal.EntryEnable.Checked;
                //If a sponsor logo dwell was specified, it's a sponsor logo
                if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
                  TickerRecPtr^.SponsorLogo_Dwell := Modal.EntryDwellTime.Value
                else
                  TickerRecPtr^.DwellTime := Modal.EntryDwellTime.Value*1000;
                TickerRecPtr^.Comments := Modal.EntryNote.Text;
                //Edit all matching objects using comparsion of GUIDs
                for j := 0 to Ticker_Collection.Count-1 do
                begin
                  TickerRecPtr := Ticker_Collection.At(j);
                  if (IsEqualGUID(TickerRecPtr^.Event_GUID, CurrentEventGUID)) then
                  begin
                    With TickerRecPtr^ do
                    begin
                      //Set values for collection record
                      for i := 1 to 50 do UserData[i] := User_Data[i];
                      TickerRecPtr^.StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                          (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                      TickerRecPtr^.EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                          (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                      TickerRecPtr^.Enabled := Modal.EntryEnable.Checked;
                      TickerRecPtr^.DwellTime := Modal.EntryDwellTime.Value*1000;
                      TickerRecPtr^.Comments := Modal.EntryNote.Text;
                      //Re-eanble to change league for all matching GUIDs
                      if (Modal.League <> '') then
                      begin
                        TickerRecPtr^.League := Modal.League;
                      end;
                    end;
                  end;
                end;
              end;
              Modal.Free;
              //Refresh grid
              RefreshPlaylistGrid;
              //Refresh the fields grid
              RefreshEntryFieldsGrid(TickerDisplayMode);
            end;
          end;
          //Alerts
       1: begin
            BreakingNewsRecPtr := BreakingNews_Collection.At(CurrentRow-1);
            CurrentEventGUID := BreakingNewsRecPtr^.Event_GUID;
            //Setup and launch editing dialog if applicable
            try
              Modal := TZipperEntryEditorDlg.Create(Application);
              //Add style chips
              if (StyleChip_Collection.Count > 0) then
              begin
                Modal.StyleChipsGrid.StoreData := TRUE;
                Modal.StyleChipsGrid.Cols := 2;
                Modal.StyleChipsGrid.Rows := StyleChip_Collection.Count;
                for i := 0 to StyleChip_Collection.Count-1 do
                begin
                  StyleChipRecPtr := StyleChip_Collection.At(i);
                  Modal.StyleChipsGrid.Cell[1,i+1] := StyleChipRecPtr^.StyleChip_Code;
                  Modal.StyleChipsGrid.Cell[2,i+1] := StyleChipRecPtr^.StyleChip_Description;
                end;
              end;
              Modal.EntryEnable.Checked := BreakingNewsRecPtr^.Enabled;
              Modal.EntryNote.Text := BreakingNewsRecPtr^.Comments;
              Modal.EntryStartEnableDate.Date := BreakingNewsRecPtr^.StartEnableDateTime;
              Modal.EntryStartEnableTime.Time := BreakingNewsRecPtr^.StartEnableDateTime;
              Modal.EntryEndEnableDate.Date := BreakingNewsRecPtr^.EndEnableDateTime;
              Modal.EntryEndEnableTime.Time := BreakingNewsRecPtr^.EndEnableDateTime;
              Modal.EntryDwellTime.Value := Trunc(BreakingNewsRecPtr^.DwellTime/1000);
              Modal.BitBtn4.Caption := 'Replace Existing Entry';
              //Set initial values for grid
              CurrentTemplateID := BreakingNewsRecPtr^.Template_ID;
              NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);
              CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
              Modal.TemplateName.Caption := CurrentTemplateDefs.Template_Description;
              if (NumTemplateFields > 0) then
              begin
                //Clear grid values
                if (Modal.RecordGrid.Rows > 0) then
                  Modal.RecordGrid.DeleteRows (1, Modal.RecordGrid.Rows);
                Modal.RecordGrid.StoreData := TRUE;
                Modal.RecordGrid.Cols := 4;
                Modal.RecordGrid.Rows := NumTemplateFields;
                //Populate the grid
                i := 1; k := 1;
                for j := 0 to Template_Fields_Collection.Count-1 do
                begin
                  TemplateFieldsRecPtr := Template_Fields_Collection.At(j);
                  if (TemplateFieldsRecPtr^.Template_ID = CurrentTemplateID) then
                  begin
                    Modal.RecordGrid.Cell[1,i] := TemplateFieldsRecPtr^.Field_ID; //Index
                    Modal.RecordGrid.Cell[2,i] := TemplateFieldsRecPtr^.Field_Label; //Description
                    Modal.RecordGrid.Cell[4,i] := TemplateFieldsRecPtr^.Field_Type;
                    //Set color & read only property
                    Modal.RecordGrid.CellReadOnly[1,i] := roOn;
                    Modal.RecordGrid.CellReadOnly[2,i] := roOn;
                    Modal.RecordGrid.CellReadOnly[4,i] := roOn;
                    if (TemplateFieldsRecPtr^.Field_Is_Manual = TRUE) then
                    begin
                      //Check to see if it's the manual league entry; if so, display choices in dropdown
                      if (TemplateFieldsRecPtr^.Field_Type = 8) then
                      begin
                        Modal.RecordGrid.AssignCellCombo(3,i);
                        Modal.RecordGrid.CellButtonType[3,i] := btCombo;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Init combo box entry
                        //Modal.RecordGrid.Cell[3,i] := TickerRecPtr^.League + '/' +
                        //  TickerRecPtr^.Subleague_Mnemonic_Standard;
                        Modal.RecordGrid.Cell[3,i] := BreakingNewsRecPtr^.League;
                        //Set flag
                        Modal.HasManualLeague := TRUE;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      else begin
                        Modal.RecordGrid.RowColor[i] := clWindow;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        if (AutoTrimText) then
                          Modal.RecordGrid.Cell[3,i] := Trim(BreakingNewsRecPtr^.UserData[k])
                        else
                          Modal.RecordGrid.Cell[3,i] := BreakingNewsRecPtr^.UserData[k];
                      end;
                      Inc(k);
                    end
                    else begin
                      //Set cell value
                      Modal.RecordGrid.Cell[3,i] := GetValueOfSymbol(PlaylistSelectTabControl.TabIndex+1,
                        TemplateFieldsRecPtr^.Field_Contents, CurrentRow-1, CurrentGameData, 0).SymbolValue;
                      Modal.RecordGrid.RowColor[i] := clAqua;
                      Modal.RecordGrid.CellReadOnly[3,i] := roOn;
                    end;
                    //Increment the template fields match counter
                    Inc (i);
                  end;
                end;
              end;
              //Show the dialog
              Control := Modal.ShowModal;
            finally
              //Set new values if user didn't cancel out
              if (Control = mrOK) then
              begin
                //Set values based on data in grid
                if (Modal.RecordGrid.Rows > 0) then
                begin
                  i := 1;
                  for j := 1 to Modal.RecordGrid.Rows do
                  begin
                    if (Modal.RecordGrid.CellReadOnly[3,j] = roOff) then
                    begin
                      //Check for manual league
                      if (Modal.RecordGrid.Cell[4,j] = 8) then
                      begin
                        //If User changed the entry via dropdown, get the new values
                        if (Modal.League <> '') then
                        begin
                          BreakingNewsRecPtr^.League := Modal.League;
                        end;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(i);
                      end
                      else begin
                        //Standard mode, so change all text values
                        //Set user value text and increment
                        User_Data[i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                      end;
                      Inc(i);
                    end;
                  end;
                end;
                //Set other values
                BreakingNewsRecPtr^.StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                    (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                BreakingNewsRecPtr^.EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                    (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                BreakingNewsRecPtr^.Enabled := Modal.EntryEnable.Checked;
                BreakingNewsRecPtr^.DwellTime := Modal.EntryDwellTime.Value*1000;
                BreakingNewsRecPtr^.Comments := Modal.EntryNote.Text;
                //Edit all matching objects using comparsion of GUIDs
                for j := 0 to BreakingNews_Collection.Count-1 do
                begin
                  BreakingNewsRecPtr := BreakingNews_Collection.At(j);
                  if (IsEqualGUID(BreakingNewsRecPtr^.Event_GUID, CurrentEventGUID)) then
                  begin
                    With BreakingNewsRecPtr^ do
                    begin
                      //Set values for collection record
                      for i := 1 to 25 do UserData[i] := User_Data[i];
                      BreakingNewsRecPtr^.StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                          (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                      BreakingNewsRecPtr^.EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                          (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                      BreakingNewsRecPtr^.Enabled := Modal.EntryEnable.Checked;
                      BreakingNewsRecPtr^.DwellTime := Modal.EntryDwellTime.Value*1000;
                      BreakingNewsRecPtr^.Comments := Modal.EntryNote.Text;
                      //BreakingNewsRecPtr^.League := Modal.League;
                    end;
                  end;
                end;
              end;
              Modal.Free;
              //Refresh grid
              RefreshPlaylistGrid;
              //Refresh the fields grid
              RefreshEntryFieldsGrid(TickerDisplayMode);
            end;
          end;
      end;
    end;
    PlaylistGrid.CurrentDataRow := CurrentRow;
    PlaylistGrid.TopRow := SaveTopRow;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES AND HANDLERS FOR ZIPPER PLAYLIST STORAGE,, LOADING & DELETION
////////////////////////////////////////////////////////////////////////////////
//Procedure to delete a specified zipper playlist from the database
procedure TMainForm.DeleteZipperPlaylist (PlaylistType: SmallInt; Playlist_ID: Double; Playlist_Description: String);
begin
  Case PlaylistType of
      //Ticker
   1: begin
        try
          if (dmMain.tblTicker_Groups.RecordCount > 0) then
          begin
            dmMain.tblTicker_Groups.First;
            While(dmMain.tblTicker_Groups.EOF = FALSE) do
            begin
              if (dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblTicker_Groups.Delete;
              end
              else dmMain.tblTicker_Groups.Next;
            end;
          end;

          //Delete any associated elements
          if (dmMain.tblTicker_Elements.RecordCount > 0) then
          begin
            dmMain.tblTicker_Elements.First;
            While(dmMain.tblTicker_Elements.EOF = FALSE) do
            begin
              if (dmMain.tblTicker_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblTicker_Elements.Delete;
              end
              else dmMain.tblTicker_Elements.Next;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the existing copy of the ' +
                     'ticker playlist from the database.', mtError, [mbOK], 0)
        end;
      end;
      //Alerts
   2: begin
        try
          if (dmMain.tblBreakingNews_Groups.RecordCount > 0) then
          begin
            dmMain.tblBreakingNews_Groups.First;
            While(dmMain.tblBreakingNews_Groups.EOF = FALSE) do
            begin
              if (dmMain.tblBreakingNews_Groups.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblBreakingNews_Groups.Delete;
              end
              else dmMain.tblBreakingNews_Groups.Next;
            end;
          end;

          //Delete any associated elements
          if (dmMain.tblBreakingNews_Elements.RecordCount > 0) then
          begin
            dmMain.tblBreakingNews_Elements.First;
            While(dmMain.tblBreakingNews_Elements.EOF = FALSE) do
            begin
              if (dmMain.tblBreakingNews_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblBreakingNews_Elements.Delete;
              end
              else dmMain.tblBreakingNews_Elements.Next;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the existing copy of the ' +
                     'Alerts playlist from the database.', mtError, [mbOK], 0)
        end;
      end;
  end;
end;

//Handler for playlist auto-save timer
procedure TMainForm.PlaylistAutoSaveTimerTimer(Sender: TObject);
begin
  //Check for a playlist being loaded and a name specified
  if (Ticker_Collection.Count > 0) AND (Trim (Edit1.Text) <> '') then
    SavePlaylistWithoutClear;
end;
//Handler for save playlist button WITHOUT clear
procedure TMainForm.SavePlaylistBtnClick(Sender: TObject);
begin
  SavePlaylistWithoutClear;
end;
procedure TMainForm.SavePlaylistWithoutClear;
begin
  //Check for invalid template entries
  if (CheckForInvalidTemplates(TickerDisplayMode) = TRUE) then
  begin
    //Alert operator
    MessageDlg('You have specified invalid templates in the playlist for the currently selected ' +
               'display mode. Playlist cannot be saved to the database.', mtError, [mbOK], 0);
  end
  else begin
    //Save playlist, but don't clear
    SaveTickerPlaylist(PlaylistSelectTabControl.TabIndex+1, FALSE);
  end;  
end;
//Handler for save playlist button WITH clear
procedure TMainForm.SaveClosePlaylistBtnClick(Sender: TObject);
begin
  //Check for invalid template entries
  if (CheckForInvalidTemplates(TickerDisplayMode) = TRUE) then
    //Alert operator
    MessageDlg('You have specified invalid templates in the playlist for the currently selected ' +
               'display mode. Playlist cannot be saved to the database.', mtError, [mbOK], 0)
  else begin
    //Save playlist and clear
     SaveTickerPlaylist(PlaylistSelectTabControl.TabIndex+1, TRUE);
    //Disable single loop mode to prevent operator error with next playlist
    SingleLoopEnable.Checked := FALSE;
  end;
end;
//General procedure to save zipper collection out to database
function TMainForm.SaveTickerPlaylist(PlaylistType: SmallInt; ClearCollection: Boolean): Boolean;
var
  i: SmallInt;
  Playlist_Description: String;
  Playlist_ID: Double;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  Control: Word;
  FoundMatch: Boolean;
  OKToGo: Boolean;
  OriginalCreationDate: TDateTime;
  Save_Cursor: TCursor;
  Success: Boolean;
  FoundBreakingNews, FoundNonBreakingNews: Boolean;
  BreakingNewsMode: SmallInt;
  PlaylistStationID: SmallInt;
begin
  //Init
  OKToGo := TRUE;
  Success := TRUE;

  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor
  Screen.Cursor := crHourGlass;

  //Set playlist name
  Playlist_Description := Trim (Edit1.Text);
  //Set the region ID
  if (RegionSelectMode.ItemIndex = 0) then PlaylistStationID := 0
  else PlaylistStationID := RegionSelect.ItemIndex+1;

  //Check to see if a playlist with a matching ID exists. If so, prompt operator for overwrite confirmation
  Case PlaylistType of
    TICKER: FoundMatch := dmMain.tblTicker_Groups.Locate('Playlist_Description; Station_ID', VarArrayOf([Playlist_Description, PlaylistStationID]), []);
    BREAKINGNEWS: FoundMatch := dmMain.tblBreakingNews_Groups.Locate('Playlist_ID', PlaylistStationID, []);
  end;
  if (Foundmatch = TRUE) then
  begin
    Control := MessageDlg('A playlist with a matching description and Playout Station identifier was found in the ' +
                          'database. Do you wish to overwrite it?',
                          mtConfirmation, [mbYes, mbNo], 0);
    if (Control = mrYes) then
    begin
      Case PlaylistType of
          //Ticker
       TICKER: begin
            Playlist_ID := dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat;
            OriginalCreationDate := dmMain.tblTicker_Groups.FieldByName('Entry_Date').AsDateTime;
            DeleteZipperPlaylist (TICKER, Playlist_ID, Playlist_Description);
          end;
          //Alerts
       BREAKINGNEWS: begin
            //Set the playlist ID to the station ID (0 = global) for breaking news
            Playlist_ID := PlaylistStationID;
            OriginalCreationDate := dmMain.tblBreakingNews_Groups.FieldByName('Entry_Date').AsDateTime;
            DeleteZipperPlaylist (BREAKINGNEWS, Playlist_ID, Playlist_Description);
          end;
      end;
    end
    else OKToGo := FALSE;
  end;

  //Proceed if confirmed
  if (OKToGo = TRUE) then
  begin
    begin
      begin
        Case PlaylistType of
         //Ticker
         TICKER: begin
              //Only proceed if at least one element in playlist
              if (Length(Playlist_Description) >= 3) then
              begin
                if (Ticker_Collection.Count > 0) then
                begin
                  //If not overwriting the list, generate a new playlist ID
                  if (FoundMatch = FALSE) then
                  begin
                    Playlist_ID := Trunc(Now*100000);
                    OriginalCreationDate := Now;
                  end;
                  //Set Playlist_ID to 0 if it's the CURRENT playlist
                  if (Playlist_Description = 'CURRENT') then Playlist_ID := 0;
                  try
                    //Append new record for playlist
                    dmMain.tblTicker_Groups.AppendRecord ([
                      Playlist_ID,
                      PlaylistStationID,
                      Playlist_Description,
                      TickerDisplayMode,
                      OriginalCreationDate, //Creation date/timestamp
                      'N/A', //Operator name
                      Now //Edit date/time
                    ]);
                    //Now, insert all elements into ticker elements table
                    try
                      //Iterate for all records in collection
                      for i := 0 to Ticker_Collection.Count-1 do
                      begin
                        //Point to collection object
                        TickerRecPtr := Ticker_Collection.At(i);
                        //Append graphic record to graphics pages table in database}
                        dmMain.tblTicker_Elements.AppendRecord ([
                          Playlist_ID, //Block ID
                          i+1, //Index
                          //Set GUID as string
                          GUIDToString(TickerRecPtr^.Event_GUID),
                          //Default to NOT a mini playlist with a zero playlist ID
                          TickerRecPtr^.Enabled,
                          TickerRecPtr^.Is_Child,
                          TickerRecPtr^.League,
                          TickerRecPtr^.Subleague_Mnemonic_Standard,
                          TickerRecPtr^.Mnemonic_LiveEvent,
                          TickerRecPtr^.Template_ID,
                          TickerRecPtr^.Record_Type,
                          TickerRecPtr^.GameID,
                          TickerRecPtr^.SponsorLogo_Name, //Sponsor logo name
                          TickerRecPtr^.SponsorLogo_Dwell, //Dwell for sponsor logo
                          //For stats
                          TickerRecPtr^.StatStoredProcedure,
                          TickerRecPtr^.StatType,
                          TickerRecPtr^.StatTeam,
                          TickerRecPtr^.StatLeague,
                          TickerRecPtr^.StatRecordNumber,
                          TickerRecPtr^.UserData[1],
                          TickerRecPtr^.UserData[2],
                          TickerRecPtr^.UserData[3],
                          TickerRecPtr^.UserData[4],
                          TickerRecPtr^.UserData[5],
                          TickerRecPtr^.UserData[6],
                          TickerRecPtr^.UserData[7],
                          TickerRecPtr^.UserData[8],
                          TickerRecPtr^.UserData[9],
                          TickerRecPtr^.UserData[10],
                          TickerRecPtr^.UserData[11],
                          TickerRecPtr^.UserData[12],
                          TickerRecPtr^.UserData[13],
                          TickerRecPtr^.UserData[14],
                          TickerRecPtr^.UserData[15],
                          TickerRecPtr^.UserData[16],
                          TickerRecPtr^.UserData[17],
                          TickerRecPtr^.UserData[18],
                          TickerRecPtr^.UserData[19],
                          TickerRecPtr^.UserData[20],
                          TickerRecPtr^.UserData[21],
                          TickerRecPtr^.UserData[22],
                          TickerRecPtr^.UserData[23],
                          TickerRecPtr^.UserData[24],
                          TickerRecPtr^.UserData[25],
                          TickerRecPtr^.UserData[26],
                          TickerRecPtr^.UserData[27],
                          TickerRecPtr^.UserData[28],
                          TickerRecPtr^.UserData[29],
                          TickerRecPtr^.UserData[30],
                          TickerRecPtr^.UserData[31],
                          TickerRecPtr^.UserData[32],
                          TickerRecPtr^.UserData[33],
                          TickerRecPtr^.UserData[34],
                          TickerRecPtr^.UserData[35],
                          TickerRecPtr^.UserData[36],
                          TickerRecPtr^.UserData[37],
                          TickerRecPtr^.UserData[38],
                          TickerRecPtr^.UserData[39],
                          TickerRecPtr^.UserData[40],
                          TickerRecPtr^.UserData[41],
                          TickerRecPtr^.UserData[42],
                          TickerRecPtr^.UserData[43],
                          TickerRecPtr^.UserData[44],
                          TickerRecPtr^.UserData[45],
                          TickerRecPtr^.UserData[46],
                          TickerRecPtr^.UserData[47],
                          TickerRecPtr^.UserData[48],
                          TickerRecPtr^.UserData[49],
                          TickerRecPtr^.UserData[50],
                          TickerRecPtr^.StartEnableDateTime,
                          TickerRecPtr^.EndEnableDateTime,
                          TickerRecPtr^.DwellTime,
                          TickerRecPtr^.Description,
                          TickerRecPtr^.Comments
                        ]);
                      end;
                    except
                      MessageDlg ('Error occurred while trying to insert ticker element into database.',
                                   mtError, [mbOk], 0);
                    end;
                    //Update label & array
                    PlaylistInfo[TICKER].PlaylistName := Edit1.Text;
                    PlaylistNameLabel.Caption := Playlist_Description;
                  except
                    MessageDlg ('Error occurred while trying to insert ticker playlist into database.',
                                 mtError, [mbOk], 0);
                  end;
                  //Clear out & pack the collection if enabled
                  if (ClearCollection) then
                  begin
                    Ticker_Collection.Clear;
                    Ticker_Collection.Pack;
                  end;
                end
                else begin
                  MessageDlg('You must specify one or more elements to save out a playlist.',
                             mtInformation, [mbOk], 0);
                end;
              end
              else begin
                //Clear flag
                Success := FALSE;
                MessageDlg('You must specify a Playlist Name/Description of at least four (4) characters ' +
                           'before committing to the database.',
                            mtInformation, [mbOk], 0);
              end;
            end;
        end; //Case

        //Now, walk through scheduled playlists, and update time/datestamp and ticker mode for any matching entries
        //to insure refresh is picked up
        Case PlaylistType of
         //Ticker
         1: begin
              dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
              dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
              if (dmMain.tblScheduled_Ticker_Groups.RecordCount > 0) then
              begin
                for i := 1 to dmMain.tblScheduled_Ticker_Groups.RecordCount do
                begin
                  if (Playlist_ID = dmMain.tblScheduled_Ticker_Groups.FieldByName('Playlist_ID').AsFloat) then
                  begin
                    //Match was found, so update the timestamp
                    dmMain.tblScheduled_Ticker_Groups.Edit;
                    dmMain.tblScheduled_Ticker_Groups.FieldByName('Entry_Date').AsFloat := Now;
                    dmMain.tblScheduled_Ticker_Groups.FieldByName('Ticker_Mode').AsInteger := TickerDisplayMode;
                    dmMain.tblScheduled_Ticker_Groups.Post;
                  end;
                dmMain.tblScheduled_Ticker_Groups.Next;
                end;
              end;
            end;
         //Alerts
         2: begin
            end;
        end;
        if (ClearCollection) then
        begin
          //Clear the edit boxes for playlist name
          Edit1.Text := '';
          PlaylistInfo[PlaylistType].PlaylistName := '';
          PlaylistNameLabel.Caption := '';
          LastSaveTimeLabel.Caption := '';
          PlaylistInfo[PlaylistType].LastPlaylistSaveTimeString :=
            LastSaveTimeLabel.Caption;
        end
        else begin
          LastSaveTimeLabel.Caption := DateTimeToStr(Now);
          PlaylistInfo[PlaylistType].LastPlaylistSaveTimeString :=
            LastSaveTimeLabel.Caption;
        end;
      end;
    end;
    //Refresh the grids
    RefreshPlaylistGrid;
    RefreshEntryFieldsGrid(TickerDisplayMode);
  end;
  //Restore cursor
  Screen.Cursor := Save_Cursor;
  //Set success value
  SaveTickerPlaylist := Success;
end;

//Handler to load zipper collection from database
procedure TMainForm.LoadPlaylistBtnClick(Sender: TObject);
begin
  LoadZipperCollection;
end;
//General procedure to load a zipper collection; modifed to handle different playlist types
procedure TMainForm.LoadZipperCollection;
var
  Control, Control2: Word;
  OKToGo: Boolean;
  Modal: TPlaylistSelectDlg;
  Modal2: TBreakingNewsPlaylistSelectDlg;
  SelectedPlaylistID: Double;
  CurrentPlaylistType: SmallInt;
  CollectionCount: SmallInt;
  Confirmed: Boolean;
begin
  //Reset the auto logout timer & init
  OKToGo := TRUE;
  if (OKToGo = TRUE) then
  begin
    //Set intial values
    CurrentPlaylistType := PlaylistSelectTabControl.TabIndex+1;

    //Display the dialog
    Case CurrentPlaylistType of
     TICKER: Modal := TPlaylistSelectDlg.Create(Application);
     BREAKINGNEWS: Modal2 := TBreakingNewsPlaylistSelectDlg.Create(Application);
    end;

    try
      Case CurrentPlaylistType of
       TICKER: begin
         Modal.PlaylistType := CurrentPlaylistType;
         Control := Modal.ShowModal;
       end;
       BREAKINGNEWS: begin
         Modal2.PlaylistType := CurrentPlaylistType;
         Control := Modal2.ShowModal;
       end;
      end;
    finally
      //Init
      Confirmed := TRUE;
      //CLEAR AND LOAD NEW PLAYLIST
      if (Control = mrOK) then
      begin
        Case CurrentPlaylistType of
         TICKER: CollectionCount := Ticker_Collection.Count;
         BREAKINGNEWS: CollectionCount := BreakingNews_Collection.Count;
        end;
        Case CurrentPlaylistType of
         TICKER: SelectedPlaylistID := Modal.SelectedPlaylistID;
         BREAKINGNEWS: SelectedPlaylistID := Modal2.SelectedPlaylistID;
        end;
        if (CollectionCount > 0) then
        begin
          Control2 := MessageDlg('There are currently entries in the playlist. ' +
            'Do you wish to save them before loading the new playlist?',
            mtWarning, [mbYes, mbNo], 0);
          //If operator confirmed, save the playlist out
          if (Control2 = mrYes) then Confirmed := SaveTickerPlaylist(CurrentPlaylistType, TRUE);
        end;
        //Load the new playlist as long as no existing entries or the old one save out OK
        if (Confirmed) then LoadPlaylistCollection (CurrentPlaylistType, 0, SelectedPlaylistID);
      end
      //APPEND PLAYLIST
      else if (Control= mrYes) then
      begin
        Case CurrentPlaylistType of
         TICKER: SelectedPlaylistID := Modal.SelectedPlaylistID;
         BREAKINGNEWS: SelectedPlaylistID := Modal2.SelectedPlaylistID;
        end;
        if (Ticker_Collection.Count > 0) then
        begin
          Control2 := MessageDlg('There are currently entries in the playlist. ' +
            'Do you wish to append the selected playlist to the existing playlist?',
            mtWarning, [mbYes, mbNo], 0);
          if (Control2 = mrYes) then
            LoadPlaylistCollection (CurrentPlaylistType, 1, SelectedPlaylistID);
        end
        else LoadPlaylistCollection (CurrentPlaylistType, 1, SelectedPlaylistID);
      end
      //INSERT PLAYLIST
      else if (Control= mrYesToAll) then
      begin
        Case CurrentPlaylistType of
         TICKER: SelectedPlaylistID := Modal.SelectedPlaylistID;
         BREAKINGNEWS: SelectedPlaylistID := Modal2.SelectedPlaylistID;
        end;
        if (Ticker_Collection.Count > 0) then
        begin
          Control2 := MessageDlg('There are currently entries in the playlist. ' +
            'Do you wish to insert the selected playlist into the existing playlist?',
            mtWarning, [mbYes, mbNo], 0);
          if (Control2 = mrYes) then
            LoadPlaylistCollection (CurrentPlaylistType, 2, SelectedPlaylistID);
        end
        else
          LoadPlaylistCollection (CurrentPlaylistType, 2, SelectedPlaylistID);
      end;
      //Free up the dialog
      Case CurrentPlaylistType of
        TICKER: Modal.Free;
        BREAKINGNEWS: Modal2.Free;
      end;
    end;
  end;
end;

//Procedure to load default zipper collection from database
procedure TMainForm.LoadPlaylistCollection (PlaylistType: SmallInt; AppendMode: SmallInt; PlaylistID: Double);
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  SelectedPlaylistID: Double;
  InsertPoint: SmallInt;
  Save_Cursor: TCursor;
begin
  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor
  Screen.Cursor := crHourGlass;
  Case AppendMode of
      //Clear and load new collection
   0: begin
        Case PlaylistType of
            //Ticker
         TICKER: begin
              //Clear the collection
              Ticker_Collection.Clear;
              Ticker_Collection.Pack;
              //Set controls to values from selected block
              SelectedPlaylistID := PlaylistID;
              dmMain.tblTicker_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);
              Edit1.Text := dmMain.tblTicker_Groups.FieldByName('Playlist_Description').AsString;
              //Set playlist ID controls
              if (dmMain.tblTicker_Groups.FieldByName('Station_ID').AsInteger = 0) then
              begin
                RegionSelectMode.ItemIndex := 0;
                RegionSelect.ItemIndex := 0;
                RegionSelectMode.Color := clBtnFace;
                RegionSelect.Color := clBtnFace;
                RegionIDNum.Text := IntToStr(0);
                RegionIDNum.Color := clBtnFace;
              end
              else begin
                RegionSelectMode.ItemIndex := 1;
                RegionSelect.ItemIndex := dmMain.tblTicker_Groups.FieldByName('Station_ID').AsInteger-1;
                RegionSelectMode.Color := clYellow;
                RegionSelect.Color := clYellow;
                RegionIDNum.Text := IntToStr(RegionSelect.ItemIndex+1);
                RegionIDNum.Color := clYellow;
              end;
              //Set looping mode and 1-line, 2-line mode; clear entry in preview mode
              Case dmMain.tblTicker_Groups.FieldByName('Ticker_Mode').AsInteger of
                1: begin
                     SingleLoopEnable.Checked := TRUE;
                   end;
                2: begin
                     SingleLoopEnable.Checked := FALSE;
                   end;
                3: begin
                     SingleLoopEnable.Checked := TRUE;
                   end;
                4: begin
                     SingleLoopEnable.Checked := FALSE;
                   end;
              end;

              //Clear the graphics output
              //

              //Set UI control values
              PlaylistInfo[PlaylistType].PlaylistName := Edit1.Text;
              PlaylistNameLabel.Caption := Edit1.Text;
              //Store playlist info
              //if (dmMain.tblTicker_Groups.FieldByName('Ticker_Mode').AsInteger = 1) then
              //  SetToSingleLineMode
              //else
              //Set to double line mode by default
              //SetToDoubleLineMode;
              //Load the collection from the database
              //Query for the elements table for those that match the playlist ID
              dmMain.Query1.Close;
              dmMain.Query1.SQL.Clear;
              dmMain.Query1.SQL.Add('SELECT * FROM Ticker_Elements ' +
                                       'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
              dmMain.Query1.Open;
              if (dmMain.Query1.RecordCount > 0) then

              //Iterate through all records in the table
              repeat
                GetMem (TickerRecPtr, SizeOf(TickerRec));
                With TickerRecPtr^ do
                begin
                  //Set values for collection record
                  Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                  Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                  Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                  Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                  League := dmMain.Query1.FieldByName('League').AsString;
                  Subleague_Mnemonic_Standard := dmMain.Query1.FieldByName('Subleague_Mnemonic_Standard').AsString;
                  Mnemonic_LiveEvent := dmMain.Query1.FieldByName('Mnemonic_LiveEvent').AsString;
                  Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                  Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                  GameID := dmMain.Query1.FieldByName('GameID').AsString;
                  SponsorLogo_Name := dmMain.Query1.FieldByName('SponsorLogoName').AsString;
                  SponsorLogo_Dwell := dmMain.Query1.FieldByName('SponsorLogoDwell').AsInteger;
                  StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
                  StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
                  StatTeam := dmMain.Query1.FieldByName('StatTeam').AsString;
                  StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
                  StatRecordNumber := dmMain.Query1.FieldByName('StatRecordNumber').AsInteger;
                  //Load the user-defined text fields
                  for i := 1 to 50 do
                    UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                  StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                  EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                  DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                  Description := dmMain.Query1.FieldByName('Description').AsString;
                  Selected := FALSE;
                  Comments := dmMain.Query1.FieldByName('Comments').AsString;
                  //Insert the object
                  Ticker_Collection.Insert(TickerRecPtr);
                  Ticker_Collection.Pack;
                  //Go to next record in query
                  dmMain.Query1.Next;
                end;
              until (dmMain.Query1.EOF = TRUE);
            end;
            //Alerts
         BREAKINGNEWS: begin
              //Clear the collection
              BreakingNews_Collection.Clear;
              BreakingNews_Collection.Pack;
              //Set controls to values from selected block
              SelectedPlaylistID := PlaylistID;
              dmMain.tblBreakingNews_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);
              Edit1.Text := dmMain.tblBreakingNews_Groups.FieldByName('Playlist_Description').AsString;
              //Set playlist ID controls
              if (dmMain.tblBreakingNews_Groups.FieldByName('Playlist_ID').AsInteger = 0) then
              begin
                RegionSelectMode.ItemIndex := 0;
                RegionSelect.ItemIndex := 0;
                RegionSelectMode.Color := clBtnFace;
                RegionSelect.Color := clBtnFace;
                RegionIDNum.Text := IntToStr(0);
                RegionIDNum.Color := clBtnFace;
              end
              else begin
                RegionSelectMode.ItemIndex := 1;
                RegionSelect.ItemIndex := dmMain.tblBreakingNews_Groups.FieldByName('Playlist_ID').AsInteger-1;
                RegionSelectMode.Color := clYellow;
                RegionSelect.Color := clYellow;
                RegionIDNum.Text := IntToStr(RegionSelect.ItemIndex+1);
                RegionIDNum.Color := clYellow;
              end;
              PlaylistInfo[PlaylistType].PlaylistName := Edit1.Text;
              PlaylistNameLabel.Caption := Edit1.Text;
              //Store playlist info
              //Load the collection from the database
              //Query for the elements table for those that match the playlist ID
              dmMain.Query1.Close;
              dmMain.Query1.SQL.Clear;
              dmMain.Query1.SQL.Add('SELECT * FROM BreakingNews_Elements ' +
                                       'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
              dmMain.Query1.Open;
              if (dmMain.Query1.RecordCount > 0) then

              //Iterate through all records in the table
              repeat
                GetMem (BreakingNewsRecPtr, SizeOf(BreakingNewsRec));
                With BreakingNewsRecPtr^ do
                begin
                  //Set values for collection record
                  Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                  Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                  Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                  League := dmMain.Query1.FieldByName('League').AsString;
                  Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                  Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                  //Load the user-defined text fields
                  for i := 1 to 2 do
                    UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                  StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                  EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                  DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                  Description := dmMain.Query1.FieldByName('Description').AsString;
                  Selected := FALSE;
                  Comments := dmMain.Query1.FieldByName('Comments').AsString;
                  //Insert the object
                  BreakingNews_Collection.Insert(BreakingNewsRecPtr);
                  BreakingNews_Collection.Pack;
                  //Go to next record in query
                  dmMain.Query1.Next;
                end;
              until (dmMain.Query1.EOF = TRUE);
            end;
        end;
        //Refresh the grid
        RefreshPlaylistGrid;
        //Refresh the field entries grid
        RefreshEntryFieldsGrid(TickerDisplayMode);
      end;
      //Append or insert to existing collection
   1: begin
        Case PlaylistType of
            //Ticker
         1: begin
              if (dmMain.tblTicker_Groups.RecordCount > 0) then
              begin
                //Set controls to values from selected block
                SelectedPlaylistID := PlaylistID;
                dmMain.tblTicker_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);
                Edit1.Text := dmMain.tblTicker_Groups.FieldByName('Playlist_Description').AsString;
                //Set looping mode
                Case dmMain.tblTicker_Groups.FieldByName('Ticker_Mode').AsInteger of
                  1, 3: SingleLoopEnable.Checked := TRUE;
                  2, 4: SingleLoopEnable.Checked := FALSE;
                end;
                PlaylistInfo[PlaylistType].PlaylistName := Edit1.Text;
                PlaylistNameLabel.Caption := Edit1.Text;
                //Load the collection from the database
                //Query for the elements table for those that match the playlist ID
                dmMain.Query1.Close;
                dmMain.Query1.SQL.Clear;
                dmMain.Query1.SQL.Add('SELECT * FROM Ticker_Elements ' +
                                         'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
                dmMain.Query1.Open;
                if (dmMain.Query1.RecordCount > 0) then

                //Iterate through all records in the table
                repeat
                  GetMem (TickerRecPtr, SizeOf(TickerRec));
                  With TickerRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                    Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                    Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                    Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                    League := dmMain.Query1.FieldByName('League').AsString;
                    Subleague_Mnemonic_Standard := dmMain.Query1.FieldByName('Subleague_Mnemonic_Standard').AsString;
                    Mnemonic_LiveEvent := dmMain.Query1.FieldByName('Mnemonic_LiveEvent').AsString;
                    Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                    Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                    GameID := dmMain.Query1.FieldByName('GameID').AsString;
                    SponsorLogo_Name := dmMain.Query1.FieldByName('SponsorLogoName').AsString;
                    SponsorLogo_Dwell := dmMain.Query1.FieldByName('SponsorLogoDwell').AsInteger;
                    StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
                    StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
                    StatTeam := dmMain.Query1.FieldByName('StatTeam').AsString;
                    StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
                    StatRecordNumber := dmMain.Query1.FieldByName('StatRecordNumber').AsInteger;
                    //Load the user-defined text fields
                    for i := 1 to 50 do
                      UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                    StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                    EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                    DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                    Description := dmMain.Query1.FieldByName('Description').AsString;
                    Selected := FALSE;
                    Comments := dmMain.Query1.FieldByName('Comments').AsString;
                    //Insert the object
                    if (PlaylistGrid.CurrentDataRow < 1) then PlaylistGrid.CurrentDataRow := 1;
                    InsertPoint := PlaylistGrid.CurrentDataRow-1;
                    if (AppendMode = 1) then
                      Ticker_Collection.Insert(TickerRecPtr)
                    else if (AppendMode = 2) then
                      Ticker_Collection.AtInsert(InsertPoint, TickerRecPtr);
                    Ticker_Collection.Pack;
                    //Go to next record in query
                    dmMain.Query1.Next;
                  end;
                until (dmMain.Query1.EOF = TRUE);
                //Refresh the grid
                RefreshPlaylistGrid;
              end;
            end;
        end;
      end;
  end;
  //Restore default cursor
  Screen.Cursor := Save_Cursor;
end;

////////////////////////////////////////////////////////////////////////////////
// PLAYLIST SCHEDULE FUNCTIONS - TICKER, BUGS, EXTRA LINE
////////////////////////////////////////////////////////////////////////////////
//Handler for adding a ticker playlist to the ticker schedule
procedure TMainForm.BitBtn42Click(Sender: TObject);
begin
  AddScheduleEntry;
end;
procedure TMainForm.AddScheduleEntry;
var
  EncodedStartEnableTime: TDateTime;
  EncodedEndEnableTime: TDateTime;
  FoundMatchingPlaylist: Boolean;
  Control: Word;
  OKToGo: Boolean;
  OverlapCount: SmallInt;
  EncodedStartEnableTimeStr: String;
  EncodedEndEnableTimeStr: String;
  CurrentRecordCount: SmallInt;
  QueryStr: String;
  StationID: SmallInt;
begin
  begin
    //Check to see if a playlist with a matching ID exists. If so, prompt operator
    //for overwrite confirmation
    OKToGo := TRUE;

    //Set the record count based on the playlist type selected
    Case PlaylistSelectTabControl.TabIndex of
         //Ticker
      0: CurrentRecordCount := dmMain.tblTicker_Groups.RecordCount;
         //Alerts
      1: CurrentRecordCount := dmMain.tblBreakingNews_Groups.RecordCount;
    end;

    //Only proceed if at least playlist in database
    if (CurrentRecordCount > 0) then
    begin
      try
        //Encode start and end enable times
        EncodedStartEnableTime := Trunc(ScheduleEntryStartDate.Date) +
          (ScheduleEntryStartTime.Time - Trunc(ScheduleEntryStartTime.Time));
        EncodedEndEnableTime := Trunc(ScheduleEntryEndDate.Date) +
          (ScheduleEntryEndTime.Time - Trunc(ScheduleEntryEndTime.Time));

        //Make sure end time is greater than start time
        if (EncodedStartEnableTime >= EncodedEndEnableTime) then
        begin
          OKToGo := FALSE;
          MessageDlg('You must enter an End Enable Time that occurs AFTER the Start Enable Time',
                     mtError, [mbOK], 0);
        end;

        //Set the station ID
        StationID := dmMain.tblTicker_Groups.FieldByName('Station_ID').AsInteger;

        //NEED TO CHECK FOR BOTH GLOBAL AND REGION-SPECIFIC PLAYLISTS
        //Check for matching start time and Global Station ID in schedule (global)
        Case PlaylistSelectTabControl.TabIndex of
            //Ticker
         0: FoundMatchingPlaylist :=
              dmMain.tblScheduled_Ticker_Groups.Locate('Start_Enable_Time; Station_ID', VarArrayOf([EncodedStartEnableTime, 0]), []);
            //Alerts
         1: FoundMatchingPlaylist :=
              dmMain.tblScheduled_BreakingNews_Groups.Locate('Start_Enable_Time; Playlist_ID', VarArrayOf([EncodedStartEnableTime, 0]), []);
        end;
        if (FoundMatchingPlaylist = TRUE) then
        begin
          Control := MessageDlg('A playlist with the same start enable time and a Global Playout Station ID was found in the ' +
                                'database. Do you wish to overwrite it?', mtConfirmation,
                                [mbYes, mbNo], 0);
          if (Control = mrYes) then
          begin
            Case PlaylistSelectTabControl.TabIndex of
                //Ticker
             0: DeletePlaylistFromSchedule(TICKER, dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsFloat, 0);
                //Alerts
             1: DeletePlaylistFromSchedule(BREAKINGNEWS, dmMain.tblScheduled_BreakingNews_Groups.FieldByName('Start_Enable_Time').AsFloat, 0);
            end;
          end
          else OKToGo := FALSE;
        end;

        //Check for matching start time and Playout Station ID in schedule (region specific)
        Case PlaylistSelectTabControl.TabIndex of
            //Ticker
         0: FoundMatchingPlaylist :=
              dmMain.tblScheduled_Ticker_Groups.Locate('Start_Enable_Time; Station_ID', VarArrayOf([EncodedStartEnableTime, StationID]), []);
            //Alerts
         1: FoundMatchingPlaylist :=
              dmMain.tblScheduled_BreakingNews_Groups.Locate('Start_Enable_Time; Playlist_ID', VarArrayOf([EncodedStartEnableTime, StationID]), []);
        end;
        if (FoundMatchingPlaylist = TRUE) then
        begin
          Control := MessageDlg('A playlist with the same start enable time and Playout Station ID was found in the ' +
                                'database. Do you wish to overwrite it?', mtConfirmation,
                                [mbYes, mbNo], 0);
          if (Control = mrYes) then
          begin
            Case PlaylistSelectTabControl.TabIndex of
                //Ticker
             0: DeletePlaylistFromSchedule(TICKER, dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsFloat, StationID);
                //Alerts
             1: DeletePlaylistFromSchedule(BREAKINGNEWS, dmMain.tblScheduled_BreakingNews_Groups.FieldByName('Start_Enable_Time').AsFloat, StationID);
            end;
          end
          else OKToGo := FALSE;
        end;

        //Check to make sure there are no overlapping tickers; if so, alert operator and promot for continue
        if (OKToGo = TRUE) then
        begin
          EncodedStartEnableTimeStr := DateTimeToStr(EncodedStartEnableTime);
          EncodedEndEnableTimeStr := DateTimeToStr(EncodedEndEnableTime);
          dmMain.Query1.Close;
          dmMain.Query1.SQL.Clear;
          //Set the query string
          Case PlaylistSelectTabControl.TabIndex of
              //Ticker
           0: QueryStr := 'SELECT * FROM Scheduled_Ticker_Groups WHERE ' +
                'Station_ID = ' + IntToStr(StationID) + ' AND ' +
                'NOT (((' + QuotedStr(EncodedStartEnableTimeStr) + '> Start_Enable_Time) AND (' +
                            QuotedStr(EncodedStartEnableTimeStr) + '> End_Enable_Time)) OR ' +
                     '((' + QuotedStr(EncodedEndEnableTimeStr) + '< Start_Enable_Time) AND (' +
                            QuotedStr(EncodedEndEnableTimeStr) + '< End_Enable_Time)))';
          end;
          dmMain.Query1.SQL.Add(QueryStr);
          dmMain.Query1.Open;
          //Get the count
          OverlapCount := dmMain.Query1.RecordCount;
          //Close the query and return
          dmMain.Query1.Close;
          //Alert if any overlapping playlists were found
          if (OverlapCount > 0) then
          begin
            //Modified for V3_0_14 to disallow overlap
            //Control := MessageDlg('One or more playlists were found in the database that overlap all or part ' +
            //                      'of the time window specified for this playlist and for the specified Playout Stations. ' +
            //                      'Do you wish to schedule this new playlist anyway?', mtWarning, [mbYes, mbNo], 0);
            Control := MessageDlg('One or more playlists were found in the database that overlap all or part ' +
                                  'of the time window specified for this playlist and for the specified Playout Stations. ' +
                                  'Please correct the scheduled in/out times and try again.', mtError, [mbOk], 0);
            //if (Control = mrNo) then OKToGo := FALSE;
            //Modified for V3_0_14 to disallow overlap
            OkToGo := FALSE;
          end;
        end;

        //If no problems, append the record
        if (OKToGo) then
        begin
          Case PlaylistSelectTabControl.TabIndex of
              //Ticker
           0: begin
                //Append new record for playlist
                dmMain.tblScheduled_Ticker_Groups.AppendRecord ([
                  dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat, //Playlist ID
                  dmMain.tblTicker_Groups.FieldByName('Station_ID').AsInteger, //Playlist ID
                  dmMain.tblTicker_Groups.FieldByName('Playlist_Description').AsString, //Playlist ID
                  EncodedStartEnableTime, //Start date/time
                  EncodedEndEnableTime, //End date/time
                  dmMain.tblTicker_Groups.FieldByName('Ticker_Mode').AsInteger, //1-Line/2-Line ticker mode
                  Now, //Creation date/timestamp
                  'N/A' //Operator name
                ]);
                //Refresh the table
                RefreshScheduleGrid;
              end;
              //Alerts
           1: begin
                //Append new record for playlist
                //dmMain.tblScheduled_BreakingNews_Groups.AppendRecord ([
                //  dmMain.tblBreakingNews_Groups.FieldByName('Playlist_ID').AsFloat, //Playlist ID
                //  dmMain.tblBreakingNews_Groups.FieldByName('Playlist_Description').AsString, //Playlist ID
                //  EncodedStartEnableTime, //Start date/time
                //  EncodedEndEnableTime, //End date/time
                //  dmMain.tblBreakingNews_Groups.FieldByName('BreakingNews_Mode').AsInteger,
                //  Now, //Creation date/timestamp
                //  'N/A' //Operator name
                //]);
                //Refresh the table
                RefreshScheduleGrid;
              end;
          end;
        end;
      except
        MessageDlg ('Error occurred while trying to insert playlist into schedule database.',
                     mtError, [mbOk], 0);
      end;
    end
    else begin
      MessageDlg('There must be at least one playlist in the database to save out to the schedule.',
                 mtInformation, [mbOk], 0);
    end;
  end;
end;

//Procedure to delete a specified stack from the database
procedure TMainForm.DeletePlaylistFromSchedule (PlaylistType: SmallInt; Start_Enable_Time: Double; Station_ID: SmallInt);
begin
  Case PlaylistType of
      //Ticker
   TICKER: begin
        try
          //Refresh table
          dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
          dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
          //Do the delete
          if (dmMain.tblScheduled_Ticker_Groups.RecordCount > 0) then
          begin
            dmMain.tblScheduled_Ticker_Groups.First;
            While(dmMain.tblScheduled_Ticker_Groups.EOF = FALSE) do
            begin
              if (dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsFloat = Start_Enable_Time) AND
                 (dmMain.tblScheduled_Ticker_Groups.FieldByName('Station_ID').AsInteger = Station_ID) then
              begin
                dmMain.tblScheduled_Ticker_Groups.Delete;
              end
              else dmMain.tblScheduled_Ticker_Groups.Next;
            end;
          end;
        except
          MessageDlg ('Error occurred while trying to delete the ticker playlist from the schedule database.',
                       mtError, [mbOk], 0);
        end;
      end;
      //Alerts
   BREAKINGNEWS: begin
        try
          //Refresh table
          //dmMain.tblScheduled_BreakingNews_Groups.Active := FALSE;
          //dmMain.tblScheduled_BreakingNews_Groups.Active := TRUE;
          //Do the delete
          //if (dmMain.tblScheduled_BreakingNews_Groups.RecordCount > 0) then
          //begin
          //  dmMain.tblScheduled_BreakingNews_Groups.First;
          //  While(dmMain.tblScheduled_BreakingNews_Groups.EOF = FALSE) do
          //  begin
          //    if (dmMain.tblScheduled_BreakingNews_Groups.FieldByName('Start_Enable_Time').AsFloat = Start_Enable_Time) then
          //    begin
          //      dmMain.tblScheduled_BreakingNews_Groups.Delete;
          //    end
          //    else dmMain.tblScheduled_BreakingNews_Groups.Next;
          //  end;
          //end;
        except
          MessageDlg ('Error occurred while trying to delete the Alerts playlist from the schedule database.',
                       mtError, [mbOk], 0);
        end;
      end;
  end;
end;

//Handler for Delete Ticker Playlist from Schedule button
procedure TMainForm.BitBtn44Click(Sender: TObject);
var
  Control: Word;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        if (dmMain.tblScheduled_Ticker_Groups.RecordCount = 1) then
        begin
          Control := MessageDlg ('There must always be at least one ticker playlist in the schedule.',
                                  mtInformation, [mbOk], 0);
        end
        else if (dmMain.tblScheduled_Ticker_Groups.RecordCount > 1) then
        begin
          //Confirm
          Control := MessageDlg ('Are you sure you want to delete the selected ticker playlist schedule entry?',
                                  mtConfirmation, [mbYes, mbNo], 0);
          if (Control = mryes) then
            DeletePlaylistFromSchedule (1, dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsFloat,
              dmMain.tblScheduled_Ticker_Groups.FieldByName('Station_ID').AsInteger);
        end;
      end;
      //Alerts
   1: begin
        //if (dmMain.tblScheduled_BreakingNews_Groups.RecordCount = 1) then
        //begin
        //  Control := MessageDlg ('There must always be at least one BreakingNews playlist in the schedule.',
        //                          mtInformation, [mbOk], 0);
        //end
        //else if (dmMain.tblScheduled_BreakingNews_Groups.RecordCount > 1) then
        //begin
        //  //Confirm
        //  Control := MessageDlg ('Are you sure you want to delete the selected Alerts playlist schedule entry?',
        //                          mtConfirmation, [mbYes, mbNo], 0);
        //  if (Control = mryes) then
        //    DeletePlaylistFromSchedule (2, dmMain.tblScheduled_BreakingNews_Groups.FieldByName('Start_Enable_Time').AsFloat);
        //end;
      end;
  end;
end;

//Procedure to refresh the schedule grid
procedure TMainForm.RefreshScheduleGrid;
var
  SaveID: Double;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        //Save the current record being pointed to in the grid
        SaveID := dmMain.tblScheduled_Ticker_Groups.FieldByName('Playlist_ID').AsFloat;
        //Refresh the table
        dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
        dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
        //Search to the saved record
        dmMain.tblScheduled_Ticker_Groups.Locate('Playlist_ID', SaveID, []);
      end;
      //Alerts
   1: begin
        //Save the current record being pointed to in the grid
        //SaveID := dmMain.tblScheduled_BreakingNews_Groups.FieldByName('Playlist_ID').AsFloat;
        //Refresh the table
        //dmMain.tblScheduled_BreakingNews_Groups.Active := FALSE;
        //dmMain.tblScheduled_BreakingNews_Groups.Active := TRUE;
        //Search to the saved record
        //dmMain.tblScheduled_BreakingNews_Groups.Locate('Playlist_ID', SaveID, []);
      end;
  end;
end;

//Handler for menu selection to purge expired scheduled playlists
procedure TMainForm.PurgeScheduledEvents1DayOld1Click(Sender: TObject);
var
  Control: Word;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        //Refresh
        dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
        dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
        //Confirm
        if (dmMain.tblScheduled_Ticker_Groups.RecordCount > 0) then
        begin
          Control := MessageDlg ('Are you sure you want to delete the expired schedule events from the ticker schedule?',
                                  mtConfirmation, [mbYes, mbNo], 0);
          if (Control = mrYes) then
          try
            //Delete the expired events
            if (dmMain.tblScheduled_Ticker_Groups.RecordCount > 0) then
            begin
              Repeat
                if (dmMain.tblScheduled_Ticker_Groups.FieldByName('End_Enable_Time').AsFloat < Now) then
                dmMain.tblScheduled_Ticker_Groups.Delete
              else dmMain.tblScheduled_Ticker_Groups.Next;
              Until (dmMain.tblScheduled_Ticker_Groups.EOF = TRUE);
              //Refresh the table
              RefreshScheduleGrid;
            end;
          finally
          end;
        end;
      end;
      //Alerts
   1: begin
        //Refresh
        //dmMain.tblScheduled_BreakingNews_Groups.Active := FALSE;
        //dmMain.tblScheduled_BreakingNews_Groups.Active := TRUE;
        //Confirm
        //if (dmMain.tblScheduled_BreakingNews_Groups.RecordCount > 0) then
        //begin
        //  Control := MessageDlg ('Are you sure you want to delete the expired schedule events from the BreakingNews schedule?',
        //                          mtConfirmation, [mbYes, mbNo], 0);
        //  if (Control = mrYes) then
        //  try
        //    //Delete the expired events
        //    if (dmMain.tblScheduled_BreakingNews_Groups.RecordCount > 0) then
        //    begin
        //      Repeat
        //        if (dmMain.tblScheduled_BreakingNews_Groups.FieldByName('End_Enable_Time').AsFloat < Now) then
        //        dmMain.tblScheduled_BreakingNews_Groups.Delete
        //      else dmMain.tblScheduled_BreakingNews_Groups.Next;
        //      Until (dmMain.tblScheduled_BreakingNews_Groups.EOF = TRUE);
        //      //Refresh the table
        //      RefreshScheduleGrid;
        //    end;
        //  finally
        //  end;
        //end;
      end;
  end;
end;

//Handler for edit selected schedule entry
procedure TMainForm.BitBtn43Click(Sender: TObject);
begin
  EditCurrentScheduleEntry;
end;
procedure TMainForm.ScheduledPlaylistsGridDblClick(Sender: TObject);
begin
  EditCurrentScheduleEntry;
end;
//Procedure to edit the current schedule entry
procedure TMainForm.EditCurrentScheduleEntry;
var
  OKToGo: Boolean;
  Modal: TScheduleEntryEditDlg;
  Start_Enable_Time, End_Enable_Time: TDateTime;
  StartHour, StartMinute, EndHour, EndMinute, Sec, mSec: Word;
  Control: Word;
begin
  //Init
  OKToGo := TRUE;
  //Setup and launch editing dialog
  Modal := TScheduleEntryEditDlg.Create(Application);
  try
    //Set control values on dialog
    Case PlaylistSelectTabControl.TabIndex of
        //Ticker
     0: begin
          Start_Enable_Time := dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsDateTime;
          End_Enable_Time := dmMain.tblScheduled_Ticker_Groups.FieldByName('End_Enable_Time').AsDateTime;
        end;
        //Alerts
     1: begin
          //Start_Enable_Time := dmMain.tblScheduled_BreakingNews_Groups.FieldByName('Start_Enable_Time').AsDateTime;
          //End_Enable_Time := dmMain.tblScheduled_BreakingNews_Groups.FieldByName('End_Enable_Time').AsDateTime;
        end;
    end;

    //Set initial value of controls
    Modal.ScheduleEntryStartDate.Date := Trunc(Start_Enable_Time);
    Modal.ScheduleEntryEndDate.Date := Trunc(End_Enable_Time);
    Modal.ScheduleEntryStartTime.Time :=
      (Start_Enable_Time - Trunc(Start_Enable_Time));
    Modal.ScheduleEntryEndTime.Time :=
      (End_Enable_Time - Trunc(End_Enable_Time));

    Control := Modal.ShowModal;
    //Set initial values
  finally
    //Set new values if user didn't cancel out
    if (Control = mrOK) then
    begin
      //Encode start and end enable times
      Start_Enable_Time := Trunc(Modal.ScheduleEntryStartDate.Date) +
        (Modal.ScheduleEntryStartTime.Time - Trunc(Modal.ScheduleEntryStartTime.Time));
      End_Enable_Time := Trunc(Modal.ScheduleEntryEndDate.Date) +
        (Modal.ScheduleEntryEndTime.Time - Trunc(Modal.ScheduleEntryEndTime.Time));

      //Make sure end time is greater than start time
      if (Start_Enable_Time >= End_Enable_Time) then
      begin
        OKToGo := FALSE;
        MessageDlg('You must enter an End Enable Time that occurs AFTER the Start Enable Time',
                   mtError, [mbOK], 0);
      end;

      //If OK, edit the values
      if (OKToGo = TRUE) then
      begin
        try
          //Edit the values
          Case PlaylistSelectTabControl.TabIndex of
              //Ticker
           0: begin
                dmMain.tblScheduled_Ticker_Groups.Edit;
                //Encode start and end enable times
                dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsDateTime := Start_Enable_Time;
                dmMain.tblScheduled_Ticker_Groups.FieldByName('End_Enable_Time').AsDateTime := End_Enable_Time;
                dmMain.tblScheduled_Ticker_Groups.Post;
              end;
              //Alerts
           1: begin
                //dmMain.tblScheduled_BreakingNews_Groups.Edit;
                //dmMain.tblScheduled_BreakingNews_Groups.FieldByName('Start_Enable_Time').AsDateTime := Start_Enable_Time;
                //dmMain.tblScheduled_BreakingNews_Groups.FieldByName('End_Enable_Time').AsDateTime := End_Enable_Time;
                //dmMain.tblScheduled_BreakingNews_Groups.Post;
              end;
          end;
          //Refresh the grid
          RefreshSchedulegrid;
        except
          MessageDlg('Error occurred while trying to edit the schedule entry in the database.',
                      mtError, [mbOK], 0);
        end;
      end;
    end;
    Modal.Free
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES AND HANDLERS FOR DATABASE MAINTENANCE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Refresh database maintenance playlist list grid
procedure TMainForm.BitBtn32Click(Sender: TObject);
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker playlists
   0: begin
        dmMain.tblTicker_Groups.Active := FALSE;
        dmMain.tblTicker_Groups.Active := TRUE;
      end;
      //Alerts playlists
   1: begin
        dmMain.tblBreakingNews_Groups.Active := FALSE;
        dmMain.tblBreakingNews_Groups.Active := TRUE;
      end;
  end;
end;

//Handler for deleting selected zipper group entry from database
procedure TMainForm.BitBtn34Click(Sender: TObject);
var
  MyPlaylistID: Double;
  PlaylistIsScheduled: Boolean;
  Control: Word;
  OKToGo: Boolean;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker playlists
   0: begin
        try
          OKToGo := TRUE;
          if (dmMain.tblTicker_Groups.RecordCount > 0) then
          begin
            MyPlaylistID := dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat;
            //First, check to see if this playlist is scheduled and alert operator if so
            PlaylistIsScheduled := dmMain.tblScheduled_Ticker_Groups.Locate('Playlist_ID', MyPlaylistID, []);
            if (PlaylistIsScheduled) then
            begin
              Control := MessageDlg('At least one schedule entry has been found that includes this ticker playlist. ' +
                                    'You must delete any associated ticker schedule entries before deleting the ' +
                                    'playlist.', mtError, [mbOk], 0);
              OKToGo := FALSE;
            end;
            if (OkToGo = TRUE) then
            begin
              //Alert operator
              Control := MessageDlg('Are you sure you want to delete the selected ticker playlist?',
                         mtConfirmation, [mbYes, mbNo], 0);
              if (Control = mrYes) then
              begin
                //Delete the entry in the stacks table
                dmMain.tblTicker_Groups.Delete;
                dmMain.tblTicker_Groups.Active := FALSE;
                dmMain.tblTicker_Groups.Active := TRUE;
                //Delete any associated events
                if (dmMain.tblTicker_Elements.RecordCount > 0) then
                begin
                  dmMain.tblTicker_Elements.First;
                  While(dmMain.tblTicker_Elements.EOF = FALSE) do
                  begin
                    if (dmMain.tblTicker_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(MyPlaylistID)) then
                    begin
                      dmMain.tblTicker_Elements.Delete;
                    end
                    else dmMain.tblTicker_Elements.Next;
                  end;
                end;
              end;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the ticker playlist.',
                      mtError, [mbOK], 0);
        end;
      end;
      //Alerts playlists
   1: begin
        try
          OKToGo := TRUE;
          if (dmMain.tblBreakingNews_Groups.RecordCount > 0) then
          begin
            MyPlaylistID := dmMain.tblBreakingNews_Groups.FieldByName('Playlist_ID').AsFloat;
            //First, check to see if this playlist is scheduled and alert operator if so
            //PlaylistIsScheduled := dmMain.tblScheduled_BreakingNews_Groups.Locate('Playlist_ID', MyPlaylistID, []);
            //if (PlaylistIsScheduled) then
            //begin
            //  Control := MessageDlg('At least one schedule entry has been found that includes this BreakingNews playlist. ' +
            //                        'You must delete any associated schedule entries before deleting the ' +
            //                        'BreakingNews playlist.', mtError, [mbOk], 0);
            //  OKToGo := FALSE;
            //end;
            if (OkToGo = TRUE) then
            begin
              //Alert operator
              Control := MessageDlg('Are you sure you want to delete the selected BreakingNews playlist?',
                         mtConfirmation, [mbYes, mbNo], 0);
              if (Control = mrYes) then
              begin
                //Delete the entry in the stacks table
                dmMain.tblBreakingNews_Groups.Delete;
                dmMain.tblBreakingNews_Groups.Active := FALSE;
                dmMain.tblBreakingNews_Groups.Active := TRUE;
                //Delete any associated events
                if (dmMain.tblBreakingNews_Elements.RecordCount > 0) then
                begin
                  dmMain.tblBreakingNews_Elements.First;
                  While(dmMain.tblBreakingNews_Elements.EOF = FALSE) do
                  begin
                    if (dmMain.tblBreakingNews_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(MyPlaylistID)) then
                    begin
                      dmMain.tblBreakingNews_Elements.Delete;
                    end
                    else dmMain.tblBreakingNews_Elements.Next;
                  end;
                end;
              end;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the Alerts playlist.',
                      mtError, [mbOK], 0);
        end;
      end;
  end; //Case
end;

procedure TMainForm.TimeOfDayTimerTimer(Sender: TObject);
begin
  DateTimeLabel.Caption := DateTimeToStr(Now);
end;

//Handler for hotkeys for playlist operations (cut, copy, paste, duplicate)
procedure TMainForm.PlaylistGridKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (ssCtrl in Shift) and (Key = Ord('X')) then Cut
  else if (ssCtrl in Shift) and (Key = Ord('C')) then Copy
  else if (ssCtrl in Shift) and (Key = Ord('V')) then Paste
  else if (ssCtrl in Shift) and (Key = Ord('E')) then PasteEnd
  else if (ssCtrl in Shift) and (Key = Ord('D')) then Duplicate
  else if (ssCtrl in Shift) and (Key = Ord('A')) then SelectAll
  else if (ssCtrl in Shift) and (Key = Ord('U')) then DeSelectAll
  else if (ssShift in Shift) and (Key = VK_UP) then MoveRecord(UP)
  else if (ssShift in Shift) and (Key = VK_DOWN) then MoveRecord(DOWN)
  else if (Key = VK_DELETE) then DeleteGraphicFromZipperPlaylist(TRUE);
end;

//Handler for Select ALL
procedure TMainForm.SelectAll1Click(Sender: TObject);
begin
  SelectAll;
end;
procedure TMainForm.SelectAll;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker playlists
   0: begin
        if (Ticker_Collection.Count > 0) then
        begin
          for i := 0 to Ticker_Collection.Count-1 do
          begin
            TickerRecPtr := Ticker_Collection.At(i);
            PlaylistGrid.RowSelected[i+1] := TRUE;
            TickerRecPtr^.Selected := TRUE;
          end;
        end;
      end;
      //Alerts playlists
   1: begin
        if (BreakingNews_Collection.Count > 0) then
        begin
          for i := 0 to BreakingNews_Collection.Count-1 do
          begin
            BreakingNewsRecPtr := BreakingNews_Collection.At(i);
            PlaylistGrid.RowSelected[i+1] := TRUE;
            BreakingNewsRecPtr^.Selected := TRUE;
          end;
        end;
      end;
  end;
end;

//Handler for De-Select ALL
procedure TMainForm.DeSelectAll1Click(Sender: TObject);
begin
  DeSelectAll;
end;
procedure TMainForm.DeSelectAll;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker playlists
   0: begin
        if (Ticker_Collection.Count > 0) then
        begin
          for i := 0 to Ticker_Collection.Count-1 do
          begin
            TickerRecPtr := Ticker_Collection.At(i);
            PlaylistGrid.RowSelected[i+1] := FALSE;
            TickerRecPtr^.Selected := FALSE;
          end;
        end;
      end;
      //Alerts playlists
   1: begin
        if (BreakingNews_Collection.Count > 0) then
        begin
          for i := 0 to BreakingNews_Collection.Count-1 do
          begin
            BreakingNewsRecPtr := BreakingNews_Collection.At(i);
            PlaylistGrid.RowSelected[i+1] := FALSE;
            BreakingNewsRecPtr^.Selected := FALSE;
          end;
        end;
      end;
  end;
end;

//Handler for click on cell - draws current page
procedure TMainForm.PlaylistGridClick(Sender: TObject);
begin
  PreviewCurrentPlaylistRecord;
end;
//Procedure for previewing the currently selected record in the playlist
procedure TMainForm.PreviewCurrentPlaylistRecord;
var
  SaveTop: LongInt;
begin
  //Save the top row
  SaveTop := PlaylistGrid.TopRow;
  if (PlaylistGrid.CurrentDataRow <= 0) then PlaylistGrid.CurrentDataRow := 1;
  Case PlaylistSelectTabControl.TabIndex of
   0: begin
        PlaylistGrid.SetFocus;
        EngineInterface.SendTickerRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
      end;
   1: begin
        PlaylistGrid.SetFocus;
        //EngineInterface.SendAlertRecord(PlaylistGrid.CurrentDataRow-1);
      end;
  end;
  //Restore the top row
  PlaylistGrid.TopRow := SaveTop;
end;
//Handler for ENTER key press on cell - draws current page
procedure TMainForm.PlaylistGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (PlaylistGrid.CurrentDataRow <= 0) then PlaylistGrid.CurrentDataRow := 1;
  Case PlaylistSelectTabControl.TabIndex of
   0: begin
//          EngineInterface.SendTickerRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
      end;
   1: begin
//          EngineInterface.SendBugRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
      end;
   2: begin
//          EngineInterface.SendExtraLineRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
      end;
  end;
end;

//Handler for arrow up/down when playlist grid has the focus
procedure TMainForm.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if ((Key = VK_UP) OR (Key = VK_DOWN)) AND (PlaylistGrid.IsFocused) then
  begin
    if (PlaylistGrid.CurrentDataRow <= 0) then PlaylistGrid.CurrentDataRow := 1;
    Case PlaylistSelectTabControl.TabIndex of
     0: begin
          EngineInterface.SendTickerRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
        end;
     1: begin
//          EngineInterface.SendBugRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
        end;
    end;
  end;
end;

//Handler to reconnect to graphics engine
procedure TMainForm.ReconnecttoGraphicsEngine1Click(Sender: TObject);
begin
  //Connect to the graphics engine if it's enabled
  if (EngineParameters.Enabled = TRUE) then
  begin
    //Check for serial port enabled
    if (EngineParameters.UseSerial) then
    begin
      try
        EngineInterface.EngineCOMPort.ComNumber := EngineParameters.COMPort;
        EngineInterface.EngineCOMPort.Baud := EngineParameters.BaudRate;
        EngineInterface.EngineCOMPort.Open := TRUE;
      except
        if (ErrorLoggingEnabled = True) then
        begin
          Error_Condition := True;
          //Label16.Caption := 'ERROR';
          //WriteToErrorLog
          EngineInterface.WriteToErrorLog('Error occurred while trying connect to graphics engine via COM port');
        end;
      end;
    end
    //Check for TCP/IP port enabled
    else begin
      try
        EngineInterface.EnginePort.Address := EngineParameters.IPAddress;
        EngineInterface.EnginePort.Port := StrToIntDef(EngineParameters.Port, 7795);
        EngineInterface.EnginePort.Active := TRUE;
      except
        if (ErrorLoggingEnabled = True) then
        begin
          Error_Condition := True;
          //Label16.Caption := 'ERROR';
          //WriteToErrorLog
          EngineInterface.WriteToErrorLog('Error occurred while trying connect to graphics engine via IP port');
        end;
      end;
    end;
  end;
end;

//Handler for template change - sets dwell time
procedure TMainForm.AvailableTemplatesDBGridRowChanged(Sender: TObject; OldRow, NewRow: Variant);
begin
  EntryDwellTime.Value :=
    Trunc(GetTemplateInformation(dmMain.Query2.FieldByName('Template_ID').AsInteger).Default_Dwell/1000);
end;

//Handlers to clear graphics from CAL output
procedure TMainForm.ClearGraphicsOutput1Click(Sender: TObject);
begin
  EngineInterface.InitTicker(TICKER_BACKPLATE_OUT);
  TickerbackplateIn := FALSE;
end;
procedure TMainForm.ClearGraphicsOutput2Click(Sender: TObject);
begin
  EngineInterface.InitTicker(TICKER_BACKPLATE_OUT);
  TickerbackplateIn := FALSE;
end;
//Clear extra line region
procedure TMainForm.ClearExtraLineRegion1Click(Sender: TObject);
begin
  //EngineInterface.ClearBreakingNews;
end;
procedure TMainForm.ClearExtraLineRegion2Click(Sender: TObject);
begin
  //EngineInterface.ClearBreakingNews;
end;

////////////////////////////////////////////////////////////////////////////////
// Handler for editing default start/end enable times for selected template
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.EditDefaultTemplateStartEndEnableTimes1Click(Sender: TObject);
var
  i: SmallInt;
  CurrentTemplateID: SmallInt;
  MyTemplateIndex: SmallInt;
  Modal: TScheduleEntryEditDlg;
  Control: Word;
  TemplateDefsRecPtr, TemplateDefsRecPtr2: ^TemplateDefsRec;
  CollectionCount: SmallInt;
  FoundRecord: Boolean;
begin
  //Get ID of current template
  CurrentTemplateID := dmMain.Query2.FieldByName('Template_ID').AsInteger;
  //Find index of template record in collection
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = CurrentTemplateID) then
        MyTemplateIndex := i;
    end;
  end;
  //Get the pointer to the selected template in the collection
  TemplateDefsRecPtr := Template_Defs_Collection.At(MyTemplateIndex);
  //Setup and launch editing dialog if applicable
  try
    Modal := TScheduleEntryEditDlg.Create(Application);
    Modal.Caption := 'Template Default Enable Time Editor';
    Modal.DefaultDwellTimeLabel.Visible := TRUE;
    Modal.DefaultDwellTime.Visible := TRUE;
    Modal.ScheduleEntryStartDate.Date := TemplateDefsRecPtr^.StartEnableDateTime;
    Modal.ScheduleEntryStartTime.Time := TemplateDefsRecPtr^.StartEnableDateTime;
    Modal.ScheduleEntryEndDate.Date := TemplateDefsRecPtr^.EndEnableDateTime;
    Modal.ScheduleEntryEndTime.Time := TemplateDefsRecPtr^.EndEnableDateTime;
    Modal.DefaultDwellTime.Value := Trunc(TemplateDefsRecPtr^.Default_Dwell/1000);
    //Show the dialog
    Control := Modal.ShowModal;
  finally
    //Set new values if user didn't cancel out
    if (Control = mrOK) then
    begin
      //Update the values in the collection
      TemplateDefsRecPtr^.StartEnableDateTime := Trunc(Modal.ScheduleEntryStartDate.Date) +
          (Modal.ScheduleEntryStartTime.Time - Trunc(Modal.ScheduleEntryStartTime.Time));
      TemplateDefsRecPtr^.EndEnableDateTime := Trunc(Modal.ScheduleEntryEndDate.Date) +
          (Modal.ScheduleEntryEndTime.Time - Trunc(Modal.ScheduleEntryEndTime.Time));
      TemplateDefsRecPtr^.Default_Dwell := Modal.DefaultDwellTime.Value*1000;
      //Update the database values
      dmMain.tblTemplate_Defs.Active := FALSE;
      dmMain.tblTemplate_Defs.Active := TRUE;
      if (dmMain.tblTemplate_Defs.RecordCount > 0) then
      begin
        FoundRecord := dmMain.tblTemplate_Defs.Locate('Template_ID', CurrentTemplateID, []);
        if (FoundRecord) then
        begin
          dmMain.tblTemplate_Defs.Edit;
          dmMain.tblTemplate_Defs.FieldByName('StartEnableTime').AsDateTime := TemplateDefsRecPtr^.StartEnableDateTime;
          dmMain.tblTemplate_Defs.FieldByName('EndEnableTime').AsDateTime := TemplateDefsRecPtr^.EndEnableDateTime;
          dmMain.tblTemplate_Defs.FieldByName('Default_Dwell').AsInteger := TemplateDefsRecPtr^.Default_Dwell;
          dmMain.tblTemplate_Defs.Post;
        end;
      end;
      dmMain.tblTemplate_Defs.Active := FALSE;
    end;
    Modal.Free;
  end;
end;

//Handler for launching sponsor logo editor
procedure TMainForm.EditSponsorLogoDefinitions1Click(Sender: TObject);
var
  Modal: TSponsorLogoEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblSponsor_Logos.Active := FALSE;
  dmMain.tblSponsor_Logos.Active := TRUE;
  Modal := TSponsorLogoEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free
  end;
end;

//Handler for launching team info editor
procedure TMainForm.EditTeamInformation1Click(Sender: TObject);
var
  Modal: TDBEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblTeams.Active := FALSE;
  dmMain.tblTeams.Active := TRUE;
  Modal := TDBEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free
  end;
end;

//Handler for launching game phase info editor dialog
procedure TMainForm.EditGamePhaseData1Click(Sender: TObject);
var
  Modal: TGamePhaseEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblGame_Phase_Codes.Active := FALSE;
  dmMain.tblGame_Phase_Codes.Active := TRUE;
  Modal := TGamePhaseEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free;
    ReloadDataFromDatabase;
    dmMain.tblGame_Phase_Codes.Active := FALSE;
  end;
end;

//Handler to launch editor for custom headers
procedure TMainForm.EditCustomHeaders1Click(Sender: TObject);
var
  Modal: TCustomHeaderEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblCustom_Headers.Active := FALSE;
  dmMain.tblCustom_Headers.Active := TRUE;
  Modal := TCustomHeaderEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free;
    ReloadDataFromDatabase;
    dmMain.tblCustom_Headers.Active := FALSE;
  end;
end;

//Handler for manual game override
procedure TMainForm.GamesDBGridDblClick(Sender: TObject);
begin
  ManualGameOverride;
end;

//Function to return the game phase code given the league and the display string
function GetGamePhaseCode(League: String; Display_Period: String): SmallInt;
var
  i, OutVal: SmallInt;
  GamePhaseRecPtr: ^GamePhaseRec;
begin
  OutVal := 0;
  if (Game_Phase_Collection.Count > 0) then
  begin
    for i := 1 to Game_Phase_Collection.Count-1 do
    begin
      GamePhaseRecPtr := Game_Phase_Collection.At(i);
      if (GamePhaseRecPtr^.League = League) AND
         (GamePhaseRecPtr^.Display_Period = Display_Period) then
        OutVal := GamePhaseRecPtr^.SI_Phase_Code;
    end;
  end;
  GetGamePhaseCode := OutVal;
end;

//General procedure for manual game override
//General procedure for manual game override
procedure TMainForm.ManualGameOverride;
var
  i: SmallInt;
  CurrentPhase: SmallInt;
  Modal: TManualGameDlg;
  CategoryRecPtr: ^CategoryRec;
  GamePhaseRecPtr: ^GamePhaseRec;
  Control: Word;
  VisitorOddsID, HomeOddsID: Double;
  TempLeague: String;
begin
  if (dmMain.SportbaseQuery.RecordCount > 0) then
  begin
    Modal := TManualGameDlg.Create(Application);
    try
      //GAME DATA
      //Set initial values
      Modal.SelectedGameLabel.Caption := dmMain.SportbaseQuery.FieldByName('League').AsString + ': ' +
        dmMain.SportbaseQuery.FieldByName('VCity').AsString + ' @ ' +
        dmMain.SportbaseQuery.FieldByName('HCity').AsString;
      Modal.ManualGamesOverrideEnable.Checked := dmMain.SportbaseQuery.FieldByName('Manual').AsBoolean;
      Modal.ManualGamesOverrideEnable.Checked := dmMain.SportbaseQuery.FieldByName('Manual').AsBoolean;
      Modal.VisitorName.Text := dmMain.SportbaseQuery.FieldByName('VCity').AsString + ' ' +
        dmMain.SportbaseQuery.FieldByName('VTeam').AsString;
      Modal.HomeName.Text := dmMain.SportbaseQuery.FieldByName('HCity').AsString + ' ' +
        dmMain.SportbaseQuery.FieldByName('HTeam').AsString;
      Modal.VisitorScore.Value := dmMain.SportbaseQuery.FieldByName('VScore').AsInteger;
      Modal.HomeScore.Value := dmMain.SportbaseQuery.FieldByName('HScore').AsInteger;
      Modal.StartTime.Time :=  dmMain.SportbaseQuery.FieldByName('Start').AsDateTime;
      //Set game state
      if (dmMain.SportbaseQuery.FieldByName('State').AsString = 'Pre-Game') then
        Modal.GameState.ItemIndex := 0
      else if (dmMain.SportbaseQuery.FieldByName('State').AsString = 'In-Progress') then
        Modal.GameState.ItemIndex := 1
      else
        Modal.GameState.ItemIndex := 2;
      //Set game phase and populate dropdown
      if (Game_Phase_Collection.Count > 0) then
      begin
        for i := 0 to Game_Phase_Collection.Count-1 do
        begin
          GamePhaseRecPtr := Game_Phase_Collection.At(i);
          if (GamePhaseRecPtr^.League = dmMain.SportbaseQuery.FieldByName('League').AsString) then
          begin
            Modal.GamePhase.Items.Add(GamePhaseRecPtr^.Display_Period);
            if (dmMain.SportbaseQuery.FieldByName('Period').AsInteger = Modal.GamePhase.Items.Count) then
              CurrentPhase := Modal.GamePhase.Items.Count;
          end;
        end;
        if (CurrentPhase > 0) then Modal.GamePhase.ItemIndex := CurrentPhase-1;
      end;

      //Show the dialog
      Control := Modal.ShowModal;
    finally
      //Set new values if user didn't cancel out
      if (Control = mrOK) then
      begin
        //GAME DATA
        //Put games table into edit mode
        dmMain.SportbaseQuery.Edit;
        //Set edited fields
        dmMain.SportbaseQuery.FieldByName('Manual').AsBoolean := Modal.ManualGamesOverrideEnable.Checked;
        if (Modal.ManualGamesOverrideEnable.Checked) then
        begin
          dmMain.SportbaseQuery.FieldByName('VScore').AsInteger := Modal.VisitorScore.Value;
          dmMain.SportbaseQuery.FieldByName('HScore').AsInteger := Modal.HomeScore.Value;
          //Set game state
          Case Modal.GameState.ItemIndex of
           0: dmMain.SportbaseQuery.FieldByName('State').AsString := 'Pre-Game';
           1: dmMain.SportbaseQuery.FieldByName('State').AsString := 'In-Progress';
           2: dmMain.SportbaseQuery.FieldByName('State').AsString := 'Final';
          end;
          //Set period
          dmMain.SportbaseQuery.FieldByName('Period').AsInteger := Modal.GamePhase.ItemIndex+1;
          //Set clock values
          //If end of period, set to min, sec = 0 so that proper phase is displayed
          if (Modal.EndOfPeriod.Checked) then
          begin
            dmMain.SportbaseQuery.FieldByName('TimeMin').AsInteger := 0;
            dmMain.SportbaseQuery.FieldByName('TimeSec').AsInteger := 0;
          end
          else begin
            //Set clock values to -1 to flag manual mode; clock will not be shown
            dmMain.SportbaseQuery.FieldByName('TimeMin').AsInteger := -1;
            dmMain.SportbaseQuery.FieldByName('TimeSec').AsInteger := -1;
          end;
          //Set game start time
          dmMain.SportbaseQuery.FieldByName('Start').AsDateTime :=
            Modal.StartTime.Time - Trunc(Modal.StartTime.Time);
        end;
        //Post the edits to the games table
        dmMain.SportbaseQuery.Post;
      end;
      Modal.Free
    end;
  end;
end;

//Function to check to make sure that the current playlist mode matches all of the templates in
//the playlist
function TMainForm.CheckForInvalidTemplates(CurrentDisplayMode: SmallInt): Boolean;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  OutVal: Boolean;
begin
  //Init
  OutVal := FALSE;
  if (Ticker_Collection.Count > 0) then
  begin
    //Walk the collection
    for i := 0 to Ticker_Collection.Count-1 do
    begin
      TickerRecPtr := Ticker_Collection.At(i);
      //Check display mode against template IDs; all 1-line (1 <= X < 1000) OR (X > 10000); all 2-line (1000 <= X < 10000)
      Case CurrentDisplayMode of
        //If invalid template ID, set flag
        //1,2: if (TickerRecPtr^.Template_ID > 1000) OR (TickerRecPtr^.Template_ID < 10000) then OutVal := TRUE;
        //3,4: if (TickerRecPtr^.Template_ID < 1000) OR (TickerRecPtr^.Template_ID > 10000)then OutVal := TRUE;
        1,2: if ((TickerRecPtr^.Template_ID >= 1000) AND (TickerRecPtr^.Template_ID < 10000)) then OutVal := TRUE;
        3,4: if (TickerRecPtr^.Template_ID < 1000) OR (TickerRecPtr^.Template_ID >= 10000)then OutVal := TRUE;
      end;
    end;
  end;
  CheckForInvalidTemplates := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// REGION SELECT FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Handler for region mode select change
procedure TMainForm.RegionSelectModeClick(Sender: TObject);
begin
  Case RegionSelectMode.ItemIndex of
    //Save as global playlist
    0: begin
         RegionSelectMode.Color := clBtnFace;
         RegionSelect.Color := clBtnFace;
         RegionIDNum.Text := '0';
         RegionIDNum.Color := clBtnFace;
       end;
    //Save as specific region playlist
    1: begin
         RegionSelectMode.Color := clYellow;
         RegionSelect.Color := clYellow;
         RegionIDNum.Text := IntToStr(RegionSelect.ItemIndex+1);
         RegionIDNum.Color := clYellow;
       end;
  end;
end;
//Handler for region select change
procedure TMainForm.RegionSelectChange(Sender: TObject);
begin
  Case RegionSelectMode.ItemIndex of
    //Save as global playlist
    0: begin
         RegionSelectMode.Color := clBtnFace;
         RegionSelect.Color := clBtnFace;
         RegionIDNum.Text := '0';
         RegionIDNum.Color := clBtnFace;
       end;
    //Save as specific region playlist
    1: begin
         RegionSelectMode.Color := clYellow;
         RegionSelect.Color := clYellow;
         RegionIDNum.Text := IntToStr(RegionSelect.ItemIndex+1);
         RegionIDNum.Color := clYellow;
       end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TEMPLATE SELECT FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Handler for mouse right-click - launches dialog to select templates; added for
//Stats Inc. Phase 2 modifications
procedure TMainForm.GamesDBGridMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Modal: TTemplateSelectDlg;
  i: SmallInt;
  CategoryRecPtr: ^CategoryRec;
  SelectedTemplatesRecPtr: ^DefaultTemplatesRec;
  QueryString: String;
  OddsOnly: SmallInt;
  QueryStr: String;
  Control: Word;
begin
  //if (EnableDefaultTemplates) then
  begin
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //First, check to see if it's a league tab
    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
    begin
      //Only act on right-click
      if (Button = mbRight) then
      begin
        Modal := TTemplateSelectDlg.Create(Application);
        try
          CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
          //Now, populate the templates grid by querying the database
          dmMain.Query4.Active := FALSE;
          dmMain.Query4.SQL.Clear;
          dmMain.Query4.Filtered := FALSE;
          QueryStr := 'SELECT * FROM Default_Templates WHERE ' +
            'Category_ID = ' + IntToStr(CategoryRecPtr^.Category_ID);
          //Add to query to filter templates based on 1 or 2 line display modes
          if (TickerDisplayMode = 1) OR (TickerDisplayMode = 2) then
            //1-line ticker
            QueryStr := QueryStr + ' AND ((Template_ID < 1000) OR (Template_ID >= 10000))'
          else
            //2-line ticker
            QueryStr := QueryStr + ' AND ((Template_ID >= 1000) AND (Template_ID < 10000))';
          dmMain.Query4.SQL.Add (QueryStr);
          dmMain.Query4.Active := TRUE;
          //Populate checklistbox
          if (dmMain.Query4.RecordCount > 0) then
          begin
            //Clear the temporary templates collection
            Selected_Templates_Collection.Clear;
            Selected_Templates_Collection.Pack;
            for i := 0 to dmMain.Query4.RecordCount-1 do
            begin
              //Add template to listbox
              Modal.TemplateCheckListbox.Items.Add(dmMain.Query4.FieldByName('Template_Description').AsString);
              Modal.TemplateCheckListbox.Checked[i] := dmMain.Query4.FieldByName('Enabled').AsBoolean;
              //Add the template to the temporary collection
              GetMem (SelectedTemplatesRecPtr, SizeOf(DefaultTemplatesRec));
              With SelectedTemplatesRecPtr^ do
              begin
                //Set values for collection record
                Category_ID := dmMain.Query4.FieldByName('Category_ID').AsInteger;
                Category_Name := dmMain.Query4.FieldByName('Category_Name').AsString;
                Template_Index := dmMain.Query4.FieldByName('Template_Index').AsInteger;
                Template_ID := dmMain.Query4.FieldByName('Template_ID').AsInteger;
                Template_Description := dmMain.Query4.FieldByName('Template_Description').AsString;
                Enabled := dmMain.Query4.FieldByName('Enabled').AsBoolean;
                Selected := TRUE;
                //Insert object to collection
                Selected_Templates_Collection.Insert(SelectedTemplatesRecPtr);
                Selected_Templates_Collection.Pack;
              end;
              //Next record
              dmMain.Query4.Next;
            end;
            //Only show if there are templates to list
            Control := Modal.ShowModal;
          end;
        finally
          //Handler for selection of "OK" - Append all enabled templates
          if (Control = mrOK) then
          begin
            //Build collection for all checked templates
            if (Modal.TemplateCheckListBox.Items.Count > 0) then
            begin
              for i := 0 to Modal.TemplateCheckListBox.Items.Count-1 do
              begin
                SelectedTemplatesRecPtr := Selected_Templates_Collection.At(i);
                SelectedTemplatesRecPtr^.Selected := Modal.TemplateCheckListbox.Checked[i];
              end;
              //Call function to add all playlist entries with all default templates
              AddPlaylistEntry(0, ALLDEFAULTTEMPLATES);
            end;
          end
          //Handler for selection of "YES" - Insert all selected templates
          else if (Control = mrYes) then
          begin
            //Build collection for all checked templates
            if (Modal.TemplateCheckListBox.Items.Count > 0) then
            begin
              for i := 0 to Modal.TemplateCheckListBox.Items.Count-1 do
              begin
                SelectedTemplatesRecPtr := Selected_Templates_Collection.At(i);
                SelectedTemplatesRecPtr^.Selected := Modal.TemplateCheckListbox.Checked[i];
              end;
              //Call function to add all playlist entries with all default templates
              InsertPlaylistEntry(0, ALLDEFAULTTEMPLATES);
            end;
          end;
          Modal.Free
        end;
      end;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// DEFAULT TEMPLATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//General procedure to clear and refresh the defasult templates grid
procedure TMainForm.RefreshDefaultTemplatesGrid;
var
  i, j: SmallInt;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
  CollectionCount: SmallInt;
  CurrentRow, CurrentTopRow: SmallInt;
begin
  //Get current row
  CurrentRow := DefaultTemplatesGrid.CurrentDataRow;
  CurrentTopRow := DefaultTemplatesGrid.TopRow;

  //Clear grid values
  if (DefaultTemplatesGrid.Rows > 0) then
    DefaultTemplatesGrid.DeleteRows (1, DefaultTemplatesGrid.Rows);

  //Init values
  CollectionCount := Default_Templates_Collection.Count;

  if (CollectionCount > 0) then
  begin
    DefaultTemplatesGrid.StoreData := TRUE;
    DefaultTemplatesGrid.Cols := 6;
    j := 0;
    //Populate the grid
    for i := 0 to CollectionCount-1 do
    begin
      DefaultTemplatesRecPtr := Default_Templates_Collection.At(i);
      //Add if matching category
      if (DefaultTemplatesRecPtr^.Category_ID = LeagueTab.TabIndex+1) then
      begin
        //Add row
        inc(j);
        DefaultTemplatesGrid.Rows := j;
        DefaultTemplatesGrid.Cell[1,j] := IntToStr(j); //Index
        DefaultTemplatesGrid.Cell[2,j] := DefaultTemplatesRecPtr^.Category_Name; //League
        DefaultTemplatesGrid.Cell[3,j] := DefaultTemplatesRecPtr^.Template_ID;
        if (DefaultTemplatesRecPtr^.Template_ID < 1000) OR (DefaultTemplatesRecPtr^.Template_ID > 10000) then
          DefaultTemplatesGrid.Cell[4,j] := '1-Line'
        else
          DefaultTemplatesGrid.Cell[4,j] := '2-Line';
        DefaultTemplatesGrid.Cell[5,j] := DefaultTemplatesRecPtr^.Template_Description;
        DefaultTemplatesGrid.Cell[6,j] := DefaultTemplatesRecPtr^.Enabled;
        DefaultTemplatesGrid.RowColor[j] := clWindow;
      end;
    end;
  end;
  //Refresh grid
  if (CurrentTopRow > 0) AND (j > 0) then
  begin
    if (CurrentTopRow > j) then CurrentTopRow := j;
    DefaultTemplatesGrid.TopRow := CurrentTopRow;
  end;
  if (CurrentRow > 0) AND (j > 0) then
  begin
    if (CurrentRow > j) then CurrentRow := j;
    DefaultTemplatesGrid.CurrentDataRow := CurrentRow;
    DefaultTemplatesGrid.RowSelected[CurrentRow] := TRUE;
  end;
end;

//General procedure for appending an entry to the default template list
procedure TMainForm.AddDefaultTemplateEntry(CurrentTemplateID: SmallInt);
var
  i,j,k,m: SmallInt;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
  SaveRow: SmallInt;
  CurrentTemplateDefs: TemplateDefsRec;
  CollectionCount: SmallInt;
  CategoryRecPtr: ^CategoryRec;
begin
  //Init
  SaveRow := DefaultTemplatesGrid.CurrentDataRow;

  //Check to see if category tab is a sport; if so, show and populate games grid
  CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);

  //Ticker
  CollectionCount := Default_Templates_Collection.Count;

  if (CollectionCount < 500) then
  begin
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //First, check to make sure that there are games listed if the template requires one
    CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
    GetMem (DefaultTemplatesRecPtr, SizeOf(DefaultTemplatesRec));
    With DefaultTemplatesRecPtr^ do
    begin
      //Set values for collection record
      Category_ID := CategoryRecPtr^.Category_ID;
      Category_Name := CategoryRecPtr^.Category_Description;
      Template_Index := Default_Templates_Collection.Count+1;
      Template_ID := CurrentTemplateID;
      Template_Description := CurrentTemplateDefs.Template_Description;
      Enabled := TRUE;

      //Insert object to collection
      Default_Templates_Collection.Insert(DefaultTemplatesRecPtr);
      Default_Templates_Collection.Pack;
    end;

    //Refresh the Default Templates grid
    RefreshDefaultTemplatesGrid;
    //Restore current grid row
    if (SaveRow < 1) then SaveRow := 1;
    DefaultTemplatesGrid.CurrentDataRow := SaveRow;
  end;
end;

//General procedure for inserting an entry into the default template list
procedure TMainForm.InsertDefaultTemplateEntry(CurrentTemplateID: SmallInt);
var
  i,j,k,m: SmallInt;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
  SaveRow: SmallInt;
  CurrentTemplateDefs: TemplateDefsRec;
  CollectionCount: SmallInt;
  CategoryRecPtr: ^CategoryRec;
begin
  //Init
  SaveRow := DefaultTemplatesGrid.CurrentDataRow;

  //Check to see if category tab is a sport; if so, show and populate games grid
  CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);

  //Ticker
  CollectionCount := Default_Templates_Collection.Count;

  if (CollectionCount < 500) then
  begin
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //First, check to make sure that there are games listed if the template requires one
    CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
    GetMem (DefaultTemplatesRecPtr, SizeOf(DefaultTemplatesRec));
    With DefaultTemplatesRecPtr^ do
    begin
      //Set values for collection record
      Category_ID := CategoryRecPtr^.Category_ID;
      Category_Name := CategoryRecPtr^.Category_Description;
      Template_Index := Default_Templates_Collection.Count+1;
      Template_ID := CurrentTemplateID;
      Template_Description := CurrentTemplateDefs.Template_Description;
      Enabled := TRUE;

      //Insert object to collection
      if (DefaultTemplatesGrid.CurrentDataRow > 0) then
      begin
        Default_Templates_Collection.AtInsert(DefaultTemplatesGrid.CurrentDataRow-1, DefaultTemplatesRecPtr);
      end
      else
        Default_Templates_Collection.Insert(DefaultTemplatesRecPtr);
      Default_Templates_Collection.Pack;
    end;

    //Refresh the Default Templates grid
    RefreshDefaultTemplatesGrid;
    //Restore current grid row
    if (SaveRow < 1) then SaveRow := 1;
    DefaultTemplatesGrid.CurrentDataRow := SaveRow;
  end;
end;

//Handler to confirm league change with operator; will clear collection
procedure TMainForm.LeagueTabChanging(Sender: TObject;
  var AllowChange: Boolean);
var
  Control: Word;
begin
  //Confirm before allowing change
  if (Default_Templates_Collection.Count > 0) then
  begin
    Control := MessageDlg('Are you sure you want to change leagues? Any changes made to the default ' +
      'templates list for this league will be lost if they have not been saved to the database.',
      mtConfirmation, [mbYes, mbNo], 0);
    if (Control = mrYes) then
    begin
      //Clear collection and allow change
      Default_Templates_Collection.Clear;
      Default_Templates_Collection.Pack;
      RefreshDefaultTemplatesGrid;
    end
    //Disallow league change
    else AllowChange := FALSE;
  end;
end;

//Handler for select all pop-up entry for Default Templates Grid
procedure TMainForm.MenuItem6Click(Sender: TObject);
var
  i: SmallInt;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
begin
  if (Default_Templates_Collection.Count > 0) then
  begin
    for i := 0 to Default_Templates_Collection.Count-1 do
    begin
      DefaultTemplatesRecPtr := Default_Templates_Collection.At(i);
      DefaultTemplatesGrid.RowSelected[i+1] := TRUE;
      DefaultTemplatesRecPtr^.Selected := TRUE;
    end;
  end;
end;

//Handler for de-select all pop-up entry for Default Templates Grid
procedure TMainForm.MenuItem7Click(Sender: TObject);
var
  i: SmallInt;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
begin
  if (Default_Templates_Collection.Count > 0) then
  begin
    for i := 0 to Default_Templates_Collection.Count-1 do
    begin
      DefaultTemplatesRecPtr := Default_Templates_Collection.At(i);
      DefaultTemplatesGrid.RowSelected[i+1] := FALSE;
      DefaultTemplatesRecPtr^.Selected := FALSE;
    end;
  end;
end;

//Handler for enable selected templates by default
procedure TMainForm.Enable2Click(Sender: TObject);
var
  i: SmallInt;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
begin
  if (Default_Templates_Collection.Count > 0) then
  begin
    for i := 0 to Default_Templates_Collection.Count-1 do
    begin
      DefaultTemplatesRecPtr := Default_Templates_Collection.At(i);
      if (DefaultTemplatesGrid.RowSelected[i+1] = TRUE) then
        DefaultTemplatesRecPtr^.Enabled := TRUE;
    end;
  end;
  //Refresh the grid
  RefreshDefaultTemplatesGrid;
end;

//Handler for disable selected templates by default
procedure TMainForm.DisablebyDefault1Click(Sender: TObject);
var
  i: SmallInt;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
begin
  if (Default_Templates_Collection.Count > 0) then
  begin
    for i := 0 to Default_Templates_Collection.Count-1 do
    begin
      DefaultTemplatesRecPtr := Default_Templates_Collection.At(i);
      if (DefaultTemplatesGrid.RowSelected[i+1] = TRUE) then
        DefaultTemplatesRecPtr^.Enabled := FALSE;
    end;
  end;
  //Refresh the grid
  RefreshDefaultTemplatesGrid;
end;

//Handler for load default templates list
procedure TMainForm.LoadTemplateListBtnClick(Sender: TObject);
var
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
  Control: Word;
  OKToGo: Boolean;
  CollectionCount: SmallInt;
begin
  //Init
  OKToGo := TRUE;
  CollectionCount := Default_Templates_Collection.Count;
  if (CollectionCount > 0) then
  begin
    Control := MessageDlg('There are currently entries in the default templates list. ' +
      'Loading the list again will overwrite any edits. Do you want to proceed?',
      mtWarning, [mbYes, mbNo], 0);
    if (Control <> mrYes) then OKToGo := FALSE;
  end;
  //If operator confirmed, save the playlist out
  if (OkToGo) then
  try
    //Clear the collection
    Default_Templates_Collection.Clear;
    Default_Templates_Collection.Pack;

    //Query for the elements table for those that match the playlist ID
    dmMain.Query1.Close;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Default_Templates ' +
                             'WHERE Category_ID = ' + IntToStr(LeagueTab.TabIndex+1));
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then

    //Iterate through all records in the table
    repeat
      GetMem (DefaultTemplatesRecPtr, SizeOf(DefaultTemplatesRec));
      With DefaultTemplatesRecPtr^ do
      begin
        //Set values for collection record
        Category_ID := dmMain.Query1.FieldByName('Category_ID').AsInteger;
        Category_Name := dmMain.Query1.FieldByName('Category_Name').AsString;
        Template_Index := dmMain.Query1.FieldByName('Template_Index').AsInteger;
        Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
        Template_Description := dmMain.Query1.FieldByName('Template_Description').AsString;
        Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
        Selected := TRUE;
        //Insert the object
        Default_Templates_Collection.Insert(DefaultTemplatesRecPtr);
        Default_Templates_Collection.Pack;
        //Go to next record in query
        dmMain.Query1.Next;
      end;
    until (dmMain.Query1.EOF = TRUE);
    //Refresh the grid
    RefreshDefaultTemplatesGrid;
  except
    MessageDlg ('Error occurred while trying to load the default templates from the database.',
                 mtError, [mbOk], 0);
  end;
end;

//Handler for save default templates list
procedure TMainForm.SaveTemplateListBtnClick(Sender: TObject);
begin
  SaveDefaultTemplatesList(LeagueTab.TabIndex+1, FALSE);
end;

//Handler for save & close default templates list
procedure TMainForm.SaveCloseTemplateListBtnClick(Sender: TObject);
begin
  SaveDefaultTemplatesList(LeagueTab.TabIndex+1, TRUE);
end;

//General procedure to save default templates collection out to database
procedure TMainForm.SaveDefaultTemplatesList(Category_ID: SmallInt; ClearCollection: Boolean);
var
  i: SmallInt;
  OKToGo: Boolean;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
  Control: Word;
  Save_Cursor: TCursor;
begin
  //Init
  OKToGo := TRUE;

  //Check for entries in collection
  if (Default_Templates_Collection.Count =0) then
  begin
    Control := MessageDlg ('No default templates have been specified', mtError, [mbOk], 0);
    Exit;
  end;

  //Confirm
  Control := MessageDlg ('Are you sure you want to overwrite the default templates currently stored ' +
                         'in the database for this league?', mtConfirmation, [mbYes, mbNo], 0);
  if (Control <> mrYes) then OKToGo := FALSE;

  if (OKToGo) then
  begin
    //Change cursor
    Save_Cursor := Screen.Cursor;
    //Show hourglass cursor
    Screen.Cursor := crHourGlass;

    //Insert all elements into Default Templates table
    try
      //Open the table
      dmMain.tblDefault_Templates.Active := TRUE;
      dmMain.tblDefault_Templates.First;
      //Delete the existing entries from the database
      While(dmMain.tblDefault_Templates.EOF = FALSE) do
      begin
        if (dmMain.tblDefault_Templates.FieldByName('Category_ID').AsString = IntToStr(Category_ID)) then
          dmMain.tblDefault_Templates.Delete
        else
          dmMain.tblDefault_Templates.Next;
      end;
      //Iterate through the collection
      for i := 0 to Default_Templates_Collection.Count-1 do
      begin
        //Point to collection object
        DefaultTemplatesRecPtr := Default_Templates_Collection.At(i);
        //Append graphic record to graphics pages table in database}
        dmMain.tblDefault_Templates.AppendRecord ([
          DefaultTemplatesRecPtr^.Category_ID,
          DefaultTemplatesRecPtr^.Category_Name,
          DefaultTemplatesRecPtr^.Template_Index,
          DefaultTemplatesRecPtr^.Template_ID,
          DefaultTemplatesRecPtr^.Template_Description,
          DefaultTemplatesRecPtr^.Enabled
        ]);
      end;
      dmMain.tblDefault_Templates.Active := FALSE;
    except
      MessageDlg ('Error occurred while trying to insert default templates element into database.',
                   mtError, [mbOk], 0);
    end;
    if (ClearCollection) then
    begin
      Default_Templates_Collection.Clear;
      Default_Templates_Collection.Pack;
    end;
    //Refresh the grid
    RefreshDefaultTemplatesGrid;
    //Restore cursor
    Screen.Cursor := Save_Cursor;
  end;
end;

//Handler for delete selected items from default templates list
procedure TMainForm.MenuItem18Click(Sender: TObject);
var
  i: SmallInt;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
  CollectionCount: SmallInt;
  Control: Word;
begin
  Control := MessageDlg('Are you sure you want to delete the selected records?', mtConfirmation, [mbYes, mbNo], 0);
  if (Control = mrYes) then
  begin
    CollectionCount := Default_Templates_Collection.Count;
    If ((CollectionCount > 0) AND (DefaultTemplatesGrid.Rows >= 0)) then
    begin
      for i := 0 to Default_Templates_Collection.Count-1 do
      begin
        DefaultTemplatesRecPtr := Default_Templates_Collection.At(i);
        if (DefaultTemplatesGrid.RowSelected[i+1] = TRUE) then DefaultTemplatesRecPtr^.Selected := TRUE
        else DefaultTemplatesRecPtr^.Selected := FALSE;
      end;
      i := 0;
      Repeat
        DefaultTemplatesRecPtr := Default_Templates_Collection.At(i);
        if (DefaultTemplatesRecPtr^.Selected = TRUE) then
        begin
          Default_Templates_Collection.AtDelete(i);
          Default_Templates_Collection.Pack;
        end
        else Inc(i);
      Until (i = Default_Templates_Collection.Count);
      //Refresh the grid
      RefreshDefaultTemplatesGrid;
    end;
  end;
end;

//Handler to use NCAAB Ranks
procedure TMainForm.UseRankforNCAAB1Click(Sender: TObject);
begin
  UseRankforNCAAB1.Checked := TRUE;
  UseSeedforNCAAB1.Checked := FALSE;
  UseSeedingForNCAABGames := FALSE;
  StorePrefs;
end;

//Handler to use NCAAB Seeds
procedure TMainForm.UseSeedforNCAAB1Click(Sender: TObject);
begin
  UseRankforNCAAB1.Checked := FALSE;
  UseSeedforNCAAB1.Checked := TRUE;
  UseSeedingForNCAABGames := TRUE;
  StorePrefs;
end;

////////////////////////////////////////////////////////////////////////////////
// FOR DATABASE SELECTION
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.SetCurrentTickerDatabase(DatabaseIndex: SmallInt);
begin
  try
    //Activate database & tables
    With dmMain do
    begin
      dbTicker.Connected := FALSE;
      dbTicker.ConnectionString := TickerDatabaseConnectionString[DatabaseIndex];
      dbTicker.Connected := TRUE;
      tblTicker_Groups.Active := TRUE;
      tblTicker_Elements.Active := TRUE;
      tblScheduled_Ticker_Groups.Active := TRUE;
      tblBreakingNews_Groups.Active := TRUE;
      tblBreakingNews_Elements.Active := TRUE;
      tblSponsor_Logos.Active := TRUE;
    end;
  except
    MessageDlg ('Error occurred while trying to switch Ticker databases.', mtError, [mbOk], 0);
  end;
end;

//Set the status bar text
procedure TMainForm.SetStatusBar(StatusText: String; HighlightText: Boolean);
begin
  StatusBar.SimpleText := ' ' + StatusText;
  StatusBar.Font.Style := [fsBold];
  if (HighlightText) then StatusBar.Color := clYellow
  else StatusBar.Color := clBtnFace;
end;

//Database #1
procedure TMainForm.TickerDB1Click(Sender: TObject);
begin
  //Set database connection
  SetCurrentTickerDatabase(1);
  //Set checkmarks & status
  TickerDB1.Checked := TRUE;
  TickerDB2.Checked := FALSE;
  TickerDB3.Checked := FALSE;
  TickerDB4.Checked := FALSE;
  TickerDB5.Checked := FALSE;
  TickerDB6.Checked := FALSE;
  TickerDB7.Checked := FALSE;
  TickerDB8.Checked := FALSE;
  TickerDB9.Checked := FALSE;
  TickerDB10.Checked := FALSE;
  //Set status bar
  SetStatusBar('SELECTED DATABASE: ' + TickerDatabaseDescription[1], FALSE)
end;
//Database #2
procedure TMainForm.TickerDB2Click(Sender: TObject);
begin
  //Set database connection
  SetCurrentTickerDatabase(2);
  //Set checkmarks & status
  TickerDB1.Checked := FALSE;
  TickerDB2.Checked := TRUE;
  TickerDB3.Checked := FALSE;
  TickerDB4.Checked := FALSE;
  TickerDB5.Checked := FALSE;
  TickerDB6.Checked := FALSE;
  TickerDB7.Checked := FALSE;
  TickerDB8.Checked := FALSE;
  TickerDB9.Checked := FALSE;
  TickerDB10.Checked := FALSE;
  //Set status bar
  SetStatusBar('SELECTED DATABASE: ' + TickerDatabaseDescription[2], TRUE)
end;
//Database #3
procedure TMainForm.TickerDB3Click(Sender: TObject);
begin
  //Set database connection
  SetCurrentTickerDatabase(3);
  //Set checkmarks & status
  TickerDB1.Checked := FALSE;
  TickerDB2.Checked := FALSE;
  TickerDB3.Checked := TRUE;
  TickerDB4.Checked := FALSE;
  TickerDB5.Checked := FALSE;
  TickerDB6.Checked := FALSE;
  TickerDB7.Checked := FALSE;
  TickerDB8.Checked := FALSE;
  TickerDB9.Checked := FALSE;
  TickerDB10.Checked := FALSE;
  //Set status bar
  SetStatusBar('SELECTED DATABASE: ' + TickerDatabaseDescription[3], TRUE)
end;
//Database #4
procedure TMainForm.TickerDB4Click(Sender: TObject);
begin
  //Set database connection
  SetCurrentTickerDatabase(4);
  //Set checkmarks & status
  TickerDB1.Checked := FALSE;
  TickerDB2.Checked := FALSE;
  TickerDB3.Checked := FALSE;
  TickerDB4.Checked := TRUE;
  TickerDB5.Checked := FALSE;
  TickerDB6.Checked := FALSE;
  TickerDB7.Checked := FALSE;
  TickerDB8.Checked := FALSE;
  TickerDB9.Checked := FALSE;
  TickerDB10.Checked := FALSE;
  //Set status bar
  SetStatusBar('SELECTED DATABASE: ' + TickerDatabaseDescription[4], TRUE)
end;
//Database #5
procedure TMainForm.TickerDB5Click(Sender: TObject);
begin
  //Set database connection
  SetCurrentTickerDatabase(5);
  //Set checkmarks & status
  TickerDB1.Checked := FALSE;
  TickerDB2.Checked := FALSE;
  TickerDB3.Checked := FALSE;
  TickerDB4.Checked := FALSE;
  TickerDB5.Checked := TRUE;
  TickerDB6.Checked := FALSE;
  TickerDB7.Checked := FALSE;
  TickerDB8.Checked := FALSE;
  TickerDB9.Checked := FALSE;
  TickerDB10.Checked := FALSE;
  //Set status bar
  SetStatusBar('SELECTED DATABASE: ' + TickerDatabaseDescription[5], TRUE)
end;
//Database #6
procedure TMainForm.TickerDB6Click(Sender: TObject);
begin
  //Set database connection
  SetCurrentTickerDatabase(6);
  //Set checkmarks & status
  TickerDB1.Checked := FALSE;
  TickerDB2.Checked := FALSE;
  TickerDB3.Checked := FALSE;
  TickerDB4.Checked := FALSE;
  TickerDB5.Checked := FALSE;
  TickerDB6.Checked := TRUE;
  TickerDB7.Checked := FALSE;
  TickerDB8.Checked := FALSE;
  TickerDB9.Checked := FALSE;
  TickerDB10.Checked := FALSE;
  //Set status bar
  SetStatusBar('SELECTED DATABASE: ' + TickerDatabaseDescription[6], TRUE)
end;
//Database #7
procedure TMainForm.TickerDB7Click(Sender: TObject);
begin
  //Set database connection
  SetCurrentTickerDatabase(7);
  //Set checkmarks & status
  TickerDB1.Checked := FALSE;
  TickerDB2.Checked := FALSE;
  TickerDB3.Checked := FALSE;
  TickerDB4.Checked := FALSE;
  TickerDB5.Checked := FALSE;
  TickerDB6.Checked := FALSE;
  TickerDB7.Checked := TRUE;
  TickerDB8.Checked := FALSE;
  TickerDB9.Checked := FALSE;
  TickerDB10.Checked := FALSE;
  //Set status bar
  SetStatusBar('SELECTED DATABASE: ' + TickerDatabaseDescription[7], TRUE)
end;
//Database #8
procedure TMainForm.TickerDB8Click(Sender: TObject);
begin
  //Set database connection
  SetCurrentTickerDatabase(8);
  //Set checkmarks & status
  TickerDB1.Checked := FALSE;
  TickerDB2.Checked := FALSE;
  TickerDB3.Checked := FALSE;
  TickerDB4.Checked := FALSE;
  TickerDB5.Checked := FALSE;
  TickerDB6.Checked := FALSE;
  TickerDB7.Checked := FALSE;
  TickerDB8.Checked := TRUE;
  TickerDB9.Checked := FALSE;
  TickerDB10.Checked := FALSE;
  //Set status bar
  SetStatusBar('SELECTED DATABASE: ' + TickerDatabaseDescription[8], TRUE)
end;
//Database #9
procedure TMainForm.TickerDB9Click(Sender: TObject);
begin
  //Set database connection
  SetCurrentTickerDatabase(9);
  //Set checkmarks & status
  TickerDB1.Checked := FALSE;
  TickerDB2.Checked := FALSE;
  TickerDB3.Checked := FALSE;
  TickerDB4.Checked := FALSE;
  TickerDB5.Checked := FALSE;
  TickerDB6.Checked := FALSE;
  TickerDB7.Checked := FALSE;
  TickerDB8.Checked := FALSE;
  TickerDB9.Checked := TRUE;
  TickerDB10.Checked := FALSE;
  //Set status bar
  SetStatusBar('SELECTED DATABASE: ' + TickerDatabaseDescription[9], TRUE)
end;
//Database #10
procedure TMainForm.TickerDB10Click(Sender: TObject);
begin
  //Set database connection
  SetCurrentTickerDatabase(10);
  //Set checkmarks & status
  TickerDB1.Checked := FALSE;
  TickerDB2.Checked := FALSE;
  TickerDB3.Checked := FALSE;
  TickerDB4.Checked := FALSE;
  TickerDB5.Checked := FALSE;
  TickerDB6.Checked := FALSE;
  TickerDB7.Checked := FALSE;
  TickerDB8.Checked := FALSE;
  TickerDB9.Checked := FALSE;
  TickerDB10.Checked := TRUE;
  //Set status bar
  SetStatusBar('SELECTED DATABASE: ' + TickerDatabaseDescription[10], TRUE)
end;

end.
